<p align="center"><a href="#" target="_blank">ETHER LOGO</a></p>

<p align="center">
stats
</p>

## About Ether

> **Note:** This repository contains the core code of the ether framework. If you want to build an application using Ether, visit the main [Ether repository](https://github.com/tbtmuse/ether-app).

Ether is a web application framework with expressive, elegant syntax. We believe development must be an enjoyable, creative experience to be truly fulfilling. Ether attempts to take the pain out of development by easing common tasks used in the majority of web projects, such as:

- [Simple, fast routing engine](https://ether.africa/docs/routing).
- [Powerful dependency injection container](https://ether.africa/docs/container).
- Multiple back-ends for [sessions](https://ether.africa/docs/session) and [caching](https://ether.africa/docs/cache) storage.
- Database agnostic [schema migrations](https://ether.africa/docs/migrations).
- [Robust background job processing](https://ether.africa/docs/queues).
- [Real-time event broadcasting](https://ether.africa/docs/broadcasting).

## Learning Ether

Ether has the most extensive and thorough documentation and video tutorial library of any modern web application framework. The [Ether documentation](https://Ether.com/docs) is in-depth and complete, making it a breeze to get started learning the framework.

## Contributing

Thank you for considering contributing to the Ether framework! The contribution guide can be found in the [Ether documentation](https://ether.africa/docs/contributions).

## Code of Conduct

In order to ensure that the Ether community is welcoming to all, please review and abide by the [Code of Conduct](https://Ether.com/docs/contributions#code-of-conduct).

## Security Vulnerabilities

Please review [our security policy](https://github.com/tbtmuse/ether-framework/security/policy) on how to report security vulnerabilities.

## License

The Ether framework is open-sourced software licensed under the [MIT license](LICENSE.md).
