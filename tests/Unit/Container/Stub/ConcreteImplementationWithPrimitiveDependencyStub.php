<?php declare(strict_types=1);

namespace Ether\Tests\Unit\Container\Stub;

class ConcreteImplementationWithPrimitiveDependencyStub implements InterfaceStub {

    private $something;
    private $concreteStub;

    public function __construct(ConcreteStub $concreteStub, $something) {
        $this->something = $something;
        $this->concreteStub = $concreteStub;
    }

    public function getSomething(): string {
        return $this->something;
    }

    public function getConcreteStub(): string {
        return $this->concreteStub;
    }
}
