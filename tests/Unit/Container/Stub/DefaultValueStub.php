<?php declare(strict_types=1);

namespace Ether\Tests\Unit\Container\Stub;

class DefaultValueStub {

    private $default;
    private $concreteStub;

    public function __construct(ConcreteStub $concreteStub, $default = 'default') {
        $this->default = $default;
        $this->concreteStub = $concreteStub;
    }

    public function getConcreteStub(): ConcreteStub {
        return $this->concreteStub;
    }

    public function getDefaultValue(): string {
        return $this->default;
    }
}
