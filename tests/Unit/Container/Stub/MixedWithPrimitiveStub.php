<?php declare(strict_types=1);

namespace Ether\Tests\Unit\Container\Stub;

class MixedWithPrimitiveStub  {

    private $first;
    private $last;
    private $concreteStub;

    public function __construct($first, ConcreteStub $concreteStub, $last) {
        $this->last = $last;
        $this->first = $first;
        $this->concreteStub = $concreteStub;
    }

    public function getFirst() {
        return $this->first;
    }

    public function getConcreteStub(): ConcreteStub {
        return $this->concreteStub;
    }

    public function getLast() {
        return $this->last;
    }
}
