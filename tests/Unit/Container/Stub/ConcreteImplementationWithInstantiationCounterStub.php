<?php declare(strict_types=1);

namespace Ether\Tests\Unit\Container\Stub;

class ConcreteImplementationWithInstantiationCounterStub implements InterfaceStub {

    public static $instantiationCounter;

    public function __construct() {
        static::$instantiationCounter++;
    }
}
