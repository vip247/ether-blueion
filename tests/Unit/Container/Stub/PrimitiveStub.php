<?php declare(strict_types=1);

namespace Ether\Tests\Unit\Container\Stub;

class PrimitiveStub  {

    private $last;
    private $first;

    public function __construct($first, $last) {
        $this->last = $last;
        $this->first = $first;
    }

    public function getFirst() {
        return $this->first;
    }

    public function getLast() {
        return $this->last;
    }
}
