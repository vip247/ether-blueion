<?php declare(strict_types=1);

namespace Ether\Tests\Unit\Container\Stub;

class NestedDependentStub {

    private $dependentStub;

    public function __construct(DependentStub $dependentStub) {
        $this->dependentStub = $dependentStub;
    }

    public function getDependentStub(): DependentStub {
        return $this->dependentStub;
    }
}
