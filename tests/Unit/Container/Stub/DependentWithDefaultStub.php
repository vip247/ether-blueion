<?php declare(strict_types=1);

namespace Ether\Tests\Unit\Container\Stub;

class DependentWithDefaultStub {

    private $concreteImplementationStub;

    public function __construct(InterfaceStub $concreteImplementationStub = null) {
        $this->concreteImplementationStub = $concreteImplementationStub;
    }

    public function getConcreteImplementationStub(): ?InterfaceStub {
        return $this->concreteImplementationStub;
    }
}
