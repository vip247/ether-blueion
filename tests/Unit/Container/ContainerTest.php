<?php

/** @noinspection ALL */

declare(strict_types=1);

namespace Ether\Tests\Unit\Application\Container;

use LogicException;
use ReflectionException;
use Ether\Container\Container;
use PHPUnit\Framework\TestCase;
use Ether\Container\Exceptions\NotFoundException;
use Ether\Container\Exceptions\ContainerException;
use Ether\Tests\Unit\Container\Stub\ConcreteStub;
use Ether\Tests\Unit\Container\Stub\AbstractStub;
use Ether\Tests\Unit\Container\Stub\PrimitiveStub;
use Ether\Tests\Unit\Container\Stub\DependentStub;
use Ether\Tests\Unit\Container\Stub\InterfaceStub;
use Ether\Tests\Unit\Container\Stub\DependentStubTwo;
use Ether\Tests\Unit\Container\Stub\DefaultValueStub;
use Ether\Tests\Unit\Container\Stub\NestedDependentStub;
use Ether\Tests\Unit\Container\Stub\MixedWithPrimitiveStub;
use Ether\Tests\Unit\Container\Stub\DependentWithDefaultStub;
use Ether\Tests\Unit\Container\Stub\ConcreteImplementationStub;
use Ether\Tests\Unit\Container\Stub\ConcreteImplementationStubTwo;
use Ether\Tests\Unit\Container\Stub\ConcreteImplementationWithPrimitiveDependencyStub;
use Ether\Tests\Unit\Container\Stub\ConcreteImplementationWithInstantiationCounterStub;

final class ContainerTest extends TestCase {

    /**
     * @throws ContainerException
     * @throws NotFoundException
     * @throws ReflectionException
     */
    public function testContainerIsBoundAsSharedInstance(): void {

		$container = new Container;

		$this->assertSame($container, $container->resolve(Container::class));
	}

    /**
     * @throws ContainerException
     * @throws NotFoundException
     * @throws ReflectionException
     */
    public function testClosureResolution(): void {

        $container = new Container;

        $container->bind(AbstractStub::class, static function () {
            return 'Random Value';
        });

        $this->assertSame('Random Value', $container->make(AbstractStub::class));
    }

    /**
     * @throws ContainerException
     * @throws NotFoundException
     * @throws ReflectionException
     */
    public function testSharedClosureResolution(): void {

        $concreteStub = new ConcreteStub();

        $container = new Container;

        $container->share(AbstractClass::class, static function () use ($concreteStub) {
            return $concreteStub;
        });

        $this->assertSame($concreteStub, $container->make(AbstractClass::class));
    }

    /**
     * @throws ContainerException
     * @throws NotFoundException
     * @throws ReflectionException
     */
    public function testAutoConcreteResolution(): void {

        $container = new Container;

        $this->assertInstanceOf(ConcreteStub::class, $container->make(ConcreteStub::class));
    }

    /**
     * @throws ContainerException
     * @throws NotFoundException
     * @throws ReflectionException
     */
    public function testSharedConcreteResolution(): void {

        $container = new Container;

        $container->share(ConcreteStub::class);

        $class1 = $container->make(ConcreteStub::class);
        $class2 = $container->make(ConcreteStub::class);

        $this->assertSame($class1, $class2);
    }

    /**
     * @throws ContainerException
     * @throws NotFoundException
     * @throws ReflectionException
     */
    public function testAbstractToConcreteResolution(): void {

        $container = new Container;

        $container->bind(InterfaceStub::class, ConcreteImplementationStub::class);

        $dependentStub = $container->make(DependentStub::class);

        $this->assertInstanceOf(ConcreteImplementationStub::class, $dependentStub->getConcreteImplementationStub());
    }

    /**
     * @throws ContainerException
     * @throws NotFoundException
     * @throws ReflectionException
     */
    public function testNestedDependencyResolution(): void {

        $container = new Container;

        $container->bind(InterfaceStub::class, ConcreteImplementationStub::class);

        $nestedDependentStub = $container->make(NestedDependentStub::class);

        $this->assertInstanceOf(DependentStub::class, $nestedDependentStub->getDependentStub());
        $this->assertInstanceOf(ConcreteImplementationStub::class, $nestedDependentStub->getDependentStub()->getConcreteImplementationStub());
    }

    /**
     * @throws ContainerException
     * @throws NotFoundException
     * @throws ReflectionException
     */
    public function testContainerIsPassedToClosure(): void {

        $container = new Container;

        $container->bind(AbstractStub::class, static function ($passedContainer) {
            return $passedContainer;
        });

        $passedContainer = $container->make(AbstractStub::class);

        $this->assertSame($passedContainer, $container);
    }

    /**
     *
     */
    public function testArrayAccessWithClosure(): void {

        $container = new Container;

        $container['abstract'] = static function () {
            return 'some value';
        };

        $this->assertTrue(isset($container['abstract']));
        $this->assertEquals('some value', $container['abstract']);
    }

    /**
     *
     */
    public function testArrayAccessWithArbitraryValue(): void {

        $container = new Container;

        $container['key'] = 'ArbitraryValue';

        $this->assertTrue(isset($container['key']));
        $this->assertEquals('ArbitraryValue', $container['key']);
    }

    /**
     *
     */
    public function testArrayAccessRemovalWithUnset(): void {

        $container = new Container;

        $container['something'] = static function () {
            return 'foo';
        };

        unset($container['something']);

        $this->assertFalse(isset($container['something']));
    }

    /**
     * @throws ContainerException
     * @throws NotFoundException
     * @throws ReflectionException
     */
    public function testAliases(): void {

        $container = new Container;

        $container[AbstractStub::class] = static function () {
            return 'Some Value';
        };

        $container->alias(AbstractStub::class, 'randomAlias');
        $container->alias('randomAlias', 'AnotherRandomAlias');

        $this->assertEquals('Some Value', $container->make(AbstractStub::class));
        $this->assertEquals('Some Value', $container->make('randomAlias'));
        $this->assertEquals('Some Value', $container->make('AnotherRandomAlias'));
    }

    /**
     * @throws ContainerException
     * @throws NotFoundException
     * @throws ReflectionException
     */
    public function testAliasesWithArrayOfParameters(): void {

        $container = new Container;

        $container->bind(AbstractStub::class, static function ($passedContainer, $config) {
            return $config;
        });

        $container->alias(AbstractStub::class, 'randomAlias');

        $this->assertEquals(array(1, 2, 3), $container->make('randomAlias', array(1, 2, 3)));
    }

    /**
     *
     */
    public function testBindingsCanBeOverridden(): void {

        $container = new Container;

        $container[AbstractStub::class] = static function() {
            return 'Initial Value';
        };

        /** @noinspection SuspiciousAssignmentsInspection */
        $container[AbstractStub::class] = static function() {
            return 'Overridden Value';
        };

        $this->assertEquals('Overridden Value', $container[AbstractStub::class]);
    }

    /**
     * @throws ContainerException
     * @throws NotFoundException
     * @throws ReflectionException
     */
    public function testResolutionOfDefaultParameters(): void {

        $container = new Container;

        $defaultValueStub = $container->make(DefaultValueStub::class);

        $this->assertInstanceOf(ConcreteStub::class, $defaultValueStub->getConcreteStub());

        $this->assertEquals('default', $defaultValueStub->getDefaultValue());
    }

    /**
     *
     */
    public function testUnsetRemoveBoundInstances(): void {

        $container = new Container;

        $container->shareInstance(AbstractStub::class, new ConcreteStub());

        unset($container[AbstractStub::class]);

        $this->assertFalse($container->isBound(AbstractStub::class));
    }

    /**
     *
     */
    public function testBoundInstanceAndAliasCheckViaArrayAccess(): void {

        $container = new Container;

        $container->shareInstance(AbstractStub::class, new ConcreteStub());

        $container->alias(AbstractStub::class, 'alias');

        $this->assertTrue(isset($container[AbstractStub::class]));
        $this->assertTrue(isset($container['alias']));
    }

    /**
     * @throws ContainerException
     * @throws NotFoundException
     * @throws ReflectionException
     */
    public function testPrimitiveDependenciesCanBeSetWithArrayOfParameters(): void {

        $container = new Container;

        $primitiveStub = $container->make(PrimitiveStub::class, array(
                'first' => 'value for first param',
                'last' => 'value for last param'
            )
        );

        $this->assertEquals('value for first param', $primitiveStub->getFirst());
        $this->assertEquals('value for last param', $primitiveStub->getLast());
    }

    /**
     * @throws ContainerException
     * @throws NotFoundException
     * @throws ReflectionException
     */
    public function testResolvedResolvesAliasToBindingNameBeforeChecking(): void {

        $container = new Container;

        $container->bind(ConcreteStub::class, static function () {
            return new ConcreteStub();
        }, true);

        $container->alias(ConcreteStub::class, 'foo');

        $this->assertFalse($container->resolved(ConcreteStub::class));
        $this->assertFalse($container->resolved('foo'));

        $container->make(ConcreteStub::class);

        $this->assertTrue($container->resolved(ConcreteStub::class));
        $this->assertTrue($container->resolved('foo'));
    }

    /**
     *
     */
    public function testGetAlias(): void {

        $container = new Container;

        $container->alias(ConcreteStub::class, 'foo');

        $this->assertEquals($container->getAlias('foo'), ConcreteStub::class);
    }

    /**
     *
     */
    public function testThrowsExceptionWhenAbstractIsSameAsAlias(): void {

        $this->expectException(LogicException::class);
        $this->expectExceptionMessage(AbstractStub::class . ' is aliased to itself');

        $container = new Container;

        $container->alias(AbstractStub::class, AbstractStub::class);

        $container->getAlias(AbstractStub::class);
    }

    /**
     * @throws ContainerException
     * @throws NotFoundException
     * @throws ReflectionException
     */
    public function testAbstractAlias(): void {

        $container = new Container;

        $container->alias(InterfaceStub::class, 'ArbitraryStringIdentifier');

        $container->shareInstance('ArbitraryStringIdentifier', new ConcreteImplementationStub());

        $this->assertInstanceOf(ConcreteImplementationStub::class, $container->make('ArbitraryStringIdentifier'));
    }

    /**
     * @throws ContainerException
     * @throws NotFoundException
     * @throws ReflectionException
     */
    public function testResolvingWithArrayOfParameters(): void {

        $container = new Container;

        $defaultValueStubWithParams = $container->make(DefaultValueStub::class, array('default' => 'random'));
        $defaultValueStubWithoutParams = $container->make(DefaultValueStub::class);

        $container->bind(AbstractStub::class, static function ($container, $config) {
            return $config;
        });

        $this->assertEquals('random', $defaultValueStubWithParams->getDefaultValue());
        $this->assertEquals('default', $defaultValueStubWithoutParams->getDefaultValue());
        $this->assertEquals(array(1, 2, 3), $container->make(AbstractStub::class, array(1, 2, 3)));
    }

    /**
     * @throws ContainerException
     * @throws NotFoundException
     * @throws ReflectionException
     */
    public function testResolvingUsingAnInterface(): void {

        $container = new Container;

        $container->bind(InterfaceStub::class, ConcreteImplementationWithPrimitiveDependencyStub::class);

        $concreteImplementationWithPrimitiveDependencyStub = $container->make(InterfaceStub::class, array('something' => "something's value"));

        $this->assertEquals("something's value", $concreteImplementationWithPrimitiveDependencyStub->getSomething());
    }

    /**
     * @throws ContainerException
     * @throws NotFoundException
     * @throws ReflectionException
     */
    public function testNestedParameterOverride(): void {

        $container = new Container;

        $container->bind('abstractOne', static function ($container, $config) {
            return $container->make('abstractTwo', array('overRideKey' => 'overRideValue'));
        });

        $container->bind('abstractTwo', static function ($container, $config) {
            return $config;
        });

        $this->assertEquals(array('overRideKey' => 'overRideValue'), $container->make('abstractOne', array('OverriddenValue')));
    }

    /**
     * @throws ContainerException
     * @throws NotFoundException
     * @throws ReflectionException
     */
    public function testCanBuildWithoutParameterStackWithNoConstructors(): void {

        $container = new Container;

        $this->assertInstanceOf(ConcreteStub::class, $container->build(ConcreteStub::class));

    }

    /**
     * @throws ContainerException
     * @throws NotFoundException
     * @throws ReflectionException
     */
    public function testCanBuildWithoutParameterStackWithConstructors(): void {

        $container = new Container;

        $container->bind(InterfaceStub::class, ConcreteImplementationStub::class);

        $this->assertInstanceOf(DependentStub::class, $container->build(DependentStub::class));
    }

    /**
     *
     */
    public function testContainerKnowsEntry(): void {

        $container = new Container;

        $container->bind(InterfaceStub::class, ConcreteImplementationStub::class);

        $this->assertTrue($container->has(InterfaceStub::class));
    }

    /**
     * @throws ContainerException
     * @throws NotFoundException
     * @throws ReflectionException
     */
    public function testContainerCanBindAnyWord(): void {

        $container = new Container;

        $container->bind('AbsolutelyRandomWord', ConcreteStub::class);

        $this->assertInstanceOf(ConcreteStub::class, $container->get('AbsolutelyRandomWord'));
    }

    /**
     *
     */
    public function testContainerCanDynamicallySetService(): void {
        $container = new Container;

        $this->assertFalse(isset($container['name']));

        $container[AbstractStub::class] = static function () {
            return 'Random Value';
        };

        $this->assertTrue(isset($container[AbstractStub::class]));
        $this->assertSame('Random Value', $container[AbstractStub::class]);
    }

    /**
     * @throws ContainerException
     * @throws NotFoundException
     * @throws ReflectionException
     */
    public function testContainerCanInjectDifferentImplementationsDependingOnContext(): void {

        $container = new Container;

        $container->bind(InterfaceStub::class, ConcreteImplementationStub::class);

        $container
            ->when(DependentStub::class)
            ->needs(InterfaceStub::class)
            ->give(ConcreteImplementationStub::class);

        $container
            ->when(DependentStubTwo::class)
            ->needs(InterfaceStub::class)
            ->give(ConcreteImplementationStubTwo::class);

        $dependentStubOne = $container->make(DependentStub::class);
        $dependentStubTwo = $container->make(DependentStubTwo::class);

        $this->assertInstanceOf(ConcreteImplementationStub::class, $dependentStubOne->getConcreteImplementationStub());
        $this->assertInstanceOf(ConcreteImplementationStubTwo::class, $dependentStubTwo->getConcreteImplementationStub());
    }

    /**
     * @throws ContainerException
     * @throws NotFoundException
     * @throws ReflectionException
     */
    public function testContainerCanInjectDifferentImplementationsDependingOnContextUsingClosure(): void {

        $container = new Container;

        $container->bind(InterfaceStub::class, ConcreteImplementationStub::class);

        $container
            ->when(DependentStub::class)
            ->needs(InterfaceStub::class)
            ->give(ConcreteImplementationStub::class);

        $container
            ->when(DependentStubTwo::class)
            ->needs(InterfaceStub::class)
            ->give(static function ($container) {
                return $container->make(ConcreteImplementationStubTwo::class);
            }
        );

        $dependentStubOne = $container->make(DependentStub::class);
        $dependentStubTwo = $container->make(DependentStubTwo::class);

        $this->assertInstanceOf(ConcreteImplementationStub::class, $dependentStubOne->getConcreteImplementationStub());
        $this->assertInstanceOf(ConcreteImplementationStubTwo::class, $dependentStubTwo->getConcreteImplementationStub());
    }

    /**
     * @throws ContainerException
     * @throws NotFoundException
     * @throws ReflectionException
     */
    public function testContextualBindingWorksForExistingSharedInstancedBindings(): void {

        $container = new Container;

        $container->shareInstance(InterfaceStub::class, new ConcreteImplementationStub());

        $container
            ->when(DependentStub::class)
            ->needs(InterfaceStub::class)
            ->give(ConcreteImplementationStubTwo::class);

        $dependentStub = $container->make(DependentStub::class);

        $this->assertInstanceOf(ConcreteImplementationStubTwo::class, $dependentStub->getConcreteImplementationStub());

    }

    /**
     * @throws ContainerException
     * @throws NotFoundException
     * @throws ReflectionException
     */
    public function testContextualBindingWorksForNewlyInstancedBindings(): void {

        $container = new Container;

        $container
            ->when(DependentStub::class)
            ->needs(InterfaceStub::class)
            ->give(ConcreteImplementationStubTwo::class);

        $container->shareInstance(InterfaceStub::class, new ConcreteImplementationStub());

        $dependentStub = $container->make(DependentStub::class);

        $this->assertInstanceOf(ConcreteImplementationStubTwo::class, $dependentStub->getConcreteImplementationStub());
    }

    /**
     * @throws ContainerException
     * @throws NotFoundException
     * @throws ReflectionException
     */
    public function testContextualBindingWorksOnNewAliasedInstances(): void {

        $container = new Container;

        $container
            ->when(DependentStub::class)
            ->needs(InterfaceStub::class)
            ->give(ConcreteImplementationStubTwo::class);

        $container->shareInstance('arbitraryString', new ConcreteImplementationStub());

        $container->alias('arbitraryString', InterfaceStub::class);

        $dependentStub = $container->make(DependentStub::class);

        $this->assertInstanceOf(ConcreteImplementationStubTwo::class, $dependentStub->getConcreteImplementationStub());

    }

    /**
     * @throws ContainerException
     * @throws NotFoundException
     * @throws ReflectionException
     */
    public function testContextualBindingWorksOnNewAliasedBindings(): void {

        $container = new Container;

        $container
            ->when(DependentStub::class)
            ->needs(InterfaceStub::class)
            ->give(ConcreteImplementationStubTwo::class);

        $container->bind('arbitraryString', ContainerImplementationStub::class);

        $container->alias('arbitraryString', InterfaceStub::class);

        $dependentStub = $container->make(DependentStub::class);

        $this->assertInstanceOf(ConcreteImplementationStubTwo::class, $dependentStub->getConcreteImplementationStub());
    }

    /**
     * @throws ContainerException
     * @throws NotFoundException
     * @throws ReflectionException
     */
    public function testContextualBindingDoesNotOverrideNonContextualResolution(): void {

        $container = new Container;

        $container->shareInstance('arbitraryString', new ConcreteImplementationStub());
        $container->alias('arbitraryString', InterfaceStub::class);

        $container
            ->when(DependentStubTwo::class)
            ->needs(InterfaceStub::class)
            ->give(ConcreteImplementationStubTwo::class);

        $dependentStubTwo = $container->make(DependentStubTwo::class);

        $this->assertInstanceOf(ConcreteImplementationStubTwo::class, $dependentStubTwo->getConcreteImplementationStub());

        $dependentStubOne = $container->make(DependentStub::class);

        $this->assertInstanceOf(ConcreteImplementationStub::class, $dependentStubOne->getConcreteImplementationStub());
    }

    /**
     * @throws ContainerException
     * @throws NotFoundException
     * @throws ReflectionException
     */
    public function testContextuallyBoundInstancesAreNotUnnecessarilyRecreated(): void {

        ConcreteImplementationWithInstantiationCounterStub::$instantiationCounter = 0;

        $container = new Container;

        $container->shareInstance(InterfaceStub::class, new ConcreteImplementationStub());
        $container->shareInstance(ConcreteImplementationWithInstantiationCounterStub::class, new ConcreteImplementationWithInstantiationCounterStub());

        $this->assertEquals(1, ConcreteImplementationWithInstantiationCounterStub::$instantiationCounter);

        $container
            ->when(DependentStub::class)
            ->needs(InterfaceStub::class)
            ->give(ConcreteImplementationWithInstantiationCounterStub::class);

        $container->make(DependentStub::class);
        $container->make(DependentStub::class);
        $container->make(DependentStub::class);
        $container->make(DependentStub::class);
        $container->make(DependentStub::class);
        $container->make(DependentStub::class);
        $container->make(DependentStub::class);
        $container->make(DependentStub::class);
        $container->make(DependentStub::class);
        $container->make(DependentStub::class);

        $this->assertEquals(1, ConcreteImplementationWithInstantiationCounterStub::$instantiationCounter);
    }

    /**
     *
     */
    public function testUnshareInstance(): void {

        $container = new Container;

        $container->shareInstance(ConcreteStub::class, new ConcreteStub());

        $this->assertTrue($container->isShared(ConcreteStub::class));

        $container->unshareInstance(ConcreteStub::class);

        $this->assertFalse($container->isShared(ConcreteStub::class));
    }

    /**
     *
     */
    public function testUnshareAll(): void {

        $container = new Container;

        $concreteStubOne = new ConcreteStub();
        $concreteStubTwo = new ConcreteStub();
        $concreteStubThree = new ConcreteStub();

        $container->shareInstance('ArbitraryStringOne', $concreteStubOne);
        $container->shareInstance('ArbitraryStringTwo', $concreteStubTwo);
        $container->shareInstance('ArbitraryStringThree', $concreteStubThree);

        $this->assertTrue($container->isShared('ArbitraryStringOne'));
        $this->assertTrue($container->isShared('ArbitraryStringTwo'));
        $this->assertTrue($container->isShared('ArbitraryStringThree'));

        $container->unshareAll();

        $this->assertFalse($container->isShared('ArbitraryStringOne'));
        $this->assertFalse($container->isShared('ArbitraryStringTwo'));
        $this->assertFalse($container->isShared('ArbitraryStringThree'));
    }

    /**
     * @throws ContainerException
     * @throws NotFoundException
     * @throws ReflectionException
     */
    public function testResolvingUnboundAbstractDependencyThatHasDefault(): void {

        $container = new Container();

        $dependentWithDefaultStub = $container->make(DependentWithDefaultStub::class);

        $this->assertInstanceOf(DependentWithDefaultStub::class, $dependentWithDefaultStub);
        $this->assertNull($dependentWithDefaultStub->getConcreteImplementationStub());
    }

    /**
     * @throws ContainerException
     * @throws NotFoundException
     * @throws ReflectionException
     */
    public function testContainerCanInjectSimpleVariable(): void {

        $container = new Container;

        $container
            ->when(MixedWithPrimitiveStub::class)
            ->needs('$first')
            ->give(100);

        $container
            ->when(MixedWithPrimitiveStub::class)
            ->needs('$last')
            ->give('Value Of Last');

        $mixedWithPrimitiveStub = $container->make(MixedWithPrimitiveStub::class);

        $this->assertEquals(100, $mixedWithPrimitiveStub->getFirst());
        $this->assertEquals('Value Of Last', $mixedWithPrimitiveStub->getLast());
    }

    /**
     * @throws ContainerException
     * @throws NotFoundException
     * @throws ReflectionException
     */
    public function testContainerContextualBindingCanInjectClosure(): void {

        $container = new Container;

        $container
            ->when(MixedWithPrimitiveStub::class)
            ->needs('$first')
            ->give(function ($container) {
                    return $container->make(ConcreteStub::class);
                }
            );

        $container
            ->when(MixedWithPrimitiveStub::class)
            ->needs('$last')
            ->give('Value Of Last');

        $mixedWithPrimitiveStub = $container->make(MixedWithPrimitiveStub::class);

        $this->assertInstanceOf(ConcreteStub::class, $mixedWithPrimitiveStub->getFirst());
        $this->assertEquals('Value Of Last', $mixedWithPrimitiveStub->getLast());
    }

    /**
     * @throws ContainerException
     * @throws NotFoundException
     * @throws ReflectionException
     */
    public function testAttemptToResolveUnboundAbstractException(): void {

        $this->expectException(ContainerException::class);
        $this->expectExceptionMessage("Cannot create an instance of non instantiable binding: Ether\Tests\Unit\Container\Stub\InterfaceStub while building Ether\Tests\Unit\Container\Stub\DependentStub.");

        $container = new Container();

        $container->make(DependentStub::class);
    }

    /**
     * @throws ContainerException
     * @throws NotFoundException
     * @throws ReflectionException
     */
    public function testNoParamsArrayForPrimitivesException(): void {

        $this->expectException(ContainerException::class);
        $this->expectExceptionMessage("Cannot resolve dependency: first, in Ether\Tests\Unit\Container\Stub\MixedWithPrimitiveStub");

        $container = new Container;

        $container->make(MixedWithPrimitiveStub::class);
    }

    /**
     * @throws ContainerException
     * @throws NotFoundException
     * @throws ReflectionException
     */
    public function testBindingResolutionExceptionMessage(): void {

        $this->expectException(ContainerException::class);
        $this->expectExceptionMessage("Cannot create an instance of non instantiable binding: Ether\Tests\Unit\Container\Stub\InterfaceStub");

        $container = new Container;

        $container->make(InterfaceStub::class, array());
    }

    /**
     * @throws ContainerException
     * @throws NotFoundException
     * @throws ReflectionException
     */
    public function testUnknownEntryThrowsException(): void {

        $this->expectException(NotFoundException::class);
        $this->expectExceptionMessage('UnknownEntry not found.');

        $container = new Container;

        $container->get('UnknownEntry');
    }

}
