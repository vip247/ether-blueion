<?php declare(strict_types=1);

/** @noinspection PhpUndefinedMethodInspection */
/** @noinspection PhpUnhandledExceptionInspection */
/** @noinspection PhpUndefinedClassInspection */
/** @noinspection PhpUnusedParameterInspection */
/** @noinspection SuspiciousAssignmentsInspection */

namespace Ether\Tests\Unit\Application;

use Mockery;
use Ether\Application;
use Ether\Config\Config;
use Mockery\MockInterface;
use org\bovigo\vfs\vfsStream;
use Ether\Container\Container;
use PHPUnit\Framework\TestCase;
use Mockery\LegacyMockInterface;
use Ether\Exceptions\RuntimeException;
use org\bovigo\vfs\vfsStreamDirectory;
use Ether\Container\ContextualBindingBuilder;
use Mockery\Adapter\Phpunit\MockeryPHPUnitIntegration as MockeryIntegration;

final class ApplicationTest extends TestCase {

    use MockeryIntegration;

    /** @var vfsStreamDirectory */
    private static $root;

    /** @var Config|LegacyMockInterface|MockInterface */
    private $config;

    /** @var Container|LegacyMockInterface|MockInterface */
    private $container;

    /** @var ContextualBindingBuilder|LegacyMockInterface|MockInterface */
    private $contextualBindingBuilder;

    public static function setUpBeforeClass(): void {

        // Setup virtual filesystem
        static::$root = vfsStream::setup('vfsroot', null, [
            'app' => [],
            'boot' => [],
            'config' => [],
            'public' => [],
            'resources' => [],
            'routes' => [],
            'storage' => [],
            'composer.json' => '{"name":"ether/ether","type":"project","license":"MIT","description":"PHP Project built with the Ether Framework.","config":{},"minimum-stability":"dev","prefer-stable":true,"repositories":[],"require":{},"require-dev":{},"autoload":{"psr-4":{"App\\":"app"}},"autoload-dev":{"psr-4":{"App\\Tests\\":"tests","Ether\\Tests\\":"vendor/aeonis/ether/tests"}}}',
            '.env' => 'APP_NAME=Ether'
        ]);

    }

    protected function setUp(): void {

        $this->config = Mockery::spy(Config::class);

        $this->config->shouldReceive('load')->withAnyArgs()->andReturn($this->config);

        $this->config->shouldReceive('get')->withAnyArgs()->andReturn([]);

        $this->container = Mockery::spy(Container::class);

        $this->contextualBindingBuilder = Mockery::spy(ContextualBindingBuilder::class);

        $this->contextualBindingBuilder->shouldReceive('needs')->withAnyArgs()->andReturn($this->contextualBindingBuilder);

        $this->contextualBindingBuilder->shouldReceive('give')->withAnyArgs()->andReturnNull();

        $this->container->shouldReceive('when')->andReturn($this->contextualBindingBuilder);

        $this->container->shouldReceive('build')->with(Config::class)->andReturn($this->config);

    }

    public function testRegistersEnvironmentVariables(): void {

        $this->getApplication();

        $this->assertNotFalse(getenv('APP_NAME'));
    }

    public function testCanGetBasePath(): void {

        $application = $this->getApplication();

        $this->assertSame(static::$root->url(), $application->basePath());
    }

    public function testCanGetAppPath(): void {

        $application = $this->getApplication();

        $this->assertSame(static::$root->url() . '/app', $application->appPath());
    }

    public function testCanGetConfigPath(): void {

        $application = $this->getApplication();

        $this->assertSame(static::$root->url() . '/config', $application->configPath());
    }

    public function testCanGetPublicPath(): void {

        $application = $this->getApplication();

        $this->assertSame(static::$root->url() . '/public', $application->publicPath());
    }

    public function testCanGetRoutesPath(): void {

        $application = $this->getApplication();

        $this->assertSame(static::$root->url() . '/routes', $application->routesPath());
    }

    public function testCanGetStoragePath(): void {

        $application = $this->getApplication();

        $this->assertSame(static::$root->url() . '/storage', $application->storagePath());
    }

    public function testCanGetCachePath(): void {

        $application = $this->getApplication();

        $this->assertSame(static::$root->url() . '/storage/cache', $application->cachePath());
    }

    private function getApplication(): Application {
        return new Application(static::$root->url(), null, $this->container);
    }

}
