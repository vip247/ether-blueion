<?php /** @noinspection PhpParamsInspection */
declare(strict_types=1);

namespace Ether\Tests\Unit\Cli;

use Mockery;
use Exception;
use Ether\Cli\Kernel;
use Ether\Application;
use Mockery\MockInterface;
use PHPUnit\Framework\TestCase;
use Mockery\LegacyMockInterface;
use Symfony\Component\Console\Helper\HelperSet;
use NunoMaduro\Collision\Provider as ErrorHandler;
use Ether\Cli\CommandLoaders\CommandLoaderFactory;
use Symfony\Component\Console\Application as Console;
use Mockery\Adapter\Phpunit\MockeryPHPUnitIntegration as MockeryIntegration;

final class KernelTest extends TestCase {

    use MockeryIntegration;

    private $app;

    /** @var LegacyMockInterface|MockInterface|Console  */
    private $console;

    /** @var LegacyMockInterface|MockInterface|ErrorHandler  */
    private $errorHandler;

    /** @var LegacyMockInterface|MockInterface|HelperSet  */
    private $helperSet;

    /** @var CommandLoaderFactory|LegacyMockInterface|MockInterface  */
    private $commandLoaderFactory;

    protected function setUp(): void {

        $this->app = Mockery::spy(Application::class);
        $this->console = Mockery::mock(Console::class);
        $this->errorHandler = Mockery::mock(ErrorHandler::class);
        $this->helperSet = Mockery::spy(HelperSet::class);
        $this->commandLoaderFactory = Mockery::mock(CommandLoaderFactory::class);

        $this->console->shouldReceive('setName', 'setVersion', 'setHelperSet', 'setCatchExceptions')->withAnyArgs()->andReturnNull();
        $this->console->shouldReceive('setCommandLoader')->with($this->commandLoaderFactory)->andReturnNull();
        $this->console->shouldReceive('run')->withAnyArgs()->andReturn(0);

        $this->commandLoaderFactory->shouldReceive('load')->with(Mockery::any())->andReturnNull();

        $this->errorHandler->shouldReceive('register')->andThrow($this->errorHandler);
    }

    public function testConfiguresConsole(): void {

        $kernel = new Kernel($this->app, $this->console, $this->commandLoaderFactory, $this->helperSet, $this->errorHandler);

        $this->commandLoaderFactory->shouldHaveReceived('load')->with(Mockery::any());

        $this->console->shouldHaveReceived('setCommandLoader')->with($this->commandLoaderFactory);
        $this->console->shouldHaveReceived('setName');
        $this->console->shouldHaveReceived('setVersion');
        $this->console->shouldHaveReceived('setHelperSet');
        $this->console->shouldHaveReceived('setCatchExceptions');

        $this->assertInstanceOf(Kernel::class, $kernel);
    }

    /**
     * @throws Exception
     */
    public function testRunsConsole(): void {

        $kernel = new Kernel($this->app, $this->console, $this->commandLoaderFactory, $this->helperSet, $this->errorHandler);

        $kernel->handle();

        $this->console->shouldHaveReceived('run');
    }
}
