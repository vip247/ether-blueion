<?php
/** @noinspection PhpIncludeInspection */
/** @noinspection PhpUnhandledExceptionInspection */

declare(strict_types=1);

##############################################################################
# SETUP
##############################################################################

date_default_timezone_set('Africa/Johannesburg');

if(getenv('APP_ENV') === 'ci') {
    require dirname(__DIR__, 1) . '/vendor/autoload.php';
}

if(getenv('APP_ENV') === 'local') {
    require dirname(__DIR__, 2) . '/ether/vendor/autoload.php';
}

use DG\BypassFinals;
use Ether\Database\Orm\Config;
use Ether\Database\Orm\Mapper;
use Ether\Database\Orm\Locator;
use Doctrine\DBAL\DBALException;
use Ether\Database\DBAL\Types\Uuid;
use Ether\Database\DBAL\Types\Encrypted;
use Doctrine\DBAL\Types\Type as DBALType;
use Ether\Database\DBAL\Types\UuidBinaryOrderedTime;

BypassFinals::enable();

##############################################################################
# ORM
##############################################################################

if ( ! empty(getenv('DB_TYPE'))) {
    $dbDsn = getenv('DB_DSN_' . strtoupper(getenv('DB_TYPE')));
}

if ( ! empty($dbDsn)) {

    $cfg = new Config();

    $cfg->addConnection('test', $dbDsn);

    $orm = new Locator($cfg);

}

##############################################################################
# HELPERS
##############################################################################

if ( ! function_exists('testOrmMapper')) {

    /**
     * Sets up ORM mapper for tests
     *
     * @param string $entityName
     *
     * @return Mapper
     *
     * @throws DBALException
     */
    function testOrmMapper(string $entityName): Mapper {

        /** @var Locator $orm */
        global $orm; // you should never do this in real code :)

        if ( ! isset(DBALType::getTypesMap()[Encrypted::NAME])) {

            Encrypted::$key = hex2bin(getenv('APP_KEY'));

            DBALType::addType(Encrypted::NAME, Encrypted::class);
        }

        if ( ! isset(DBALType::getTypesMap()[UuidBinaryOrderedTime::NAME])) {
            DBALType::addType(UuidBinaryOrderedTime::NAME, UuidBinaryOrderedTime::class);
        }

        if ( ! isset(DBALType::getTypesMap()[Uuid::NAME])) {
            DBALType::addType(Uuid::NAME, Uuid::class);
        }

        return $orm->mapper($entityName);
    }

}
