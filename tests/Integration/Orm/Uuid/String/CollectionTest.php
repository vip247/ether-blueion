<?php declare(strict_types=1);

namespace Ether\Tests\Integration\Orm\Uuid\String;

use Ramsey\Uuid\Uuid;
use PHPUnit\Framework\TestCase;
use Ether\Tests\Integration\Orm\Uuid\String\Entity\Tag;

class CollectionTest extends TestCase {

    private static $entities = ['Tag'];

    private static $tagIds = [];

    public static function setupBeforeClass():void  {

        foreach (self::$entities as $entity) {
            testOrmMapper('Ether\Tests\Integration\Orm\Uuid\String\Entity\\' . $entity)->migrate();
        }

        $tagCount = 3;

        // Create some tags
        $tagMapper = testOrmMapper(Tag::class);

        for ($i = 1; $i <= $tagCount; $i++) {

            $id = Uuid::uuid4();

            static::$tagIds[] = $id->toString();

            $tagMapper->create([
                'id' => $id,
                'name' => "Title {$i}"
            ]);
        }
    }

    public static function tearDownAfterClass(): void {
        foreach (self::$entities as $entity) {
            testOrmMapper('Ether\Tests\Integration\Orm\Uuid\String\Entity\\' . $entity)->dropTable();
        }
    }

    public function testMergeIntersecting(): void {

        $mapper = testOrmMapper(Tag::class);

        // Fetch 3 entries
        $tags = $mapper->all()->execute();

        // Fetch 1 tag we already have
        $newTags = $mapper->where(['name' => 'Title 1'])->execute();

        // Check counts before merge
        $this->assertCount(3, $tags);
        $this->assertCount(1, $newTags);

        // Merge new tag in and expect count to remain the same (duplicate tag)
        $tags->merge($newTags);
        $this->assertCount(3, $tags);
    }

    public function testCollectionJsonSerialize(): void {

        $mapper = testOrmMapper(Tag::class);

        $tags = $mapper->all()->execute();

        $data = json_encode($tags->toArray());
        $json = json_encode($tags);

        $this->assertSame($data, $json);
    }

    public function testQueryCallsCollectionMethods(): void {

        $mapper = testOrmMapper(Tag::class);

        $tagsArray = array_map(static function($item) {
            return $item->toString();
        }, $mapper->all()->resultsIdentities());

        $this->assertSame(ksort(static::$tagIds), ksort($tagsArray));
    }

    public function testQueryCallsCollectionMethodsWithArguments(): void {

        $mapper = testOrmMapper(Tag::class);

        $matchingTag = $mapper->all()->filter(static function (Tag $tag) {
            return $tag->id->toString() === static::$tagIds[2];
        });

        $this->assertSame(static::$tagIds[2], $matchingTag[0]->id->toString());
    }
}
