<?php declare(strict_types=1);

namespace Ether\Tests\Integration\Orm\Uuid\String\Entity;

use DateTime;
use Ether\Database\Orm\Query;
use Ether\Database\Orm\Entity;
use Ether\Database\Orm\EventEmitter;
use Ether\Database\Orm\EntityInterface;
use Ether\Database\Orm\MapperInterface;
use Ether\Tests\Integration\Orm\Uuid\String\Entity\Post\Comment;

class Post extends Entity {

    protected static $table = 'test_posts_uuid';

    // For testing purposes only
    public static $events = [];

    public static function fields(): array {
        return [
            'id'           => ['type' => 'uuid', 'primary' => true],
            'author_id'    => ['type' => 'uuid', 'required' => true],
            'title'        => ['type' => 'string', 'required' => true],
            'body'         => ['type' => 'text', 'required' => true],
            'status'       => ['type' => 'boolean', 'default' => false, 'index' => true],
            'date_created' => ['type' => 'datetime', 'value' => new DateTime()],
            'data'         => ['type' => 'json']
        ];
    }

    public static function relations(MapperInterface $mapper, EntityInterface $entity): array {
        return [
            'tags'                 => $mapper->hasManyThrough($entity, Tag::class, PostTag::class, 'tag_id', 'post_id'),
            'comments'             => $mapper->hasMany($entity, Comment::class, 'post_id')->order(['date_created' => 'ASC']),
            'polymorphic_comments' => $mapper->hasMany($entity, PolymorphicComment::class, 'item_id')->where(['item_type' => 'post']),
            'author'               => $mapper->belongsTo($entity, Author::class, 'author_id')
        ];
    }

    /**
     * @param EventEmitter $eventEmitter
     */
    public static function events(EventEmitter $eventEmitter): void {
        // This is done only to allow events to be set dynamically in a very
        // specific way for testing purposes. You probably don't want to do
        // this in your code...
        foreach (static::$events as $eventName => $methods) {
            $eventEmitter->on($eventName, static function ($entity, $mapper) use ($methods) {
                foreach ($methods as $method) {
                    $entity->$method();
                }
            });
        }
    }

    public static function scopes(): array {
        return [
            'active' => static function (Query $query) {
                return $query->where(['status' => 1]);
            }
        ];
    }

    public function mock_save_hook() {
        $this->status++;
    }
}
