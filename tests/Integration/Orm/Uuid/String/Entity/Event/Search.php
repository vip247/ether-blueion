<?php declare(strict_types=1);

namespace Ether\Tests\Integration\Orm\Uuid\String\Entity\Event;

use Ether\Database\Orm\Entity;
use Ether\Database\Orm\EntityInterface;
use Ether\Database\Orm\MapperInterface;
use Ether\Tests\Integration\Orm\Uuid\String\Entity\Event;

class Search extends Entity {

    protected static $table = 'test_events_search_uuid';

    /**
     * @return array
     */
    public static function fields(): array {
        return [
            'id'       => ['type' => 'uuid', 'primary' => true],
            'event_id' => ['type' => 'uuid', 'index' => true, 'required' => true],
            'body'     => ['type' => 'text', 'required' => true, 'fulltext' => true]
        ];
    }

    /**
     * @param MapperInterface $mapper
     * @param EntityInterface $entity
     *
     * @return array
     */
    public static function relations(MapperInterface $mapper, EntityInterface $entity): array {
        return [
            'event' => $mapper->belongsTo($entity, Event::class, 'event_id')
        ];
    }
}
