<?php declare(strict_types=1);

namespace Ether\Tests\Integration\Orm\Uuid\String\Entity;

use Ether\Database\Orm\Entity;
use Ether\Database\Orm\MapperInterface;
use Ether\Database\Orm\EntityInterface;

class PostTag extends Entity {

    protected static $table = 'test_posttags_uuid';

    public static function fields(): array {
        return [
            'id'      => ['type' => 'uuid', 'primary' => true],
            'tag_id'  => ['type' => 'uuid', 'required' => true, 'unique' => 'post_tag'],
            'post_id' => ['type' => 'uuid', 'required' => true, 'unique' => 'post_tag'],
            'random'  => ['type' => 'string'] // Totally unnecessary, but makes testing upserts easy
        ];
    }

    /**
     * @param MapperInterface $mapper
     * @param EntityInterface $entity
     *
     * @return array
     */
    public static function relations(MapperInterface $mapper, EntityInterface $entity): array {
        return [
            'post' => $mapper->belongsTo($entity, Post::class, 'post_id'),
            'tag'  => $mapper->belongsTo($entity, Tag::class, 'tag_id')
        ];
    }
}
