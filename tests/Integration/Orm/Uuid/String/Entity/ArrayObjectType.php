<?php declare(strict_types=1);

namespace Ether\Tests\Integration\Orm\Uuid\String\Entity;

use stdClass;
use Ether\Database\Orm\Entity;

/**
 * ArrayObjectType
 * An entity using all built-in array and object field types
 */
class ArrayObjectType extends Entity {

    protected static $table = 'test_array_object_uuid';

    public static function fields(): array {
        return [
            'id'               => ['type' => 'uuid', 'primary' => true],
            'fld_array'        => ['type' => 'array', 'value' => []],
            'fld_simple_array' => ['type' => 'simple_array', 'value' => []],
            'fld_json_array'   => ['type' => 'json_array', 'value' => []],
            'fld_object'       => ['type' => 'object', 'value' => new stdClass()],
        ];
    }

}
