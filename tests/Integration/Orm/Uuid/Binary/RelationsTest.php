<?php /** @noinspection PhpUndefinedFieldInspection */
declare(strict_types=1);

namespace Ether\Tests\Integration\Orm\Uuid\Binary;

use DateTime;
use RuntimeException;
use Ramsey\Uuid\Uuid;
use InvalidArgumentException;
use PHPUnit\Framework\TestCase;
use Doctrine\DBAL\DBALException;
use Ether\Database\Orm\EntityInterface;
use Ether\Database\Orm\Relation\HasMany;
use Ether\Tests\Integration\Orm\Uuid\Binary\Entity\Tag;
use Ether\Tests\Integration\Orm\Uuid\Binary\Entity\Post;
use Ether\Database\Orm\Exception as OrmException;
use Ether\Tests\Integration\Orm\Uuid\Binary\Entity\Event;
use Ether\Tests\Integration\Orm\Uuid\Binary\Entity\Author;
use Ether\Tests\Integration\Orm\Uuid\Binary\Entity\PostTag;
use Ether\Tests\Integration\Orm\Uuid\Binary\Entity\Post\Comment;

class RelationsTest extends TestCase {

    private static $authorId;

    private static $entities = [
        'PostTag',
        'Post\Comment',
        'Post',
        'Tag',
        'Author',
        'Event\Search',
        'Event'
    ];

    public static function setupBeforeClass(): void {

        foreach (self::$entities as $entity) {
            testOrmMapper('Ether\Tests\Integration\Orm\Uuid\Binary\Entity\\' . $entity)->migrate();
        }

        $authorMapper = testOrmMapper(Author::class);

        static::$authorId = Uuid::uuid1();
        $author = $authorMapper->build([
            'id'       => static::$authorId,
            'email'    => 'example@example.com',
            'password' => 't00r',
            'is_admin' => false
        ]);

        $result = $authorMapper->insert($author);

        if ( ! $result) {
            throw new RuntimeException('Unable to create author: ' . var_export($author->data(), true));
        }
    }

    /**
     * @throws OrmException
     */
    public static function tearDownAfterClass(): void {
        foreach (self::$entities as $entity) {
            testOrmMapper('Ether\Tests\Integration\Orm\Uuid\Binary\Entity\\' . $entity)->dropTable();
        }
    }

    /**
     * @return bool|int|mixed|string
     * @throws DBALException
     * @throws OrmException
     */
    public function testBlogPostInsert() {

        $mapper = testOrmMapper(Post::class);

        $post = $mapper->get();
        $post->title = 'My Awesome Blog Post';
        $post->body = "<p>This is a really awesome super-duper post.</p><p>It's testing the relationship functions.</p>";
        $post->date_created = new DateTime();
        $post->author_id = static::$authorId;
        $postId = $mapper->insert($post);

        $this->assertNotFalse($postId);

        // Test selecting it to ensure it exists
        $postx = $mapper->get($postId);

        $this->assertInstanceOf(Post::class, $postx);

        return $postId;
    }

    /**
     * @depends testBlogPostInsert
     *
     * @param $postId
     *
     * @throws DBALException
     * @throws OrmException
     */
    public function testPostCommentsInsert($postId): void {

        $mapper = testOrmMapper(Post::class);

        $commentMapper = testOrmMapper(Comment::class);
        $mapper->get($postId);

        // Array will usually come from POST/JSON data or other source
        $comment = $commentMapper->get();
        $comment->data([
            'post_id'      => $postId,
            'name'         => 'Testy McTester',
            'email'        => 'test@test.com',
            'body'         => 'This is a test comment. Yay!',
            'date_created' => new DateTime()
        ]);

        $commentSaved = $commentMapper->save($comment);

        if ( ! $commentSaved) {
            $this->fail('Comment NOT saved');
        }

        $this->assertNotFalse($commentSaved);
    }

    /**
     * @depends testBlogPostInsert
     *
     * @param $postId
     *
     * @throws DBALException
     * @throws OrmException
     */
    public function testPostCommentsCanIterate($postId): void {

        $mapper = testOrmMapper(Post::class);
        $post = $mapper->get($postId);

        foreach ($post->comments as $comment) {
            $this->assertInstanceOf(Comment::class, $comment);
        }
    }

    /**
     * @throws DBALException
     * @throws OrmException
     */
    public function testHasManyRelationCountZero(): void {
        $mapper = testOrmMapper(Post::class);
        $post = $mapper->get();
        $post->title = 'No Comments';
        $post->body = '<p>Comments relation test</p>';
        $mapper->save($post);

        $this->assertNull($post->comments);
    }

    /**
     * @throws DBALException
     * @throws OrmException
     */
    public function testIterateEmptySetDoesNotThrowError(): void {

        $mapper = testOrmMapper(Post::class);

        $post = $mapper->get();
        $post->title = 'No Comments';
        $post->body = '<p>Comments relation test</p>';
        $post->author_id = static::$authorId;

        $mapper->save($post);

        $comments = [];

        foreach ($post->comments as $comment) {
            $comments[] = $comment;
        }

        $this->assertCount(0, $comments);
    }

    /**
     * @depends testBlogPostInsert
     *
     * @param $postId
     *
     * @throws DBALException
     * @throws OrmException
     */
    public function testRelationsNotInData($postId): void {
        $mapper = testOrmMapper(Post::class);
        $post = $mapper->get($postId);

        $this->assertNotContains('comments', array_keys($post->data()));
    }

    /**
     * @depends testBlogPostInsert
     *
     * @param $postId
     *
     * @throws DBALException
     * @throws OrmException
     */
    public function testBlogCommentsRelationCountOne($postId): void {
        $mapper = testOrmMapper(Post::class);
        $post = $mapper->get($postId);

        $this->assertEquals(count($post->comments), 1);
    }

    /**
     * @depends testBlogPostInsert
     *
     * @param $postId
     *
     * @throws DBALException
     * @throws OrmException
     */
    public function testBlogCommentsRelationCanBeModified($postId): void {

        $mapper = testOrmMapper(Post::class);

        $post = $mapper->get($postId);

        /** @var HasMany $sortedComments */
        $sortedComments = $post->comments->order(['date_created' => 'DESC']);
        $this->assertInstanceOf(HasMany::class, $sortedComments);

        $this->assertStringContainsStringIgnoringCase('ORDER BY', $sortedComments->query()->toSql());
    }

    /**
     * @depends testBlogPostInsert
     *
     * @param $postId
     *
     * @throws DBALException
     * @throws OrmException
     */
    public function testRelationshipQueryNotReset($postId): void {
        $mapper = testOrmMapper(Post::class);
        $post = $mapper->get($postId);

        $before_count = $post->comments->count();
        foreach ($post->comments as $comment) {
            $comment->post;
        }

        $this->assertSame($before_count, $post->comments->count());
    }

    /**
     * @depends testBlogPostInsert
     *
     * @param $postId
     *
     * @throws DBALException
     * @throws OrmException
     */
    public function testBlogTagsHasManyThrough($postId): void {

        $mapper = testOrmMapper(Post::class);
        $post = $mapper->get($postId);

        $this->assertCount(0, $post->tags);
    }

    /**
     * @depends testBlogPostInsert
     *
     * @param $postId
     *
     * @throws DBALException
     * @throws OrmException
     */
    public function testPostTagInsertHasManyThroughCountIsAccurate($postId): void {

        $mapper = testOrmMapper(Post::class);
        $post = $mapper->get($postId);

        $tagCount = 3;

        // Create some tags
        $tags = array();
        $tagMapper = testOrmMapper(Tag::class);
        for ($i = 1; $i <= $tagCount; $i++) {
            $tags[] = $tagMapper->create([
                'name' => "Title {$i}"
            ]);
        }

        // Insert all tags for current post
        $postTagMapper = testOrmMapper(PostTag::class);
        foreach ($tags as $tag) {
            $postTagMapper->create([
                'post_id' => $post->id,
                'tag_id'  => $tag->id
            ]);
        }

        $this->assertCount($tagCount, $post->tags);

        $tagData = [];
        foreach ($tags as $tag) {
            $tagData[] = $tag->data();
        }

        $resultData = $post->tags->map(static function (Tag $tag) {
            return $tag->data();
        });

        $tagData = array_map(static function($item) {
            $item['id'] = $item['id']->toString();
        }, $tagData);

        $resultData = array_map(static function($item) {
            $item['id'] = $item['id']->toString();
        }, $resultData);

        $this->assertEquals($tagData, $resultData);
    }

    /**
     * @return mixed
     * @throws DBALException
     * @throws OrmException
     */
    public function testEventInsert() {

        $mapper = testOrmMapper(Event::class);
        $event = $mapper->get();
        $event->title = 'My Awesome Event';
        $event->description = 'Some equally awesome event description here.';
        $event->type = 'free';
        $event->date_start = new DateTime();
        $eventId = $mapper->save($event);

        $this->assertNotFalse($eventId);

        return $event->id;
    }

    /**
     * @depends testEventInsert
     *
     * @param $eventId
     *
     * @throws DBALException
     * @throws OrmException
     */
    public function testEventHasOneSearchIndex($eventId): void {
        $mapper = testOrmMapper(Event::class);
        $event = $mapper->get($eventId);
        $eventSearch = $event->search->execute();
        $this->assertInstanceOf(Event\Search::class, $eventSearch);
        $this->assertEquals($eventSearch->event_id->toString(), $eventId->toString());
    }

    /**
     * @depends testEventInsert
     *
     * @param $eventId
     *
     * @throws DBALException
     * @throws OrmException
     */
    public function testEventSearchBelongsToEvent($eventId): void {

        $mapper = testOrmMapper(Event\Search::class);
        $eventSearch = $mapper->first(['event_id' => $eventId]);
        $event = $eventSearch->event->execute();

        $this->assertInstanceOf(Event::class, $event);
        $this->assertEquals($event->id->toString(), $eventId->toString());
    }

    /**
     * @depends testEventInsert
     *
     * @param $eventId
     *
     * @throws DBALException
     * @throws OrmException
     */
    public function testEventSearchEntityAccessibleWithEntityMethod($eventId): void {

        $mapper = testOrmMapper(Event\Search::class);

        $eventSearch = $mapper->first(['event_id' => $eventId]);
        $event = $eventSearch->event->entity();

        $this->assertInstanceOf(Event::class, $event);
        $this->assertEquals($event->id->toString(), $eventId->toString());
    }

    /**
     * @depends testEventInsert
     *
     * @param $eventId
     *
     * @throws DBALException
     * @throws OrmException
     */
    public function testEventSearchEntityMethodCalledOnEntityDoesNotError($eventId): void {

        $mapper = testOrmMapper(Event\Search::class);

        $eventSearch = $mapper->first(['event_id' => $eventId]);
        $event = $eventSearch->event->entity()->entity();

        $this->assertInstanceOf(Event::class, $event);
        $this->assertEquals($event->id->toString(), $eventId->toString());
    }

    /**
     * @throws DBALException
     * @throws OrmException
     */
    public function testInvalidRelationClass(): void {

        $this->expectException(InvalidArgumentException::class);

        $mapper = testOrmMapper(Post::class);

        /** @var EntityInterface $entity */
        $entity = $mapper->first();
        $entity->fake = $mapper->hasOne($entity, 'Nonexistent\Entity', 'fake_field');

        $entity->fake->something;
    }

    /**
     * @throws DBALException
     * @throws OrmException
     */
    public function testAccessingRelationObjectProperty(): void {

        $email = 'test@test.com';
        $postMapper = testOrmMapper(Post::class);
        $authorMapper = testOrmMapper(Author::class);

        $author = $authorMapper->create([
            'id'       => Uuid::uuid1(),
            'email'    => $email,
            'password' => 'password',
            'is_admin' => false,
        ]);

        $post = $postMapper->create([
            'title'     => 'Testing Property Access',
            'body'      => 'I hope array access is set correctly',
            'author_id' => $author->id,
        ]);

        $this->assertEquals($post->author['email'], $email);
    }

    /**
     * @throws DBALException
     * @throws OrmException
     */
    public function testLazyLoadRelationIsset(): void {
        $postMapper = testOrmMapper(Post::class);
        $authorMapper = testOrmMapper(Author::class);

        $author = $authorMapper->create([
            'id'       => Uuid::uuid1(),
            'email'    => 'test1@test.com',
            'password' => 'password',
            'is_admin' => false,
        ]);
        $post = $postMapper->create([
            'title'     => 'Testing Property Access',
            'body'      => 'I hope array access is set correctly',
            'author_id' => $author->id,
        ]);

        $this->assertTrue(isset($post->author));
    }
}
