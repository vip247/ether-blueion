<?php declare(strict_types=1);

namespace Ether\Tests\Integration\Orm\Uuid\Binary;

use DateTime;
use PHPUnit\Framework\TestCase;
use Ether\Tests\Integration\Orm\Uuid\Binary\Entity\Post;
use Ether\Tests\Integration\Orm\Uuid\Binary\Entity\Event;
use Ether\Tests\Integration\Orm\Uuid\Binary\Entity\Author;
use Ether\Tests\Integration\Orm\Uuid\Binary\Entity\PolymorphicComment;

class PolymorphicRelationsTest extends TestCase {

    private static $entities = [
        'PolymorphicComment',
        'Post',
        'Author',
        'Event\Search',
        'Event'
    ];

    public static function setupBeforeClass(): void {

        foreach (self::$entities as $entity) {
            testOrmMapper('Ether\Tests\Integration\Orm\Uuid\Binary\Entity\\' . $entity)->migrate();
        }

        $authorMapper = testOrmMapper(Author::class);
        $author = $authorMapper->create([
            'email'    => 'chester@tester.com',
            'password' => 'password',
            'is_admin' => true
        ]);

        $posts = [];
        $postsCount = 3;
        $mapper = testOrmMapper(Post::class);
        for ($i = 1; $i <= $postsCount; $i++) {
            $posts[] = $mapper->create([
                'title'     => "Eager Loading Test Post $i",
                'body'      => "Eager Loading Test Post Content Here $i",
                'author_id' => $author->id
            ]);
        }

        $commentMapper = testOrmMapper(PolymorphicComment::class);
        foreach ($posts as $post) {
            $commentCount = 3;
            for ($i = 1; $i <= $commentCount; $i++) {
                $commentMapper->create([
                    'item_type' => 'post',
                    'item_id'   => $post->id,
                    'name'      => 'Chester Tester',
                    'email'     => 'chester@tester.com',
                    'body'      => "This is a test POST comment $i. Yay!"
                ]);
            }
        }

        $events = [];
        $eventMapper = testOrmMapper(Event::class);
        $events[] = $eventMapper->create([
            'title'       => 'Eager Load Test Event',
            'description' => 'some test eager loading description',
            'type'        => 'free',
            'date_start'  => new DateTime('+1 second')
        ]);

        $events[] = $eventMapper->create([
            'title'       => 'Eager Load Test Event 2',
            'description' => 'some test eager loading description 2',
            'type'        => 'free',
            'date_start'  => new DateTime('+1 second')
        ]);

        foreach ($events as $event) {
            $commentCount = 3;
            for ($i = 1; $i <= $commentCount; $i++) {
                $commentMapper->create([
                    'item_type' => 'event',
                    'item_id'   => $event->id,
                    'name'      => 'Chester Tester',
                    'email'     => 'chester@tester.com',
                    'body'      => "This is a test EVENT comment $i. Yay!"
                ]);
            }
        }
    }

    public static function tearDownAfterClass(): void {
        foreach (self::$entities as $entity) {
            testOrmMapper('Ether\Tests\Integration\Orm\Uuid\Binary\Entity\\' . $entity)->dropTable();
        }
    }

    public function testEventHasManyPolymorphicComments(): void {

        $mapper = testOrmMapper(Event::class);

        $event = $mapper->first();

        $event->polymorphic_comments->query();

        $this->assertInstanceOf(Event::class, $event);
        $this->assertCount(3, $event->polymorphic_comments);
    }

    public function testPostHasManyPolymorphicComments(): void {

        $mapper = testOrmMapper(Post::class);
        $post = $mapper->first();

        $post->polymorphic_comments->query();

        $this->assertInstanceOf(Post::class, $post);
        $this->assertCount(3, $post->polymorphic_comments);
    }

}
