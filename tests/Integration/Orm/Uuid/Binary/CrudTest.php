<?php declare(strict_types=1);

namespace Ether\Tests\Integration\Orm\Uuid\Binary;

use DateTime;
use Exception;
use Throwable;
use Ramsey\Uuid\Uuid;
use RuntimeException;
use PHPUnit\Framework\TestCase;
use Ether\Tests\Integration\Orm\Type\Encrypted;
use Ether\Database\Orm\Entity\Collection;
use Ether\Tests\Integration\Orm\Uuid\Binary\Entity\Tag;
use Ether\Tests\Integration\Orm\Uuid\Binary\Entity\Post;
use Ether\Database\Orm\Exception as OrmException;
use Ether\Tests\Integration\Orm\Uuid\Binary\Entity\Event;
use Ether\Tests\Integration\Orm\Uuid\Binary\Entity\Author;
use Ether\Tests\Integration\Orm\Uuid\Binary\Entity\PostTag;
use Ether\Tests\Integration\Orm\Uuid\Binary\Entity\Setting;
use Ether\Tests\Integration\Orm\Uuid\Binary\Entity\Post\Comment;
use Ether\Tests\Integration\Orm\Uuid\Binary\Entity\Event\Search;

class CrudTest extends TestCase {

    private static $authorId;

    private static $entities = [
        'PolymorphicComment',
        'PostTag',
        'Post\Comment',
        'Post',
        'Tag',
        'Author',
        'Setting',
        'Event\Search',
        'Event'
    ];

    public static function setupBeforeClass(): void {

        foreach (self::$entities as $entity) {
            testOrmMapper('Ether\Tests\Integration\Orm\Uuid\Binary\Entity\\' . $entity)->migrate();
        }

        $authorMapper = testOrmMapper(Author::class);

        static::$authorId = Uuid::uuid1();

        $author = $authorMapper->build([
            'id'       => static::$authorId,
            'email'    => 'example@example.com',
            'password' => 't00r',
            'is_admin' => false
        ]);

        $result = $authorMapper->insert($author);

        if ( ! $result) {
            throw new RuntimeException('Unable to create author: ' . var_export($author->data(), true));
        }
    }

    public static function tearDownAfterClass(): void {
        foreach (self::$entities as $entity) {
            testOrmMapper('Ether\Tests\Integration\Orm\Uuid\Binary\Entity\\' . $entity)->dropTable();
        }
    }

    public function testSampleNewsInsert(): void {

        $mapper = testOrmMapper(Post::class);
        $post = $mapper->get();

        $post->id = Uuid::uuid1();
        $post->title = 'Test Post';
        $post->body = "<p>This is a really awesome super-duper post.</p><p>It's really quite lovely.</p>";
        $post->author_id = static::$authorId;
        $post->date_created = new DateTime();
        $result = $mapper->insert($post); // returns an id

        $this->assertNotFalse($result);
    }

    public function testSampleNewsInsertWithEmptyNonRequiredFields(): void {

        $mapper = testOrmMapper(Post::class);
        $post = $mapper->get();

        $post->id = Uuid::uuid1();
        $post->title = 'Test Post With Empty Values';
        $post->body = '<p>Test post here.</p>';
        $post->author_id = static::$authorId;
        $post->date_created = null;

        try {
            $result = $mapper->insert($post); // returns an id
        } catch (Exception $e) {
            $result = false;
        }

        $this->assertNotFalse($result);
    }

    /**
     * @depends testSampleNewsInsert
     */
    public function testSelect(): void {

        $mapper = testOrmMapper(Post::class);
        $post = $mapper->first(['title' => 'Test Post']);

        $this->assertInstanceOf(Post::class, $post);
    }

    public function testInsertThenSelectReturnsProperTypes(): void {

        // Insert Post into database
        $mapper = testOrmMapper(Post::class);
        $post = $mapper->get();

        $post->id = Uuid::uuid1();
        $post->title = 'Types Test';
        $post->body = "<p>This is a really awesome super-duper post.</p><p>It's really quite lovely.</p>";
        $post->status = 1;
        $post->date_created = new DateTime();
        $post->author_id = static::$authorId;
        $result = $mapper->insert($post); // returns an id

        // Read Post from database
        $post = $mapper->get($result);

        // Strict equality
        $this->assertTrue($post->status);
        $postData = $post->data();
        $this->assertTrue($postData['status']);
    }

    /**
     * @depends testSampleNewsInsert
     */
    public function testSampleNewsUpdate(): void {

        $mapper = testOrmMapper(Post::class);

        $post = $mapper->first(['title' => 'Test Post']);

        $this->assertInstanceOf(Post::class, $post);

        $post->title = 'Test Post Modified';
        $mapper->update($post);

        $updatedPost = $mapper->first(['title' => 'Test Post Modified']);
        $this->assertInstanceOf(Post::class, $updatedPost);
    }

    /**
     * @depends testSampleNewsUpdate
     */
    public function testSampleNewsDelete(): void {

        $mapper = testOrmMapper(Post::class);

        $post = $mapper->first(['title' => 'Test Post Modified']);

        $result = $mapper->delete($post);

        $this->assertTrue((boolean) $result);
    }

    public function testMultipleConditionDelete(): void {

        $mapper = testOrmMapper(Post::class);

        for ($i = 1; $i <= 10; $i++) {

            $mapper->insert([
                'id'           => Uuid::uuid1(),
                'title'        => (($i % 2) ? 'odd' : 'even') . '_title',
                'author_id'    => static::$authorId,
                'body'         => '<p>' . $i . '_body</p>',
                'status'       => 1,
                'date_created' => new DateTime()
            ]);

        }

        $result = $mapper->delete(['body !=' => ['<p>3_body</p>', '<p>4_body</p>', '<p>5_body</p>'], 'title' => 'odd_title']);
        $this->assertTrue((boolean) $result);
        $this->assertEquals(3, $result);
    }

    public function testPostTagUpsert(): void {

        $tagMapper = testOrmMapper(Tag::class);

        $tagId = Uuid::uuid1();

        $tag = $tagMapper->build([
            'id'   => $tagId,
            'name' => 'Example Tag'
        ]);

        $result = $tagMapper->insert($tag);

        if ( ! $result) {
            throw new RuntimeException('Unable to create tag: ' . var_export($tag->data(), true));
        }

        $postMapper = testOrmMapper(Post::class);

        $postId = Uuid::uuid1();

        $post = $postMapper->build([
            'id'           => $postId,
            'title'        => 'Example Title',
            'author_id'    => static::$authorId,
            'body'         => '<p>body</p>',
            'status'       => 0,
            'date_created' => new DateTime()
        ]);
        $result = $postMapper->insert($post);

        if ( ! $result) {
            throw new RuntimeException('Unable to create post: ' . var_export($post->data(), true));
        }

        $postTagMapper = testOrmMapper(PostTag::class);
        $data = [
            'id'  => Uuid::uuid1(),
            'tag_id'  => $tagId,
            'post_id' => $postId
        ];
        $where = [
            'tag_id' => $tagId
        ];

        // Post tags has unique constraint on tag+post, so insert will fail the second time
        $result = $postTagMapper->upsert($data, $where);
        $result2 = $postTagMapper->upsert(array_merge($data, ['random' => 'blah blah']), $where);
        $postTag = $postTagMapper->first($where);

        $this->assertTrue((boolean) $result);
        $this->assertTrue((boolean) $result2);
        $this->assertSame('blah blah', $postTag->random);
    }

    public function testUniqueConstraintUpsert(): void {

        $mapper = testOrmMapper(Setting::class);

        $data = [
            'id' => Uuid::uuid1(),
            'skey'   => 'my_setting',
            'svalue' => 'abc123'
        ];

        // Post tags has unique constraint on tag+post, so insert will fail the second time
        $result = $mapper->upsert($data, ['skey' => 'my_setting']);

        $result2 = $mapper->upsert(['svalue' => 'abcdef123456'],  ['skey' => 'my_setting']);

        $entity = $mapper->first(['skey' => 'my_setting']);

        $this->assertTrue((boolean) $result);
        $this->assertTrue((boolean) $result2);
        $this->assertSame('abcdef123456', $entity->svalue);
    }

    /**
     * @throws OrmException
     * @throws Throwable
     *
     * @depends testPostTagUpsert
     */
    public function testTruncate(): void {
        $postTagMapper = testOrmMapper(PostTag::class);
        $result = $postTagMapper->truncateTable();

        $this->assertNull($result);
    }

    /**
     * @depends testPostTagUpsert
     */
    public function testDeleteAll(): void {
        $postTagMapper = testOrmMapper(PostTag::class);
        $result = $postTagMapper->delete();
        $testResult = false;

        if($result === 0 || $result === 1) {
           $testResult = true;
        }

        $this->assertTrue($testResult);
    }

    public function testStrictInsert(): void {

        $this->expectException(OrmException::class);

        $postMapper = testOrmMapper(Post::class);
        $postMapper->insert([
            'id'               => Uuid::uuid1(),
            'title'            => 'irrelevant_title',
            'author_id'        => static::$authorId,
            'body'             => '<p>test_body</p>',
            'status'           => 1,
            'date_created'     => new DateTime(),
            'additional_field' => 'Should cause an error'
        ]);
    }

    public function testNonStrictInsert(): void {
        $postMapper = testOrmMapper(Post::class);
        $result = $postMapper->insert([
            'id'               => Uuid::uuid1(),
            'title'            => 'irrelevant_title',
            'author_id'        => static::$authorId,
            'body'             => '<p>test_body</p>',
            'status'           => 10,
            'date_created'     => new DateTime(),
            'additional_field' => 'Should cause an error'
        ], ['strict' => false]);

        $this->assertTrue((boolean) $result);
    }

    public function testStrictUpdate(): void {

        $this->expectException(OrmException::class);

        $postMapper = testOrmMapper(Post::class);
        $post = $postMapper->create([
            'id'           => Uuid::uuid1(),
            'title'        => 'irrelevant_title',
            'author_id'    => static::$authorId,
            'body'         => '<p>test_body</p>',
            'status'       => 10,
            'date_created' => new DateTime()
        ]);

        $post->additional_field = 'Should cause an error';
        $postMapper->update($post);
    }

    public function testNonStrictUpdate(): void {

        $postMapper = testOrmMapper(Post::class);
        $post = $postMapper->create([
            'id'           => Uuid::uuid1(),
            'title'        => 'irrelevant_title',
            'author_id'    => static::$authorId,
            'body'         => '<p>test_body</p>',
            'status'       => 1,
            'date_created' => new DateTime()
        ]);

        $post->status = 0;
        $post->additional_field = 'Should cause an error';

        $result = $postMapper->update($post, ['strict' => false]);

        $this->assertTrue(filter_var($result, FILTER_VALIDATE_BOOLEAN));
        $this->assertFalse($post->isModified());
    }

    public function testStrictSave(): void {

        $this->expectException(OrmException::class);

        $postMapper = testOrmMapper(Post::class);
        $post = $postMapper->build([
            'id'               => Uuid::uuid1(),
            'title'            => 'irrelevant_title',
            'author_id'        => static::$authorId,
            'body'             => '<p>test_body</p>',
            'status'           => 0,
            'date_created'     => new DateTime(),
            'additional_field' => 'Should cause an error'
        ]);

        $postMapper->save($post);
    }

    public function testNonStrictSave(): void {

        $postMapper = testOrmMapper(Post::class);
        $post = $postMapper->build([
            'id'           => Uuid::uuid1(),
            'title'            => 'irrelevant_title',
            'author_id'        => static::$authorId,
            'body'             => '<p>test_body</p>',
            'status'           => 10,
            'date_created'     => new DateTime(),
            'additional_field' => 'Should cause an error'
        ]);

        $result = $postMapper->save($post, ['strict' => false]);

        $this->assertTrue((boolean) $result);
    }

    public function testHasOneNewEntitySaveRelation(): void {

        $eventMapper = testOrmMapper(Event::class);
        $searchMapper = testOrmMapper(Search::class);

        $search = new Search([
            'id' => Uuid::uuid1(),
            'body' => 'Some body content'
        ]);

        $event = $eventMapper->build([
            'id'           => Uuid::uuid1(),
            'title'       => 'Test',
            'description' => 'Test description',
            'type'        => 'free',
            'token'       => 'some-token',
            'date_start'  => new DateTime
        ]);

        $event->relation('search', $search);

        $eventMapper->save($event, ['relations' => true]);

        $this->assertEquals($event->id->toString(), $search->event_id->toString());

        $this->assertEquals($event->search->id->toString(), $search->id->toString());

        //Check that old related entity gets deleted when updating relationship
        $search2 = new Search([
            'id' => Uuid::uuid1(),
            'body' => 'body2'
        ]);

        $event->relation('search', $search2);
        $eventMapper->save($event, ['relations' => true]);

        $result = $searchMapper->where(['event_id' => $event->id]);

        $this->assertEquals(count($result), 1);
        $this->assertEquals($result->first()->get('body'), 'body2');
    }

    public function testHasOneRelatedEntityAlreadyExists(): void {

        $eventMapper = testOrmMapper(Event::class);

        $searchMapper = testOrmMapper(Search::class);

        $eventId1 = Uuid::uuid1();
        $event1 = $eventMapper->build([
            'id'          => $eventId1,
            'title'       => 'Test',
            'description' => 'Test description',
            'type'        => 'free',
            'date_start'  => new DateTime()
        ]);

        $eventMapper->insert($event1);

        $eventId2 = Uuid::uuid1();
        $event2 = $eventMapper->build([
            'id'          => $eventId2,
            'title'       => 'Test',
            'description' => 'Test description',
            'type'        => 'free',
            'date_start'  => new DateTime()
        ]);

        $eventMapper->insert($event2);

        $search2 = new Search([
            'id'       => Uuid::uuid1(),
            'event_id' => $eventId2,
            'body'     => 'body2'
        ]);

        $searchMapper->save($search2);

        $savedEvent = $eventMapper->get($event1->primaryKey());

        $savedEvent->relation('search', $search2);

        $eventMapper->save($savedEvent, ['relations' => true]);

        $savedEvent = $eventMapper->get($savedEvent->primaryKey());

        $this->assertEquals($savedEvent->search->id->toString(), $search2->id->toString());

        $this->assertEquals($savedEvent->search->event_id->toString(), $search2->event_id->toString());
        $this->assertEquals($savedEvent->id->toString(), $search2->event_id->toString());
        $this->assertEquals($savedEvent->search->body, $search2->body);
    }

    public function testHasOneIgnoreRelationNotLoaded(): void {

        $eventMapper = testOrmMapper(Event::class);

        $searchMapper = testOrmMapper(Search::class);

        $event = $eventMapper->build([
            'id'          => Uuid::uuid1(),
            'title'       => 'Test',
            'description' => 'Test description',
            'type'        => 'free',
            'token'       => 'some-token',
            'date_start'  => new DateTime
        ]);
        $eventMapper->save($event);

        $searchMapper->delete(['event_id' => $event->id]);

        $savedEvent = $eventMapper->get($event->primaryKey());

        $savedEvent->set('title', 'Test 2');

        $this->assertEquals($eventMapper->save($savedEvent, ['relations' => true]), 1);
    }

    public function testBelongsToNewEntitySaveRelation(): void {

        $mapper = testOrmMapper(Post::class);

        $author = new Author([
            'id' => Uuid::uuid1(),
            'email' => 'test@example.com',
            'password' => '123456'
        ]);

        $post = $mapper->build([
            'id'    => Uuid::uuid1(),
            'title' => 'Test',
            'body'  => 'Test description',
        ]);

        $post->relation('author', $author);
        $mapper->save($post, ['relations' => true]);

        $this->assertEquals($post->author_id, $author->id);
        $this->assertFalse($post->isNew());
        $this->assertFalse($author->isNew());

        $author2 = new Author([
            'id' => Uuid::uuid1(),
            'email' => 'test2@example.com',
            'password' => '123456789'
        ]);

        $post->relation('author', $author2);
        $mapper->save($post, ['relations' => true]);

        $this->assertEquals($post->author_id, $author2->id);
    }

    public function testHasManyNewEntitySaveRelation(): void {

        $postMapper = testOrmMapper(Post::class);

        $commentMapper = testOrmMapper(Comment::class);

        $comments = [];
        for ($i = 1; $i < 3; $i++) {
            $comments[] = new Comment([
                'id'    => Uuid::uuid1(),
                'name'  => 'John Doe',
                'email' => 'test@example.com',
                'body'  => '#' . $i . ': Lorem ipsum is dolor.',
            ]);
        }

        $post = $postMapper->build([
            'id'        => Uuid::uuid1(),
            'title'     => 'Test',
            'body'      => 'Test description',
            'author_id' => static::$authorId
        ]);

        $post->relation('comments', new Collection($comments));

        $postMapper->save($post, ['relations' => true]);

        $this->assertFalse($post->isNew());

        foreach ($post->comments as $comment) {
            $this->assertFalse($comment->isNew());
            $this->assertEquals($comment->post_id->toString(), $post->id->toString());
        }

        // Test comment deleted from DB when removed from relation
        $removedComment = array_shift($comments);

        $post->relation('comments', new Collection($comments));

        $postMapper->save($post, ['relations' => true]);

        $this->assertEquals($commentMapper->get($removedComment->primaryKey()), false);
        $this->assertEquals($commentMapper->where(['post_id' => $post->id])->count(), 1);

        // Test all comments removed when relation set to false
        $post->relation('comments', false);
        $postMapper->save($post, ['relations' => true]);

        foreach ($comments as $comment) {
            $this->assertEquals($commentMapper->get($comment->primaryKey()), false);
        }

    }

    public function testHasManyExistingEntitySaveRelation(): void {

        $mapper = testOrmMapper(Post::class);
        $postId = Uuid::uuid1();
        $data = [
            'id'        => Uuid::uuid1(),
            'title'     => 'Test',
            'body'      => 'Test description',
            'author_id' => static::$authorId
        ];
        $mapper->save($mapper->build(array_merge($data, ['id' => $postId])));

        $commentMapper = testOrmMapper(Comment::class);
        $comments = [];
        for ($i = 1; $i < 3; $i++) {
            $comment = new Comment([
                'id'      => Uuid::uuid1(),
                'name'    => 'John Doe',
                'email'   => 'test@example.com',
                'post_id' => $postId,
                'body'    => '#' . $i . ': Lorem ipsum is dolor.',
            ]);
            $commentMapper->insert($comment);
            $comments[] = $comment;
        }
        $post = $mapper->build($data);
        $post->relation('comments', new Collection($comments));
        $mapper->save($post, ['relations' => true]);

        $post = $mapper->get($post->primaryKey());

        $this->assertSame(count($post->comments), 2);
    }

    public function testHasManyThroughRelationSave(): void {

        $postMapper = testOrmMapper(Post::class);
        $postTagMapper = testOrmMapper(PostTag::class);

        $tags = [];
        for ($i = 1; $i < 3; $i++) {
            $tags[] = new Tag([
                'id'   => Uuid::uuid1(),
                'name' => 'Tag #' . $i
            ]);
        }

        $post = $postMapper->build([
            'id'      => Uuid::uuid1(),
            'title'     => 'Test',
            'body'      => 'Test description',
            'author_id' => static::$authorId
        ]);
        $post->relation('tags', new Collection($tags));
        $postMapper->save($post, ['relations' => true]);

        $this->assertFalse($post->isNew());
        $this->assertEquals($postTagMapper->all()->count(), 2);

        $i = 1;
        foreach ($post->tags as $tag) {
            $this->assertFalse($tag->isNew());
            $this->assertEquals($tag->name, 'Tag #' . $i);
            $i++;
        }

        // Test comment deleted from DB when removed from relation
        $removedTag = array_shift($tags);

        $post->relation('tags', new Collection($tags));
        $postMapper->save($post, ['relations' => true]);

        $this->assertEquals($postTagMapper->where(['tag_id' => $removedTag->primaryKey()])->count(), 0);

        // Test all comments removed when relation set to false
        $post->relation('tags', false);
        $postMapper->save($post, ['relations' => true]);

        $this->assertEquals($postTagMapper->all()->count(), 0);
    }

    /**
     * @depends testSampleNewsInsert
     */
    public function testQueryWithDateTimeObjectValue(): void {

        $mapper = testOrmMapper(Post::class);

        $results = $mapper->where(['date_created <=' => new DateTime()])->toArray();

        $this->assertTrue(count($results) > 0);
    }
}
