<?php declare(strict_types=1);

namespace Ether\Tests\Integration\Orm\Uuid\Binary;

use DateTime;
use RuntimeException;
use Ramsey\Uuid\Uuid;
use PHPUnit\Framework\TestCase;
use Ether\Tests\Integration\Orm\Uuid\Binary\Entity\Post;
use Ether\Tests\Integration\Orm\Uuid\Binary\Entity\Author;
use Ether\Tests\Integration\Orm\Uuid\Binary\Entity\CustomMethods;

class EntityTest extends TestCase {

    private static $entities = [
        'Post',
        'Author',
        'CustomMethods'
    ];

    /** @var Uuid */
    private static $authorId;

    public static function setupBeforeClass(): void {

        foreach (self::$entities as $entity) {
            testOrmMapper('Ether\Tests\Integration\Orm\Uuid\Binary\Entity\\' . $entity)->migrate();
        }

        $authorMapper = testOrmMapper(Author::class);

        static::$authorId = Uuid::uuid1();

        $author = $authorMapper->build([
            'id'       => static::$authorId,
            'email'    => 'example@example.com',
            'password' => 't00r',
            'is_admin' => false
        ]);

        $result = $authorMapper->insert($author);

        if ( ! $result) {
            throw new RuntimeException('Unable to create author: ' . var_export($author->data(), true));
        }
    }

    public static function tearDownAfterClass(): void {
        foreach (self::$entities as $entity) {
            testOrmMapper('Ether\Tests\Integration\Orm\Uuid\Binary\Entity\\' . $entity)->dropTable();
        }
    }

    public function testEntitySetDataProperties(): void {

        $mapper = testOrmMapper(Post::class);

        $post = $mapper->get();

        $date = new DateTime();

        // Set data
        $post->title = 'My Awesome Post';
        $post->body = '<p>Body</p>';
        $post->author_id = static::$authorId;
        $post->date_created = $date;

        $data = $post->data();

        ksort($data);

        $testData = [
            'id'           => null,
            'title'        => 'My Awesome Post',
            'body'         => '<p>Body</p>',
            'status'       => false,
            'date_created' => $date,
            'data'         => null,
            'author_id'    => static::$authorId
        ];

        ksort($testData);

        $this->assertEquals($testData, $data);

        $this->assertNull($post->asdf);
    }

    public function testEntitySetDataConstruct(): void {

        testOrmMapper(Post::class);

        $date = new DateTime();

        $post = new Post([
            'title'        => 'My Awesome Post',
            'body'         => '<p>Body</p>',
            'author_id'    => static::$authorId,
            'date_created' => $date
        ]);

        $data = $post->data();
        ksort($data);

        $testData = [
            'id'           => null,
            'title'        => 'My Awesome Post',
            'body'         => '<p>Body</p>',
            'status'       => 0,
            'date_created' => null,
            'data'         => null,
            'author_id'    => static::$authorId,
            'date_created' => $date
        ];

        ksort($testData);

        $this->assertEquals($testData, $data);
    }

    public function testEntityErrors(): void {

        $post = new Post([
            'title' => 'My Awesome Post',
            'body'  => '<p>Body</p>'
        ]);

        $postErrors = [
            'title' => ['Title cannot contain the word awesome']
        ];

        // Has NO errors
        $this->assertNotTrue($post->hasErrors());

        // Set errors
        $post->errors($postErrors);

        // Has errors
        $this->assertTrue($post->hasErrors());

        // Full error array
        $this->assertEquals($postErrors, $post->errors());

        // Errors for one key only
        $this->assertEquals($postErrors['title'], $post->errors('title'));
    }

    public function testDataModified(): void {

        $data = [
            'title' => 'My Awesome Post 2',
            'body'  => '<p>Body 2</p>'
        ];

        $testData = [
            'id'           => null,
            'title'        => 'My Awesome Post',
            'body'         => '<p>Body</p>',
            'status'       => 0,
            'date_created' => null,
            'data'         => null,
            'author_id'    => static::$authorId
        ];

        // Set initial data
        $post = new Post($testData);

        $this->assertEquals($testData, $post->dataUnmodified());
        $this->assertEquals([], $post->dataModified());
        $this->assertFalse($post->isModified());

        $post->data($data);
        $this->assertEquals($data, $post->dataModified());
        $this->assertTrue($post->isModified('title'));
        $this->assertFalse($post->isModified('id'));
        $this->assertNull($post->isModified('asdf'));
        $this->assertTrue($post->isModified());
        $this->assertEquals($data['title'], $post->dataModified('title'));
        $this->assertEquals($testData['title'], $post->dataUnmodified('title'));
        $this->assertNull($post->dataModified('id'));
        $this->assertNull($post->dataModified('status'));
    }

    public function testDataNulls(): void {

        $data = [
            'title'     => 'A Post',
            'body'      => 'A Body',
            'status'    => 0,
            'author_id' => static::$authorId,
        ];

        $post = new Post($data);

        $post->status = null;
        $this->assertTrue($post->isModified('status'));

        $post->status = 1;
        $this->assertTrue($post->isModified('status'));

        $post->data(['status' => null]);
        $this->assertTrue($post->isModified('status'));

        $post->title = '';
        $this->assertTrue($post->isModified('title'));

        $this->title = null;
        $this->assertTrue($post->isModified('title'));

        $this->title = 'A Post';
        $post->data(['title' => null]);
        $this->assertTrue($post->isModified('title'));
    }

    public function testJsonArray(): void {

        $postId = Uuid::uuid1();

        $data = [
            'id'           => $postId,
            'title'        => 'A Post',
            'body'         => 'A Body',
            'status'       => 0,
            'author_id'    => static::$authorId,
            'data'         => ['posts' => 'are cool', 'another field' => 'to serialize'],
            'date_created' => new DateTime()
        ];

        $post = new Post($data);

        $this->assertEquals($post->data, ['posts' => 'are cool', 'another field' => 'to serialize']);

        $mapper = testOrmMapper(Post::class);
        $mapper->save($post);

        $post = $mapper->get($post->id);

        $this->assertEquals($post->data, ['posts' => 'are cool', 'another field' => 'to serialize']);

        $post->data = ['updated' => true];
        $this->assertEquals($post->data, ['updated' => true]);

        $mapper->update($post);
        $post = $mapper->get($post->id);

        $this->assertEquals($post->data, ['updated' => true]);
    }

    public function testDataReferences(): void {

        $id = Uuid::uuid1();

        $data = [
            'id'           => $id,
            'title'        => 'A Post',
            'body'         => 'A Body',
            'status'       => 0,
            'data'         => ['posts' => 'are cool', 'another field' => 'to serialize'],
            'date_created' => new DateTime()
        ];

        $post = new Post($data);

        // Reference test
        $title = $post->title;
        $this->assertEquals($title, $post->title);
        $title = 'asdf';
        $this->assertEquals('A Post', $post->title);
        $this->assertEquals('asdf', $title);

        // Property setting
        $post->date_created = null;
        $this->assertNull($post->date_created);

        $post->data['posts'] = 'are really cool';
        $this->assertEquals($post->data, ['posts' => 'are really cool', 'another field' => 'to serialize']);

        $data =& $post->data;
        $data['posts'] = 'are still cool';
        $this->assertEquals($post->data, ['posts' => 'are still cool', 'another field' => 'to serialize']);
    }

    public function testLocalVariablesAreNotByReference(): void {

        $id = Uuid::uuid1();

        $data = [
            'id'           => $id,
            'title'        => 'A Post',
            'body'         => 'A Body',
            'status'       => 0,
            'data'         => ['posts' => 'are cool', 'another field' => 'to serialize'],
            'date_created' => new DateTime()
        ];

        $post = new Post($data);

        $title = $post->title;
        /** @noinspection SuspiciousAssignmentsInspection */
        $title = 'A Post Title';

        $this->assertNotEquals($title, $post->title);
    }

    public function testLocalArrayVariablesAreNotByReference(): void {

        $id = Uuid::uuid1();

        $data = [
            'id'           => $id,
            'title'        => 'A Post',
            'body'         => 'A Body',
            'status'       => 0,
            'data'         => ['posts' => 'are cool', 'another field' => 'to serialize'],
            'date_created' => new DateTime()
        ];

        $post = new Post($data);

        $data = $post->data;
        $data['posts'] = 'are not by reference';

        $this->assertNotEquals($data, $post->data);
    }

    public function testJsonEncodeJsonSerializable(): void {

        $id = Uuid::uuid1();

        $post = new Post([
            'id'           => $id,
            'title'        => 'A Post',
            'body'         => 'A Body',
            'status'       => 0,
            'data'         => ['posts' => 'are cool', 'another field' => 'to serialize'],
            'date_created' => new DateTime()
        ]);
        $json = json_encode($post);
        $data = json_decode($json, true);

        $this->assertEquals('A Post', $data['title']);
    }

    public function testToStringReturnsJson(): void {

        $id = Uuid::uuid1();

        $post = new Post([
            'id'           => $id,
            'title'        => 'A Post',
            'body'         => 'A Body',
            'status'       => 0,
            'data'         => ['posts' => 'are cool', 'another field' => 'to serialize'],
            'date_created' => new DateTime()
        ]);
        $json = (string) $post;
        $data = json_decode($json, true);

        $this->assertEquals('A Post', $data['title']);
    }

    public function testEntityValueMutator(): void {

        $entity = new CustomMethods();

        $entity->test1 = 'test';

        $this->assertEquals('test_has_been_mutated_by_mutator', $entity->test1);
    }

    public function testEntityValueAccessor(): void {

        $entity = new CustomMethods();

        $entity->updated = 'updated';

        $this->assertEquals('updated_has_been_mutated_by_accessor', $entity->updated);
    }

    public function testEntityValueMutatorWithArrayLoad(): void {

        $entity = new CustomMethods(['updated2' => 'to_be_mutated']);

        $this->assertEquals('to_be_mutated_has_been_mutated_by_mutator', $entity->updated2);
    }

    public function testEntityValueAccessorWithArrayData(): void {

        $entity = new CustomMethods(['test1' => 'test']);

        $data = $entity->data();

        $this->assertEquals('test_has_been_mutated_by_mutator', $data['test1']);
    }

    public function testEntityValueMutatorShouldNotTriggerModified(): void {

        $id = Uuid::uuid1();

        $mapper = testOrmMapper(CustomMethods::class);

        $entity = new CustomMethods([
            'id'       => $id,
            'test1'    => 'test',
            'updated2' => 'copy'
        ]);

        $mapper->save($entity);

        unset($entity);

        $entity = $mapper->get($id);

        $this->assertFalse($entity->isModified('test1'));
        $this->assertFalse($entity->isModified('test2'));
        $this->assertFalse($entity->isModified('test3'));
        $this->assertFalse($entity->isModified());
    }

    public function testGetPrimaryKeyField(): void {
        $entity = new CustomMethods([
            'test1' => 'test'
        ]);
        $this->assertEquals('id', $entity->primaryKeyField());
    }

    public function testGetPrimaryKeyFieldValue(): void {

        $entity = new CustomMethods([
            'test1' => 'test'
        ]);

        $this->assertEquals($entity->id, $entity->primaryKey());
    }

}
