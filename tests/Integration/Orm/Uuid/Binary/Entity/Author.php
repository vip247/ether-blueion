<?php declare(strict_types=1);

namespace Ether\Tests\Integration\Orm\Uuid\Binary\Entity;

use DateTime;
use Ether\Database\Orm\Entity;

class Author extends Entity {

    protected static $table = 'test_authors_binary_uuid';

    public static function fields(): array {
        return [
            'id'           => ['type' => 'uuid_binary_ordered_time', 'primary' => true],
            'email'        => [
                'type'       => 'string', 'required' => true, 'unique' => true,
                'validation' => [
                    'email', 'length' => [4, 255]
                ]
            ], // Unique
            'password'     => ['type' => 'text', 'required' => true],
            'is_admin'     => ['type' => 'boolean', 'value' => false],
            'date_created' => ['type' => 'datetime', 'value' => new DateTime()]
        ];
    }
}
