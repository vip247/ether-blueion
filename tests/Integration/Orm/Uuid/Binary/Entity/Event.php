<?php declare(strict_types=1);

namespace Ether\Tests\Integration\Orm\Uuid\Binary\Entity;

use DateTime;
use Ramsey\Uuid\Uuid;
use Ether\Database\Orm\Entity;
use Ether\Database\Orm\Exception;
use Ether\Database\Orm\EventEmitter;
use Ether\Database\Orm\MapperInterface;
use Ether\Database\Orm\EntityInterface;
use Ether\Tests\Integration\Orm\Uuid\Binary\Entity\Event\Search;
use Ether\Tests\Integration\Orm\Common\Mapper\Event as EventMapper;

class Event extends Entity {

    protected static $table = 'test_events_binary_uuid';

    // Use a custom mapper for this object
    protected static $mapper = EventMapper::class;

    public static function fields(): array {
        return [
            'id'           => ['type' => 'uuid_binary_ordered_time', 'primary' => true],
            'title'        => ['type' => 'string', 'required' => true],
            'description'  => ['type' => 'text', 'required' => true],
            'type'         => [
                'type' => 'string', 'required' => true, 'options' => [
                    'free', // 'Free',
                    'private', // 'Private (Ticket Required)'
                    'vip' // 'VIPs only'
                ]
            ],
            'token'        => [
                'type' => 'string',
                'required' => true
            ],
            'date_start'   => [
                'type' => 'datetime', 'required' => true, 'validation' => [
                    'dateAfter' => new DateTime('-1 second')
                ]
            ],
            'status'       => [
                'type' => 'string', 'default' => 1, 'options' => [
                    0, // 'Inactive'
                    1, // 'Active
                ]
            ],
            'date_created' => [
                'type' => 'datetime',
                'value' => new DateTime()
            ]
        ];
    }

    public static function relations(MapperInterface $mapper, EntityInterface $entity): array {
        return [
            'search'               => $mapper->hasOne($entity, Search::class, 'event_id'),
            'polymorphic_comments' => $mapper->hasMany($entity, PolymorphicComment::class, 'item_id')->where(['item_type' => 'event'])
        ];
    }

    /**
     * @param EventEmitter $eventEmitter
     */
    public static function events(EventEmitter $eventEmitter): void {

        $eventEmitter->on('beforeInsert', static function ($entity, $mapper) {
            $entity->token = uniqid('', true);
        });

        $eventEmitter->on('afterInsert', static function ($entity, $mapper) {

            $mapper = testOrmMapper(Search::class);

            $result = $mapper->create([
                'id'       => Uuid::uuid1(),
                'event_id' => $entity->id,
                'body'     => 'Added with afterInsert event listener'
            ]);

            if ( ! $result) {
                throw new Exception('Event search index entity failed to save!');
            }
        });

    }
}
