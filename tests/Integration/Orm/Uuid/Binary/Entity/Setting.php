<?php declare(strict_types=1);

namespace Ether\Tests\Integration\Orm\Uuid\Binary\Entity;

use Ether\Database\Orm\Entity;
use Ether\Tests\Integration\Orm\Type\Encrypted;

class Setting extends Entity {

    protected static $table = 'test_settings_binary_uuid';

    public static function fields(): array {
        return [
            'id'     => ['type' => 'uuid_binary_ordered_time', 'primary' => true],
            'skey'   => ['type' => 'string', 'required' => true, 'unique' => true],
            'svalue' => ['type' => 'encrypted', 'required' => true]
        ];
    }
}
