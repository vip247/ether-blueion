<?php declare(strict_types=1);

namespace Ether\Tests\Integration\Orm\Common\Mapper;

use Ether\Database\Orm\Query;
use Ether\Database\Orm\Mapper;
use Doctrine\DBAL\DBALException;
use Ether\Database\Orm\Exception;

class Event extends Mapper {

    public function scopes(): array {
        return [
            'free'   => static function (Query $query) {
                return $query->where(['type' => 'free']);
            },
            'active' => static function (Query $query) {
                return $query->where(['status' => 1]);
            }
        ];
    }

    /**
     *
     * Just generate a test query so we can ensure this method is getting called
     *
     * @return Query
     *
     * @throws DBALException
     * @throws Exception
     */
    public function testQuery(): Query {
        return $this->where(['title' => 'test']);
    }
}
