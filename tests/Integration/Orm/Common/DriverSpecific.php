<?php declare(strict_types=1);

namespace Ether\Tests\Integration\Orm\Common;

class DriverSpecific {

    public static function getWeekFunction($mapper, $field = null) {

        if ($mapper->connectionIs('mysql')) {

            return "WEEK($field)";

        } else if ($mapper->connectionIs('pgsql')) {

            return "EXTRACT(WEEK FROM TIMESTAMP $field)";

        } else if ($mapper->connectionIs('sqlite')) {

            return "STRFTIME('%W', $field)";

        }

        return false;
    }
}
