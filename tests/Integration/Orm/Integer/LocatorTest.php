<?php declare(strict_types=1);

namespace Ether\Tests\Integration\Orm\Integer;

use Ether\Database\Orm\Config;
use PHPUnit\Framework\TestCase;
use Ether\Database\Orm\Locator;
use Ether\Database\Orm\Mapper as OrmMapper;
use Ether\Tests\Integration\Orm\Integer\Entity\Post;

class LocatorTest extends TestCase {

    public function testGetConfig(): void {

        $cfg = new Config();
        $spot = new Locator($cfg);

        $this->assertInstanceOf(Config::class, $spot->config());
    }

    public function testGetMapper(): void {
        $cfg = new Config();
        $orm = new Locator($cfg);

        $post = $orm->mapper(Post::class);

        /** @noinspection UnnecessaryAssertionInspection */
        $this->assertInstanceOf(OrmMapper::class, $post);
    }
}
