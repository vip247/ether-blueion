<?php /** @noinspection PhpUndefinedFieldInspection */
declare(strict_types=1);

namespace Ether\Tests\Integration\Orm\Integer;

use DateTime;
use RuntimeException;
use PHPUnit\Framework\TestCase;
use Doctrine\DBAL\DBALException;
use Ether\Database\Orm\Exception;
use Doctrine\DBAL\ConnectionException;
use Ether\Database\Orm\MapperInterface;
use Ether\Tests\Integration\Orm\Integer\Entity\Post;
use Ether\Tests\Integration\Orm\Integer\Entity\Author;

class TransactionsTest extends TestCase {

    /**
     * @var array
     */
    private static $entities = ['Post', 'Author'];


    public static function setupBeforeClass(): void {
        foreach (self::$entities as $entity) {
            testOrmMapper('Ether\Tests\Integration\Orm\Integer\Entity\\' . $entity)->migrate();
        }

        $authorMapper = testOrmMapper(Author::class);
        $author = $authorMapper->build([
            'id'       => 1,
            'email'    => 'example@example.com',
            'password' => 't00r',
            'is_admin' => false
        ]);
        $result = $authorMapper->insert($author);

        if ( ! $result) {
            throw new RuntimeException('Unable to create author: ' . var_export($author->data(), true));
        }
    }


    public static function tearDownAfterClass(): void {
        foreach (self::$entities as $entity) {
            testOrmMapper('Ether\Tests\Integration\Orm\Integer\Entity\\' . $entity)->dropTable();
        }
    }

    /**
     * @throws ConnectionException
     * @throws DBALException
     * @throws Exception
     */
    public function testInsertWithTransaction(): void {

        $post = new Post();
        $mapper = testOrmMapper(Post::class);
        $post->title = 'Test Post with Transaction';
        $post->body = '<p>This is a really awesome super-duper post -- in a TRANSACTION!.</p>';
        $post->date_created = new DateTime();
        $post->author_id = 1;

        $mapper->transaction(static function (MapperInterface $mapper) use ($post) {
            $mapper->insert($post);
        });

        $this->assertInstanceOf(Post::class, $mapper->first(['title' => $post->title]));
    }

    /**
     * @throws ConnectionException
     * @throws DBALException
     * @throws Exception
     */
    public function testInsertWithTransactionRollbackOnException(): void {

        $post = new Post();
        $mapper = testOrmMapper(Post::class);
        $post->title = 'Rolledback';
        $post->body = '<p>This is a really awesome super-duper post -- in a TRANSACTION!.</p>';
        $post->date_created = new DateTime();
        $post->author_id = 1;

        try {

            $mapper->transaction(static function (MapperInterface $mapper) use ($post) {

                $mapper->insert($post);

                // Trigger rollback
                throw new RuntimeException('Exceptions should trigger auto-rollback');
            });

        } catch (RuntimeException $e) {

            $this->assertFalse($mapper->first(['title' => $post->title]));
        }
    }

    /**
     * @throws ConnectionException
     * @throws DBALException
     * @throws Exception
     */
    public function testInsertWithTransactionRollbackOnReturnFalse(): void {

        $post = new Post();
        $mapper = testOrmMapper(Post::class);
        $post->title = 'Rolledback';
        $post->body = '<p>This is a really awesome super-duper post -- in a TRANSACTION!.</p>';
        $post->date_created = new DateTime();
        $post->author_id = 1;

        $mapper->transaction(static function (MapperInterface $mapper) use ($post) {
            $mapper->insert($post);

            // Trigger rollback
            return false;
        });

        $this->assertFalse($mapper->first(['title' => $post->title]));
    }
}
