<?php

/** @noinspection SqlResolve */

declare(strict_types=1);

namespace Ether\Tests\Integration\Orm\Integer;

use DateTime;
use Exception;
use RuntimeException;
use InvalidArgumentException;
use Ether\Database\Orm\Query;
use PHPUnit\Framework\TestCase;
use Doctrine\DBAL\DBALException;
use Doctrine\DBAL\Schema\Column;
use Doctrine\DBAL\Query\QueryBuilder;
use Ether\Database\Orm\Entity\Collection;
use Doctrine\DBAL\Platforms\MySqlPlatform;
use Ether\Tests\Integration\Orm\Integer\Entity\Tag;
use Doctrine\DBAL\Platforms\MySQL57Platform;
use Doctrine\DBAL\Platforms\MySQL80Platform;
use Ether\Tests\Integration\Orm\Integer\Entity\Post;
use Ether\Tests\Integration\Orm\Integer\Entity\Author;
use Ether\Tests\Integration\Orm\Common\DriverSpecific;
use Ether\Tests\Integration\Orm\Integer\Entity\PostTag;
use Ether\Database\Orm\Exception as OrmException;
use Ether\Tests\Integration\Orm\Integer\Entity\Post\Comment;

class QuerySqlTest extends TestCase {

    /** @var array */
    private static $entities = [
        'PostTag',
        'Post\Comment',
        'Post',
        'Tag',
        'Author'
    ];

    /**
     * @throws DBALException
     * @throws OrmException
     * @throws Exception
     */
    public static function setupBeforeClass(): void {

        foreach (self::$entities as $entity) {
            testOrmMapper('Ether\Tests\Integration\Orm\Integer\Entity\\' . $entity)->migrate();
        }

        // Insert blog dummy data
        $tags = [];
        for ($i = 1; $i <= 3; $i++) {
            $tags[] = testOrmMapper(Tag::class)->insert([
                'name' => "Title {$i}"
            ]);
        }

        for ($i = 1; $i <= 3; $i++) {
            testOrmMapper(Author::class)->insert([
                'email'    => $i . 'user@somewhere.com',
                'password' => 'securepassword'
            ]);
        }

        for ($i = 1; $i <= 10; $i++) {
            $post_id = testOrmMapper(Post::class)->insert([
                'title'        => (($i % 2) ? 'odd' : 'even') . '_title',
                'body'         => '<p>' . $i . '_body</p>',
                'status'       => $i,
                'date_created' => new DateTime(),
                'author_id'    => random_int(1, 3)
            ]);

            for ($j = 1; $j <= 2; $j++) {
                testOrmMapper(Comment::class)->insert([
                    'post_id' => $post_id,
                    'name'    => (($j % 2) ? 'odd' : 'even') . '_title',
                    'email'   => 'bob@somewhere.com',
                    'body'    => (($j % 2) ? 'odd' : 'even') . '_comment_body',
                ]);
            }

            foreach ($tags as $tag_id) {
                testOrmMapper(PostTag::class)->insert([
                    'post_id' => $post_id,
                    'tag_id'  => $tag_id
                ]);
            }
        }
    }

    /**
     * @throws OrmException
     * @throws DBALException
     */
    public static function tearDownAfterClass(): void {
        foreach (self::$entities as $entity) {
            testOrmMapper('Ether\Tests\Integration\Orm\Integer\Entity\\' . $entity)->dropTable();
        }
    }

    /**
     * @throws DBALException
     * @throws OrmException
     */
    public function testWhereArrayMultipleSeparatedByAnd(): void {
        $mapper = testOrmMapper(Post::class);
        $query = $mapper->select()->noQuote()->where(['status' => 2, 'title' => 'even_title']);

        $this->assertEquals('SELECT * FROM test_posts WHERE test_posts.status = ? AND test_posts.title = ?', $query->toSql());
    }

    /**
     * @throws DBALException
     * @throws OrmException
     */
    public function testInsertPostTagWithUniqueConstraint(): void {

        $tagMapper = testOrmMapper(Tag::class);

        $tag = $tagMapper->build([
            'id'   => 55,
            'name' => 'Example Tag'
        ]);
        $result = $tagMapper->insert($tag);

        if ( ! $result) {
            throw new RuntimeException('Unable to create tag: ' . var_export($tag->data(), true));
        }

        $postMapper = testOrmMapper(Post::class);
        $post = $postMapper->build([
            'id'           => 55,
            'title'        => 'Example Title',
            'author_id'    => 1,
            'body'         => '<p>body</p>',
            'status'       => 0,
            'date_created' => new DateTime()
        ]);
        $result = $postMapper->insert($post);

        if ( ! $result) {
            throw new RuntimeException('Unable to create post: ' . var_export($post->data(), true));
        }

        $mapper = testOrmMapper(PostTag::class);
        $posttag_id = $mapper->insert([
            'post_id' => 55,
            'tag_id'  => 55
        ]);

        $result1 = $mapper->delete(['id' => $posttag_id]);
        $result2 =  $postMapper->delete($post);
        $result3 = $tagMapper->delete($tag);

        $this->assertEquals([1, 1, 1], [$result1, $result2, $result3]);
    }

    /**
     * @throws DBALException
     * @throws OrmException
     */
    public function testQueryCollectionInstance(): void {

        $mapper = testOrmMapper(Post::class);
        $posts = $mapper->where(['title' => 'even_title']);

        $this->assertInstanceOf(Collection::class, $posts->execute());
    }

    // Bare (implicit equals)

    /**
     * @throws DBALException
     * @throws OrmException
     */
    public function testOperatorNone(): void {

        $mapper = testOrmMapper(Post::class);

        $query = $mapper->select()->noQuote()->where(['status' => 2]);

        $this->assertEquals('SELECT * FROM test_posts WHERE test_posts.status = ?', $query->toSql());

        $this->assertEquals(count($query), 1);
    }

    // Equals

    /**
     * @throws DBALException
     * @throws OrmException
     */
    public function testOperatorEq(): void {

        $mapper = testOrmMapper(Post::class);

        $query = $mapper->select()->noQuote()->where(['status :eq' => 2]);

        $this->assertEquals('SELECT * FROM test_posts WHERE test_posts.status = ?', $query->toSql());
        $this->assertEquals(count($query), 1);
    }

    // Less than

    /**
     * @throws DBALException
     * @throws OrmException
     */
    public function testOperatorLt(): void {

        $mapper = testOrmMapper(Post::class);

        $this->assertEquals(4, $mapper->where(['status <' => 5])->count());
        $this->assertEquals(4, $mapper->where(['status :lt' => 5])->count());
    }

    // Greater than

    /**
     * @throws DBALException
     * @throws OrmException
     */
    public function testOperatorGt(): void {

        $mapper = testOrmMapper(Post::class);

        $this->assertFalse($mapper->first(['status >' => 10]));
        $this->assertFalse($mapper->first(['status :gt' => 10]));
    }

    // Greater than or equal to

    /**
     * @throws DBALException
     * @throws OrmException
     */
    public function testOperatorGte(): void {

        $mapper = testOrmMapper(Post::class);

        $this->assertEquals(6, $mapper->where(['status >=' => 5])->count());
        $this->assertEquals(6, $mapper->where(['status :gte' => 5])->count());
    }

    /**
     * @throws DBALException
     * @throws OrmException
     */
    public function testOperatorRegexp(): void {

        $mapper = testOrmMapper(Post::class);

        // "REGEXP" only supported by MySQL
        if ( ! $mapper->connectionIs('mysql')) {
            $this->markTestSkipped();
        }

        $this->assertEquals(10, $mapper->where(['title :regex' => '_title$'])->count());
        $this->assertEquals(5, $mapper->where(['title :regex' => '^odd_'])->count());
    }

    /**
     * @throws DBALException
     * @throws OrmException
     */
    public function testOrderBy(): void {

        $mapper = testOrmMapper(Post::class);

        $query = $mapper->select()->noQuote()->where(['status' => 2])->order(['date_created' => 'ASC']);

        $this->assertStringContainsStringIgnoringCase('ORDER BY test_posts.date_created ASC', $query->toSql());
        $this->assertEquals(count($query), 1);
    }

    /**
     * @throws DBALException
     * @throws OrmException
     */
    public function testOrderByFunction(): void {
        $mapper = testOrmMapper(Post::class);

        $query = $mapper->select()->noQuote()->where(['status' => 2])->order(['TRIM(body)' => 'ASC']);

        $this->assertStringContainsStringIgnoringCase('ORDER BY TRIM(test_posts.body) ASC', $query->toSql());
        $this->assertEquals(count($query), 1);
    }

    /**
     * @throws DBALException
     * @throws OrmException
     */
    public function testOrderByComplexFunction(): void {

        $mapper = testOrmMapper(Post::class);

        if ( ! DriverSpecific::getWeekFunction($mapper)) {
            $this->markTestSkipped('This test is not supported with the current driver.');
        }

        $query = $mapper->select()->noQuote()->where(['status' => 2])->order([DriverSpecific::getWeekFunction($mapper, 'date_created') => 'ASC']);

        $this->assertStringContainsStringIgnoringCase('ORDER BY ' . DriverSpecific::getWeekFunction($mapper, 'test_posts.date_created') . ' ASC', $query->toSql());
        $this->assertEquals(count($query), 1);
    }

    /**
     * @throws DBALException
     * @throws OrmException
     */
    public function testGroupBy(): void {

        $mapper = testOrmMapper(Post::class);
        $query = $mapper->select()->noQuote()->where(['status' => 2])->group(['id']);

        $this->assertEquals('SELECT * FROM test_posts WHERE test_posts.status = ? GROUP BY test_posts.id', $query->toSql());
        $this->assertEquals(count($query), 1);
    }

    /**
     * @throws DBALException
     * @throws OrmException
     */
    public function testGroupByFunction(): void {

        $mapper = testOrmMapper(Post::class);

        $query = $mapper->select()->noQuote()->where(['status' => 2])->group(['TRIM(body)']);

        $this->assertEquals('SELECT * FROM test_posts WHERE test_posts.status = ? GROUP BY TRIM(test_posts.body)', $query->toSql());
        $this->assertEquals(count($query), 1);
    }

    /**
     * Test using same column name more than once
     *
     * @throws DBALException
     * @throws OrmException
     */
    public function testFieldMultipleUsage(): void {

        $mapper = testOrmMapper(Post::class);

        $countResult = $mapper->where(['status' => 1])
            ->orWhere(['status' => 2])
            ->count();

        $this->assertEquals(2, $countResult);
    }

    /**
     * @throws DBALException
     * @throws OrmException
     */
    public function testArrayDefaultIn(): void {

        $mapper = testOrmMapper(Post::class);

        $query = $mapper->select()->noQuote()->where(['status' => [2]]);
        $post = $query->first();

        $this->assertEquals('SELECT * FROM test_posts WHERE test_posts.status IN (?) LIMIT 1', $query->toSql());
        $this->assertEquals(2, $post->status);
    }

    /**
     * @throws DBALException
     * @throws OrmException
     */
    public function testArrayInEmpty(): void {

        $mapper = testOrmMapper(Post::class);

        $query = $mapper->where(['status' => []]);

        $this->assertStringContainsStringIgnoringCase('IS NULL', $query->toSql());
        $this->assertEquals(0, $query->count());
    }

    /**
     * @throws DBALException
     * @throws OrmException
     */
    public function testArrayInSingle(): void {

        $mapper = testOrmMapper(Post::class);

        // Numeric
        $query = $mapper->where(['status :in' => [2]]);

        $this->assertStringContainsStringIgnoringCase('IN', $query->toSql());
        $this->assertEquals(2, $query->first()->status);
    }

    /**
     * @throws DBALException
     * @throws OrmException
     */
    public function testArrayNotInEmpty(): void {

        $mapper = testOrmMapper(Post::class);

        $query = $mapper->where(['status !=' => []]);

        $this->assertStringContainsStringIgnoringCase('IS NOT NULL', $query->toSql());
        $this->assertEquals(10, $query->count());
    }

    /**
     * @throws DBALException
     * @throws OrmException
     */
    public function testArrayNotInSingle(): void {

        $mapper = testOrmMapper(Post::class);

        $post = $mapper->first(['status !=' => [2]]);

        $this->assertNotEquals($post->status, 2);

        $post = $mapper->first(['status :not' => [2]]);

        $this->assertNotEquals($post->status, 2);
    }

    /**
     * @throws DBALException
     * @throws OrmException
     */
    public function testArrayMultiple(): void {

        $mapper = testOrmMapper(Post::class);

        $posts = $mapper->where(['status' => [3, 4, 5]]);

        $this->assertStringContainsStringIgnoringCase('IN', $posts->toSql());
        $this->assertEquals(3, $posts->count());

        $posts = $mapper->where(['status :in' => [3, 4, 5]]);

        $this->assertStringContainsStringIgnoringCase('IN', $posts->toSql());
        $this->assertEquals(3, $posts->count());
    }

    /**
     * @throws DBALException
     * @throws OrmException
     */
    public function testArrayNotInMultiple(): void {

        $mapper = testOrmMapper(Post::class);

        $posts = $mapper->where(['status !=' => [3, 4, 5]]);

        $this->assertStringContainsStringIgnoringCase('NOT IN', $posts->toSql());
        $this->assertEquals(7, $posts->count());

        $posts = $mapper->where(['status :not' => [3, 4, 5]]);

        $this->assertStringContainsStringIgnoringCase('NOT IN', $posts->toSql());
        $this->assertEquals(7, $posts->count());
    }

    /**
     * @throws DBALException
     * @throws OrmException
     */
    public function testQueryHavingClause(): void {

        $mapper = testOrmMapper(Post::class);

        // "HAVING" aliases are only supported by MySQL
        if ( ! in_array(classBasename($mapper->connection()->getDatabasePlatform()), [classBasename(MySqlPlatform::class), classBasename(MySQL57Platform::class), classBasename(MySQL80Platform::class)], true)) {
            $this->markTestSkipped();
        }

        $posts = $mapper->select('ANY_VALUE(`id`), MAX(status) as maximus')
            ->having(['maximus' => 10]);

        $this->assertStringContainsStringIgnoringCase('HAVING', $posts->toSql());
        $this->assertCount(1, $posts->toArray());
    }

    /**
     * @throws DBALException
     * @throws OrmException
     */
    public function testQueryEmptyArrayIsNullToAvoidSQLErrorOnEmptyINClause(): void {
        $mapper = testOrmMapper(Post::class);

        $posts = $mapper->where(['status' => []]);

        $this->assertStringContainsStringIgnoringCase('IS NULL', $posts->toSql());
        $this->assertCount(0, $posts);
    }

    /**
     * @throws DBALException
     * @throws OrmException
     */
    public function testWhereSqlSubqueryInClause(): void {

        $mapper = testOrmMapper(Post::class);

        $postsSub = $mapper->where(['status !=' => [3, 4, 5]]);
        $posts = $mapper->select()->whereSql('id IN(' . $postsSub->toSql() . ')');

        $this->assertStringContainsStringIgnoringCase('IN', $posts->toSql());
    }

    /**
     * @throws DBALException
     * @throws OrmException
     */
    public function testWhereFieldSqlSubqueryInClause(): void {

        $mapper = testOrmMapper(Post::class);
        $params = [3, 4, 5];

        $postsSub = $mapper->where(['status !=' => $params]);
        $posts = $mapper->select()->whereFieldSql('id', 'IN(' . $postsSub->toSql() . ')', [$params]);

        $this->assertStringContainsStringIgnoringCase('IN', $posts->toSql());
    }

    /**
     * @throws DBALException
     * @throws OrmException
     */
    public function testWhereFieldSqlWithMultipleParams(): void {
        $mapper = testOrmMapper(Post::class);
        $params = [3, 5];

        $posts = $mapper->select()->whereFieldSql('id', 'BETWEEN ? AND ?', $params);

        $this->assertCount(3, $posts);
    }

    /**
     * @throws OrmException
     * @throws DBALException
     */
    public function testQueryArrayAccess(): void {
        $mapper = testOrmMapper(Post::class);

        $posts = $mapper->all();

        $this->assertInstanceOf(Post::class, $posts[0]);
    }

    /**
     * @throws OrmException
     * @throws DBALException
     */
    public function testQueryCountIsInteger(): void {
        $mapper = testOrmMapper(Post::class);

        $posts = $mapper->all();

        $this->assertSame(count($posts), $posts->count());
    }

    /**
     * @throws OrmException
     * @throws DBALException
     */
    public function testQueryCountIsAccurate(): void {

        $mapper = testOrmMapper(Post::class);

        $posts = $mapper->all();
        $postCount = count($posts);

        $i = 0;

        /** @noinspection PhpUnusedLocalVariableInspection */
        foreach ($posts as $post) {
            $i++;
        }

        $this->assertSame($postCount, $i);
    }

    /**
     * @throws DBALException
     * @throws OrmException
     */
    public function testCustomQueryWithSQL(): void {
        $mapper = testOrmMapper(Post::class);

        $posts = $mapper->query('SELECT * FROM ' . $mapper->table());
        $this->assertInstanceOf(Collection::class, $posts);
        $postCount = count($posts);

        $i = 0;
        foreach ($posts as $post) {
            $i++;
            $this->assertInstanceOf(Post::class, $post);
        }

        $this->assertSame($postCount, $i);
    }

    /**
     * @throws DBALException
     * @throws OrmException
     */
    public function testCustomQueryWithSqlAndIndexedParams(): void {

        $mapper = testOrmMapper(Post::class);

        $posts = $mapper->query('SELECT * FROM ' . $mapper->table() . ' WHERE status < ?', [10]);
        $this->assertInstanceOf(Collection::class, $posts);
        $postCount = count($posts);

        $i = 0;
        foreach ($posts as $post) {
            $i++;
            $this->assertInstanceOf(Post::class, $post);
        }

        $this->assertSame($postCount, $i);
    }

    /**
     * @throws DBALException
     * @throws OrmException
     */
    public function testCustomQueryWithSqlAndNamedParams(): void {

        $mapper = testOrmMapper(Post::class);

        $posts = $mapper->query('SELECT * FROM ' . $mapper->table() . ' WHERE status < :status', ['status' => 10]);
        $this->assertInstanceOf(Collection::class, $posts);
        $postCount = count($posts);

        $i = 0;
        foreach ($posts as $post) {
            $i++;
            $this->assertInstanceOf(Post::class, $post);
        }

        $this->assertSame($postCount, $i);
    }

    /**
     * @dataProvider identifierProvider
     *
     * @param $identifier
     * @param $expected
     *
     * @throws DBALException
     * @throws OrmException
     */
    public function testEscapingIdentifier($identifier, $expected): void {
        $mapper = testOrmMapper(Post::class);
        $quote = $mapper->connection()->getDatabasePlatform()->getIdentifierQuoteCharacter();

        $this->assertEquals(
            sprintf($expected, $quote),
            $mapper->where(['id !=' => null])->escapeIdentifier($identifier)
        );
    }

    /**
     * @return array
     */
    public function identifierProvider(): array {
        return [
            ['table', '%1$stable%1$s'],
            ['table.field', '%1$stable%1$s.%1$sfield%1$s'],
            ['count field', 'count field'],
            ['distinct(field)', 'distinct(field)'],
            ['max(field) as max', 'max(field) as max'],
        ];
    }

    /**
     * @throws DBALException
     * @throws OrmException
     */
    public function testEscapingInQuery(): void {

        $mapper = testOrmMapper(Post::class);

        $expected = str_replace(
            '`',
            $mapper->connection()->getDatabasePlatform()->getIdentifierQuoteCharacter(),
            'SELECT * FROM `test_posts` WHERE `test_posts`.`title` LIKE ? AND `test_posts`.`status` >= ?'
        );

        $query = $mapper->where(['title :like' => 'lorem', 'status >=' => 1])->toSql();

        $this->assertEquals($expected, $query);
    }

    /**
     * @throws DBALException
     * @throws OrmException
     */
    public function testWildcardLikeSupport(): void {

        $mapper = testOrmMapper(Post::class);

        $expected = 'SELECT * FROM test_posts WHERE test_posts.title LIKE ? AND test_posts.status >= ?';

        $query = $mapper->where(['title :like' => '%lorem%', 'status >=' => 1])->noQuote()->toSql();

        $this->assertEquals($expected, $query);
    }

    /**
     * @throws DBALException
     * @throws OrmException
     */
    public function testExecRawQuery(): void {

        $mapper = testOrmMapper(Post::class);

        $sql = 'UPDATE test_posts SET status = :status WHERE test_posts.title = :title';

        $affectedRows = $mapper->exec($sql, ['title' => 'even_title', 'status' => 1]);

        $this->assertEquals(5, $affectedRows);
    }

    /**
     * @throws OrmException
     * @throws DBALException
     */
    public function testQueryJsonSerialize(): void {

        $mapper = testOrmMapper(Tag::class);

        $tags = $mapper->all();

        $data = json_encode($tags->toArray());
        $json = json_encode($tags);

        $this->assertSame($data, $json);
    }

    /**
     * @throws DBALException
     * @throws OrmException
     */
    public function testQueryCustomWhereOperator(): void {

        Query::addWhereOperator(':json_exists', static function ($builder, $column, $value) {
            /* @var $builder QueryBuilder */
            /* @var $column Column */
            return 'jsonb_exists(' . $column . ', ' . $builder->createPositionalParameter($value) . ')';
        });

        $mapper = testOrmMapper(Post::class);
        $query = $mapper->where(['data :json_exists' => 'author']);
        $this->assertStringContainsStringIgnoringCase('jsonb_exists(', $query->toSql());
    }

    /**
     * @throws DBALException
     * @throws OrmException
     */
    public function testInvalidQueryOperatorThrowsException(): void {

        $this->expectException(InvalidArgumentException::class);

        $mapper = testOrmMapper(Post::class);

        $mapper->where(['data :nonsense' => 'author']);
    }
}
