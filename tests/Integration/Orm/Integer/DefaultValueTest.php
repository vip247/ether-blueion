<?php declare(strict_types=1);

namespace Ether\Tests\Integration\Orm\Integer;

use PHPUnit\Framework\TestCase;
use Ether\Tests\Integration\Orm\Integer\Entity\DefaultValue;

class DefaultValueTest extends TestCase {

    private static $entities = ['DefaultValue'];

    public static function setupBeforeClass(): void {
        foreach (self::$entities as $entity) {
            testOrmMapper('Ether\Tests\Integration\Orm\Integer\Entity\\' . $entity)->migrate();
        }
    }

    public static function tearDownAfterClass(): void {
        foreach (self::$entities as $entity) {
            testOrmMapper('Ether\Tests\Integration\Orm\Integer\Entity\\' . $entity)->dropTable();
        }
    }

    public function testDefaultValue(): void {
        $mapper = testOrmMapper(DefaultValue::class);

        $defaultValue = $mapper->get();

        $this->assertEquals(2, $defaultValue->data1);
        $this->assertEquals(3, $defaultValue->data2);
        $this->assertEquals(5, $defaultValue->data3);
    }
}
