<?php declare(strict_types=1);

namespace Ether\Tests\Integration\Orm\Integer;

use PHPUnit\Framework\TestCase;
use Ether\Tests\Integration\Orm\Integer\Entity\Post;

class ForeignKeyTest extends TestCase {

    private static $entities = ['Post', 'Author', 'RecursiveEntity'];

    public static function setupBeforeClass(): void {
        foreach (self::$entities as $entity) {
            testOrmMapper('Ether\Tests\Integration\Orm\Integer\Entity\\' . $entity)->migrate();
        }
    }

    public static function tearDownAfterClass(): void {
        foreach (self::$entities as $entity) {
            testOrmMapper('Ether\Tests\Integration\Orm\Integer\Entity\\' . $entity)->dropTable();
        }
    }

    public function testForeignKeyMigration(): void {

        $mapper = testOrmMapper(Post::class);

        $entity = $mapper->entity();
        $table = $entity::table();
        $schemaManager = $mapper->connection()->getSchemaManager();

        $foreignKeys = $schemaManager->listTableForeignKeys($table);

        $this->assertCount(1, $foreignKeys);
    }
}
