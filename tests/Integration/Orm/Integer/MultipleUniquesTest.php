<?php declare(strict_types=1);

namespace Ether\Tests\Integration\Orm\Integer;

use PHPUnit\Framework\TestCase;
use Ether\Tests\Integration\Orm\Integer\Entity\MultipleUniques;

class MultipleUniquesTest extends TestCase {

    private static $entities = ['MultipleUniques'];

    public static function setupBeforeClass(): void {
        foreach (self::$entities as $entity) {
            testOrmMapper('Ether\Tests\Integration\Orm\Integer\Entity\\' . $entity)->migrate();
        }
    }

    public static function tearDownAfterClass(): void {
        foreach (self::$entities as $entity) {
            testOrmMapper('Ether\Tests\Integration\Orm\Integer\Entity\\' . $entity)->dropTable();
        }
    }

    public function testMultipleUniques(): void {

        $mapper = testOrmMapper(MultipleUniques::class);

        $entity1 = new MultipleUniques([
            'data1' => 'data1_test1',
            'data2' => 1,
            'data3' => 'data3_test1'
        ]);
        $mapper->save($entity1);

        $entity2 = new MultipleUniques([
            'data1' => 'data1_test2',
            'data2' => 2,
            'data3' => 'data3_test2'
        ]);
        $mapper->save($entity2);

        $entity3 = new MultipleUniques([
            'data1' => 'data1_test3',
            'data2' => 1,
            'data3' => 'data3_test3'
        ]);
        $mapper->save($entity3);

        $entity4 = new MultipleUniques([
            'data1' => 'data1_test1',
            'data2' => 4,
            'data3' => 'data3_test4'
        ]);
        $mapper->save($entity4);

        $entity5 = new MultipleUniques([
            'data1' => 'data1_test5',
            'data2' => 1,
            'data3' => 'data3_test1'
        ]);
        $mapper->save($entity5);

        $entity6 = new MultipleUniques([
            'data1' => 'data1_test1',
            'data2' => 1,
            'data3' => 'data3_test6'
        ]);
        $mapper->save($entity6);

        $entity7 = new MultipleUniques([
            'data1' => 'data1_test2',
            'data2' => 1,
            'data3' => 'data3_test2'
        ]);
        $mapper->save($entity7);

        $entity8 = new MultipleUniques([
            'data1' => 'data1_test1',
            'data2' => 1,
            'data3' => 'data3_test4'
        ]);
        $mapper->save($entity8);

        $this->assertFalse($entity1->hasErrors());
        $this->assertFalse($entity2->hasErrors());
        $this->assertFalse($entity3->hasErrors());
        $this->assertFalse($entity4->hasErrors());
        $this->assertFalse($entity5->hasErrors());
        $this->assertTrue($entity6->hasErrors());
        $this->assertContains("Uniq1 'data1_test1-1' is already taken.", $entity6->errors('uniq1'));
        $this->assertTrue($entity7->hasErrors());
        $this->assertContains("Uniq2 'data1_test2-data3_test2' is already taken.", $entity7->errors('uniq2'));
        $this->assertTrue($entity8->hasErrors());
        $this->assertContains("Uniq1 'data1_test1-1' is already taken.", $entity8->errors('uniq1'));
        $this->assertContains("Uniq2 'data1_test1-data3_test4' is already taken.", $entity8->errors('uniq2'));
    }
}
