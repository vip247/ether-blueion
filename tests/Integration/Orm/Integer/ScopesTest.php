<?php declare(strict_types=1);

namespace Ether\Tests\Integration\Orm\Integer;

use RuntimeException;
use Ether\Database\Orm\Mapper;
use PHPUnit\Framework\TestCase;
use Doctrine\DBAL\DBALException;
use Ether\Database\Orm\Exception;
use Ether\Tests\Integration\Orm\Integer\Entity\Post;
use Ether\Tests\Integration\Orm\Integer\Entity\Event;
use Ether\Tests\Integration\Orm\Integer\Entity\Author;

class ScopesTest extends TestCase {

    /**
     * @var array
     */
    private static $entities = ['Post\Comment', 'Post', 'Event\Search', 'Event', 'Author'];


    public static function setupBeforeClass(): void {

        foreach (self::$entities as $entity) {
            testOrmMapper('Ether\Tests\Integration\Orm\Integer\Entity\\' . $entity)->migrate();
        }

        $authorMapper = testOrmMapper(Author::class);
        $author = $authorMapper->build([
            'id'       => 1,
            'email'    => 'example@example.com',
            'password' => 't00r',
            'is_admin' => false
        ]);
        $result = $authorMapper->insert($author);

        if ( ! $result) {
            throw new RuntimeException('Unable to create author: ' . var_export($author->data(), true));
        }
    }


    public static function tearDownAfterClass(): void {
        foreach (self::$entities as $entity) {
            testOrmMapper('Ether\Tests\Integration\Orm\Integer\Entity\\' . $entity)->dropTable();
        }
    }


    public function testSingleScopes(): void {

        /** @var  $mapper  Mapper */
        $mapper = testOrmMapper(Event::class);
        $query = $mapper->all()->noQuote()->active();

        $this->assertEquals('SELECT * FROM test_events WHERE test_events.status = ?', $query->toSql());
    }


    public function testMultipleScopes(): void {

        $mapper = testOrmMapper(Event::class);

        $query = $mapper->select()->noQuote()->free()->active();

        $this->assertEquals('SELECT * FROM test_events WHERE (test_events.type = ?) AND (test_events.status = ?)', $query->toSql());
    }


    public function testEntityScopes(): void {

        $mapper = testOrmMapper(Post::class);

        $query = $mapper->select()->noQuote()->active();

        $this->assertEquals('SELECT * FROM test_posts WHERE test_posts.status = ?', $query->toSql());
    }


    public function testRelationScopes(): void {

        $mapper = testOrmMapper(Post::class);
        $mapper->insert([
            'title'     => 'Test',
            'body'      => 'Test body',
            'author_id' => 1,
        ]);

        $query = $mapper->get(1)->comments->yesterday()->query();
        $sql = str_replace(['`', '"'], '', $query->toSql());

        $this->assertEquals('SELECT * FROM test_post_comments WHERE (test_post_comments.post_id = ?) AND (test_post_comments.date_created > ? AND test_post_comments.date_created < ?) ORDER BY test_post_comments.date_created ASC', $sql);
    }
}
