<?php declare(strict_types=1);

namespace Ether\Tests\Integration\Orm\Integer;

use DateTime;
use Exception;
use PHPUnit\Framework\TestCase;
use Ether\Database\Orm\Entity\Collection;
use Ether\Database\Orm\Mapper as OrmMapper;
use Ether\Tests\Integration\Orm\Integer\Entity\Tag;
use Ether\Tests\Integration\Orm\Integer\Entity\Post;
use Ether\Tests\Integration\Orm\Integer\Entity\Author;
use Ether\Tests\Integration\Orm\Integer\Entity\PostTag;
use Ether\Tests\Integration\Orm\Integer\Entity\Post\Comment;

class EventsTest extends TestCase {

    private static $entities = ['PostTag', 'Post\Comment', 'Post', 'Tag', 'Author'];

    public static function setupBeforeClass(): void {

        foreach (self::$entities as $entity) {
            testOrmMapper('Ether\Tests\Integration\Orm\Integer\Entity\\' . $entity)->migrate();
        }

        // Insert blog dummy data
        for ($i = 1; $i <= 3; $i++) {
            testOrmMapper(Tag::class)->insert([
                'name' => "Title {$i}"
            ]);
        }

        for ($i = 1; $i <= 4; $i++) {
            testOrmMapper(Author::class)->insert([
                'email'    => $i . 'user@somewhere.com',
                'password' => 'securepassword'
            ]);
        }

        $postMapper = testOrmMapper(Post::class);

        for ($i = 1; $i <= 10; $i++) {

            $post = $postMapper->build([
                'title'        => (($i % 2) ? 'odd' : 'even') . '_title',
                'body'         => '<p>' . $i . '_body</p>',
                'status'       => $i,
                'date_created' => new DateTime(),
                'author_id'    => rand(1, 3)
            ]);

            $result = $postMapper->insert($post);

            if ( ! $result) {
                throw new Exception('Unable to create post: ' . var_export($post->data(), true));
            }

            for ($j = 1; $j <= 2; $j++) {
                testOrmMapper(Comment::class)->insert([
                    'post_id' => $post->id,
                    'name'    => (($j % 2) ? 'odd' : 'even') . '_title',
                    'email'   => 'bob@somewhere.com',
                    'body'    => (($j % 2) ? 'odd' : 'even') . '_comment_body',
                ]);
            }

            for ($j = 1; $j <= $i % 3; $j++) {
                testOrmMapper(PostTag::class)->insert([
                    'post_id' => $post->id,
                    'tag_id'  => $j
                ]);
            }
        }
    }

    public static function tearDownAfterClass(): void {
        foreach (self::$entities as $entity) {
            testOrmMapper('Ether\Tests\Integration\Orm\Integer\Entity\\' . $entity)->dropTable();
        }
    }

    protected function setUp(): void {
        Post::$events = [];
    }

    public function testSaveHooks(): void {
        $mapper = testOrmMapper(Post::class);
        $testcase = $this;

        $post = new Post([
            'title'     => 'A title',
            'body'      => '<p>body</p>',
            'status'    => 1,
            'author_id' => 1
        ]);

        $hooks = [];

        $eventEmitter = $mapper->eventEmitter();
        $eventEmitter->on('beforeSave', static function ($post, $mapper) use (&$hooks, &$testcase) {
            $testcase->assertEquals($hooks, []);
            $hooks[] = 'called beforeSave';
        });

        $eventEmitter->on('afterSave', static function ($post, $mapper, $result) use (&$hooks, &$testcase) {
            $testcase->assertEquals($hooks, ['called beforeSave']);
            $testcase->assertInstanceOf(Post::class, $post);
            $testcase->assertInstanceOf(OrmMapper::class, $mapper);
            $hooks[] = 'called afterSave';
        });

        $this->assertEquals($hooks, []);

        $mapper->save($post);

        $this->assertEquals(['called beforeSave', 'called afterSave'], $hooks);

        $eventEmitter->removeAllListeners('afterSave');
        $eventEmitter->removeAllListeners('beforeSave');

        $mapper->save($post);

        // Verify that hooks were de-registered (not called again)
        $this->assertEquals(['called beforeSave', 'called afterSave'], $hooks);
    }

    public function testInsertHooks() {
        $mapper = testOrmMapper(Post::class);
        $testcase = $this;

        $post = new Post([
            'title'        => 'A title',
            'body'         => '<p>body</p>',
            'status'       => 1,
            'author_id'    => 1,
            'date_created' => new DateTime()
        ]);

        $hooks = [];

        $eventEmitter = $mapper->eventEmitter();
        $eventEmitter->on('beforeInsert', static function ($post, $mapper) use (&$hooks, &$testcase) {
            $testcase->assertEquals($hooks, []);
            $hooks[] = 'called beforeInsert';
        });

        $eventEmitter->on('afterInsert', static function ($post, $mapper, $result) use (&$hooks, &$testcase) {
            $testcase->assertEquals($hooks, ['called beforeInsert']);
            $hooks[] = 'called afterInsert';
        });

        $this->assertEquals($hooks, []);

        $mapper->save($post);

        $this->assertEquals($hooks, ['called beforeInsert', 'called afterInsert']);

        $eventEmitter->removeAllListeners('beforeInsert');
        $eventEmitter->removeAllListeners('afterInsert');
    }

    public function testInsertHooksUpdatesProperty(): void {
        $mapper = testOrmMapper(Post::class);
        $post = new Post([
            'title'        => 'A title',
            'body'         => '<p>body</p>',
            'status'       => 1,
            'author_id'    => 4,
            'date_created' => new DateTime()
        ]);

        $eventEmitter = $mapper->eventEmitter();
        $eventEmitter->on('beforeInsert', static function ($post, $mapper) {
            $post->status = 2;
        });
        $mapper->save($post);
        $post = $mapper->first(['author_id' => 4]);
        $this->assertEquals(2, $post->status);

        $eventEmitter->removeAllListeners('beforeInsert');
    }

    public function testUpdateHooks(): void {
        $mapper = testOrmMapper(Post::class);
        $testcase = $this;

        $post = new Post([
            'title'        => 'A title',
            'body'         => '<p>body</p>',
            'status'       => 1,
            'author_id'    => 1,
            'date_created' => new DateTime()
        ]);
        $mapper->save($post);

        $hooks = [];

        $eventEmitter = $mapper->eventEmitter();
        $eventEmitter->on('beforeInsert', static function ($post, $mapper) use (&$testcase) {
            $testcase->assertTrue(false);
        });

        $eventEmitter->on('beforeUpdate', static function ($post, $mapper) use (&$hooks, &$testcase) {
            $testcase->assertEquals($hooks, []);
            $hooks[] = 'called beforeUpdate';
        });

        $eventEmitter->on('afterUpdate', static function ($post, $mapper, $result) use (&$hooks, &$testcase) {
            $testcase->assertEquals($hooks, ['called beforeUpdate']);
            $hooks[] = 'called afterUpdate';
        });

        $this->assertEquals($hooks, []);

        $mapper->save($post);

        $this->assertEquals($hooks, ['called beforeUpdate', 'called afterUpdate']);

        $eventEmitter->removeAllListeners('beforeInsert');
        $eventEmitter->removeAllListeners('beforeUpdate');
        $eventEmitter->removeAllListeners('afterUpdate');
    }

    public function testUpdateHookUpdatesProperly(): void {

        $author_id = 111;
        testOrmMapper(Author::class)->insert([
            'id'       => $author_id,
            'email'    => $author_id . 'user@somewhere.com',
            'password' => 'securepassword'
        ]);

        $mapper = testOrmMapper(Post::class);

        $post = new Post([
            'title'        => 'A title',
            'body'         => '<p>body</p>',
            'status'       => 1,
            'author_id'    => $author_id,
            'date_created' => new DateTime()
        ]);

        $mapper->save($post);

        $this->assertEquals(1, $post->status);

        $eventEmitter = $mapper->eventEmitter();
        $eventEmitter->on('beforeUpdate', static function ($post, $mapper) {
            $post->status = 9;
        });

        $mapper->save($post);
        $post = $mapper->first(['author_id' => $author_id]);

        $this->assertEquals(9, $post->status);

        $eventEmitter->removeAllListeners('beforeUpdate');
    }

    public function testDeleteHooks(): void {
        $mapper = testOrmMapper(Post::class);
        $testCase = $this;

        $post = new Post([
            'title'        => 'A title',
            'body'         => '<p>body</p>',
            'status'       => 1,
            'author_id'    => 1,
            'date_created' => new DateTime()
        ]);
        $mapper->save($post);

        $hooks = [];

        $eventEmitter = $mapper->eventEmitter();
        $eventEmitter->on('beforeDelete', static function ($post, $mapper) use (&$hooks, &$testCase) {
            $testCase->assertEquals($hooks, []);
            $hooks[] = 'called beforeDelete';
        });

        $eventEmitter->on('afterDelete', static function ($post, $mapper, $result) use (&$hooks, &$testCase) {
            $testCase->assertEquals($hooks, ['called beforeDelete']);
            $hooks[] = 'called afterDelete';
        });

        $this->assertEquals($hooks, []);

        $mapper->delete($post);

        $this->assertEquals($hooks, ['called beforeDelete', 'called afterDelete']);

        $eventEmitter->removeAllListeners('beforeDelete');
        $eventEmitter->removeAllListeners('afterDelete');
    }

    public function testDeleteHooksForArrayConditions(): void {
        $mapper = testOrmMapper(Post::class);
        $testcase = $this;

        $post = new Post([
            'title'        => 'A title',
            'body'         => '<p>body</p>',
            'status'       => 1,
            'author_id'    => 1,
            'date_created' => new DateTime()
        ]);
        $mapper->save($post);

        $entityHooks = [];
        $arrayHooks = [];

        $eventEmitter = $mapper->eventEmitter();
        $eventEmitter->on('beforeDelete', static function ($conditions, $mapper) use (&$entityHooks) {
            $entityHooks[] = 'called beforeDelete';
        });
        $eventEmitter->on('beforeDeleteConditions', static function ($conditions, $mapper) use (&$arrayHooks, &$testcase) {
            $testcase->assertEquals($arrayHooks, []);
            $arrayHooks[] = 'called beforeDeleteConditions';
        });

        $eventEmitter->on('afterDelete', static function ($conditions, $mapper, $result) use (&$entityHooks) {
            $entityHooks[] = 'called afterDelete';
        });
        $eventEmitter->on('afterDeleteConditions', static function ($conditions, $mapper, $result) use (&$arrayHooks, &$testcase) {
            $testcase->assertEquals($arrayHooks, ['called beforeDeleteConditions']);
            $arrayHooks[] = 'called afterDeleteConditions';
        });

        $this->assertEquals($entityHooks, []);
        $this->assertEquals($arrayHooks, []);

        $mapper->delete([
            $post->primaryKeyField() => $post->primaryKey()
        ]);

        $this->assertEquals($entityHooks, []);
        $this->assertEquals($arrayHooks, ['called beforeDeleteConditions', 'called afterDeleteConditions']);

        $eventEmitter->removeAllListeners('beforeDelete');
        $eventEmitter->removeAllListeners('beforeDeleteConditions');
        $eventEmitter->removeAllListeners('afterDelete');
        $eventEmitter->removeAllListeners('afterDeleteConditions');
    }

    public function testEntityHooks(): void {
        $mapper = testOrmMapper(Post::class);
        $eventEmitter = $mapper->eventEmitter();
        $post = new Post([
            'title'        => 'A title',
            'body'         => '<p>body</p>',
            'status'       => 1,
            'author_id'    => 1,
            'date_created' => new DateTime()
        ]);

        $i = $post->status;

        Post::$events = [
            'beforeSave' => ['mock_save_hook']
        ];
        $mapper->loadEvents();

        $mapper->save($post);

        $this->assertEquals($i + 1, $post->status);
        $eventEmitter->removeAllListeners('beforeSave');

        Post::$events = [
            'beforeSave' => ['mock_save_hook', 'mock_save_hook']
        ];
        $mapper->loadEvents();

        $i = $post->status;

        $mapper->save($post);

        $this->assertEquals($i + 2, $post->status);

        $eventEmitter->removeAllListeners('beforeSave');
    }

    public function testWithHooks(): void {

        $mapper = testOrmMapper(Post::class);
        $eventEmitter = $mapper->eventEmitter();
        $testcase = $this;

        $hooks = [];

        $eventEmitter->on('beforeWith', static function ($mapper, $collection, $with) use (&$hooks, &$testcase) {
            $testcase->assertEquals(Post::class, $mapper->entity());
            $testcase->assertInstanceOf(Collection::class, $collection);
            $testcase->assertEquals(['comments'], $with);
            $testcase->assertInstanceOf(OrmMapper::class, $mapper);
            $hooks[] = 'Called beforeWith';
        });

        $eventEmitter->on('loadWith', static function ($mapper, $collection, $relationName) use (&$hooks, &$testcase) {
            $testcase->assertEquals(Post::class, $mapper->entity());
            $testcase->assertInstanceOf(Collection::class, $collection);
            $testcase->assertInstanceOf(OrmMapper::class, $mapper);
            $testcase->assertEquals('comments', $relationName);
            $hooks[] = 'Called loadWith';
        });

        $eventEmitter->on('afterWith', static function ($mapper, $collection, $with) use (&$hooks, &$testcase) {
            $testcase->assertEquals(Post::class, $mapper->entity());
            $testcase->assertInstanceOf(Collection::class, $collection);
            $testcase->assertEquals(['comments'], $with);
            $testcase->assertInstanceOf(OrmMapper::class, $mapper);
            $hooks[] = 'Called afterWith';
        });

        $mapper->all(Post::class, ['id' => [1, 2]])->with('comments')->execute();

        $this->assertEquals(['Called beforeWith', 'Called loadWith', 'Called afterWith'], $hooks);
        $eventEmitter->removeAllListeners();
    }

    public function testWithAssignmentHooks(): void {

        $mapper = testOrmMapper(Post::class);
        $eventEmitter = $mapper->eventEmitter();

        $eventEmitter->on('loadWith', static function ($mapper, $collection, $relationName) {
            foreach ($collection as $post) {
                $comments = [];
                $comments[] = new Comment([
                    'post_id' => $post->id,
                    'name'    => 'Chester Tester',
                    'email'   => 'chester@tester.com',
                    'body'    => 'Some body content here that Chester made!'
                ]);

                $post->relation($relationName, new Collection($comments));
            }

            return false;
        });

        $posts = $mapper->all()->with('comments')->execute();
        foreach ($posts as $post) {
            $this->assertEquals(1, $post->comments->count());
        }

        $eventEmitter->removeAllListeners();
    }

    public function testHookReturnsFalse(): void {

        $mapper = testOrmMapper(Post::class);
        $post = new Post([
            'title'        => 'A title',
            'body'         => '<p>body</p>',
            'status'       => 1,
            'author_id'    => 1,
            'date_created' => new DateTime()
        ]);

        $hooks = [];

        $eventEmitter = $mapper->eventEmitter();
        $eventEmitter->on('beforeSave', static function ($post, $mapper) use (&$hooks) {
            $hooks[] = 'called beforeSave';

            return false;
        });

        $eventEmitter->on('afterSave', static function ($post, $mapper, $result) use (&$hooks) {
            $hooks[] = 'called afterSave';
        });

        $mapper->save($post);

        $this->assertEquals($hooks, ['called beforeSave']);

        $eventEmitter->removeAllListeners('afterSave');
    }

    public function testAfterSaveEvent(): void {

        $mapper = testOrmMapper(Post::class);
        $eventEmitter = $mapper->eventEmitter();
        $post = new Post([
            'title'        => 'A title',
            'body'         => '<p>body</p>',
            'status'       => 1,
            'author_id'    => 1,
            'date_created' => new DateTime()
        ]);

        $eventEmitter->removeAllListeners('afterSave');
        Post::$events = [
            'afterSave' => ['mock_save_hook']
        ];
        $mapper->loadEvents();

        $result = $mapper->save($post);

        $this->assertEquals(2, $post->status);

        $eventEmitter->removeAllListeners('afterSave');
    }

    public function testValidationEvents(): void {

        $mapper = testOrmMapper(Post::class);
        $post = new Post([
            'title'        => 'A title',
            'body'         => '<p>body</p>',
            'status'       => 1,
            'author_id'    => 1,
            'date_created' => new DateTime()
        ]);

        $hooks = [];
        $eventEmitter = $mapper->eventEmitter();
        $eventEmitter->on('beforeValidate', static function ($post, $mapper, $validator) use (&$hooks) {
            $hooks[] = 'called beforeValidate';
        });

        $eventEmitter->on('afterValidate', static function ($post, $mapper, $validator) use (&$hooks) {
            $hooks[] = 'called afterValidate';
        });

        $mapper->validate($post);

        $this->assertEquals(['called beforeValidate', 'called afterValidate'], $hooks);

        $eventEmitter->removeAllListeners();
    }

    public function testBeforeValidateEventStopsValidation(): void {

        $mapper = testOrmMapper(Post::class);

        $post = new Post([
            'title'        => 'A title',
            'body'         => '<p>body</p>',
            'status'       => 1,
            'author_id'    => 1,
            'date_created' => new DateTime()
        ]);

        $hooks = [];
        $eventEmitter = $mapper->eventEmitter();
        $eventEmitter->on('beforeValidate', static function ($post, $mapper, $validator) use (&$hooks) {
            $hooks[] = 'called beforeValidate';

            return false; // Should stop validation
        });

        $eventEmitter->on('afterValidate', static function ($post, $mapper, $validator) use (&$hooks) {
            $hooks[] = 'called afterValidate';
        });

        $mapper->validate($post);

        $this->assertEquals(['called beforeValidate'], $hooks);

        $eventEmitter->removeAllListeners();
    }

    public function testSaveEventsTriggeredOnCreate(): void {

        $mapper = testOrmMapper(Post::class);

        $hooks = [];
        $eventEmitter = $mapper->eventEmitter();

        $eventEmitter->on('beforeSave', static function ($post, $mapper) use (&$hooks) {
            $hooks[] = 'before';
        });

        $eventEmitter->on('afterSave', static function ($post, $mapper) use (&$hooks) {
            $hooks[] = 'after';
        });

        $mapper->create([
            'title'        => 'A title',
            'body'         => '<p>body</p>',
            'status'       => 1,
            'author_id'    => 1,
            'date_created' => new DateTime()
        ]);

        $this->assertEquals(['before', 'after'], $hooks);
        $eventEmitter->removeAllListeners();
    }

    public function testLoadEventCallOnGet(): void {

        $mapper = testOrmMapper(Post::class);

        $hooks = [];
        $eventEmitter = $mapper->eventEmitter();

        $eventEmitter->on('afterLoad', static function ($post, $mapper) use (&$hooks) {
            $hooks[] = 'after';
        });

        $mapper->create([
            'title'        => 'A title',
            'body'         => '<p>body</p>',
            'status'       => 1,
            'author_id'    => 1,
            'date_created' => new DateTime()
        ]);

        $this->assertEquals(['after'], $hooks);
        $eventEmitter->removeAllListeners();
    }

    public function testSaveEventsTriggeredOnUpdate(): void {

        $mapper = testOrmMapper(Post::class);

        $hooks = [];
        $eventEmitter = $mapper->eventEmitter();
        $eventEmitter->on('beforeSave', static function ($post, $mapper) use (&$hooks) {
            $hooks[] = 'before';
        });

        $eventEmitter->on('afterSave', static function ($post, $mapper) use (&$hooks) {
            $hooks[] = 'after';
        });

        $post = $mapper->create([
            'title'        => 'A title',
            'body'         => '<p>body</p>',
            'status'       => 1,
            'author_id'    => 1,
            'date_created' => new DateTime()
        ]);

        $post->status = 2;
        $mapper->update($post);

        $this->assertEquals(['before', 'after', 'before', 'after'], $hooks);
        $eventEmitter->removeAllListeners();
    }
}
