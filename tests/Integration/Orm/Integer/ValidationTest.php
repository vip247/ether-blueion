<?php declare(strict_types=1);

namespace Ether\Tests\Integration\Orm\Integer;

use DateTime;
use Throwable;
use PHPUnit\Framework\TestCase;
use Doctrine\DBAL\DBALException;
use Ether\Database\Orm\Entity\Collection;
use Ether\Tests\Integration\Orm\Integer\Entity\Tag;
use Ether\Tests\Integration\Orm\Integer\Entity\Post;
use Ether\Tests\Integration\Orm\Integer\Entity\Event;
use Ether\Tests\Integration\Orm\Integer\Entity\Report;
use Ether\Tests\Integration\Orm\Integer\Entity\Author;
use Ether\Database\Orm\Exception as OrmException;
use Ether\Tests\Integration\Orm\Integer\Entity\Post\Comment;
use Ether\Tests\Integration\Orm\Integer\Entity\Event\Search;

class ValidationTest extends TestCase {

    /**
     * @var array
     */
    private static $entities = ['Author', 'Report'];

    /**
     * @throws DBALException
     */
    public static function setupBeforeClass(): void {
        foreach (self::$entities as $entity) {
            testOrmMapper('Ether\Tests\Integration\Orm\Integer\Entity\\' . $entity)->migrate();
        }
    }

    /**
     * @throws OrmException
     */
    public static function tearDownAfterClass(): void {
        foreach (self::$entities as $entity) {
            testOrmMapper('Ether\Tests\Integration\Orm\Integer\Entity\\' . $entity)->dropTable();
        }
    }

    /**
     * @throws OrmException
     * @throws Throwable
     */
    public function tearDown(): void {
        $mapper = testOrmMapper(Author::class);
        $mapper->truncateTable();
    }

    /**
     * @throws DBALException
     * @throws OrmException
     */
    public function testRequiredField(): void {
        $mapper = testOrmMapper(Author::class);

        $entity = new Author([
            'is_admin' => true
        ]);
        $mapper->save($entity);

        $this->assertTrue($entity->hasErrors());
        $this->assertContains('Email is required', $entity->errors('email'));
    }

    /**
     * @throws DBALException
     * @throws OrmException
     */
    public function testUniqueField(): void {
        $mapper = testOrmMapper(Author::class);

        // Setup new user
        $user1 = new Author([
            'email'    => 'test@test.com',
            'password' => 'test',
            'is_admin' => true
        ]);
        $mapper->save($user1);

        // Setup new user (identical, expecting a validation error)
        $user2 = new Author([
            'email'    => 'test@test.com',
            'password' => 'test',
            'is_admin' => false
        ]);
        $mapper->save($user2);

        $this->assertFalse($user1->hasErrors());
        $this->assertTrue($user2->hasErrors());
        $this->assertContains("Email 'test@test.com' is already taken.", $user2->errors('email'));
    }

    /**
     * @throws DBALException
     * @throws OrmException
     */
    public function testUniqueFieldConvertToDb(): void {
        $mapper = testOrmMapper(Report::class);

        // Setup new report
        $report1 = new Report([
            'date'   => new DateTime('2016-05-04'),
            'result' => ['a' => 1, 'b' => 2],
        ]);
        $mapper->save($report1);

        // Setup new report (same date, expecting error)
        $report2 = new Report([
            'date'   => new DateTime('2016-05-04'),
            'result' => ['a' => 2, 'b' => 1],
        ]);
        $mapper->save($report2);

        $this->assertFalse($report1->hasErrors());
        $this->assertTrue($report2->hasErrors());
        $this->assertContains("Date '2016-05-04' is already taken.", $report2->errors('date'));
    }

    /**
     * @throws DBALException
     * @throws OrmException
     */
    public function testEmail(): void {
        $mapper = testOrmMapper(Author::class);

        $entity = new Author([
            'email'    => 'test',
            'password' => 'test'
        ]);

        $mapper->save($entity);

        $this->assertTrue($entity->hasErrors());
        $this->assertContains('Email is not a valid email address', $entity->errors('email'));
    }

    /**
     * @throws DBALException
     * @throws OrmException
     */
    public function testLength(): void {

        $mapper = testOrmMapper(Author::class);

        $entity = new Author([
            'email'    => 't@t',
            'password' => 'test'
        ]);

        $mapper->save($entity);

        $this->assertTrue($entity->hasErrors());
        $this->assertContains('Email must be 4 characters long', $entity->errors('email'));
    }

    /**
     * @throws DBALException
     * @throws OrmException
     */
    public function testDisabledValidation(): void {
        $mapper = testOrmMapper(Author::class);

        $entity = new Author([
            'email'    => 't@t',
            'password' => 'test'
        ]);
        $mapper->save($entity, ['validate' => false]);

        $this->assertFalse($entity->hasErrors());
    }

    /**
     * @throws DBALException
     * @throws OrmException
     */
    public function testHasOneRelationValidation(): void {
        $mapper = testOrmMapper(Event::class);
        $search = new Search();
        $event = $mapper->build([]);
        $event->relation('search', $search);
        $mapper->validate($event, ['relations' => true]);

        $this->assertTrue(isset($event->errors()['search']));
    }

    /**
     * @throws DBALException
     * @throws OrmException
     */
    public function testBelongsToRelationValidation(): void {

        $mapper = testOrmMapper(Post::class);

        $author = new Author();
        $post = $mapper->build([]);
        $post->relation('author', $author);
        $mapper->validate($post, ['relations' => true]);

        $this->assertTrue(isset($post->errors()['author']));
    }

    /**
     * @throws DBALException
     * @throws OrmException
     */
    public function testHasManyRelationValidation(): void {

        $mapper = testOrmMapper(Post::class);

        $comment = new Comment();
        $post = $mapper->build([]);
        $post->relation('comments', new Collection([$comment]));
        $mapper->validate($post, ['relations' => true]);

        $this->assertTrue(isset($post->errors()['comments'][0]));
    }

    /**
     * @throws DBALException
     * @throws OrmException
     */
    public function testHasManyThroughRelationValidation(): void {

        $mapper = testOrmMapper(Post::class);

        $tag = new Tag();
        $post = $mapper->build([]);
        $post->relation('tags', new Collection([$tag]));
        $mapper->validate($post, ['relations' => true]);

        $this->assertTrue(isset($post->errors()['tags'][0]));
    }
}
