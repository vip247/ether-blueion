<?php declare(strict_types=1);

namespace Ether\Tests\Integration\Orm\Integer;

use Doctrine\DBAL\Connection;
use PHPUnit\Framework\TestCase;
use Ether\Database\Orm\Config;
use Doctrine\DBAL\Configuration;
use Doctrine\DBAL\DriverManager;

class ConfigTest extends TestCase {

    private const DSN = 'mysql://root:root@192.168.64.100:30005/default';

    public function testAddConnectionSqlite(): void {

        $cfg = new Config();
        $dsn = $cfg->parseDsn('sqlite::memory:');
        $this->assertEquals('pdo_sqlite', $dsn['driver']);

        $adapter = $cfg->addConnection('test_sqlite', 'sqlite::memory:');
        $this->assertInstanceOf(Connection::class, $adapter);
    }

    public function testAddSqliteConnectionWithDSNString(): void {
        $cfg = new Config();
        $adapter = $cfg->addConnection('test_sqlite', 'sqlite::memory:');
        $this->assertInstanceOf(Connection::class, $adapter);
    }

    public function testAddConnectionWithDSNString(): void {
        $cfg = new Config();
        $adapter = $cfg->addConnection('test_mysql', self::DSN);
        $this->assertInstanceOf(Connection::class, $adapter);
    }

    public function testConfigCanSerialize(): void {

        $cfg = new Config();
        $cfg->addConnection('test_mysql', self::DSN);

        $this->assertIsString(serialize($cfg));
    }

    public function testConfigCanUnserialize(): void {

        $cfg = new Config();
        $cfg->addConnection('test_mysql', self::DSN);

        $this->assertInstanceOf(Config::class, unserialize(serialize($cfg)));
    }

    public function testAddConnectionWithArray(): void {

        $cfg = new Config();

        $dbalArray = [
            'dbname'   => 'default',
            'user'     => 'root',
            'password' => 'root',
            'host'     => '192.168.64.100',
            'driver'   => 'pdo_mysql',
            'port'     => 30005
        ];

        $adapter = $cfg->addConnection('test_array', $dbalArray);

        $this->assertInstanceOf(Connection::class, $adapter);
    }

    public function testAddConnectionWithExistingDBALConnection(): void {

        $cfg = new Config();

        $dbalArray = [
            'dbname'   => 'default',
            'user'     => 'root',
            'password' => 'root',
            'host'     => '192.168.64.100',
            'driver'   => 'pdo_mysql',
            'port'     => 30005
        ];

        $config = new Configuration();
        $connection = DriverManager::getConnection($dbalArray, $config);

        $adapter = $cfg->addConnection('test_dbalconnection', $connection);

        $this->assertInstanceOf(Connection::class, $adapter);
    }
}
