<?php declare(strict_types=1);

namespace Ether\Tests\Integration\Orm\Integer;

use DateTime;
use Exception;
use PHPUnit\Framework\TestCase;
use Ether\Tests\Integration\Orm\Integer\Entity\Post;
use Ether\Tests\Integration\Orm\Integer\Entity\Author;
use Ether\Tests\Integration\Orm\Integer\Entity\CustomMethods;

class EntityTest extends TestCase {

    private static $entities = ['Post', 'Author', 'CustomMethods'];

    public static function setupBeforeClass(): void {
        foreach (self::$entities as $entity) {
            testOrmMapper('Ether\Tests\Integration\Orm\Integer\Entity\\' . $entity)->migrate();
        }

        $authorMapper = testOrmMapper(Author::class);
        $author = $authorMapper->build([
            'id'       => 1,
            'email'    => 'example@example.com',
            'password' => 't00r',
            'is_admin' => false
        ]);
        $result = $authorMapper->insert($author);

        if ( ! $result) {
            throw new Exception('Unable to create author: ' . var_export($author->data(), true));
        }
    }

    public static function tearDownAfterClass(): void {
        foreach (self::$entities as $entity) {
            testOrmMapper('Ether\Tests\Integration\Orm\Integer\Entity\\' . $entity)->dropTable();
        }
    }

    public function testEntitySetDataProperties(): void {
        $mapper = testOrmMapper(Post::class);
        $post = $mapper->get();

        $date = new DateTime();

        // Set data
        $post->title = 'My Awesome Post';
        $post->body = '<p>Body</p>';
        $post->author_id = 1;
        $post->date_created = $date;

        $data = $post->data();

        ksort($data);

        $testData = [
            'id'           => null,
            'title'        => 'My Awesome Post',
            'body'         => '<p>Body</p>',
            'status'       => 0,
            'date_created' => $date,
            'data'         => null,
            'author_id'    => 1
        ];

        ksort($testData);

        $this->assertEquals($testData, $data);

        $this->assertNull($post->asdf);
    }

    public function testEntitySetDataConstruct(): void {
        testOrmMapper(Post::class);

        $date = new DateTime();

        $post = new Post([
            'title'        => 'My Awesome Post',
            'body'         => '<p>Body</p>',
            'author_id'    => 1,
            'date_created' => $date
        ]);

        $data = $post->data();
        ksort($data);

        $testData = [
            'id'           => null,
            'title'        => 'My Awesome Post',
            'body'         => '<p>Body</p>',
            'status'       => 0,
            'date_created' => null,
            'data'         => null,
            'author_id'    => 1,
            'date_created' => $date
        ];

        ksort($testData);

        $this->assertEquals($testData, $data);
    }

    public function testEntityErrors(): void {

        $post = new Post([
            'title' => 'My Awesome Post',
            'body'  => '<p>Body</p>'
        ]);

        $postErrors = [
            'title' => ['Title cannot contain the word awesome']
        ];

        // Has NO errors
        $this->assertNotTrue($post->hasErrors());

        // Set errors
        $post->errors($postErrors);

        // Has errors
        $this->assertTrue($post->hasErrors());

        // Full error array
        $this->assertEquals($postErrors, $post->errors());

        // Errors for one key only
        $this->assertEquals($postErrors['title'], $post->errors('title'));
    }

    public function testDataModified(): void {

        $data = [
            'title' => 'My Awesome Post 2',
            'body'  => '<p>Body 2</p>'
        ];

        $testData = [
            'id'           => null,
            'title'        => 'My Awesome Post',
            'body'         => '<p>Body</p>',
            'status'       => 0,
            'date_created' => null,
            'data'         => null,
            'author_id'    => 1
        ];

        // Set initial data
        $post = new Post($testData);

        $this->assertEquals($testData, $post->dataUnmodified());
        $this->assertEquals([], $post->dataModified());
        $this->assertFalse($post->isModified());

        $post->data($data);
        $this->assertEquals($data, $post->dataModified());
        $this->assertTrue($post->isModified('title'));
        $this->assertFalse($post->isModified('id'));
        $this->assertNull($post->isModified('asdf'));
        $this->assertTrue($post->isModified());
        $this->assertEquals($data['title'], $post->dataModified('title'));
        $this->assertEquals($testData['title'], $post->dataUnmodified('title'));
        $this->assertNull($post->dataModified('id'));
        $this->assertNull($post->dataModified('status'));
    }

    public function testDataNulls(): void {

        $data = [
            'title'     => 'A Post',
            'body'      => 'A Body',
            'status'    => 0,
            'author_id' => 1,
        ];

        $post = new Post($data);

        $post->status = null;
        $this->assertTrue($post->isModified('status'));

        $post->status = 1;
        $this->assertTrue($post->isModified('status'));

        $post->data(['status' => null]);
        $this->assertTrue($post->isModified('status'));

        $post->title = '';
        $this->assertTrue($post->isModified('title'));

        $this->title = null;
        $this->assertTrue($post->isModified('title'));

        $this->title = 'A Post';
        $post->data(['title' => null]);
        $this->assertTrue($post->isModified('title'));
    }

    public function testJsonArray(): void {

        $data = [
            'title'        => 'A Post',
            'body'         => 'A Body',
            'status'       => 0,
            'author_id'    => 1,
            'data'         => ['posts' => 'are cool', 'another field' => 'to serialize'],
            'date_created' => new DateTime()
        ];

        $post = new Post($data);
        $this->assertEquals($post->data, ['posts' => 'are cool', 'another field' => 'to serialize']);

        $mapper = testOrmMapper(Post::class);
        $mapper->save($post);

        $post = $mapper->get($post->id);
        $this->assertEquals($post->data, ['posts' => 'are cool', 'another field' => 'to serialize']);

        $post->data = 'asdf';
        $this->assertEquals($post->data, 'asdf');

        $mapper->save($post);
        $post = $mapper->get($post->id);
        $this->assertEquals($post->data, 'asdf');
    }

    public function testDataReferences(): void {

        $data = [
            'title'        => 'A Post',
            'body'         => 'A Body',
            'status'       => 0,
            'data'         => ['posts' => 'are cool', 'another field' => 'to serialize'],
            'date_created' => new DateTime()
        ];

        $post = new Post($data);

        // Reference test
        $title = $post->title;
        $this->assertEquals($title, $post->title);
        $title = 'asdf';
        $this->assertEquals('A Post', $post->title);
        $this->assertEquals('asdf', $title);

        // Property settting
        $post->date_created = null;
        $this->assertNull($post->date_created);

        $post->data['posts'] = 'are really cool';
        $this->assertEquals($post->data, ['posts' => 'are really cool', 'another field' => 'to serialize']);

        $data =& $post->data;
        $data['posts'] = 'are still cool';
        $this->assertEquals($post->data, ['posts' => 'are still cool', 'another field' => 'to serialize']);
    }

    public function testLocalVariablesAreNotByReference(): void {
        $data = [
            'title'        => 'A Post',
            'body'         => 'A Body',
            'status'       => 0,
            'data'         => ['posts' => 'are cool', 'another field' => 'to serialize'],
            'date_created' => new DateTime()
        ];

        $post = new Post($data);

        $title = $post->title;
        /** @noinspection SuspiciousAssignmentsInspection */
        $title = 'A Post Title';

        $this->assertNotEquals($title, $post->title);
    }

    public function testLocalArrayVariablesAreNotByReference(): void {
        $data = [
            'title'        => 'A Post',
            'body'         => 'A Body',
            'status'       => 0,
            'data'         => ['posts' => 'are cool', 'another field' => 'to serialize'],
            'date_created' => new DateTime()
        ];

        $post = new Post($data);

        $data = $post->data;
        $data['posts'] = 'are not by reference';

        $this->assertNotEquals($data, $post->data);
    }

    public function testJsonEncodeJsonSerializable(): void {
        $post = new Post([
            'title'        => 'A Post',
            'body'         => 'A Body',
            'status'       => 0,
            'data'         => ['posts' => 'are cool', 'another field' => 'to serialize'],
            'date_created' => new DateTime()
        ]);
        $json = json_encode($post);
        $data = json_decode($json, true);

        $this->assertEquals('A Post', $data['title']);
    }

    public function testToStringReturnsJson(): void {
        $post = new Post([
            'title'        => 'A Post',
            'body'         => 'A Body',
            'status'       => 0,
            'data'         => ['posts' => 'are cool', 'another field' => 'to serialize'],
            'date_created' => new DateTime()
        ]);
        $json = (string) $post;
        $data = json_decode($json, true);

        $this->assertEquals('A Post', $data['title']);
    }

    public function testEntityValueMutator(): void {

        $entity = new CustomMethods();

        $entity->test1 = 'test';

        $this->assertEquals('test_has_been_mutated_by_mutator', $entity->test1);
    }

    public function testEntityValueAccessor(): void {

        $entity = new CustomMethods();

        $entity->updated = 'updated';

        $this->assertEquals('updated_has_been_mutated_by_accessor', $entity->updated);
    }

    public function testEntityValueMutatorWithArrayLoad(): void {

        $entity = new CustomMethods(['updated2' => 'to_be_mutated']);

        $this->assertEquals('to_be_mutated_has_been_mutated_by_mutator', $entity->updated2);
    }

    public function testEntityValueAccessorWithArrayData(): void {

        $entity = new CustomMethods(['test1' => 'test']);

        $data = $entity->data();

        $this->assertEquals('test_has_been_mutated_by_mutator', $data['test1']);
    }

    public function testEntityValueMutatorShouldNotTriggerModified(): void {

        $mapper = testOrmMapper(CustomMethods::class);

        $entity = new CustomMethods([
            'test1' => 'test',
            'updated2' => 'copy'
        ]);

        $id = $mapper->save($entity);

        unset($entity);

        $entity = $mapper->get($id);

        $this->assertFalse($entity->isModified('test1'));
        $this->assertFalse($entity->isModified('test2'));
        $this->assertFalse($entity->isModified('test3'));
        $this->assertFalse($entity->isModified());
    }

    public function testGetPrimaryKeyField(): void {
        $entity = new CustomMethods([
            'test1' => 'test'
        ]);
        $this->assertEquals('id', $entity->primaryKeyField());
    }

    public function testGetPrimaryKeyFieldValue(): void {
        $entity = new CustomMethods([
            'test1' => 'test'
        ]);
        $this->assertEquals($entity->id, $entity->primaryKey());
    }

}
