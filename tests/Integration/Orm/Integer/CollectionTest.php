<?php declare(strict_types=1);

namespace Ether\Tests\Integration\Orm\Integer;

use PHPUnit\Framework\TestCase;
use Ether\Tests\Integration\Orm\Integer\Entity\Tag;

class CollectionTest extends TestCase {

    private static $entities = ['Tag'];

    public static function setupBeforeClass():void  {
        foreach (self::$entities as $entity) {
            testOrmMapper('Ether\Tests\Integration\Orm\Integer\Entity\\' . $entity)->migrate();
        }

        $tagCount = 3;

        // Create some tags
        $tags = array();
        $tagMapper = testOrmMapper(Tag::class);
        for ($i = 1; $i <= $tagCount; $i++) {
            $tags[] = $tagMapper->create([
                'name' => "Title {$i}"
            ]);
        }
    }

    public static function tearDownAfterClass(): void {
        foreach (self::$entities as $entity) {
            testOrmMapper('Ether\Tests\Integration\Orm\Integer\Entity\\' . $entity)->dropTable();
        }
    }

    public function testMergeIntersecting(): void {
        $mapper = testOrmMapper(Tag::class);

        // Fetch 3 entries
        $tags = $mapper->all()->execute();

        // Fetch 1 tag we already have
        $newTags = $mapper->where(['name' => 'Title 1'])->execute();

        // Check counts before merge
        $this->assertCount(3, $tags);
        $this->assertCount(1, $newTags);

        // Merge new tag in and expect count to remain the same (duplicate tag)
        $tags->merge($newTags);
        $this->assertCount(3, $tags);
    }

    public function testCollectionJsonSerialize(): void {
        $mapper = testOrmMapper(Tag::class);

        $tags = $mapper->all()->execute();

        $data = json_encode($tags->toArray());
        $json = json_encode($tags);

        $this->assertSame($data, $json);
    }

    public function testQueryCallsCollectionMethods(): void {
        $mapper = testOrmMapper(Tag::class);

        // Method on Ether\Database\Orm\Entity\Collection being called through Ether\Database\Orm\Query object
        $tagsArray = $mapper->all()->resultsIdentities();

        $this->assertSame([1, 2, 3], $tagsArray);
    }

    public function testQueryCallsCollectionMethodsWithArguments(): void {
        $mapper = testOrmMapper(Tag::class);

        // Method on Ether\Database\Orm\Entity\Collection being called through Ether\Database\Orm\Query object
        $matchingTag = $mapper->all()->filter(static function ($tag) {
            return $tag->id === 1;
        });

        $this->assertSame(1, $matchingTag[0]->id);
    }
}
