<?php declare(strict_types=1);

namespace Ether\Tests\Integration\Orm\Integer;

use Ether\Database\Orm\Query;
use PHPUnit\Framework\TestCase;
use Ether\Database\Orm\Mapper as OrmMapper;
use Ether\Tests\Integration\Orm\Integer\Entity\Event;
use Ether\Tests\Integration\Orm\Integer\Entity\Author;

class MapperTest extends TestCase {

    public function testGetGenericMapper(): void {
        $mapper = testOrmMapper(Author::class);
        $this->assertInstanceOf(OrmMapper::class, $mapper);
    }

    public function testGetCustomEntityMapper(): void {

        $mapper = testOrmMapper(Event::class);

        $this->assertInstanceOf((string) Event::mapper(), $mapper);

        $query = $mapper->testQuery();

        /** @noinspection UnnecessaryAssertionInspection */
        $this->assertInstanceOf(Query::class, $query);
    }
}
