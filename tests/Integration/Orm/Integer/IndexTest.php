<?php declare(strict_types=1);

namespace Ether\Tests\Integration\Orm\Integer;

use PHPUnit\Framework\TestCase;
use Ether\Tests\Integration\Orm\Integer\Entity\Zip;

class IndexTest extends TestCase {

    private static $entities = ['Zip'];

    public static function setupBeforeClass(): void {
        foreach (self::$entities as $entity) {
            testOrmMapper('Ether\Tests\Integration\Orm\Integer\Entity\\' . $entity)->migrate();
        }
    }

    public static function tearDownAfterClass(): void {
        foreach (self::$entities as $entity) {
            testOrmMapper('Ether\Tests\Integration\Orm\Integer\Entity\\' . $entity)->dropTable();
        }
    }

    public function testUniqueCompoundIndexDuplicateCausesValidationError(): void {

        $zipMapper = testOrmMapper(Zip::class);

        $data = [
            'code'  => '12345',
            'city'  => 'Testville',
            'state' => 'NY',
            'lat'   => 1,
            'lng'   => 2
        ];

        $zip1 = $zipMapper->create($data);
        $zip2 = $zipMapper->build($data);
        $zipMapper->save($zip2);

        $this->assertEmpty($zip1->errors());
        $this->assertNotEmpty($zip2->errors());
    }

    public function testUniqueCompoundIndexNoValidationErrorWhenDataDifferent(): void {

        $zipMapper = testOrmMapper(Zip::class);

        $data = [
            'code'  => '23456',
            'city'  => 'Testville',
            'state' => 'NY',
            'lat'   => 1,
            'lng'   => 2
        ];

        $zip1 = $zipMapper->create($data);

        // Make data slightly different on unique compound index
        $data2 = array_merge($data, ['city' => 'Testville2']);
        $zip2 = $zipMapper->create($data2);

        $this->assertEmpty($zip1->errors());
        $this->assertEmpty($zip2->errors());
    }
}
