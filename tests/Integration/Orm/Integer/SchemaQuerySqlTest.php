<?php /** @noinspection SqlResolve */
declare(strict_types=1);

namespace Ether\Tests\Integration\Orm\Integer;

use PHPUnit\Framework\TestCase;
use Ether\Tests\Integration\Orm\Integer\Entity\Schema\Test;

class SchemaQuerySqlTest extends TestCase {


    public static function setupBeforeClass(): void {

        testOrmMapper(Test::class)->migrate();

        for ($i = 1; $i <= 10; $i++) {
            testOrmMapper(Test::class)->insert([
                'index'  => $i % 5,
                'unique' => $i * 2
            ]);
        }
    }


    public static function tearDownAfterClass(): void {
        testOrmMapper(Test::class)->dropTable();
    }


    public function testFilteringWithWhere(): void {

        $mapper = testOrmMapper(Test::class);

        $query = $mapper->where(['unique' => 6])->noQuote();

        $this->assertEquals('SELECT * FROM test_schema_test WHERE test_schema_test.unique = ?', $query->toSql());
    }


    public function testOrderBy(): void {

        $mapper = testOrmMapper(Test::class);
        $query = $mapper->where(['index' => 2])->order(['unique' => 'ASC'])->noQuote();

        $this->assertStringContainsStringIgnoringCase('ORDER BY test_schema_test.unique ASC', $query->toSql());
        $this->assertEquals('SELECT * FROM test_schema_test WHERE test_schema_test.index = ? ORDER BY test_schema_test.unique ASC', $query->toSql());
    }


    public function testQuoting(): void {

        $mapper = testOrmMapper(Test::class);

        $expected = str_replace(
            '`',
            $mapper->connection()->getDatabasePlatform()->getIdentifierQuoteCharacter(),
            'SELECT * FROM `test_schema_test` WHERE `test_schema_test`.`index` >= ?'
        );

        $query = $mapper->where(['index >=' => 2])->toSql();

        $this->assertEquals($expected, $query);
    }
}
