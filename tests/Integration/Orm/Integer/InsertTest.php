<?php /** @noinspection ALL */
declare(strict_types=1);

namespace Ether\Tests\Integration\Orm\Integer;

use DateTime;
use Exception;
use PHPUnit\Framework\TestCase;
use Ether\Tests\Integration\Orm\Integer\Entity\Post;
use Ether\Tests\Integration\Orm\Integer\Entity\Event;
use Ether\Tests\Integration\Orm\Integer\Entity\Author;
use Ether\Tests\Integration\Orm\Integer\Entity\NoSerial;
use Ether\Database\Orm\Exception as OrmException;

class InsertTest extends TestCase {

    private static $entities = ['Post', 'Author', 'Event\Search', 'Event', 'NoSerial'];

    public static function setupBeforeClass(): void {

        foreach (self::$entities as $entity) {
            testOrmMapper('Ether\Tests\Integration\Orm\Integer\Entity\\' . $entity)->migrate();
        }

        $authorMapper = testOrmMapper(Author::class);
        $author = $authorMapper->build([
            'id'       => 1,
            'email'    => 'example@example.com',
            'password' => 't00r',
            'is_admin' => false
        ]);

        $result = $authorMapper->insert($author);

        if ( ! $result) {
            throw new Exception('Unable to create author: ' . var_export($author->data(), true));
        }
    }

    public static function tearDownAfterClass(): void {
        foreach (self::$entities as $entity) {
            testOrmMapper('Ether\Tests\Integration\Orm\Integer\Entity\\' . $entity)->dropTable();
        }
    }

    public function testInsertPostEntity(): void {

        $post = new Post();
        $mapper = testOrmMapper(Post::class);
        $post->title = 'Test Post';
        $post->body = "<p>This is a really awesome super-duper post.</p><p>It's really quite lovely.</p>";
        $post->date_created = new DateTime();
        $post->author_id = 1;

        $result = $mapper->insert($post);

        $this->assertNotFalse($result);
        $this->assertNotNull($post->id);
        $this->assertNotTrue($post->isModified());
    }

    public function testInsertPostEntitySequencesAreCorrect(): void {

        $mapper = testOrmMapper(Post::class);

        $post = new Post();
        $post->title = 'Test Post';
        $post->body = "<p>This is a really awesome super-duper post.</p><p>It's really quite lovely.</p>";
        $post->date_created = new DateTime();
        $post->author_id = 1;
        $mapper->insert($post);

        $post2 = new Post();
        $post2->title = 'Test Post';
        $post2->body = "<p>This is a really awesome super-duper post.</p><p>It's really quite lovely.</p>";
        $post2->date_created = new DateTime();
        $post2->author_id = 1;
        $mapper->insert($post2);

        // Ensure sequence is incrementing number
        $this->assertNotEquals($post->id, $post2->id);
    }

    public function testInsertPostArray(): void {

        $mapper = testOrmMapper(Post::class);

        $post = [
            'title'        => 'Test Post',
            'author_id'    => 1,
            'body'         => "<p>This is a really awesome super-duper post.</p><p>It's really quite lovely.</p>",
            'date_created' => new DateTime()
        ];

        $result = $mapper->insert($post); // returns inserted id

        $this->assertNotFalse($result);
    }

    public function testCreateInsertsEntity(): void {

        $mapper = testOrmMapper(Post::class);

        $post = [
            'title'        => 'Test Post 101',
            'author_id'    => 1,
            'body'         => "<p>Test Post 101</p><p>It's really quite lovely.</p>",
            'date_created' => new DateTime()
        ];

        $result = $mapper->create($post);

        $this->assertNotFalse($result);
    }

    public function testBuildReturnsEntityUnsaved(): void {

        $mapper = testOrmMapper(Post::class);

        $post = [
            'title'        => 'Test Post 100',
            'author_id'    => 1,
            'body'         => '<p>Test Post 100</p>',
            'date_created' => new DateTime()
        ];

        $result = $mapper->build($post);

        $this->assertInstanceOf(Post::class, $result);
        $this->assertTrue($result->isNew());
        $this->assertEquals(null, $result->id);
    }

    public function testCreateReturnsEntity(): void {

        $mapper = testOrmMapper(Post::class);

        $post = [
            'title'        => 'Test Post 101',
            'author_id'    => 1,
            'body'         => '<p>Test Post 101</p>',
            'date_created' => new DateTime()
        ];

        $result = $mapper->create($post);

        $this->assertInstanceOf(Post::class, $result);
        $this->assertFalse($result->isNew());
    }

    public function testInsertNewEntitySavesWithIdAlreadySet(): void {

        $mapper = testOrmMapper(Post::class);
        $post = new Post([
            'id'        => 2001,
            'title'     => 'Test Post 2001',
            'author_id' => 1,
            'body'      => '<p>Test Post 2001</p>'
        ]);

        $mapper->insert($post);

        $entity = $mapper->get($post->id);

        $this->assertInstanceOf(Post::class, $entity);
        $this->assertFalse($entity->isNew());
    }

    public function testInsertEventRunsValidation(): void {

        $mapper = testOrmMapper(Event::class);

        $event = new Event([
            'title'       => 'Test Event 1',
            'description' => 'Test Description',
            'date_start'  => new DateTime('+1 day')
        ]);

        $result = $mapper->insert($event);

        $this->assertFalse($result);
        $this->assertContains('Type is required', $event->errors('type'));
    }

    public function testSaveEventRunsAfterInsertHook(): void {

        $mapper = testOrmMapper(Event::class);

        $event = new Event([
            'title'       => 'Test Event 1',
            'description' => 'Test Description',
            'type'        => 'free',
            'date_start'  => new DateTime('+1 day')
        ]);

        $result = $mapper->save($event);

        $this->assertNotFalse($result);
    }

    public function testInsertEventRunsDateValidation(): void {

        $mapper = testOrmMapper(Event::class);

        $event = new Event([
            'title'       => 'Test Event 1',
            'description' => 'Test Description',
            'type'        => 'vip',
            'date_start'  => new DateTime('-1 day')
        ]);

        $result = $mapper->insert($event);

        $dsErrors = $event->errors('date_start');

        $this->assertFalse($result);
        $this->assertStringContainsStringIgnoringCase('Date Start must be date after', $dsErrors[0]);
    }

    public function testInsertEventRunsTypeOptionsValidation(): void {

        $mapper = testOrmMapper(Event::class);

        $event = new Event([
            'title'       => 'Test Event 1',
            'description' => 'Test Description',
            'type'        => 'invalid_value',
            'date_start'  => new DateTime('+1 day')
        ]);

        $result = $mapper->insert($event);

        $this->assertFalse($result);
        $this->assertEquals(['Type contains invalid value'], $event->errors('type'));
    }

    public function testCreateWithErrorsThrowsException(): void {

        $this->expectException(OrmException::class);

        $mapper = testOrmMapper(Event::class);

        $mapper->create([
            'title'       => 'Test Event 1',
            'description' => 'Test Description',
            'date_start'  => new DateTime('+1 day')
        ]);
    }

    public function testInsertWithoutAutoIncrement(): void {

        $mapper = testOrmMapper(NoSerial::class);

        $entity = $mapper->build([
            'id'   => 101,
            'data' => 'Testing insert'
        ]);

        $result = $mapper->insert($entity);

        $this->assertEquals(101, $result);
    }

    public function testInsertWithoutAutoIncrementWithoutPKValueHasValidationError(): void {

        $mapper = testOrmMapper(NoSerial::class);

        $entity = $mapper->build([
            'data' => 'Testing insert'
        ]);

        $result = $mapper->insert($entity);

        $this->assertEquals(false, $result);
        $this->assertCount(1, $entity->errors('id'));
    }
}
