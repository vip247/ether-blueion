<?php declare(strict_types=1);

namespace Ether\Tests\Integration\Orm\Integer;

use DateTime;
use Exception;
use PHPUnit\Framework\TestCase;
use Ether\Database\Orm\Relation\HasMany;
use Ether\Tests\Integration\Orm\Integer\Entity\Post;
use Ether\Tests\Integration\Orm\Integer\Entity\Legacy;
use Ether\Tests\Integration\Orm\Integer\Entity\Author;
use Ether\Tests\Integration\Orm\Common\DriverSpecific;
use Ether\Tests\Integration\Orm\Integer\Entity\PolymorphicComment;

class FieldAliasTest extends TestCase {

    public static $legacyTable;

    private static $entities = ['PolymorphicComment', 'Legacy', 'Post', 'Author'];

    public static function setupBeforeClass(): void {

        self::$legacyTable = new Legacy();

        foreach (self::$entities as $entity) {
            testOrmMapper('Ether\Tests\Integration\Orm\Integer\Entity\\' . $entity)->migrate();
        }

        $authorMapper = testOrmMapper(Author::class);
        $author = $authorMapper->build([
            'id'       => 1,
            'email'    => 'example@example.com',
            'password' => 't00r',
            'is_admin' => false
        ]);

        $result = $authorMapper->insert($author);

        if ( ! $result) {
            throw new Exception('Unable to create author: ' . var_export($author->data(), true));
        }

        $postMapper = testOrmMapper(Post::class);
        $post = $postMapper->build([
            'title'        => 'title',
            'body'         => '<p>body</p>',
            'status'       => 1,
            'date_created' => new DateTime(),
            'author_id'    => 1
        ]);
        $result = $postMapper->insert($post);

        if ( ! $result) {
            throw new Exception('Unable to create post: ' . var_export($post->data(), true));
        }
    }

    public static function tearDownAfterClass(): void {
        foreach (self::$entities as $entity) {
            testOrmMapper('Ether\Tests\Integration\Orm\Integer\Entity\\' . $entity)->dropTable();
        }
    }

    public function testLegacySelectFieldsAreAliases(): void {
        $mapper = testOrmMapper(Legacy::class);

        $query = $mapper->select()->noQuote()->where(['number' => 2, 'name' => 'legacy_crud']);
        $this->assertEquals('SELECT * FROM test_legacy WHERE test_legacy.' . self::$legacyTable->getNumberFieldColumnName() . ' = ? AND test_legacy.' . self::$legacyTable->getNameFieldColumnName() . ' = ?', $query->toSql());
    }

    public function testLegacyOrderBy(): void {
        $mapper = testOrmMapper(Legacy::class);
        $query = $mapper->where(['number' => 2])->order(['date_created' => 'ASC'])->noQuote();
        $this->assertStringContainsStringIgnoringCase('ORDER BY test_legacy.' . self::$legacyTable->getDateCreatedColumnName() . ' ASC', $query->toSql());
    }

    public function testLegacyOrderByFunction(): void {

        $mapper = testOrmMapper(Legacy::class);
        $query = $mapper->where(['number' => 2])->order(['TRIM(name)' => 'ASC'])->noQuote();

        $this->assertStringContainsStringIgnoringCase('ORDER BY TRIM(test_legacy.' . self::$legacyTable->getNameFieldColumnName() . ') ASC', $query->toSql());
    }

    public function testLegacyOrderByComplexFunction(): void {

        $mapper = testOrmMapper(Legacy::class);

        if ( ! DriverSpecific::getWeekFunction($mapper)) {
            $this->markTestSkipped('This test is not supported with the current driver.');
        }

        $query = $mapper->where(['number' => 2])->order([DriverSpecific::getWeekFunction($mapper, 'date_created') => 'ASC'])->noQuote();

        $this->assertStringContainsStringIgnoringCase('ORDER BY ' . DriverSpecific::getWeekFunction($mapper, 'test_legacy.' . self::$legacyTable->getDateCreatedColumnName()) . ' ASC', $query->toSql());
    }

    public function testLegacyGroupBy(): void {
        $mapper = testOrmMapper(Legacy::class);

        $query = $mapper->where(['name' => 'test_group'])->group(['id'])->noQuote();

        $this->assertEquals('SELECT * FROM test_legacy WHERE test_legacy.' . self::$legacyTable->getNameFieldColumnName() . ' = ? GROUP BY test_legacy.' . self::$legacyTable->getIdFieldColumnName(), $query->toSql());
    }

    public function testLegacyGroupByFunction(): void {

        $mapper = testOrmMapper(Legacy::class);

        $query = $mapper->where(['number' => 2])->group(['TRIM(name)'])->noQuote();

        $this->assertEquals('SELECT * FROM test_legacy WHERE test_legacy.' . self::$legacyTable->getNumberFieldColumnName() . ' = ? GROUP BY TRIM(test_legacy.' . self::$legacyTable->getNameFieldColumnName() . ')', $query->toSql());
    }

    public function testLegacyInsert(): void {

        $legacy = new Legacy();
        $legacy->name = 'Something Here';
        $legacy->number = 5;

        $mapper = testOrmMapper(Legacy::class);
        $result = $mapper->save($legacy);

        $this->assertEquals(1, $result);
    }

    public function testLegacyEntityToArrayUsesFieldMappings(): void {

        $mapper = testOrmMapper(Legacy::class);

        $legacy = new Legacy();
        $legacy->name = 'Something Here';
        $legacy->number = 5;

        $mapper->save($legacy);

        $savedLegacyItem = $mapper->first();

        $data = $savedLegacyItem->toArray();

        $this->assertEquals($data['name'], 'Something Here');
        $this->assertEquals($data['number'], 5);
    }

    public function testLegacyUpdate(): void {

        $mapper = testOrmMapper(Legacy::class);

        $legacy = new Legacy();
        $legacy->name = 'Something Here';
        $legacy->number = 5;

        $id = $mapper->save($legacy);

        $legacyToUpdate = $mapper->get($id);

        $legacyToUpdate->name = 'Something ELSE Here';
        $legacyToUpdate->number = 6;

        $mapper->update($legacyToUpdate);

        $updatedLegacy = $mapper->get($id);

        $this->assertEquals('Something ELSE Here', $updatedLegacy->name);
    }

    public function testLegacyEntityFieldMapping(): void {

        $mapper = testOrmMapper(Legacy::class);

        $legacy = new Legacy();
        $legacy->name = 'Something Here 6';
        $legacy->number = 5;

        $id = $mapper->save($legacy);

        $savedLegacyItem = $mapper->get($id);

        $this->assertEquals($legacy->name, $savedLegacyItem->name);
        $this->assertEquals($legacy->number, $savedLegacyItem->number);
    }

    public function testLegacyRelations(): void {

        $mapper = testOrmMapper(Legacy::class);

        $legacy = new Legacy();
        $legacy->name = 'Something Here 2';
        $legacy->number = 5;

        $id = $mapper->save($legacy);

        /** @var PolymorphicComment $commentMapper */
        $commentMapper = testOrmMapper(PolymorphicComment::class);

        $comment = new PolymorphicComment([
            'item_id'   => $id,
            'item_type' => 'legacy',
            'name'      => 'Testy McTesterpants',
            'email'     => 'tester@chester.com',
            'body'      => '<p>Comment Text</p>'
        ]);

        $commentMapper->save($comment);

        $updatedLegacy = $mapper->get($id);

        $this->assertInstanceOf(HasMany::class, $updatedLegacy->polymorphic_comments);
        $this->assertCount(1, $updatedLegacy->polymorphic_comments);
    }

    public function testFieldAliasMapping(): void {

        $testId = 2545;
        $testArray = ['testKey' => 'testValue'];

        $legacy = new Legacy();
        $legacy->id = $testId;
        $legacy->name = 'Something Here';
        $legacy->number = 5;
        $legacy->array = $testArray;
        $legacy->arrayAliased = $testArray;

        $mapper = testOrmMapper(Legacy::class);
        $mapper->save($legacy);

        unset($legacy);
        $legacy = $mapper->get($testId);

        $this->assertEquals($testArray, $legacy->array);
        $this->assertEquals($testArray, $legacy->arrayAliased);
    }
}
