<?php declare(strict_types=1);

namespace Ether\Tests\Integration\Orm\Integer\Entity;

use Ether\Database\Orm\Entity;

/**
 * Entity with no serial/autoincrement
 */
class NotNullOverride extends Entity {

    protected static $table = 'test_notnulloverride';

    public static function fields(): array {
        return [
            'id'    => ['type' => 'integer', 'primary' => true],
            'data1' => ['type' => 'string', 'required' => true],
            'data2' => ['type' => 'string', 'required' => true, 'notnull' => true],
            'data3' => ['type' => 'string', 'required' => true, 'notnull' => false]
        ];
    }
}
