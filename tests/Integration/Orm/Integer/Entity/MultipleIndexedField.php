<?php declare(strict_types=1);

namespace Ether\Tests\Integration\Orm\Integer\Entity;

use Ether\Database\Orm\Entity;

class MultipleIndexedField extends Entity {

    protected static $table = 'test_multipleindexedfield';

    public static function fields(): array {
        return [
            'id'           => ['type' => 'integer', 'primary' => true],
            'companyGroup' => ['type' => 'integer', 'required' => false, 'index' => true],
            'company'      => ['type' => 'integer', 'required' => false, 'index' => [true, 'employee']],
            'user'         => ['type' => 'string', 'required' => false, 'index' => [true, 'employee']],
        ];
    }
}
