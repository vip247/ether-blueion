<?php declare(strict_types=1);

namespace Ether\Tests\Integration\Orm\Integer\Entity;

use Ether\Database\Orm\Entity;
use Ether\Database\Orm\MapperInterface;
use Ether\Database\Orm\EntityInterface;

class RecursiveEntity extends Entity {

    protected static $table = 'test_recursive';

    public static function fields(): array {
        return [
            'id'           => [
                'type' => 'integer', 'primary' => true, 'autoincrement' => true,
                'form' => false
            ],
            'priority'     => [
                'type' => 'integer', 'index' => true,
                'form' => false,
            ],
            'status'       => [
                'type'    => 'smallint', 'required' => true, 'default' => 1,
                'options' => [1, 0],
            ],
            'date_publish' => [
                'type' => 'date',
            ],
            'name'         => [
                'type'       => 'string', 'required' => true, 'label' => true,
                'validation' => ['lengthMax' => 255],
            ],
            'description'  => [
                'type' => 'text'
            ],
            'parent_id'    => [
                'type' => 'integer', 'index' => true,
            ],
            'siblingId'    => [
                'type' => 'integer', 'index' => true, 'column' => 'sibling_id'
            ],
        ];
    }

    public static function relations(MapperInterface $mapper, EntityInterface $entity): array {
        return [
            'children'   => $mapper->hasMany($entity, __CLASS__, 'parent_id'),
            'parent'     => $mapper->belongsTo($entity, __CLASS__, 'parent_id'),
            'my_sibling' => $mapper->belongsTo($entity, __CLASS__, 'siblingId'),
            'sibling'    => $mapper->hasOne($entity, __CLASS__, 'siblingId')
        ];
    }
}
