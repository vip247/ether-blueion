<?php declare(strict_types=1);

namespace Ether\Tests\Integration\Orm\Integer\Entity;

use Ether\Database\Orm\Entity;
use Ether\Database\Orm\MapperInterface;
use Ether\Database\Orm\EntityInterface;

class PostTag extends Entity {

    protected static $table = 'test_posttags';

    public static function fields(): array {
        return [
            'id'      => ['type' => 'integer', 'primary' => true, 'autoincrement' => true],
            'tag_id'  => ['type' => 'integer', 'required' => true, 'unique' => 'post_tag'],
            'post_id' => ['type' => 'integer', 'required' => true, 'unique' => 'post_tag'],
            'random'  => ['type' => 'string'] // Totally unnecessary, but makes testing upserts easy
        ];
    }

    /**
     * @param MapperInterface $mapper
     * @param EntityInterface $entity
     *
     * @return array
     */
    public static function relations(MapperInterface $mapper, EntityInterface $entity): array {
        return [
            'post' => $mapper->belongsTo($entity, Post::class, 'post_id'),
            'tag'  => $mapper->belongsTo($entity, Tag::class, 'tag_id')
        ];
    }
}
