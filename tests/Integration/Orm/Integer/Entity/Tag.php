<?php declare(strict_types=1);

namespace Ether\Tests\Integration\Orm\Integer\Entity;

use Ether\Database\Orm\Entity;
use Ether\Database\Orm\MapperInterface;
use Ether\Database\Orm\EntityInterface;

class Tag extends Entity {

    protected static $table = 'test_tags';

    public static function fields(): array {
        return [
            'id'   => ['type' => 'integer', 'primary' => true, 'autoincrement' => true],
            'name' => ['type' => 'string', 'required' => true]
        ];
    }

    /**
     * @param MapperInterface $mapper
     * @param EntityInterface $entity
     *
     * @return array
     */
    public static function relations(MapperInterface $mapper, EntityInterface $entity): array {
        return [
            'posts' => $mapper->hasManyThrough($entity, Post::class, PostTag::class, 'tag_id', 'post_id')
        ];
    }
}
