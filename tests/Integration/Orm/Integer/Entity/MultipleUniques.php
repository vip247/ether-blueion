<?php declare(strict_types=1);

namespace Ether\Tests\Integration\Orm\Integer\Entity;

use Ether\Database\Orm\Entity;

/**
 * Entity with a mix of default and value definitions in fields
 */
class MultipleUniques extends Entity {
    protected static $table = 'test_multipleuniques';

    public static function fields(): array {
        return [
            'id'    => ['type' => 'integer', 'autoincrement' => true, 'primary' => true],
            'data1' => ['type' => 'string', 'required' => true, 'unique' => ['uniq1', 'uniq2']],
            'data2' => ['type' => 'integer', 'required' => true, 'unique' => 'uniq1'],
            'data3' => ['type' => 'string', 'required' => true, 'unique' => ['uniq2']]
        ];
    }
}
