<?php declare(strict_types=1);

namespace Ether\Tests\Integration\Orm\Integer\Entity;

use Ether\Database\Orm\Entity;

/**
 * Entity with no serial/autoincrement
 */
class NoSerial extends Entity {
    protected static $table = 'test_noserial';

    public static function fields(): array {
        return [
            'id'   => ['type' => 'integer', 'primary' => true],
            'data' => ['type' => 'string', 'required' => true]
        ];
    }
}
