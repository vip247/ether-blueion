<?php declare(strict_types=1);

namespace Ether\Tests\Integration\Orm\Integer\Entity;

use Ether\Database\Orm\Entity;
use Ether\Tests\Integration\Orm\Type\Encrypted;
use Doctrine\DBAL\Types\Type as DBALType;

class Setting extends Entity {

    protected static $table = 'test_settings';

    public static function fields(): array {
        return [
            'id'     => ['type' => 'integer', 'primary' => true, 'autoincrement' => true],
            'skey'   => ['type' => 'string', 'required' => true, 'unique' => true],
            'svalue' => ['type' => 'encrypted', 'required' => true]
        ];
    }
}
