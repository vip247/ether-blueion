<?php declare(strict_types=1);

namespace Ether\Tests\Integration\Orm\Integer\Entity;

use Ether\Database\Orm\Entity;

/**
 * Exists solely for the purpose of testing custom types
 */
class Type extends Entity {

    protected static $_datasource = 'test_types';

    // Declared 'public static' here so they can be modified by tests - this is for TESTING ONLY
    public static $_fields = [
        'id'           => ['type' => 'integer', 'primary' => true, 'serial' => true],
        'serialized'   => ['type' => 'json_array'],
        'date_created' => ['type' => 'datetime']
    ];

    public static function fields(): array {
        return self::$_fields;
    }
}
