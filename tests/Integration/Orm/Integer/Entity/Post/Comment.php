<?php declare(strict_types=1);

namespace Ether\Tests\Integration\Orm\Integer\Entity\Post;

use DateTime;
use Ether\Database\Orm\Query;
use Ether\Database\Orm\Entity;
use Ether\Database\Orm\MapperInterface;
use Ether\Database\Orm\EntityInterface;
use Ether\Tests\Integration\Orm\Integer\Entity\Post;

class Comment extends Entity {

    protected static $table = 'test_post_comments';

    public static function fields(): array {
        return [
            'id'           => ['type' => 'integer', 'primary' => true, 'autoincrement' => true],
            'post_id'      => ['type' => 'integer', 'index' => true, 'required' => true],
            'name'         => ['type' => 'string', 'required' => true],
            'email'        => ['type' => 'string', 'required' => true],
            'body'         => ['type' => 'text', 'required' => true],
            'date_created' => ['type' => 'datetime']
        ];
    }

    public static function scopes(): array {
        return [
            'yesterday' => static function (Query $query) {
                return $query->where(['date_created :gt' => new DateTime('yesterday'), 'date_created :lt' => new DateTime('today')]);
            }
        ];
    }


    public static function relations(MapperInterface $mapper, EntityInterface $entity): array {
        return [
            'post' => $mapper->belongsTo($entity, Post::class, 'post_id')
        ];
    }
}
