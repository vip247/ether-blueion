<?php declare(strict_types=1);

namespace Ether\Tests\Integration\Orm\Integer\Entity;

use DateTime;
use Ether\Database\Orm\Entity;
use Ether\Database\Orm\MapperInterface;
use Ether\Database\Orm\EntityInterface;

class Legacy extends Entity {

    protected static $table = 'test_legacy';

    public static function fields(): array {
        return [
            'id'           => ['type' => 'integer', 'autoincrement' => true, 'primary' => true, 'column' => self::getIdFieldColumnName()],
            'name'         => ['type' => 'string', 'required' => true, 'column' => self::getNameFieldColumnName()],
            'number'       => ['type' => 'integer', 'required' => true, 'column' => self::getNumberFieldColumnName()],
            'date_created' => ['type' => 'datetime', 'value' => new DateTime(), 'column' => self::getDateCreatedColumnName()],
            'array'        => ['type' => 'array', 'required' => false],
            'arrayAliased' => ['type' => 'array', 'required' => false, 'column' => 'array_aliased']
        ];
    }

    public static function relations(MapperInterface $mapper, EntityInterface $entity): array {
        return [
            'polymorphic_comments' => $mapper->hasMany($entity, PolymorphicComment::class, 'item_id')->where(['item_type' => 'legacy'])
        ];
    }

    /**
     * Helpers for field/column names - methods with public access so we can avoid duplication in tests
     */
    public static function getIdFieldColumnName(): string {
        return 'obnoxiouslyObtuse_IdentityColumn';
    }

    public static function getNameFieldColumnName(): string {
        return 'string_54_LegacyDB_x8';
    }

    public static function getNumberFieldColumnName(): string {
        return 'xbf86_haikusInTheDark';
    }

    public static function getDateCreatedColumnName(): string {
        return 'dtCreatedAt';
    }
}
