<?php declare(strict_types=1);

namespace Ether\Tests\Integration\Orm\Integer\Entity;

use Ether\Database\Orm\Entity;

/**
 * Entity with a mix of default and value definitions in fields
 */
class DefaultValue extends Entity {

    protected static $table = 'test_defaultvalue';

    public static function fields(): array {
        return [
            'id'    => ['type' => 'integer', 'autoincrement' => true, 'primary' => true],
            'data1' => ['type' => 'integer', 'value' => 2],
            'data2' => ['type' => 'integer', 'default' => 3],
            'data3' => ['type' => 'integer', 'default' => 4, 'value' => 5],
        ];
    }
}
