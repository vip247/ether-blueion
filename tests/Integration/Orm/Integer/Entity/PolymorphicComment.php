<?php declare(strict_types=1);

namespace Ether\Tests\Integration\Orm\Integer\Entity;

use Ether\Database\Orm\Entity;
use Ether\Database\Orm\MapperInterface;
use Ether\Database\Orm\EntityInterface;

class PolymorphicComment extends Entity {

    protected static $table = 'test_polymorphic_comments';

    public static function fields(): array {
        return [
            'id'           => ['type' => 'integer', 'primary' => true, 'autoincrement' => true],
            'item_type'    => ['type' => 'string', 'index' => 'item_type_id', 'required' => true, 'value' => 'post'],
            'item_id'      => ['type' => 'integer', 'index' => 'item_type_id', 'required' => true],
            'name'         => ['type' => 'string', 'required' => true],
            'email'        => ['type' => 'string', 'required' => true],
            'body'         => ['type' => 'text', 'required' => true],
            'date_created' => ['type' => 'datetime']
        ];
    }

    /**
     * @param MapperInterface $mapper
     * @param EntityInterface $entity
     *
     * @return array
     */
    public static function relations(MapperInterface $mapper, EntityInterface $entity): array {
        return [
            'item' => $mapper->hasOne($entity, 'Ether\Tests\Integration\Orm\Integer\Entity\\' . ucwords($entity->item_type), 'item_id')
        ];
    }
}
