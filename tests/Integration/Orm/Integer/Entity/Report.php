<?php declare(strict_types=1);

namespace Ether\Tests\Integration\Orm\Integer\Entity;

use DateTime;
use Ether\Database\Orm\Entity;

class Report extends Entity {

    protected static $table = 'test_reports';

    public static function fields(): array {
        return [
            'id'     => ['type' => 'integer', 'autoincrement' => true, 'primary' => true],
            'date'   => ['type' => 'date', 'value' => new DateTime(), 'required' => true, 'unique' => true],
            'result' => ['type' => 'json_array', 'required' => true]
        ];
    }
}
