<?php declare(strict_types=1);

namespace Ether\Tests\Integration\Orm\Integer\Entity;

use Ether\Database\Orm\Entity;

class CustomMethods extends Entity {

    protected static $table = 'test_custom_methods';

    public static function fields(): array {
        return [
            'id'      => ['type' => 'integer', 'primary' => true, 'autoincrement' => true],
            'test1'   => ['type' => 'text'],
            'test2'   => ['type' => 'text'],
            'test3'   => ['type' => 'text'],
            'updated' => ['type' => 'text'],
            'updated2' => ['type' => 'text']
        ];
    }

    // Mutator
    public function setTest1(string $value) {
        $this->_data['test1'] = $value . '_has_been_mutated_by_mutator';
    }

    // Mutator
    public function setUpdated2(string $value) {
        $this->_data['updated2'] = $value . '_has_been_mutated_by_mutator';
    }

    // Accessor
    public function getUpdated(string $value): string {
        return $value . '_has_been_mutated_by_accessor';
    }
}
