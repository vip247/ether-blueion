<?php declare(strict_types=1);

namespace Ether\Tests\Integration\Orm\Integer\Entity\Schema;

use Ether\Database\Orm\Entity;

class Test extends Entity {
    protected static $table = 'test_schema_test';

    public static function fields(): array {
        return [
            'id'     => ['type' => 'integer', 'autoincrement' => true, 'primary' => true],
            'unique' => ['type' => 'integer', 'default' => 0, 'unique' => true],
            'index'  => ['type' => 'integer', 'default' => 0, 'index' => true]
        ];
    }
}
