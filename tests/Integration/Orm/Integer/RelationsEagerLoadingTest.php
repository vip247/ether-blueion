<?php declare(strict_types=1);

namespace Ether\Tests\Integration\Orm\Integer;

use DateTime;
use PHPUnit\Framework\TestCase;
use Doctrine\DBAL\DBALException;
use Ether\Database\Orm\Exception;
use Doctrine\DBAL\Logging\DebugStack;
use Ether\Tests\Integration\Orm\Integer\Entity\Tag;
use Ether\Tests\Integration\Orm\Integer\Entity\Post;
use Ether\Tests\Integration\Orm\Integer\Entity\Event;
use Ether\Tests\Integration\Orm\Integer\Entity\Author;
use Ether\Tests\Integration\Orm\Integer\Entity\PostTag;
use Ether\Tests\Integration\Orm\Integer\Entity\Post\Comment;

class RelationsEagerLoadingTest extends TestCase {

    /**
     * @var array
     */
    private static $entities = ['PostTag', 'Post\Comment', 'Post', 'Tag', 'Author', 'Event\Search', 'Event'];


    public static function setupBeforeClass(): void {

        foreach (self::$entities as $entity) {
            testOrmMapper('Ether\Tests\Integration\Orm\Integer\Entity\\' . $entity)->migrate();
        }

        $authorMapper = testOrmMapper(Author::class);
        $author = $authorMapper->create([
            'email'    => 'test@test.com',
            'password' => 'password',
            'is_admin' => false
        ]);

        $posts = [];
        $postsCount = 3;
        $mapper = testOrmMapper(Post::class);
        for ($i = 1; $i <= $postsCount; $i++) {
            $posts[] = $mapper->create([
                'title'     => "Eager Loading Test Post $i",
                'body'      => "Eager Loading Test Post Content Here $i",
                'author_id' => $author->id
            ]);
        }

        foreach ($posts as $post) {
            $commentCount = 3;
            $commentMapper = testOrmMapper(Comment::class);
            for ($i = 1; $i <= $commentCount; $i++) {
                $commentMapper->create([
                    'post_id' => $post->id,
                    'name'    => 'Testy McTester',
                    'email'   => 'test@test.com',
                    'body'    => "This is a test comment $i. Yay!"
                ]);
            }
        }

        $tags = [];
        $tagCount = 3;
        $tagMapper = testOrmMapper(Tag::class);
        for ($i = 1; $i <= $tagCount; $i++) {
            $tags[] = $tagMapper->create([
                'name' => "Tag {$i}"
            ]);
        }

        $postTagMapper = testOrmMapper(PostTag::class);
        foreach ($posts as $post) {
            foreach ($tags as $tag) {
                $postTagMapper->create([
                    'post_id' => $post->id,
                    'tag_id'  => $tag->id
                ]);
            }
        }

        $eventMapper = testOrmMapper(Event::class);

        $eventMapper->create([
            'title'       => 'Eager Load Test Event',
            'description' => 'some test eager loading description',
            'type'        => 'free',
            'date_start'  => new DateTime('+1 second')
        ]);

        $eventMapper->create([
            'title'       => 'Eager Load Test Event 2',
            'description' => 'some test eager loading description 2',
            'type'        => 'free',
            'date_start'  => new DateTime('+1 second')
        ]);
    }


    public static function tearDownAfterClass(): void {
        foreach (self::$entities as $entity) {
            testOrmMapper('Ether\Tests\Integration\Orm\Integer\Entity\\' . $entity)->dropTable();
        }
    }


    public function testEagerLoadHasMany(): void {

        $mapper = testOrmMapper(Post::class);

        $logger = new DebugStack();
        $mapper->connection()->getConfiguration()->setSQLLogger($logger);

        $startCount = count($logger->queries);

        $posts = $mapper->all()->with('comments');
        foreach ($posts as $post) {
            foreach ($post->comments as $comment) {
                // Do nothing - just had to iterate to execute the queries
                $this->assertEquals($post->id, $comment->post_id);
            }
        }

        $endCount = count($logger->queries);

        // Eager-loaded relation should be only 2 queries
        $this->assertEquals(($startCount + 2), $endCount);
    }


    public function testEagerLoadHasManyCounts(): void {

        $mapper = testOrmMapper(Post::class);

        // Set SQL logger
        $logger = new DebugStack();
        $mapper->connection()->getConfiguration()->setSQLLogger($logger);

        $startCount = count($logger->queries);

        $posts = $mapper->all()->order(['date_created' => 'DESC'])->with(['comments']);
        foreach ($posts as $post) {
            $this->assertCount(3, $post->comments);
        }

        $endCount = count($logger->queries);

        // Eager-loaded relation should be only 2 queries
        $this->assertEquals(($startCount + 2), $endCount);
    }


    public function testEagerLoadBelongsTo(): void {

        $mapper = testOrmMapper(Post::class);

        // Set SQL logger
        $logger = new DebugStack();
        $mapper->connection()->getConfiguration()->setSQLLogger($logger);

        $startCount = count($logger->queries);

        $posts = $mapper->all()->with('author');
        foreach ($posts as $post) {
            $this->assertEquals($post->author_id, $post->author->id);
        }
        $endCount = count($logger->queries);

        // Eager-loaded relation should be only 2 queries
        $this->assertEquals($startCount + 2, $endCount);
    }


    public function testEagerLoadHasOne(): void {

        $mapper = testOrmMapper(Event::class);

        // Set SQL logger
        $logger = new DebugStack();
        $mapper->connection()->getConfiguration()->setSQLLogger($logger);

        $startCount = count($logger->queries);

        $events = $mapper->all()->with('search');
        foreach ($events as $event) {
            $this->assertEquals($event->id, $event->search->event_id);
        }
        $endCount = count($logger->queries);

        // Eager-loaded relation should be only 2 queries
        $this->assertEquals(($startCount + 2), $endCount);
    }


    public function testEagerLoadHasManyThrough(): void {

        $mapper = testOrmMapper(Post::class);

        // Set SQL logger
        $logger = new DebugStack();
        $mapper->connection()->getConfiguration()->setSQLLogger($logger);

        $startCount = count($logger->queries);

        $posts = $mapper->all()->with('tags');
        foreach ($posts as $post) {
            foreach ($post->tags as $tags) { // @TODO: do this in a better way
                // Do nothing - just had to iterate to execute the queries
            }
            $this->assertCount(3, $post->tags);
        }
        $endCount = count($logger->queries);

        // Eager-loaded HasManyThrough relation should be only 3 queries
        // (1 query more than other relations, for the join table)
        $this->assertEquals(($startCount + 3), $endCount);
    }


    public function testEagerLoadHasManyThroughToArray(): void {

        $mapper = testOrmMapper(Post::class);

        $post = $mapper->all()->with('tags')->first();
        $result = $post->toArray();

        $this->assertIsArray($result['tags']);
    }


    public function testEagerLoadHasManyThroughToArrayShouldNotLoadRelation(): void {

        $mapper = testOrmMapper(Post::class);

        $post = $mapper->all()->first();

        $result = $post->toArray();

        $this->assertFalse(isset($result['tags']));
    }


    public function testEagerLoadBelongsToArray(): void {

        $mapper = testOrmMapper(Post::class);

        $posts = $mapper->all()->with('author')->first();

        $result = $posts->toArray();

        $this->assertIsArray($result['author']);
    }


    public function testEagerLoadBelongsToArrayShouldNotLoadRelation(): void {

        $mapper = testOrmMapper(Post::class);

        $posts = $mapper->all()->first();

        $result = $posts->toArray();

        $this->assertFalse(isset($result['author']));
    }


    public function testEagerLoadHasOneToArray(): void {

        $mapper = testOrmMapper(Event::class);

        $events = $mapper->all()->with('search')->first();

        $result = $events->toArray();

        $this->assertIsArray($result['search']);
    }


    public function testEagerLoadHasOneToArrayShouldNotLoadRelation(): void {

        $mapper = testOrmMapper(Event::class);

        $events = $mapper->all()->first();

        $result = $events->toArray();

        $this->assertFalse(isset($result['search']));
    }


    public function testEagerLoadingEntityDepthIsLimitedToOneLevel(): void {

        // Retrieve a post
        $postMapper = testOrmMapper(Post::class);
        $post = $postMapper->get(1);

        // And its comments
        $comments = $post->comments->execute();
        $post->relation('comments', $comments);

        $comment_mapper = testOrmMapper(Comment::class);

        $comment = $comment_mapper->create([
            'post_id' => 1,
            'name'    => 'Testy McTester',
            'email'   => 'test@test.com',
            'body'    => 'This is a test comment 4. Yay!'
        ]);
        $comment->relation('post', $post);
        $comments->add($comment);

        $result = $post->toArray();

        $this->assertFalse(isset($result['comments'][0]['post']['comments']));
        $this->assertCount(4, $result['comments']);
    }
}
