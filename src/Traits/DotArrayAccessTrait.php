<?php declare(strict_types=1);

namespace Ether\Traits;

trait DotArrayAccessTrait {

    /**
     *
     * @param $key
     *
     * @param null $array
     *
     * @return null|mixed
     *
     */
    public function get($key, $array = null) {

        if ($key === null) {
            return $this->items;
        }

        if ($array === null) {
            $array = $this->items;
        }

		$result = null;
		foreach (explode('.', $key) as $segment) {

			if (( $result !== null && ! array_key_exists($segment, $result)) || ($result === null && ! array_key_exists($segment, $array))) {
				return null;
			}

			if ($result === null) {
				$result = $array[$segment];
			} else {
				$result = $result[$segment];
			}
		}

		return $result;
	}
}
