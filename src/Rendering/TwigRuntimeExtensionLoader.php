<?php declare(strict_types=1);

namespace Ether\Rendering;

use ReflectionException;
use Ether\Application as Ether;
use Ether\Container\Exceptions\NotFoundException;
use Ether\Container\Exceptions\ContainerException;
use Twig\RuntimeLoader\RuntimeLoaderInterface as TwigRuntimeLoader;

final class TwigRuntimeExtensionLoader implements TwigRuntimeLoader {

    private $ether;

    public function __construct(Ether $ether) {
        $this->ether = $ether;
    }

    /**
     *
     * @param $class
     *
     * @return null|object
     *
     * @throws ContainerException
     * @throws NotFoundException
     * @throws ReflectionException
     */
    public function load($class) {
        return $this->ether->build($class);
    }

}
