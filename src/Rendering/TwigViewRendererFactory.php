<?php declare(strict_types=1);

namespace Ether\Rendering;

use Ether\Config\Config;
use ReflectionException;
use Ether\Application as Ether;
use Symfony\Component\Finder\Finder;
use Ether\Exceptions\RuntimeException;
use Twig\Environment as TwigEnvironment;
use Ether\Container\Exceptions\NotFoundException;
use Ether\Container\Exceptions\ContainerException;
use Symfony\Component\HttpFoundation\Session\Session;
use Twig\Extension\DebugExtension as TwigDebugExtension;
use Twig\Loader\FilesystemLoader as TwigLoaderFilesystem;
use Twig\Extension\ExtensionInterface as TwigExtensionInterface;

final class TwigViewRendererFactory {

    /** @var Ether  */
    private $ether;

    /** @var Config  */
    private $config;

    /** @var Finder  */
    private $finder;

    /** @var Session  */
    private $session;

    /** @var TwigExtension  */
    private $twigExtension;

    /** @var TwigRuntimeExtensionLoader */
    private $twigRuntimeExtensionLoader;

    public function __construct(
        Ether $ether,
        Config $config,
        Finder $finder,
        TwigRuntimeExtensionLoader $twigRuntimeExtensionLoader,
        TwigExtension $twigExtension,
        Session $session
    ) {
        $this->ether = $ether;
        $this->config = $config;
        $this->finder = $finder;
        $this->session = $session;
        $this->twigExtension = $twigExtension;
        $this->twigRuntimeExtensionLoader = $twigRuntimeExtensionLoader;
    }

    /**
     * @return TwigViewRenderer
     *
     * @throws ContainerException
     * @throws NotFoundException
     * @throws ReflectionException
     * @throws RuntimeException
     */
    public function create(): TwigViewRenderer {

		$twigLoaderFilesystem = new TwigLoaderFilesystem($this->getViewsPath());

		$twigEnvironment = new TwigEnvironment($twigLoaderFilesystem, [
			'debug' => filter_var($this->config->get('app.debug'), FILTER_VALIDATE_BOOLEAN),
			'cache' => filter_var($this->config->get('app.debug'), FILTER_VALIDATE_BOOLEAN) ? false : $this->config->get('app.paths.cache')  . '/views'
        ]);

        $twigEnvironment->addGlobal('session', $this->session);

        $twigEnvironment->addRuntimeLoader($this->twigRuntimeExtensionLoader);

        $twigEnvironment = $this->registerTwigExtensions($twigEnvironment);

		return new TwigViewRenderer($twigEnvironment);
	}

    /**
     * @param TwigEnvironment $twigEnvironment
     *
     * @return TwigEnvironment
     *
     * @throws ContainerException
     * @throws RuntimeException
     * @throws NotFoundException
     * @throws ReflectionException
     */
    private function registerTwigExtensions(TwigEnvironment $twigEnvironment): TwigEnvironment {

        if(filter_var($this->config->get('app.debug'), FILTER_VALIDATE_BOOLEAN) === true){
            $twigEnvironment->addExtension(new TwigDebugExtension());
        }

        $twigEnvironment->addExtension($this->twigExtension);

        if(class_exists("{$this->ether->getNamespace()}Extensions\Twig\TwigExtension")) {

            /** @var TwigExtensionInterface $appTwigExtension */
            $appTwigExtension = $this->ether->build("{$this->ether->getNamespace()}Extensions\Twig\TwigExtension");

            $twigEnvironment->addExtension($appTwigExtension);
        }

        return $twigEnvironment;
    }

	private function getViewsPath(): array {

    	$viewsPath = $this->config->get('app.paths.views');

        $viewsPaths = [$viewsPath];

        $this->finder->followLinks()->directories()->in($viewsPath);

        foreach($this->finder as $file) {
	        $viewsPaths[] = $file->getRealPath();
        }

        return $viewsPaths;
    }
}
