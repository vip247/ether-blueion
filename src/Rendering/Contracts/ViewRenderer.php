<?php

namespace Ether\Rendering\Contracts;

interface ViewRenderer {

    /**
     * @param string $view
     * @param array $data
     *
     * @return string
     */
    public function render(string $view, array $data = array()): string;
}
