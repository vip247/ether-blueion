<?php declare(strict_types=1);

namespace Ether\Rendering;

use Ether\Rendering\Contracts\ViewRenderer;

final class PhpViewRenderer implements ViewRenderer {

    /** @var string */
    private $viewPath;

    public function __construct(string $viewPath) {
        $this->viewPath = $viewPath;
    }

    /**
     * {@inheritdoc}
     */
    public function render(string $viewName, array $data = []): string {

        ob_start();

        $viewFile = "{$this->viewPath}/{$viewName}.php";

        extract($data, EXTR_OVERWRITE);

        /** @noinspection PhpIncludeInspection */
        require $viewFile;

        return ob_get_clean();
    }

}
