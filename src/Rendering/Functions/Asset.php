<?php declare(strict_types=1);

namespace Ether\Rendering\Functions;

use Ether\Config\Config;
use Twig\Extension\RuntimeExtensionInterface as RuntimeExtension;

final class Asset implements RuntimeExtension {

    /** @var Config */
    private $config;

    public function __construct(Config $config) {
        $this->config = $config;
    }

    /**
     * @param string $assetPath
     * @param string $subDir
     *
     * @return string|null
     */
    public function asset(string $assetPath, string $subDir = null): ?string {

        $subDir = ! empty($subDir) ? "/$subDir/" : '/';

	    $assetManifestPath = realpath($this->config->get('app.paths.public') . $subDir . 'assets.json');

        if(($assetManifestPath !== false) && file_exists($assetManifestPath)) {

	        $assetManifest = json_decode(file_get_contents($assetManifestPath, true), true);

	        if( ! empty($assetManifest)) {

		        $resolvedAssetPath = $subDir . str_replace(basename($assetPath), $assetManifest[basename($assetPath)], $assetPath);

		        $realAssetPath = realpath($this->config->get('app.paths.public') . "/{$resolvedAssetPath}");

		        if(($realAssetPath !== false) && file_exists($realAssetPath)) {
			        return $resolvedAssetPath;
		        }
	        }
        }

        return null;
    }

}
