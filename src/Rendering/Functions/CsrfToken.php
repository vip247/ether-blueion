<?php declare(strict_types=1);

namespace Ether\Rendering\Functions;

use Symfony\Component\Security\Csrf\CsrfTokenManager;
use Twig\Extension\RuntimeExtensionInterface as RuntimeExtension;
use Symfony\Component\Security\Csrf\CsrfToken as SymfonyCsrfToken;

final class CsrfToken implements RuntimeExtension {

    /** @var CsrfTokenManager  */
    private $csrfTokenManager;

    public function __construct(CsrfTokenManager $csrfTokenManager) {
        $this->csrfTokenManager = $csrfTokenManager;
    }

    /**
     * @param string $name
     *
     * @return SymfonyCsrfToken
     */
    public function csrfToken(string $name): SymfonyCsrfToken {
        return $this->csrfTokenManager->getToken($name);
    }

}
