<?php declare(strict_types=1);

namespace Ether\Rendering\Functions;

use Carbon\Carbon;
use DateTimeImmutable;
use Twig\Extension\RuntimeExtensionInterface as RuntimeExtension;

final class DateForHumans implements RuntimeExtension {

    /**
     *
     * @param DateTimeImmutable $date
     *
     * @return string
     *
     */
    public function formatForHumans(DateTimeImmutable $date): string {

        $date = Carbon::instance($date);

        return $date->diffForHumans();

    }

}
