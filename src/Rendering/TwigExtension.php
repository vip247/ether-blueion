<?php declare(strict_types=1);

namespace Ether\Rendering;

use Twig\TwigFilter;
use Twig\TwigFunction;
use Ether\Rendering\Functions\Asset;
use Ether\Rendering\Functions\CsrfToken;
use Ether\Rendering\Functions\DateForHumans;
use Twig\Extension\AbstractExtension as TwigAbstractExtension;

final class TwigExtension extends TwigAbstractExtension {

    /*
     *
     * Function Extensions
     *
     * Functions can be called to generate content. Functions are called by their name followed by parentheses (())
     * and may have arguments.
     *
     */
    public function getFunctions(): array {
        return array(
            new TwigFunction('csrf_token', array(CsrfToken::class, 'csrfToken')),
            new TwigFunction('asset', array(Asset::class, 'asset'))
        );
    }

    /*
     *
     * Filter Extensions
     *
     * Variables can be modified by filters. Filters are separated from the variable by a pipe symbol (|)
     * and may have optional arguments in parentheses. Multiple filters can be chained. The output of one
     * filter is applied to the next.
     *
     */
    public function getFilters(): array {
        return array(
            new TwigFilter('for_humans', array(DateForHumans::class, 'formatForHumans'))
        );
    }

}
