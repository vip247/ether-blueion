<?php declare(strict_types=1);

namespace Ether\Rendering;

use Twig\Environment as TwigEnvironment;
use Ether\Rendering\Contracts\ViewRenderer;
use Twig\Error\LoaderError as TwigErrorLoader;
use Twig\Error\SyntaxError as TwigErrorSyntax;
use Twig\Error\RuntimeError as TwigErrorRuntime;

final class TwigViewRenderer implements ViewRenderer {

    /** @var TwigEnvironment  */
	private $twigEnvironment;

	public function __construct(TwigEnvironment $twigEnvironment) {
		$this->twigEnvironment = $twigEnvironment;
	}

    /**
     * {@inheritdoc}
     *
     * @throws TwigErrorLoader
     * @throws TwigErrorRuntime
     * @throws TwigErrorSyntax
     */
    public function render(string $viewName, $data = []): string {
        return $this->twigEnvironment->render($viewName, $data);
    }
}
