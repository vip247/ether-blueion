<?php declare(strict_types=1);

namespace Ether\Bootstrap;

use Ether\Application as Ether;

abstract class Agent {

    protected $ether;

    public function __construct(Ether $ether) {
        $this->ether = $ether;
    }

    abstract public function register(): void;
}
