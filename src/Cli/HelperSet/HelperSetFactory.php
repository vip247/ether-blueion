<?php declare(strict_types=1);

namespace Ether\Cli\HelperSet;

use Symfony\Component\Console\Helper\HelperSet;
use Symfony\Component\Console\Helper\QuestionHelper;
use Doctrine\DBAL\Tools\Console\Helper\ConnectionHelper;
use Doctrine\Migrations\Tools\Console\Helper\ConfigurationHelper;

final class HelperSetFactory {

	private $questionHelper;
	private $connectionHelper;
	private $configurationHelper;

	public function __construct(QuestionHelper $questionHelper, ConnectionHelper $connectionHelper, ConfigurationHelper $configurationHelper) {
		$this->questionHelper = $questionHelper;
		$this->connectionHelper = $connectionHelper;
		$this->configurationHelper = $configurationHelper;
	}

	public function create(): HelperSet  {

        $helperSet = new HelperSet();

		$helperSet->set($this->questionHelper, 'question');
		$helperSet->set($this->connectionHelper, 'db');
		$helperSet->set($this->configurationHelper);

		return $helperSet;
	}
}
