<?php declare(strict_types=1);

namespace Ether\Cli\Commands;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

final class EtherTestCommand extends Command {

	protected static $defaultName = 'ether:test';

	protected function configure(): void {
		$this->setDescription('App CLI test');
		$this->setHelp("This command allows you test this app's CLI.");
	}

	protected function execute(InputInterface $input, OutputInterface $output): void {
		$output->writeln('Ether CLI test successful' . getenv('APP_NAME'));
	}
}
