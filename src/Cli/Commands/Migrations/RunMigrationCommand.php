<?php declare(strict_types=1);

namespace Ether\Cli\Commands\Migrations;

use Doctrine\Migrations\MigratorConfiguration;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Doctrine\Migrations\Tools\Console\Command\AbstractCommand;

final class RunMigrationCommand extends AbstractCommand {

    protected function configure(): void {
		$this
			->setName('run:migration')
			->setDescription('Run a single migration version up or down.')
			->addArgument(
			    'version',
                InputArgument::REQUIRED,
                'The version to execute.',
                null
            )
			->addOption(
			    'dry-run',
                null,
                InputOption::VALUE_NONE,
                'Execute the migration as a dry run.')
			->addOption(
			    'direction',
                null,
                InputOption::VALUE_OPTIONAL,
                'Run the migration (up), or reverse the migration (down)',
                'up')
			->setHelp(file_get_contents(__DIR__ . '/help/run.help'));

		parent::configure();
	}

	public function execute(InputInterface $input, OutputInterface $output): ?int {

		$version = $input->getArgument('version');
		$dryRun = (bool) $input->getOption('dry-run');
		$direction = $input->getOption('direction');

		$version = $this->migrationRepository->getVersion($version);

		if ( ! $dryRun && ! $this->canExecute('Run migration? (y/n): ', $input, $output)) {
			$output->writeln('<info>Migration cancelled.</info>');

			return 1;
		}

		$migratorConfiguration = (new MigratorConfiguration())
			->setDryRun($dryRun);

		$version->execute($direction, $migratorConfiguration);

		return 0;
	}
}
