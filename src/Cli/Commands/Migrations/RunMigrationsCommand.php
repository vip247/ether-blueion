<?php declare(strict_types=1);

namespace Ether\Cli\Commands\Migrations;

use Doctrine\Migrations\MigratorConfiguration;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Formatter\OutputFormatter;
use Doctrine\Migrations\Tools\Console\Command\AbstractCommand;

/**
 * The MigrateCommand class is responsible for executing a migration from the current version to another
 * version up or down. It will calculate all the migration versions that need to be executed and execute them.
 */
final class RunMigrationsCommand extends AbstractCommand {

    protected function configure(): void {
        $this
            ->setName('run:migrations')
            ->setDescription('Run migrations and stop at either specified or latest version.')
            ->addArgument(
                'version',
                InputArgument::OPTIONAL,
                'The version number (YYYYMMDDHHMMSS) or alias (first, prev, next, latest) to migrate to.',
                'latest'
            )
            ->addOption(
                'dry-run',
                null,
                InputOption::VALUE_NONE,
                'Execute the migration as a dry run.'
            )
            ->addOption(
                'allow-no-migration',
                null,
                InputOption::VALUE_NONE,
                'Don\'t throw an exception if no migration is available (CI).'
            )
            ->addOption(
                'all-or-nothing',
                null,
                InputOption::VALUE_OPTIONAL,
                'Wrap the entire migration in a transaction.',
                false
            )
            ->setHelp(file_get_contents(__DIR__ . '/help/migrate.help'));

        parent::configure();
    }

    public function execute(InputInterface $input, OutputInterface $output): ?int {
        $this->outputHeader($output);

        $version = (string) $input->getArgument('version');
        $allowNoMigration = (bool) $input->getOption('allow-no-migration');
        $dryRun = (bool) $input->getOption('dry-run');
        $allOrNothing = $this->getAllOrNothing($input->getOption('all-or-nothing'));

        $this->configuration->setIsDryRun($dryRun);

        $version = $this->getVersionNameFromAlias($version, $output);

        if($version === '') {
            return 1;
        }

        if($this->checkExecutedUnavailableMigrations($input, $output) === 1) {
            return 1;
        }

        $migrator = $this->dependencyFactory->getMigrator();

        $migratorConfiguration = (new MigratorConfiguration())
            ->setDryRun($dryRun)
            ->setNoMigrationException($allowNoMigration)
            ->setAllOrNothing($allOrNothing);

        $migrator->migrate($version, $migratorConfiguration);

        return 0;
    }

    private function checkExecutedUnavailableMigrations(InputInterface $input, OutputInterface $output): int {

        $executedUnavailableMigrations = $this->migrationRepository->getExecutedUnavailableMigrations();

        if(\count($executedUnavailableMigrations) !== 0) {
            $output->writeln(sprintf(
                '<error>WARNING! You have %s previously executed migrations in the database that are not registered migrations.</error>',
                \count($executedUnavailableMigrations)
            ));

            foreach($executedUnavailableMigrations as $executedUnavailableMigration) {
                $output->writeln(sprintf(
                    '    <comment>>></comment> %s (<comment>%s</comment>)',
                    $this->configuration->getDateTime($executedUnavailableMigration),
                    $executedUnavailableMigration
                ));
            }

            $question = 'Are you sure you wish to continue? (y/n)';

            if( ! $this->canExecute($question, $input, $output)) {
                $output->writeln('<error>Migration cancelled!</error>');

                return 1;
            }
        }

        return 0;
    }

    private function getVersionNameFromAlias(string $versionAlias, OutputInterface $output): string {

        $version = $this->configuration->resolveVersionAlias($versionAlias);

        if($version !== null) {
            return $version;
        }

        if($versionAlias === 'prev') {
            $output->writeln('<error>Already at first version.</error>');

            return '';
        }

        if($versionAlias === 'next') {
            $output->writeln('<error>Already at latest version.</error>');

            return '';
        }

        if(strpos($versionAlias, 'current') === 0) {

            $output->writeln('<error>The delta couldn\'t be reached.</error>');

            return '';
        }

        $output->writeln(sprintf('<error>Unknown version: %s</error>', OutputFormatter::escape($versionAlias)));

        return '';
    }

    private function getAllOrNothing($allOrNothing): bool {
        if($allOrNothing !== false) {
            return $allOrNothing !== null ? (bool) $allOrNothing : true;
        }

        return $this->configuration->isAllOrNothing();
    }
}
