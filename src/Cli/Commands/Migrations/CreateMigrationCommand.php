<?php declare(strict_types=1);

namespace Ether\Cli\Commands\Migrations;

use Ether\Database\Migrations\Generator;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Doctrine\Migrations\Tools\Console\Command\AbstractCommand;

final class CreateMigrationCommand extends AbstractCommand {

	private $generator;

	public function __construct(Generator $generator, ?string $name = null) {

		$this->generator = $generator;

		parent::__construct($name);
	}

	protected function configure(): void {
        $this
			->setName('create:migration')
            ->setDescription('Generate a blank migration.')
			->addArgument('migration_name', InputArgument::REQUIRED, 'Name of migration? e.g. create_users_table')
            ->setHelp(file_get_contents(__DIR__ . '/help/run.help'));

        parent::configure();
    }

    public function execute(InputInterface $input, OutputInterface $output): ?int {

        $versionNumber = $this->configuration->generateVersionNumber();

        $path = $this->generator->generateMigration($versionNumber, $input->getArgument('migration_name'));

        $output->writeln(array(
            sprintf( 'Generated new migration class in: "<info>%s</info>"', $path),
			'',
            sprintf('To run this migration, use: <info>migrations:run --up %s</info>', $versionNumber),
            '',
            sprintf('To revert the migration, use: <info>migrations:run --down %s</info>', $versionNumber),
		));

        return 0;
    }
}
