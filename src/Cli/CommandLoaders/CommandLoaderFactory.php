<?php declare(strict_types=1);

namespace Ether\Cli\CommandLoaders;

use Ether\Application as Ether;
use Symfony\Component\Finder\Finder;
use Ether\Cli\Commands\EtherTestCommand;
use Symfony\Component\Console\Command\Command;
use Ether\Cli\Commands\Migrations\RunMigrationsCommand;
use Ether\Cli\Commands\Migrations\RunMigrationCommand;
use Ether\Cli\Commands\Migrations\CreateMigrationCommand;
use Ether\Cli\Commands\Migrations\StatusMigrationsCommand;
use Symfony\Component\Console\Exception\CommandNotFoundException;
use Symfony\Component\Console\CommandLoader\CommandLoaderInterface;

final class CommandLoaderFactory implements CommandLoaderInterface {

	private $ether;

    private $commands = array(
    	'ether:test'            => EtherTestCommand::class,
    	'create:migration'      => CreateMigrationCommand::class,
    	'run:migration'         => RunMigrationCommand::class,
        'run:migrations'        => RunMigrationsCommand::class,
        'status:migrations'     => StatusMigrationsCommand::class
	);

    public function __construct(Ether $ether) {
        $this->ether = $ether;
    }

    public function load(string $path): void {

		$namespace = $this->ether->getNamespace();

        foreach ((new Finder)->in($path)->files() as $command) {
            $command = $namespace . str_replace(
                    array('/', '.php'),
                    array('\\', ''),
					array_reverse(explode($this->ether->appPath() . '/', $command->getPathname(), 2))[0]
                );

			if(is_subclass_of($command, Command::class)) {
				$this->commands[$this->getNiceName($command)] = $command;
			}
        }
    }

    private function getNiceName(string $commandFQN): string {

    	$commandFQNSegments = explode('\\', $commandFQN);

    	$commandName = end($commandFQNSegments);

    	# Split commandName into segments based on capital letters eg. AppTestCommand -> array('App', 'Test', 'Command')
		$commandNameSegments = preg_split('/(?=[A-Z])/', $commandName, -1, PREG_SPLIT_NO_EMPTY);

		# Remove Command segment before recreating name in nice format
		if( end($commandNameSegments) === 'Command') {
			array_pop($commandNameSegments);
			$commandName = implode(':', $commandNameSegments);
		}

    	return strtolower($commandName);
	}

    public function has($name): bool {
        return isset($this->commands[$name]);
    }

    public function get($name) {
        if ( ! $this->has($name)) {
            throw new CommandNotFoundException(sprintf('Command "%s" does not exist.', $name));
        }

        return $this->ether->build($this->commands[$name]);
    }

    public function getNames() {
        return array_keys($this->commands);
    }
}
