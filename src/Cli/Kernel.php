<?php declare(strict_types=1);

namespace Ether\Cli;

use Exception;
use Ether\Application as Ether;
use Symfony\Component\Console\Helper\HelperSet;
use NunoMaduro\Collision\Provider as ErrorHandler;
use Ether\Cli\CommandLoaders\CommandLoaderFactory;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Application as Console;
use Symfony\Component\Console\Output\OutputInterface;

final class Kernel {

	public const NAME = 'Ether';
	public const VERSION = '0.1';

	private $ether;
	private $console;
	private $helperSet;
	private $errorHandler;
	private $commandLoaderFactory;

	public function __construct(
	    Ether $ether,
        Console $console,
        CommandLoaderFactory $commandLoaderFactory,
        HelperSet $helperSet,
        ErrorHandler $errorHandler
    ) {
		$this->ether = $ether;
		$this->console = $console;
		$this->helperSet = $helperSet;
		$this->errorHandler = $errorHandler;
		$this->commandLoaderFactory = $commandLoaderFactory;

		$this->configure();
	}

	private function configure(): void {

		$this->errorHandler->register();

		$this->console->setName(self::NAME);
		$this->console->setVersion(self::VERSION);
		$this->console->setHelperSet($this->helperSet);
		$this->console->setCatchExceptions(true);

        $this->commandLoaderFactory->load($this->ether->appPath() . '/Console/Commands');

        $this->console->setCommandLoader($this->commandLoaderFactory);
	}

    /**
     *
     * @param InputInterface|null $input
     * @param OutputInterface|null $output
     *
     * @return int
     *
     * @throws Exception
     *
     */
    public function handle(InputInterface $input = null, OutputInterface $output = null): int {
        return $this->console->run($input, $output);
    }
}
