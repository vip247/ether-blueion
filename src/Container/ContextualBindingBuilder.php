<?php declare(strict_types=1);

namespace Ether\Container;

final class ContextualBindingBuilder  {

    /**
     *
     * The container instance.
     * @var Container
     *
     */
    private $container;

    /**
     *
     * The concrete instance.
     * @var string
     *
     */
    private $concrete;

    /**
     *
     * The abstract target.
     * @var string
     *
     */
    private $needs;

    /**
     *
     * Create a new contextual binding builder.
     *
     * @param $container
     *
     * @param  string $concrete
     *
     * @return void
     *
     */
    public function __construct(Container $container, $concrete) {
        $this->concrete = $concrete;
        $this->container = $container;
    }

    /**
     *
     * Define the abstract target that depends on the context.
     *
     * @param  string $abstract
     *
     * @return $this
     *
     */
    public function needs($abstract): self {
        $this->needs = $abstract;

        return $this;
    }

    /**
     *
     * Define the implementation for the contextual binding.
     *
     * @param  \Closure|string $implementation
     *
     * @return void
     *
     */
    public function give($implementation): void {
        $this->container->addContextualBinding($this->concrete, $this->needs, $implementation);
    }
}
