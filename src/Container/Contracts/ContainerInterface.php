<?php declare(strict_types=1);

namespace Ether\Container\Contracts;

use ArrayAccess;
use Ether\Container\Exceptions\NotFoundException;
use Ether\Container\Exceptions\ContainerException;
use Psr\Container\ContainerInterface as PsrContainerInterface;

interface ContainerInterface extends PsrContainerInterface, ArrayAccess {

    /**
     * Bind a class into the container.
     *
     * @param  string $abstract
     * @param  mixed $concrete
     * @param  bool $shared
     * @return  void
     * @throws ContainerException
     */
    public function bind($abstract, $concrete = null, $shared = false): void;

    /**
     * Resolve binding.
     *
     * @param  string $id
     * @return object
     * @throws NotFoundException
     * @throws ContainerException
     */
    public function resolve($id);

	/**
	 * Bind and then resolve to return an instantiated binding.
	 *
	 * @param $abstract
	 * @param array $parameters
	 * @return object
	 */
    public function make($abstract, array $parameters = array());

    /**
     * Create an alias to an existing cached binding.
     *
     * @param  string $alias
     * @param  string $binding
     * @throws NotFoundException
     * @throws ContainerException
     */
    public function alias($alias, $binding);


    /********************************************
     * ContainerInterface Methods
     ********************************************/

    /**
     * Interface method for ContainerInterface.
     * Get the binding with the given $id.
     *
     * @param  string $id
     * @return object
     * @throws NotFoundException
     * @throws ContainerException
     */
    public function get($id);

    /**
     * Interface method for ContainerInterface.
     * Check if binding with $id exists.
     *
     * @param  string $id
     * @return bool
     */
    public function has($id): bool;

    /********************************************
     * ArrayAccess Methods
     ********************************************/

    /**
     * Interface method for ArrayAccess.
     * Checks if binding at $offset exists.
     *
     * @param  string $offset
     * @return bool
     */
    public function offsetExists($offset): bool;

    /**
     * Interface method for ArrayAccess.
     * Returns instance identified by $offset binding.
     *
     * @param  string $offset
     * @return object
     * @throws NotFoundException
     * @throws ContainerException
     */
    public function offsetGet($offset);

    /**
     * Interface method for ArrayAccess.
     * Set binding at $offset (abstract) with $value (concrete).
     *
     * @param  string $offset
     * @param  mixed $value
     * @throws ContainerException
     */
    public function offsetSet($offset, $value);

    /**
     * Interface method for ArrayAccess.
     * Remove the binding at $offset.
     *
     * @param  string $offset
     */
    public function offsetUnset($offset);
}
