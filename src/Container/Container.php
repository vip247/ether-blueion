<?php declare(strict_types=1);

namespace Ether\Container;

use Closure;
use LogicException;
use ReflectionClass;
use ReflectionException;
use ReflectionParameter;
use Ether\Container\Contracts\ContainerInterface;
use Ether\Container\Exceptions\NotFoundException;
use Ether\Container\Exceptions\ContainerException;

final class Container implements ContainerInterface {

	/**
	 *
	 * Array registry of container bindings.
	 * @var array
	 *
	 */
	private $bindings = array();

	/**
	 *
	 * The container's shared instances.
	 * @var array
	 *
	 */
	private $sharedInstances = array();

	/**
	 *
	 * Array of aliased cached bindings keyed by alias.
	 * @var array
	 *
	 */
	private $aliases = array();

    /**
     *
     * The registered aliases keyed by the abstract name.
     * @var array
     *
     */
    private $abstractAliases = array();

	/**
	 *
	 * The stack of concretions currently being built.
	 * @var array
	 *
	 */
	private $buildStack = array();

    /**
     *
     * Parameter stack.
     * @var array
     *
     */
    private $parameterStack = array();

    /**
     *
     * Array registry of contextual container bindings.
     * @var array
     *
     */
    public $contextualBindings = array();

	/**
	 *
	 * An array of the types that have been resolved.
	 * @var array
	 *
	 */
	private $resolved = array();

	/**
	 *
	 * Container constructor.
	 *
	 */
	public function __construct() {
		$this->shareInstance(self::class, $this);
	}

	/**
	 *
	 * @param  string $abstract
	 * @param  mixed $concrete
	 * @param  bool $shared
	 *
	 * @return void
	 *
	 */
	public function bind($abstract, $concrete = null, $shared = false): void {

		// Clear bindings, aliases, & cache
		unset($this->bindings[$abstract], $this->aliases[$abstract]);

		// Set $concrete to $abstract if $concrete not provided.
		$concrete = $concrete ?? $abstract;

		if( ! $concrete instanceof Closure) {
			$concrete = $this->getClosure($abstract, $concrete);
		}

		$this->bindings[$abstract] = compact('concrete', 'shared');

	}

    /**
     *
     * Resolve all of the dependencies from the ReflectionParameters.
     *
     * @param  array $dependencies
     *
     * @return array
     *
     * @throws ContainerException
     * @throws NotFoundException
     * @throws ReflectionException
     *
     */
	private function resolveDependencies(array $dependencies): array {

		$results = array();

		foreach ($dependencies as $dependency) {

            // If this dependency has a override for this particular build we will use
            // that instead as the value. Otherwise, we will continue with this run
            // of resolutions and let reflection attempt to determine the result.
            if ($this->hasParameterOverride($dependency)) {
                $results[] = $this->getParameterOverride($dependency);

                continue;
            }

            // If the class is null, it means the dependency is a string or some other primitive type
			$results[] = $dependency->getClass() === null ? $this->resolvePrimitive($dependency) : $this->resolveClass($dependency);
		}

		return $results;
	}

    /**
     *
     * Determine if the given dependency has a parameter override.
     *
     * @param  \ReflectionParameter  $dependency
     *
     * @return bool
     *
     */
    private function hasParameterOverride($dependency): bool {
        return array_key_exists($dependency->name, $this->getLastParameterOverride());
    }

    /**
     *
     * Get a parameter override for a dependency.
     *
     * @param  \ReflectionParameter  $dependency
     *
     * @return mixed
     *
     */
    private function getParameterOverride($dependency) {
        return $this->getLastParameterOverride()[$dependency->name];
    }

    /**
     *
     * Get the last parameter override.
     *
     * @return array
     *
     */
    private function getLastParameterOverride(): array {
        return \count($this->parameterStack) ? end($this->parameterStack) : array();
    }

	/**
	 *
	 * Resolve a non-class hinted primitive dependency.
	 *
	 * @param  ReflectionParameter $parameter
	 *
	 * @return mixed
	 *
	 * @throws ContainerException
	 *
	 */
	private function resolvePrimitive(ReflectionParameter $parameter) {

        if (($concrete = $this->getContextualConcrete('$' . $parameter->name)) !== null) {
            return $concrete instanceof Closure ? $concrete($this) : $concrete;
        }

		if ($parameter->isDefaultValueAvailable()) {
			return $parameter->getDefaultValue();
		}

		throw new ContainerException("Cannot resolve dependency: {$parameter->name}, in {$parameter->getDeclaringClass()->getName()}");
	}

    /**
     *
     * Resolve a class based dependency from the container.
     *
     * @param  ReflectionParameter $parameter
     *
     * @return mixed
     *
     * @throws ContainerException
     * @throws NotFoundException
     * @throws ReflectionException
     *
     */
	private function resolveClass(ReflectionParameter $parameter) {
		try {

			return $this->make($parameter->getClass()->name);

		} catch (ContainerException $e) {

			if ($parameter->isOptional()) {
				return $parameter->getDefaultValue();
			}

			throw $e;
		}
	}

	/**
	 *
	 * Resolve the given type and return an instantiated binding.
	 *
	 * @param $abstract
	 * @param array $parameters
	 *
	 * @return object
	 *
	 * @throws ContainerException
	 * @throws NotFoundException
	 * @throws ReflectionException
	 *
	 */
	public function make($abstract, array $parameters = array()) {
		return $this->resolve($abstract, $parameters);
	}

	/**
	 *
	 * Resolve the given type from the container.
	 *
	 * @param  string $abstract
	 * @param  array $parameters
	 *
	 * @return mixed
	 *
	 * @throws ContainerException
	 * @throws NotFoundException
	 * @throws ReflectionException
	 *
	 */
	public function resolve($abstract, $parameters = array()) {

		$abstract = $this->getAlias($abstract);

        $needsContextualBuild = ! empty($parameters) || $this->getContextualConcrete($abstract) !== null;

		// If an instance of the type is currently being managed as a singleton we'll
		// just return an existing instance instead of instantiating new instances
		if ( ! $needsContextualBuild && isset($this->sharedInstances[$abstract])) {
			return $this->sharedInstances[$abstract];
		}

		$this->parameterStack[] = $parameters;

		$concrete = $this->getConcrete($abstract);

		// We're ready to instantiate an instance of the concrete type registered for
		// the binding. This will instantiate the types, as well as resolve any of
		// its "nested" dependencies recursively until all have gotten resolved.
		if ($this->isBuildable($concrete, $abstract)) {
			$object = $this->build($concrete);
		} else {
			$object = $this->make($concrete);
		}

		// If the requested type is registered as a singleton we'll want to cache off
		// the instances in "memory" so we can return it later without creating an
		// entirely new instance of an object on each subsequent request for it.
		if ( ! $needsContextualBuild && $this->isShared($abstract)) {
			$this->sharedInstances[$abstract] = $object;
		}

		$this->resolved[$abstract] = true;

		array_pop($this->parameterStack);

 		return $object;
	}

    /**
     *
     * @param $concrete
     *
     * @return mixed|object
     *
     * @throws ContainerException
     * @throws NotFoundException
     * @throws ReflectionException
     *
     */
    public function build($concrete) {

        // If concrete type is a Closure, call closure
        if ($concrete instanceof Closure) {
            return $concrete($this, $this->getLastParameterOverride());
        }

        $reflector = new ReflectionClass($concrete);

        // This will happen when attempting to resolve an abstract type (Abstract Class or Interface)
        // that has no registered binding
        if ( ! $reflector->isInstantiable()) {

            if ( ! empty($this->buildStack)) {

                $previous = implode(', ', $this->buildStack);

                $message = "Cannot create an instance of non instantiable binding: {$concrete} while building {$previous}.";

            } else {

                $message = "Cannot create an instance of non instantiable binding: {$concrete}.";

            }

            throw new ContainerException($message);
        }

        $this->buildStack[] = $concrete;

        $constructor = $reflector->getConstructor();

        // No constructor means no dependencies,
        // create instance and return it.
        if ($constructor === null) {
            array_pop($this->buildStack);

            return new $concrete;
        }

        $dependencies = $constructor->getParameters();

        // Once we have all the constructor's parameters we can create each of the
        // dependency instances and then use the reflection instances to make a
        // new instance of this class, injecting the created dependencies in.
        $dependencyInstances = $this->resolveDependencies($dependencies);

        array_pop($this->buildStack);

        return $reflector->newInstanceArgs($dependencyInstances);
    }

    /**
     *
     * Register a shared binding in the container.
     *
     * @param  string  $abstract
     * @param  Closure|string|null  $concrete
     *
     * @return void
     *
     */
    public function share($abstract, $concrete = null): void {
        $this->bind($abstract, $concrete, true);
    }

    /**
     *
     * Register an existing instance as shared in the container.
     *
     * @param  string  $abstract
     * @param  mixed   $instance
     *
     * @return mixed
     *
     */
    public function shareInstance(string $abstract, Object $instance): object {

        $this->removeAbstractAlias($abstract);

        unset($this->aliases[$abstract]);

        $this->sharedInstances[$abstract] = $instance;

        return $instance;
    }

    /**
     *
     * Remove a resolved instance from the instance cache.
     *
     * @param  string  $abstract
     *
     * @return void
     *
     */
    public function unshareInstance($abstract): void {
        unset($this->sharedInstances[$abstract]);
    }

    /**
     *
     * Clear all shared instances.
     *
     * @return void
     *
     */
    public function unshareAll(): void {
        $this->sharedInstances = array();
    }

    /**
     *
     * Alias a type to a different name.
     *
     * @param  string  $abstract
     * @param  string  $alias
     *
     * @return void
     *
     */
    public function alias($abstract, $alias): void {

        $this->aliases[$alias] = $abstract;

        $this->abstractAliases[$abstract][] = $alias;
    }

    /**
     *
     * Get the concrete type for a given abstract.
     *
     * @param  string  $abstract
     *
     * @return mixed   $concrete
     *
     */
    private function getConcrete($abstract) {

        if (($concrete = $this->getContextualConcrete($abstract)) !== null) {
            return $concrete;
        }

        // If we don't have a registered resolver or concrete for the type, we'll just
        // assume each type is a concrete name and will attempt to resolve it as is
        // since the container should be able to resolve concretes automatically.
        if (isset($this->bindings[$abstract])) {
            return $this->bindings[$abstract]['concrete'];
        }

        return $abstract;
    }

    /**
     *
     * @param string $abstract
     * @param $concrete
     *
     * @return callable
     *
     */
    private function getClosure(string $abstract, $concrete): callable {
        return function (Container $container, $parameters = array()) use ($abstract, $concrete) {
            if ($abstract === $concrete) {
                return $container->build($concrete);
            }

            return $container->make($concrete, $parameters);
        };
    }

    /**
     *
     * Get the alias for an abstract if available.
     *
     * @param  string  $abstract
     *
     * @return string
     *
     * @throws LogicException
     *
     */
    public function getAlias($abstract): string {

        if ( ! isset($this->aliases[$abstract])) {
            return $abstract;
        }

        if ($this->aliases[$abstract] === $abstract) {
            throw new LogicException("{$abstract} is aliased to itself.");
        }

        return $this->getAlias($this->aliases[$abstract]);
    }

    /**
     *
     * Remove an alias from the contextual binding alias cache.
     *
     * @param  string  $searched
     *
     * @return void
     *
     */
    private function removeAbstractAlias($searched): void {

        if (! isset($this->aliases[$searched])) {
            return;
        }

        foreach ($this->abstractAliases as $abstract => $aliases) {
            foreach ($aliases as $index => $alias) {
                if ($alias === $searched) {
                    unset($this->abstractAliases[$abstract][$index]);
                }
            }
        }
    }

    /**
     *
     * Add a contextual binding to the container.
     *
     * @param  string  $concrete
     * @param  string  $abstract
     *
     * @param  \Closure|string  $implementation
     *
     * @return void
     *
     */
    public function addContextualBinding($concrete, $abstract, $implementation): void {
        $this->contextualBindings[$concrete][$this->getAlias($abstract)] = $implementation;
    }

    /**
     *
     * Find the concrete binding for the given abstract in the contextual binding array.
     *
     * @param  string  $abstract
     *
     * @return mixed|null
     *
     */
    private function findInContextualBindings($abstract) {

        $result = null;
        if (isset($this->contextualBindings[end($this->buildStack)][$abstract])) {
            $result = $this->contextualBindings[end($this->buildStack)][$abstract];
        }

        return $result;
    }

    /**
     *
     * Get the contextual concrete binding for the given abstract.
     *
     * @param  string  $abstract
     *
     * @return mixed|null
     *
     */
    private function getContextualConcrete($abstract) {

        if (($binding = $this->findInContextualBindings($abstract)) !== null) {
            return $binding;
        }

        // Next we need to see if a contextual binding might be bound under an alias of the
        // given abstract type. So, we will need to check if any aliases exist with this
        // type and then spin through them and check for contextual bindings on these.
        if (empty($this->abstractAliases[$abstract])) {
            return null;
        }

        foreach ($this->abstractAliases[$abstract] as $alias) {
            if (($binding = $this->findInContextualBindings($alias)) !== null) {
                return $binding;
            }
        }

        return null;
    }

    /**
     *
     * Define a contextual binding.
     *
     * @param  string  $concrete
     *
     * @return ContextualBindingBuilder
     *
     */
    public function when($concrete): ContextualBindingBuilder {
        return new ContextualBindingBuilder($this, $this->getAlias($concrete));
    }

    /**
     *
     * Determine if the given abstract type has been resolved.
     *
     * @param  string  $abstract
     *
     * @return bool
     *
     */
    public function resolved($abstract): bool {
        if ($this->isAliased($abstract)) {
            $abstract = $this->getAlias($abstract);
        }

        return isset($this->resolved[$abstract]) || isset($this->instances[$abstract]);
    }

	/**
	 *
	 * Determine if a given string is an alias.
	 *
	 * @param  string  $name
	 *
	 * @return bool
	 *
	 */
	public function isAliased($name): bool {
		return isset($this->aliases[$name]);
	}

    /**
     *
     * Determine if the given abstract type has been bound to the Container.
     *
     * @param  string  $abstract
     *
     * @return bool
     *
     */
    public function isBound($abstract): bool {
        return isset($this->bindings[$abstract]) || isset($this->sharedInstances[$abstract]) || $this->isAliased($abstract);
    }

	/**
	 *
	 * Determine if a given type is shared.
	 *
	 * @param  string  $abstract
	 *
	 * @return bool
	 *
	 */
	public function isShared($abstract): bool {
		return isset($this->sharedInstances[$abstract]) || (isset($this->bindings[$abstract]['shared']) && $this->bindings[$abstract]['shared'] === true);
	}

	/**
	 *
	 * Determine if the given concrete is buildable.
	 *
	 * @param  mixed   $concrete
	 *
	 * @param  string  $abstract
	 *
	 * @return bool
	 *
	 */
	private function isBuildable($concrete, $abstract): bool {
		return $concrete === $abstract || $concrete instanceof Closure;
	}

	/**
	 *
	 * Interface method for ContainerInterface.
	 * Get the binding with the given $id.
	 *
	 * @param  string $id
	 *
	 * @return object
	 *
	 * @throws NotFoundException
	 * @throws ContainerException
	 * @throws ReflectionException
	 *
	 */
	public function get($id) {
        if ($this->has($id)) {
            return $this->resolve($id);
        }

        throw new NotFoundException("{$id} not found.");
	}

	/**
	 *
	 * Check if binding with $id exists.
	 *
	 * @param  string $id
	 *
	 * @return bool
	 *
	 */
	public function has($id): bool {
		return $this->isBound($id);
	}

	/**
	 *
	 * Interface method for ArrayAccess.
	 * Checks if binding at $offset exists.
	 *
	 * @param  string $key
	 *
	 * @return bool
	 *
	 */
	public function offsetExists($key): bool {
		return $this->isBound($key);
	}

	/**
	 *
	 * Returns instance identified by $offset binding.
	 *
	 * @param  string $key
	 *
	 * @return object
	 *
	 * @throws NotFoundException
	 * @throws ContainerException
	 * @throws ReflectionException
	 *
	 */
	public function offsetGet($key) {
        return $this->make($key);
	}

	/**
	 *
	 * Set binding at $offset (abstract) with $value (concrete).
	 *
	 * @param  string $key
	 * @param  mixed $value
	 *
	 * @return void
	 *
	 */
	public function offsetSet($key, $value): void {

	    if($value instanceof Closure) {

            $concrete = $value;

        } else {

	        $concrete = function () use ($value) {
                return $value;
            };

        }

        $this->bind($key, $concrete);
	}

	/**
	 *
	 * Remove the binding at $offset.
	 *
	 * @param  string $key
	 *
	 * @return void
	 *
	 */
	public function offsetUnset($key): void {
        unset($this->bindings[$key], $this->sharedInstances[$key], $this->resolved[$key]);
	}
}
