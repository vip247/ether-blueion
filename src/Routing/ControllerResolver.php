<?php declare(strict_types=1);

namespace Ether\Routing;

use ReflectionException;
use Ether\Application as Ether;
use Ether\Exceptions\RuntimeException;
use Symfony\Component\HttpFoundation\Request;
use Ether\Container\Exceptions\NotFoundException;
use Ether\Container\Exceptions\ContainerException;
use Ether\Exceptions\Throwable\ControllerNotSpecifiedException;
use Symfony\Component\HttpKernel\Controller\ControllerResolverInterface;

final class ControllerResolver implements ControllerResolverInterface {

    /** @var Ether */
    private $ether;

    public function __construct(Ether $ether) {
        $this->ether = $ether;
    }

    /**
     *
     * @param Request $request
     *
     * @return callable
     *
     * @throws ContainerException
     * @throws RuntimeException
     * @throws NotFoundException
     * @throws ReflectionException
     *
     */
    public function getController(Request $request): callable {

        $controller = $request->attributes->get('controller');

        if($controller === null) {
            throw new ControllerNotSpecifiedException('Controller not specified.');
        }

        // If the controller is an array then both class and method are expected to be specified,
        // Expected format [ClassName::class, 'method'] -> ['App\Namespace\ClassName', 'method']
        if(is_array($controller)) {

            [$fqn, $method] = $controller;

            if( ! is_callable([$fqn, $method])) {
                throw new RuntimeException("Method: {$method}, not found in {$fqn}");
            }

            $callable = [$this->ether->build($fqn), $method];

        } else {

            // Method not specified so treat controller as single function controller
            // with __invoke function i.e: Invokable class.
            $callable = $this->ether->build($controller);
        }

        return $callable;
    }
}
