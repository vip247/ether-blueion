<?php declare(strict_types=1);

namespace Ether\Routing;

use Exception;
use RuntimeException;
use InvalidArgumentException;
use Psr\Log\LoggerInterface as Logger;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\RequestContext;
use Symfony\Component\Routing\RouteCollection;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Config\ConfigCacheFactory;
use Symfony\Component\Config\ConfigCacheFactoryInterface;
use Symfony\Component\Routing\Matcher\CompiledUrlMatcher;
use Symfony\Component\Routing\Matcher\UrlMatcherInterface;
use Symfony\Component\Routing\Generator\CompiledUrlGenerator;
use Symfony\Component\Config\Loader\LoaderInterface as Loader;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Routing\Matcher\RequestMatcherInterface;
use Symfony\Component\Config\ConfigCacheInterface as ConfigCache;
use Symfony\Component\Routing\Matcher\Dumper\MatcherDumperInterface;
use Symfony\Component\Routing\Matcher\Dumper\CompiledUrlMatcherDumper;
use Symfony\Component\Routing\Generator\Dumper\GeneratorDumperInterface;
use Symfony\Component\Routing\Generator\Dumper\CompiledUrlGeneratorDumper;
use Symfony\Component\Routing\Generator\ConfigurableRequirementsInterface;
use Symfony\Component\ExpressionLanguage\ExpressionFunctionProviderInterface;

final class RouteResolver implements RouterInterface, RequestMatcherInterface {

	/** @var UrlMatcherInterface|null */
    private $matcher;

	/** @var UrlGeneratorInterface|null */
    private $generator;

	/** @var RequestContext */
    private $requestContext;

	/** @var Loader */
    private $loader;

    /** @var array */
    private $routeResources;

	/** @var array */
    private $options;

	/** @var Logger|null */
    private $logger;

    /** @var string|null */
    private $defaultLocale;

	/** @var ConfigCacheFactoryInterface|null */
	private $configCacheFactory;

	/** @var ExpressionFunctionProviderInterface */
	private $expressionLanguageProviders = [];

    /** @var RouteCollection|null $routeCollection*/
    private $routeCollection;

    /**
     * @param Loader $loader A LoaderInterface instance
     * @param array $routeResources
     * @param array $options An array of options
     * @param RequestContext $requestContext The context
     * @param Logger $logger A logger instance
     * @param string|null $defaultLocale
     */
	public function __construct(
        Loader $loader,
        array $routeResources = [],
        array $options = [],
        ?RequestContext $requestContext = null,
        ?Logger $logger = null,
        ?string $defaultLocale = null
    ) {
		$this->loader = $loader;
        $this->logger = $logger;
		$this->routeResources = $routeResources;
        $this->defaultLocale = $defaultLocale;
        $this->requestContext = $requestContext ?? new RequestContext();

		$this->setOptions($options);
    }

    /**
     * Sets options.
     *
     * Available options:
     * cache_dir:              The cache directory (or null to disable caching)
     * debug:                  Whether to enable debugging or not (false by default)
     * generator_class:        The name of a UrlGeneratorInterface implementation
     * generator_dumper_class: The name of a GeneratorDumperInterface implementation
     * matcher_class:          The name of a UrlMatcherInterface implementation
     * matcher_dumper_class:   The name of a MatcherDumperInterface implementation
     * resource_type:          Type hint for the main resource (optional)
     * strict_requirements:    Configure strict requirement checking for generators implementing
     *                         ConfigurableRequirementsInterface (default is true)
     *
     * @param array $options
     *
     * @throws InvalidArgumentException When unsupported option is provided
     */
	public function setOptions(array $options): void {

		$this->options = [
			'cache_dir' => null,
			'debug' => false,
            'matcher_class' => CompiledUrlMatcher::class,
            'matcher_dumper_class' => CompiledUrlMatcherDumper::class,
			'generator_class' => CompiledUrlGenerator::class,
			'generator_dumper_class' => CompiledUrlGeneratorDumper::class,
			'resource_type' => null,
			'strict_requirements' => true
        ];

		// check option names and live merge, if errors are encountered Exception will be thrown
		$invalid = [];

		foreach($options as $key => $option) {

			if(array_key_exists($key, $this->options)) {
				$this->options[$key] = $option;
			} else {
				$invalid[] = $key;
			}
		}

		if($invalid) {
			throw new InvalidArgumentException(sprintf('The RouteResolver does not support the following options: "%s".', implode('", "', $invalid)));
		}
	}

	/**
     *
	 * Sets an option.
	 *
	 * @param string $key The key
	 * @param mixed $value The value
	 *
	 * @throws InvalidArgumentException
     *
	 */
	public function setOption($key, $value): void {

		if( ! array_key_exists($key, $this->options)) {
			throw new InvalidArgumentException(sprintf('The RouteResolver does not support the "%s" option.', $key));
		}

		$this->options[$key] = $value;
	}

	/**
     *
	 * Gets an option value.
	 *
	 * @param string $key The key
	 *
	 * @return mixed The value
	 *
	 * @throws InvalidArgumentException
     *
	 */
	public function getOption($key) {

		if( ! array_key_exists($key, $this->options)) {
			throw new InvalidArgumentException(sprintf('The Router does not support the "%s" option.', $key));
		}

		return $this->options[$key];
	}

    /**
     * {@inheritdoc}
     *
     * @throws Exception
     */
    public function getRouteCollection(): RouteCollection {

        if(empty($this->routeResources)) {
            throw new RuntimeException('No routes specified.');
        }

        if (null === $this->routeCollection) {

            $routeCollection = new RouteCollection();

            foreach ($this->routeResources as $routeResource) {
                $routeCollection->addCollection($this->loader->load($routeResource, $this->options['resource_type']));
            }

            $this->routeCollection = $routeCollection;
        }

        return $this->routeCollection;
    }

    /**
     * {@inheritdoc}
     *
     * @throws Exception
     */
	public function setContext(RequestContext $requestContext): void {

		$this->requestContext = $requestContext;

		if(null !== $this->matcher && null !== ($matcher = $this->getMatcher())) {
            $matcher->setContext($this->requestContext);
		}

		if(null !== $this->generator && null !== ($generator = $this->getGenerator())) {
            $generator->setContext($this->requestContext);
		}
	}

    /**
     * {@inheritdoc}
     */
	public function getContext(): RequestContext {
		return $this->requestContext;
	}

	/**
     *
	 * Sets the ConfigCache factory to use.
     *
	 * @param ConfigCacheFactoryInterface $configCacheFactory
     *
	 */
	public function setConfigCacheFactory(ConfigCacheFactoryInterface $configCacheFactory) {
		$this->configCacheFactory = $configCacheFactory;
	}

    /**
     * {@inheritdoc}
     *
     * @throws Exception
     */
	public function generate($name, $parameters = [], $referenceType = self::ABSOLUTE_PATH): string {

	    if(null !== ($generator = $this->getGenerator())) {
            return $generator->generate($name, $parameters, $referenceType);
        }

        return '';
	}

    /**
     * {@inheritdoc}
     *
     * @throws Exception
     */
	public function match($pathInfo): array {

	    if(null !== ($matcher = $this->getMatcher())) {
            return $matcher->match($pathInfo);
        }

	    return [];
	}

    /**
     * {@inheritdoc}
     *
     * @throws Exception
     */
	public function matchRequest(Request $request): array {

		$matcher = $this->getMatcher();

		if( ! $matcher instanceof RequestMatcherInterface) {
			// fallback to the default UrlMatcherInterface
			return $matcher->match($request->getPathInfo());
		}

		return $matcher->matchRequest($request);
	}

    /**
     * @return UrlMatcherInterface|null
     *
     * @throws Exception
     */
    public function getMatcher(): ?UrlMatcherInterface {

		if(null !== $this->matcher) {
			return $this->matcher;
		}

		if(null === $this->options['cache_dir']) {

            $routeCollection = $this->getRouteCollection();

            $routes = (new CompiledUrlMatcherDumper($routeCollection))->getCompiledRoutes();

            $this->matcher = new $this->options['matcher_class']($routes, $this->requestContext);

			if(method_exists($this->matcher, 'addExpressionLanguageProvider')) {

				foreach($this->expressionLanguageProviders as $provider) {
					$this->matcher->addExpressionLanguageProvider($provider);
				}

			}

			return $this->matcher;
		}

        $matcherDumper = $this->getMatcherDumperInstance();

        $expressionLanguageProviders = $this->expressionLanguageProviders;

		$cache = $this->getConfigCacheFactory()->cache("{$this->options['cache_dir']}/routes/routes.php.cache", static function(ConfigCache $cache) use ($matcherDumper, $expressionLanguageProviders) {

				if(method_exists($matcherDumper, 'addExpressionLanguageProvider')) {

					foreach($expressionLanguageProviders as $expressionLanguageProvider) {
                        $matcherDumper->addExpressionLanguageProvider($expressionLanguageProvider);
					}

				}

				$cache->write($matcherDumper->dump());
			}
		);

        /** @noinspection PhpIncludeInspection */
        return $this->matcher = new $this->options['matcher_class'](require $cache->getPath(), $this->requestContext);
	}

    /**
     * @return UrlGeneratorInterface|null
     *
     * @throws Exception
     */
    public function getGenerator(): ?UrlGeneratorInterface {

		if(null !== $this->generator) {
			return $this->generator;
		}

		if(null === $this->options['cache_dir']) {

            $routeCollection = $this->getRouteCollection();

            $routes = (new CompiledUrlGeneratorDumper($routeCollection))->getCompiledRoutes();

            $this->generator = new $this->options['generator_class']($routes, $this->requestContext, $this->logger, $this->defaultLocale);

		} else {

            $generatorDumper = $this->getGeneratorDumperInstance();

			$cache = $this->getConfigCacheFactory()->cache("{$this->options['cache_dir']}/routes/generator.php.cache", static function(ConfigCache $cache) use ($generatorDumper) {
					$cache->write($generatorDumper->dump());
				}
			);

            /** @noinspection PhpIncludeInspection */
            $this->generator = new $this->options['generator_class'](require $cache->getPath(), $this->requestContext, $this->logger);
		}

		if($this->generator instanceof ConfigurableRequirementsInterface) {
			$this->generator->setStrictRequirements($this->options['strict_requirements']);
		}

		return $this->generator;
	}

	public function addExpressionLanguageProvider(ExpressionFunctionProviderInterface $provider) {
		$this->expressionLanguageProviders[] = $provider;
	}

	/**
	 *
	 * @return GeneratorDumperInterface
	 *
	 * @throws Exception
	 *
	 */
	protected function getGeneratorDumperInstance(): GeneratorDumperInterface {
		return new $this->options['generator_dumper_class']($this->getRouteCollection());
	}

	/**
	 *
	 * @return MatcherDumperInterface
	 *
	 * @throws Exception
	 *
	 */
	protected function getMatcherDumperInstance(): MatcherDumperInterface {
		return new $this->options['matcher_dumper_class']($this->getRouteCollection());
	}

	/**
	 *
	 * Provides the ConfigCache factory implementation, falling back to a
	 * default implementation if necessary.
	 *
	 * @return ConfigCacheFactoryInterface
	 *
	 */
	private function getConfigCacheFactory(): ConfigCacheFactoryInterface {

		if(null === $this->configCacheFactory) {
			$this->configCacheFactory = new ConfigCacheFactory($this->options['debug']);
		}

		return $this->configCacheFactory;
	}
}
