<?php declare(strict_types=1);

namespace Ether\Exceptions\Throwable;

use LogicException;

class ControllerNotSpecifiedException extends LogicException {}
