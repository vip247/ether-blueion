<?php declare(strict_types=1);

namespace Ether\Exceptions\Throwable;

use ErrorException;
use Ether\Exceptions\HtmlDumper;

final class ViewException extends ErrorException {

    /** @var array */
    protected $viewData = [];

    /** @var string */
    protected $view = '';

    public function setViewData(array $data): void {
        $this->viewData = $data;
    }

    public function getViewData(): array {
        return $this->viewData;
    }

    public function setView(string $path): void {
        $this->view = $path;
    }

    protected function dumpViewData($variable): string {
        return (new HtmlDumper())->dumpVariable($variable);
    }

}
