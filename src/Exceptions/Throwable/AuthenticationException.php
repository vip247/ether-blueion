<?php declare(strict_types=1);

namespace Ether\Exceptions\Throwable;

use Exception;

class AuthenticationException extends Exception {}
