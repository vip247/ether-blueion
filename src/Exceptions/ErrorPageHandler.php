<?php declare(strict_types=1);

namespace Ether\Exceptions;

use Exception;
use ErrorException;
use RuntimeException;
use Whoops\Util\Misc;
use Whoops\Handler\Handler;
use InvalidArgumentException;
use UnexpectedValueException;
use League\Flysystem\Filesystem;
use Whoops\Exception\FrameCollection;
use League\Flysystem\FilesystemOperator;
use Ether\Rendering\Contracts\ViewRenderer;
use League\Flysystem\Local\LocalFilesystemAdapter;

final class ErrorPageHandler extends Handler {

    public const EDITOR_SUBLIME = 'sublime';
    public const EDITOR_TEXTMATE = 'textmate';
    public const EDITOR_EMACS = 'emacs';
    public const EDITOR_MACVIM = 'macvim';
    public const EDITOR_PHPSTORM = 'phpstorm';
    public const EDITOR_IDEA = 'idea';
    public const EDITOR_VSCODE = 'vscode';
    public const EDITOR_ATOM = 'atom';
    public const EDITOR_ESPRESSO = 'espresso';

    public static $tabs = ['stack_trace', 'request'];

    public static $styles = [
        'error.css' =>  __DIR__ . '/Presentation/resources/css/error.css'
    ];

    public static $scripts = [];

    /** @var array */
    private $applicationPaths;

    /** @var ViewRenderer */
    private $viewRenderer;

    /** @var FilesystemInterface */
    private $filesystem;

    /** @var array */
    private $blacklist = [
        '_GET'     => [],
        '_POST'    => [],
        '_FILES'   => [],
        '_COOKIE'  => [],
        '_SESSION' => [],
        '_SERVER'  => [],
        '_ENV'     => []
    ];

    /**
     * A string identifier for a known IDE/text editor, or a closure
     * that resolves a string that can be used to open a given file
     * in an editor. If the string contains the special substrings
     * %file or %line, they will be replaced with the correct data.
     *
     * @example "txmt://open?url=%file&line=%line"
     *
     * @var mixed $editor
     *
     */
    private $editor;

    /**
     * A list of known editor strings
     *
     * @var array
     */
    private $editors = [
        self::EDITOR_SUBLIME  => 'subl://open?url=file://%file&line=%line',
        self::EDITOR_TEXTMATE => 'txmt://open?url=file://%file&line=%line',
        self::EDITOR_EMACS    => 'emacs://open?url=file://%file&line=%line',
        self::EDITOR_MACVIM   => 'mvim://open/?url=file://%file&line=%line',
        self::EDITOR_PHPSTORM => 'phpstorm://open?file=%file&line=%line',
        self::EDITOR_IDEA     => 'idea://open?file=%file&line=%line',
        self::EDITOR_VSCODE   => 'vscode://file/%file:%line',
        self::EDITOR_ATOM     => 'atom://core/open/file?filename=%file&line=%line',
        self::EDITOR_ESPRESSO => 'x-espresso://open?filepath=%file&lines=%line'
    ];

    public function __construct(ViewRenderer $viewRenderer, ?FilesystemOperator $filesystem = null) {
        $this->viewRenderer = $viewRenderer;
        $this->filesystem = $filesystem ?? new Filesystem(new LocalFilesystemAdapter(__DIR__ . '/Presentation/resources'));
    }

    /**
     * @return int|null
     *
     * @throws Exception
     */
    public function handle(): ?int {

        $exception = $this->getException();
        $inspector = $this->getInspector();
        $frames = $this->getExceptionFrames();
        $code = $this->getExceptionCode();

        $data = [
            'styles' => ['error.css'],
            'scripts' => []
        ];

        echo $this->viewRenderer->render('error', $data);

        return Handler::QUIT;
    }

    /**
     * Get the stack trace frames of the exception that is currently being handled.
     *
     * @return FrameCollection;
     */
    protected function getExceptionFrames(): FrameCollection {

        $frames = $this->getInspector()->getFrames();

        if ($this->getApplicationPaths()) {

            foreach ($frames as $frame) {

                foreach ($this->getApplicationPaths() as $path) {

                    if (strpos($frame->getFile(), $path) === 0) {
                        $frame->setApplication(true);
                        break;
                    }

                }
            }
        }

        return $frames;
    }

    /**
     * Get the code of the exception that is currently being handled.
     *
     * @return int
     */
    protected function getExceptionCode(): int {

        $exception = $this->getException();

        $code = $exception->getCode();

        if ($exception instanceof ErrorException) {
            // ErrorExceptions wrap the php-error types within the 'severity' property
            $code = Misc::translateErrorCode($exception->getSeverity());
        }

        return $code;
    }

    /**
     * Given a string file path, and an integer file line,
     * executes the editor resolver and returns, if available,
     * a string that may be used as the href property for that
     * file reference.
     *
     * @param string $filePath
     * @param int $line
     *
     * @return string|bool
     * @throws InvalidArgumentException If editor resolver does not return a string
     */
    public function getEditorHref($filePath, $line) {

        $editor = $this->getEditor($filePath, $line);

        if (empty($editor)) {
            return false;
        }

        // Check that the editor is a string, and replace the
        // %line and %file placeholders:
        if ( ! isset($editor['url']) || ! is_string($editor['url'])) {
            throw new UnexpectedValueException(__METHOD__ . ' should always resolve to a string or a valid editor array; got something else instead.');
        }

        $editor['url'] = str_replace('%line', rawurlencode($line), $editor['url']);
        $editor['url'] = str_replace('%file', rawurlencode($filePath), $editor['url']);

        return $editor['url'];
    }

    /**
     * Given a boolean if the editor link should
     * act as an Ajax request. The editor must be a
     * valid callable function/closure
     *
     * @param string $filePath
     * @param int $line
     *
     * @return array
     */
    protected function getEditor($filePath, $line): array {

        if ( ! $this->editor || ( ! is_string($this->editor) && ! is_callable($this->editor))) {
            return [];
        }

        if (is_string($this->editor) && isset($this->editors[$this->editor]) && ! is_callable($this->editors[$this->editor])) {
            return [
                'ajax' => false,
                'url'  => $this->editors[$this->editor],
            ];
        }

        if (is_callable($this->editor) || (isset($this->editors[$this->editor]) && is_callable($this->editors[$this->editor]))) {

            if (is_callable($this->editor)) {
                $callback = call_user_func($this->editor, $filePath, $line);
            } else {
                $callback = call_user_func($this->editors[$this->editor], $filePath, $line);
            }

            if (empty($callback)) {
                return [];
            }

            if (is_string($callback)) {
                return [
                    'ajax' => false,
                    'url'  => $callback,
                ];
            }

            return [
                'ajax' => $callback['ajax'] ?? false,
                'url'  => $callback['url'] ?? $callback,
            ];
        }

        return [];
    }

    /**
     * Adds a path to the list of paths to be searched for
     * resources.
     *
     * @param string $path
     *
     * @return void
     * @throws InvalidArgumentException If $path is not a valid directory
     *
     */
    public function addResourcePath($path): void {

        if ( ! is_dir($path)) {
            throw new InvalidArgumentException("'$path' is not a valid directory");
        }

        array_unshift($this->searchPaths, $path);
    }

    /**
     * @return array
     */
    public function getResourcePaths(): array {
        return $this->searchPaths;
    }

    /**
     * Finds a resource, by its relative path, in all available search paths.
     * The search is performed starting at the last search path, and all the
     * way back to the first, enabling a cascading-type system of overrides
     * for all resources.
     *
     * @param string $resource
     *
     * @return string
     * @throws RuntimeException If resource cannot be found in any of the available paths
     *
     */
    protected function getResource($resource): string {

        // Search through available search paths, until we find the
        // resource we're after:
        foreach ($this->searchPaths as $path) {
            $fullPath = $path . "/$resource";

            if (is_file($fullPath)) {
                // Cache the result:
                $this->resourceCache[$resource] = $fullPath;
                return $fullPath;
            }
        }

        // If we got this far, nothing was found.
        throw new RuntimeException("Could not find resource '$resource' in any resource paths." . '(searched: ' . implode(', ', $this->searchPaths) . ')');
    }

    /**
     * @param string $resourcesPath
     *
     * @return void
     * @deprecated
     *
     */
    public function setResourcesPath($resourcesPath): void {
        $this->addResourcePath($resourcesPath);
    }

    /**
     * Return the application paths.
     *
     * @return array|null
     */
    public function getApplicationPaths(): ?array {
        return $this->applicationPaths;
    }

    /**
     * Set the application paths.
     *
     * @param array $applicationPaths
     */
    public function setApplicationPaths($applicationPaths): void {
        $this->applicationPaths = $applicationPaths;
    }

    /**
     * Set the application root path.
     *
     * @param string $applicationRootPath
     */
    public function setApplicationRootPath($applicationRootPath): void {
        $this->templateHelper->setApplicationRootPath($applicationRootPath);
    }

    /**
     * blacklist a sensitive value within one of the super-global arrays.
     *
     * @param $superGlobalName string the name of the super-global array, e.g. '_GET'
     * @param $key string the key within the super-global
     */
    public function blacklist($superGlobalName, $key): void {
        $this->blacklist[$superGlobalName][] = $key;
    }

    /**
     * Checks all values within the given super-global array.
     * Blacklisted values will be replaced by a equal length string containing only '*' characters.
     *
     * We intentionally dont rely on $GLOBALS as it depends on 'auto_globals_jit' php.ini setting.
     *
     * @param $superGlobal array One of the super-global arrays
     * @param $superGlobalName string the name of the super-global array, e.g. '_GET'
     *
     * @return array $values without sensitive data
     */
    private function masked(array $superGlobal, $superGlobalName): array {

        $blacklisted = $this->blacklist[$superGlobalName];

        $values = $superGlobal;

        foreach ($blacklisted as $key) {

            if (isset($superGlobal[$key]) && is_string($superGlobal[$key])) {
                $values[$key] = str_repeat('*', strlen($superGlobal[$key]));
            }
        }

        return $values;
    }

}
