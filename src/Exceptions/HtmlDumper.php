<?php declare(strict_types=1);

namespace Ether\Exceptions;

use Symfony\Component\VarDumper\Cloner\Data;
use Symfony\Component\VarDumper\Cloner\VarCloner;
use Symfony\Component\VarDumper\Dumper\HtmlDumper as BaseHtmlDumper;

final class HtmlDumper extends BaseHtmlDumper {

    protected $dumpHeader = '';

    public function dumpVariable($variable): string {

        $clonedData = (new VarCloner())->cloneVar($variable)->withMaxDepth(3);

        return $this->dump($clonedData);
    }

    public function dump(Data $data, $output = null, array $extraDisplayOptions = []): string {
        return parent::dump($data, true, [
            'maxDepth' => 3,
            'maxStringLength' => 160
        ]);
    }
}
