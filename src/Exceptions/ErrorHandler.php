<?php declare(strict_types=1);

namespace Ether\Exceptions;

use Whoops\Util\Misc;
use Ether\Config\Config;
use Whoops\Run as Whoops;
use Whoops\Handler\Handler;
use Ether\Application as Ether;
use Whoops\Handler\PrettyPageHandler;
use Whoops\Handler\JsonResponseHandler;

final class ErrorHandler {

    /** @var Ether */
    private $ether;

    /** @var Config */
    private $config;

    /** @var Whoops */
    private $whoops;

    /** @var PrettyPageHandler */
    private $prettyPageHandler;

    /** @var JsonResponseHandler */
    private $jsonResponseHandler;

    public function __construct(
        Ether $ether,
        Config $config,
        PrettyPageHandler $prettyPageHandler,
        JsonResponseHandler $jsonResponseHandler,
        Whoops $whoops
    ) {
        $this->ether = $ether;
        $this->config = $config;
        $this->whoops = $whoops;
        $this->prettyPageHandler = $prettyPageHandler;
        $this->jsonResponseHandler = $jsonResponseHandler;
    }

    public function register(): void {

        $debug = filter_var($this->config->get('app.debug'), FILTER_VALIDATE_BOOLEAN);

        if ($debug === null || $debug === true) {

            if (Misc::isAjaxRequest()) {
                $this->whoops->appendHandler($this->jsonResponseHandler);
            } else {
                $this->whoops->appendHandler($this->prettyPageHandler);
            }

        } else {

            $this->whoops->appendHandler(function (/** @noinspection PhpUnusedParameterInspection */ $exception, $inspector, $run) {

                require $this->ether->publicPath() . '/views/errors/error.500.php';

                return Handler::QUIT;

            });

        }

        $this->whoops->register();
    }
}
