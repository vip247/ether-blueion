<?php
/** @noinspection PhpUndefinedVariableInspection */
?>
<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <meta name="robots" content="noindex,nofollow"/>
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"/>

        <title>Error</title>

        <?php foreach ($styles as $style) { ?>
            <link rel="stylesheet" href="/error/css/<?php echo $style ?>">
        <?php } ?>
    </head>
    <body>

        <header>
            <div class="header__top"></div>
            <div class="header__bottom"></div>
        </header>

        <?php foreach ($scripts as $script) { ?>
            <script src="/error/js/<?php $script ?>"></script>
        <?php } ?>
    </body>
</html>
