<?php declare(strict_types=1);

namespace Ether\Exceptions\Presentation;

use Ether\Exceptions\ErrorPageHandler;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

final class ExceptionHandlerResourceController {

    /**
     * @param Request $request
     * @param string $type
     * @param string $name
     *
     * @return Response
     */
    public function __invoke(Request $request, string $type, string $name): Response {

        $content = '';
        $contentType = ['Content-Type' => 'plain/text'];

        if($type === 'css') {
            $content = file_get_contents(ErrorPageHandler::$styles[$name]);
            $contentType = ['Content-Type' => 'text/css'];
        }

        if($type === 'js') {
            $content = file_get_contents(ErrorPageHandler::$scripts[$name]);
            $contentType = ['Content-Type' => 'application/js'];
        }

       return new Response($content, 200, $contentType);
    }

}
