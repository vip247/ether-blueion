<?php declare(strict_types=1);

namespace Ether\Exceptions\Domain\Tab;

use JsonSerializable;

final class Tab implements JsonSerializable {

    /** @var string */
    private $name;

    /** @var array  */
    public $styles = [];

    /** @var array  */
    public $scripts = [];

    /**
     * @return string
     */
    private function name(): string {
        return $this->name;
    }

    /**
     * @param string $name
     * @param string $path
     *
     * @return void
     */
    public function addStyle(string $name, string $path): void {
        $this->styles[$name] = $path;
    }

    /**
     * @param string $name
     * @param string $path
     *
     * @return void
     */
    public function addScript(string $name, string $path): void {
        $this->scripts[$name] = $path;
    }

    public function jsonSerialize() {
        return [
            'title' => $this->name()
        ];
    }
}
