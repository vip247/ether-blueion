<?php

use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollection;
use Ether\Exceptions\Presentation\ExceptionHandlerResourceController;

$routes = new RouteCollection();

/* ----------------------------------------------------------------------------------------------- */
/* Exception Handler Resources Route
/* ----------------------------------------------------------------------------------------------- */

$routes->add('exception.handler.resources', new Route('/error/{type}/{name}', [
    'controller' => ExceptionHandlerResourceController::class
]));

return $routes;
