<?php declare(strict_types=1);

/**
 *
 * Gets the short name of a class or object from
 * a fully qualified name e.g:
 * self::class, static::class, __CLASS__
 *
 * @param object|$class
 *
 */
if ( ! function_exists('classBasename')) {

    function classBasename($class) {
        $class = \is_object($class) ? get_class($class) : $class;

        return basename(str_replace('\\', '/', $class));
    }

}

/**
 *
 * Format int (size) with specific preceision
 *
 * @param float|int $size
 * @param int $precision
 *
 */
if ( ! function_exists('formatSize')) {

    function formatSize($size, $precision = 2) {

        static $units = ['B','kB','MB','GB','TB','PB','EB','ZB','YB'];
        $step = 1024;
        $i = 0;

        while (($size / $step) > 0.9) {
            $size = $size / $step;
            $i++;
        }

        return round($size, $precision).$units[$i];
    }

}

/**
 *
 * Finds the position of the first occurrence of a substring in a string
 * NB: needle can be an array of needles
 *
 * @param float|int $size
 * @param int $precision
 *
 */
if ( ! function_exists('strposa')) {

    function strposa($haystack, $needles, $offset = 0) {

        if (is_array($needles)) {

            foreach ($needles as $needle) {

                $pos = strposa($haystack, $needle);

                if ($pos !== false) {
                    return $pos;
                }
            }

            return false;

        } else {
            return strpos($haystack, $needles, $offset);
        }
    }

}
