<?php declare(strict_types=1);

namespace Ether\Config;

use Ether\Application as Ether;
use League\Flysystem\Filesystem;
use Ether\Traits\DotArrayAccessTrait;
use Ether\Exceptions\RuntimeException;
use League\Flysystem\FilesystemOperator;
use League\Flysystem\Local\LocalFilesystemAdapter;
use Symfony\Component\Config\Resource\FileResource;
use Symfony\Component\Config\Loader\LoaderInterface as Loader;
use Symfony\Component\Config\ConfigCacheInterface as ConfigCache;

final class Config {

    use DotArrayAccessTrait;

    /** @var Ether */
    private $ether;

    /** @var ConfigCache  */
    private $configCache;

    /** @var array  */
    private $items = [];

    /** @var Loader */
    private $loader;

    /** @var FilesystemOperator */
    private $filesystem;

    public function __construct(
        Ether $ether,
        Loader $loader,
        ConfigCache $configCache,
        ?FilesystemOperator $filesystem = null
    ) {
        $this->ether = $ether;
        $this->loader = $loader;
        $this->filesystem = $filesystem ?? new Filesystem(new LocalFilesystemAdapter($this->ether->storagePath()));
        $this->configCache = $configCache;
    }

    public function clearCacheIfDebug(): void {

        if(filter_var(getenv('APP_DEBUG'), FILTER_VALIDATE_BOOLEAN)) {

            foreach ($this->filesystem->listContents('cache') as $item) {

                if($item['type'] === 'dir' && in_array(basename($item['path']), ['config', 'views', 'routes'])) {
                    $this->filesystem->deleteDir($item['path']);
                }
            }

        }

    }

    /**
     * @param string $configPath
     *
     * @return Config
     *
     * @throws RuntimeException
     */
    public function load(string $configPath): Config {

        if ( ! $this->configCache->isFresh()) {

            $this->loader->setCurrentDir($configPath);

            $items = $this->loader->import('*.config.php');

            $this->read($items);

            foreach ($this->loader->getFileResources() as $resource ) {
                $configResources[] = new FileResource($resource);
            }

            if(file_exists($this->ether->basePath() . '/.env.local')) {

                $configResources[] = new FileResource($this->ether->basePath() . '/.env.local');

            } else if(file_exists($this->ether->basePath() . '/.env')) {

                $configResources[] = new FileResource($this->ether->basePath() . '/.env');

            }

            if ( ! empty($configResources) && filter_var($this->items['app']['debug'], FILTER_VALIDATE_BOOLEAN) !== true) {

                $this->configCache->write(sprintf('<?php return %s;', var_export($items, true)));

            }

        } else {

            /** @noinspection PhpIncludeInspection */
            $items = require $this->configCache->getPath();

            $this->read($items);

        }

        if(empty($items)) {
            throw new RuntimeException('Config resources not found. Unable to load application configuration.');
        }

        return $this;
    }

    /**
     * Read config values into class items array
     *
     * @param array $items
     */
    private function read(array $items): void {

        foreach ($items as $key => $value) {

            $configName = key($value);
            $this->items[$configName] = $value[$configName];

        }
    }
}
