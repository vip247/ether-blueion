<?php

namespace Ether\Database;

use Ether\Config\Config;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\DriverManager;
use Doctrine\DBAL\DBALException;
use Ether\Database\DBAL\Helpers\TypeHelper;

final class DBManager {

    private $config;

    public function __construct(Config $config) {
        $this->config = $config;
    }

    public function getConnection(string $connectionConfigKey = null): Connection {

        if($connectionConfigKey === null) {

            $connectionConfig = $this->config->get('database.connection.default');

        } else {

            $connectionConfig = $this->config->get($connectionConfigKey);
        }


        $connection = null;

        try {

            $typeHelper = new TypeHelper(DriverManager::getConnection($connectionConfig));

            $connection = $typeHelper->registerTypes();

        } catch (DBALException $e) {

            error_log($e->getMessage());

        }

        return $connection;

    }

}
