<?php declare(strict_types=1);

namespace Ether\Database\Orm;

class Locator {

    protected $config;
    protected $mapper = [];

    public function __construct(Config $config) {
        $this->config = $config;
    }

    /**
     * Get config class mapper was instantiated with
     *
     * @param null $cfg
     *
     * @return Config|null
     *
     * @throws Exception
     */
    public function config($cfg = null): ?Config {

        if ($cfg !== null) {
            $this->config = $cfg;
        }

        if (empty($this->config)) {
            throw new Exception("Config object must be set with \Ether\Database\Orm\Config->config(<\Ether\Database\Orm\Config object>)");
        }

        return $this->config;
    }

    /**
     * Get mapper for specified entity
     *
     * @param string $entityName Name of Entity object to load mapper for
     *
     * @return Mapper
     */
    public function mapper($entityName): Mapper {

        if ( ! isset($this->mapper[$entityName])) {

            // Get custom mapper, if set
            $mapper = $entityName::mapper();

            // Fallback to generic mapper
            if ($mapper === false) {
                $mapper = Mapper::class;
            }

            $this->mapper[$entityName] = new $mapper($this, $entityName);
        }

        return $this->mapper[$entityName];
    }
}
