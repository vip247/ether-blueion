<?php declare(strict_types=1);

namespace Ether\Database\Orm;

use PDO;
use Closure;
use Throwable;
use PDOStatement;
use Ramsey\Uuid\Uuid;
use Valitron\Validator;
use Doctrine\DBAL\DBALException;
use Ether\Database\Orm\Entity\Manager;
use Doctrine\DBAL\Types\Type;
use InvalidArgumentException;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\ConnectionException;
use Ether\Database\Orm\Query\Resolver;
use Ether\Database\Orm\Relation\HasOne;
use Ether\Database\Orm\Relation\HasMany;
use Doctrine\DBAL\Schema\SchemaException;
use Ether\Database\Orm\Relation\BelongsTo;
use Ether\Database\Orm\Relation\HasManyThrough;
use Ether\Database\Orm\Relation\RelationAbstract;
use Ether\Database\Orm\Entity\Collection;

class Mapper implements MapperInterface {

    protected static $entityManager = [];
    protected static $eventEmitter;

    // Entity manager
    protected $locator;
    protected $entityName;

    // Temporary relations
    protected $withRelations = [];

    // Class Names for required classes - Here so they can be easily overridden
    protected $_collectionClass = Collection::class;
    protected $_queryClass = Query::class;

    // Array of hooks
    protected $_hooks = [];

    /**
     *  Constructor Method
     *
     * @param Locator $locator
     * @param $entityName
     */
    public function __construct(Locator $locator, $entityName) {
        $this->locator = $locator;
        $this->entityName = $entityName;

        $this->loadEvents();
    }

    /**
     * Reset and load Events for mapped entity
     */
    public function loadEvents() {
        $entityName = $this->entity();
        $this->eventEmitter()->removeAllListeners();
        $entityName::events($this->eventEmitter());
    }

    /**
     * Get name of the Entity class mapper was instantiated with
     *
     * @return string $entityName
     */
    public function entity(): string {
        return $this->entityName;
    }

    /**
     * Event emitter for this mapper
     *
     * @return EventEmitter
     */
    public function eventEmitter(): EventEmitter {

        $entityName = $this->entity();

        if (empty(self::$eventEmitter[$entityName])) {
            self::$eventEmitter[$entityName] = new EventEmitter();
        }

        return self::$eventEmitter[$entityName];
    }

    /**
     * Get query class name to use
     *
     * @return string
     */
    public function queryClass(): string {
        return $this->_queryClass;
    }

    /**
     * Relation: HasMany
     *
     * @param EntityInterface $entity
     * @param $entityName
     * @param $foreignKey
     * @param null $localValue
     *
     * @return HasMany
     *
     * @throws Exception
     */
    public function hasMany(EntityInterface $entity, $entityName, $foreignKey, $localValue = null): HasMany {
        if ($localValue === null) {
            $localValue = $this->primaryKey($entity);
        }

        if ( ! is_subclass_of($entityName, EntityInterface::class)) {
            throw new InvalidArgumentException('Related entity name must be a '
                . "valid entity that extends Ether\Database\Orm\Entity. Given '" . $entityName . "'.");
        }

        return new Relation\HasMany($this, $entityName, $foreignKey, $this->primaryKeyField(), $localValue);
    }

    /**
     * Get value of primary key for given row result
     *
     * @param EntityInterface $entity Instance of an entity to find the primary key of
     *
     * @throws Exception
     */
    public function primaryKey(EntityInterface $entity) {

        $pkField = $this->entityManager()->primaryKeyField();

        if (empty($pkField)) {
            throw new Exception(get_class($entity) . ' has no primary key field. Please mark one of its fields as autoincrement or primary.');
        }

        return $entity->$pkField;
    }

    /**
     * Entity manager class for storing information and meta-data about entities
     *
     * @return Manager
     * @throws Exception
     */
    public function entityManager(): Manager {

        $entityName = $this->entity();

        if ( ! isset(self::$entityManager[$entityName])) {
            self::$entityManager[$entityName] = new Entity\Manager($entityName);
        }

        return self::$entityManager[$entityName];
    }

    /**
     * Get value of primary key for given row result
     */
    public function primaryKeyField() {
        return $this->entityManager()->primaryKeyField();
    }

    /**
     * Relation: HasManyThrough
     *
     * @param EntityInterface $entity
     * @param $hasManyEntity
     * @param $throughEntity
     * @param $selectField
     * @param $whereField
     *
     * @return HasManyThrough
     */
    public function hasManyThrough(EntityInterface $entity, $hasManyEntity, $throughEntity, $selectField, $whereField): HasManyThrough {

        $localPkField = $this->primaryKeyField();
        $localValue = $entity->$localPkField;

        if ( ! is_subclass_of($hasManyEntity, EntityInterface::class)) {
            throw new InvalidArgumentException('Related entity name must be a '
                . "valid entity that extends Ether\Database\Orm\Entity. Given '" . $hasManyEntity . "'.");
        }

        if ( ! is_subclass_of($throughEntity, EntityInterface::class)) {
            throw new InvalidArgumentException('Related entity name must be a '
                . "valid entity that extends Ether\Database\Orm\Entity. Given '" . $throughEntity . "'.");
        }

        return new Relation\HasManyThrough($this, $hasManyEntity, $throughEntity, $selectField, $whereField, $localValue);
    }

    /**
     * Relation: HasOne
     *
     * HasOne assumes that the foreignKey will be on the foreignEntity.
     *
     * @param EntityInterface $entity
     * @param $foreignEntity
     * @param $foreignKey
     *
     * @return HasOne
     */
    public function hasOne(EntityInterface $entity, $foreignEntity, $foreignKey): HasOne {
        $localKey = $this->primaryKeyField();

        if ( ! is_subclass_of($foreignEntity, EntityInterface::class)) {
            throw new InvalidArgumentException('Related entity name must be a '
                . "valid entity that extends Ether\Database\Orm\Entity. Given '" . $foreignEntity . "'.");
        }

        // Return relation object so query can be lazy-loaded
        return new HasOne($this, $foreignEntity, $foreignKey, $localKey, $entity->$localKey);
    }

    /**
     * Relation: BelongsTo
     *
     * BelongsTo assumes that the localKey will reference the foreignEntity's
     * primary key. If this is not the case, you probably want to use the
     * 'hasOne' relationship instead.
     *
     * @param EntityInterface $entity
     * @param $foreignEntity
     * @param $localKey
     *
     * @return BelongsTo
     */
    public function belongsTo(EntityInterface $entity, $foreignEntity, $localKey): BelongsTo {

        if ( ! is_subclass_of($foreignEntity, EntityInterface::class)) {
            throw new InvalidArgumentException('Related entity name must be a '
                . "valid entity that extends Ether\Database\Orm\Entity. Given '" . $foreignEntity . "'.");
        }

        $foreignMapper = $this->getMapper($foreignEntity);
        $foreignKey = $foreignMapper->primaryKeyField();

        // Return relation object so query can be lazy-loaded
        return new BelongsTo($this, $foreignEntity, $foreignKey, $localKey, $entity->$localKey);
    }

    /**
     * Get mapper for specified entity
     *
     * @param string $entityName Name of Entity object to load mapper for
     *
     * @return Mapper
     */
    public function getMapper($entityName): Mapper {
        return $this->locator->mapper($entityName);
    }

    /**
     * Get field information exactly how it is defined in the class
     *
     * @return array
     * Defined fields plus all defaults for full array of all possible options
     * @throws Exception
     */
    public function fieldsDefined(): array {
        return $this->entityManager()->fieldsDefined();
    }

    /**
     * Get defined relations
     */
    public function relations(): array {
        return $this->entityManager()->relations();
    }

    /**
     * Return scopes defined by this mapper. Scopes are called from the
     * Query object as a sort of in-context dynamic query method
     *
     * @return array
     * Array of closures with method name as the key
     */
    public function scopes(): array {
        $entityClass = $this->entityName;
        return $entityClass::scopes();
    }

    /**
     * Return field type
     *
     * @param string $field Field name
     *
     * @return mixed  Field type string or boolean false
     * @throws Exception
     */
    public function fieldType($field) {
        $fields = $this->fields();

        return $this->fieldExists($field) ? $fields[$field]['type'] : false;
    }

    /**
     * Get a new entity object, set given data on it, and save it
     *
     * @param array $data array of key/values to set on new Entity instance
     * @param array $options array of save options that will be passed to insert()
     *
     * @return object         Instance of $entityClass with $data set on it
     * @throws DBALException
     * @throws Exception
     */
    public function create(array $data, array $options = []) {

        $entity = $this->build($data);

        if ($this->insert($entity, $options)) {
            return $entity;
        }

        throw new Exception('Unable to insert new ' . get_class($entity) . ' - Errors: ' . var_export($entity->errors(), true));
    }

    /**
     * Get a new entity object, set given data on it
     *
     * @param array $data array of key/values to set on new Entity instance
     *
     * @return EntityInterface Instance of $entityClass with $data set on it
     */
    public function build(array $data): EntityInterface {
        $className = $this->entity();

        return new $className($data);
    }

    /**
     * Insert record
     *
     * @param mixed $entity Entity object or array of field => value pairs
     * @param array $options Array of adapter-specific options
     *
     * @return bool|int|mixed|string
     * @throws Exception
     * @throws DBALException
     */
    public function insert($entity, array $options = []) {

        if (is_object($entity)) {
            $entityName = get_class($entity);
            $this->entity($entityName);
        } elseif (is_array($entity)) {
            $entityName = $this->entity();
            $entity = $this->get()->data($entity);
        } else {
            throw new Exception(__METHOD__ . ' Accepts either an entity object or entity data array');
        }

        // Run beforeSave and beforeInsert to know whether or not we can continue
        if (
            false === $this->eventEmitter()->emit('beforeSave', [$entity, $this])
            || false === $this->eventEmitter()->emit('beforeInsert', [$entity, $this])
        ) {
            return false;
        }

        // Run validation unless disabled via options
        if ( ! isset($options['validate']) || (isset($options['validate']) && $options['validate'] !== false)) {
            if ( ! $this->validate($entity, $options)) {
                return false;
            }
        }

        // Ensure there is actually data to update
        $data = $entity->data(null, true, false);
        if (count($data) > 0) {

            if (isset($options['relations']) && $options['relations'] === true) {
                $this->saveBelongsToRelations($entity, $options);
                $data = $entity->data(null, true, false);
            }

            $pkField = $this->primaryKeyField();
            $pkFieldInfo = $this->fieldInfo($pkField);

            // Save only known, defined fields
            $entityFields = $this->fields();
            $extraData = array_diff_key($data, $entityFields);
            $data = array_intersect_key($data, $entityFields);

            // If there are extra fields here, throw an error
            if ( ! isset($options['strict']) || (isset($options['strict']) && $options['strict'] === true)) {
                if (count($extraData) > 0) {
                    throw new Exception('Insert error: Unknown fields provided for ' . $entityName . ": '" . implode("', '", array_keys($extraData)) . "'");
                }
            }

            // Don't pass NULL for "serial" columns (causes issues with PostgreSQL + others)
            if (array_key_exists($pkField, $data) && empty($data[$pkField])) {

                if($pkFieldInfo['type'] === 'uuid_binary_ordered_time') {

                    $data[$pkField] = Uuid::uuid1();

                } else if($pkFieldInfo['type'] === 'uuid') {

                    $data[$pkField] = Uuid::uuid4();

                } else if($pkFieldInfo['type'] === 'guid') {

                    $data[$pkField] = Uuid::uuid4()->toString();

                } else {

                    unset($data[$pkField]);

                }

            }

            // Send to adapter via named connection
            $table = $this->table();
            $result = $this->resolver()->create($table, $data, $this->getTypes($entity::fields()));

            if ($result) {

                $connection = $this->connection();

                if (array_key_exists($pkField, $data)) {

                    // PK value was given on insert, just return it
                    $result = $data[$pkField];

                } else if ($pkFieldInfo && $pkFieldInfo['autoincrement'] === true) {

                    if ($this->connectionIs('pgsql')) {

                        // Allow custom sequence name
                        $fieldAliasMappings = $this->entityManager()->fieldAliasMappings();
                        $sequenceField = $fieldAliasMappings[$pkField] ?? $pkField;
                        $sequenceName = $pkFieldInfo['sequence_name'] ?? $this->resolver()->escapeIdentifier($table . '_' . $sequenceField . '_seq');

                        $result = ($pkFieldInfo['type'] === 'integer') ? (int) $connection->lastInsertId($sequenceName) : $connection->lastInsertId($sequenceName);

                    } else {

                        $result = ($pkFieldInfo['type'] === 'integer') ? (int) $connection->lastInsertId() : $connection->lastInsertId();

                    }

                }
            }

            // Update primary key on entity object
            $entity->$pkField = $result;

            $entity->isNew(false);
            $entity->data($entity->data(null, true, false), false);

            if (isset($options['relations']) && $options['relations'] === true) {
                $this->saveHasRelations($entity, $options);
            }

            if ($result) {
                $this->prepareEntity($entity);
            }

            // Run afterSave and afterInsert
            if (
                false === $this->eventEmitter()->emit('afterSave', [$entity, $this, &$result])
                || false === $this->eventEmitter()->emit('afterInsert', [$entity, $this, &$result])
            ) {
                $result = false;
            }
        } else {
            $result = false;
        }

        return $result;
    }

    /**
     * Get a new entity object, or an existing
     * entity from identifiers
     *
     * @param mixed $identifier Primary key or array of key/values
     *
     * @return mixed Depends on input, false if $identifier is scalar and no entity exists
     *
     * @throws DBALException
     * @throws Exception
     */
    public function get($identifier = false) {

        $entityClass = $this->entity();
        $pkField = $this->primaryKeyField();
        $pkFieldType = $this->fieldType($pkField);

        if (false === $identifier) {

            // No parameter passed, create a new empty entity object
            $entity = new $entityClass();
            $entity->data([$pkField => null]);

        } elseif (is_array($identifier)) {

            // An array was passed, create a new entity with that data
            $entity = new $entityClass($identifier);

            $entity->data([$pkField => null]);

        } else {

            // Find record by primary key
            $pkValue = $this->connection()->convertToDatabaseValue($identifier, $pkFieldType);
            $entity = $this->first([$pkField => $pkValue]);

            if ( ! $entity) {
                return false;
            }

        }

        // Set default values if entity not loaded
        if ( ! $this->primaryKey($entity)) {

            $entityDefaultValues = $this->entityManager()->fieldDefaultValues();

            if (count($entityDefaultValues) > 0) {
                $entity->data($entityDefaultValues);
            }
        }

        return $entity;
    }

    /**
     * Find first record matching given conditions
     *
     * @param array $conditions Array of conditions in column => value pairs
     *
     * @return bool
     * @throws DBALException
     * @throws Exception
     */
    public function first(array $conditions = []) {

        if (empty($conditions)) {

            $query = $this->select()->limit(1);

        } else {

            $query = $this->where($conditions)->limit(1);

        }

        $collection = $query->execute();

        if ($collection) {
            return $collection->first();
        } else {
            return false;
        }
    }

    /**
     * Begin a new database query - get query builder
     * Acts as a kind of factory to get the current adapter's query builder object
     *
     * @param mixed $fields String for single field or array of fields
     *
     * @return Query
     * @throws Exception
     */
    public function select($fields = '*') {
        $table = $this->table();

        return $this->queryBuilder()->select($fields)->from($table);
    }

    /**
     * Get table name
     *
     * @return string Name of table defined on entity class
     * @throws Exception
     */
    public function table() {
        return $this->entityManager()->table();
    }

    /**
     * Begin a new database query - get query builder
     * Acts as a kind of factory to get the current adapter's query builder object
     *
     * @return Query
     */
    public function queryBuilder() {
        $query = new $this->_queryClass($this);

        return $query;
    }

    /**
     * Find records with given conditions
     * If all parameters are empty, find all records
     *
     * @param array $conditions Array of conditions in column => value pairs
     *
     * @return Query
     * @throws DBALException
     * @throws Exception
     */
    public function where(array $conditions = []): Query {
        return $this->select()->where($conditions);
    }

    /**
     * Run set validation rules on fields
     *
     * @param EntityInterface $entity
     * @param array $options
     *
     * @return boolean
     * @throws DBALException
     * @throws Exception
     */
    public function validate(EntityInterface $entity, array $options = []): bool {

        $v = new Validator($entity->data());

        // Run beforeValidate to know whether or not we can continue
        if (false === $this->eventEmitter()->emit('beforeValidate', [$entity, $this, $v])) {
            return false;
        }

        // Check validation rules on each field
        $uniqueWhere = [];
        foreach ($this->fields() as $field => $fieldAttrs) {

            // Required field
            if (
                // Explicitly required
                (isset($fieldAttrs['required']) && true === $fieldAttrs['required'])
                // Primary key without autoincrement and not uuid or uuid_binary_ordered_time or guid
                || ($fieldAttrs['primary'] === true && $fieldAttrs['autoincrement'] === false && $fieldAttrs['type'] !== 'uuid_binary_ordered_time' && $fieldAttrs['type'] !== 'uuid' && $fieldAttrs['type'] !== 'guid')
            ) {
                $v->rule('required', $field);
            }

            // Unique field
            if (isset($fieldAttrs['unique']) && ! empty($fieldAttrs['unique']) && $entity->$field !== null && $entity->isNew()) {

                if (is_string($fieldAttrs['unique'])) {
                    $fieldAttrs['unique'] = [$fieldAttrs['unique']];
                }

                if (is_array($fieldAttrs['unique'])) {

                    foreach ($fieldAttrs['unique'] as $fieldKeyName) {
                        // Named group
                        $uniqueWhere[$fieldKeyName][$field] = $entity->$field;
                    }

                } else {
                    $uniqueWhere[$field] = $entity->$field;
                }

            }

            // Run only if field required
            if ($entity->$field !== null || $fieldAttrs['required'] === true) {

                // Field with 'options'
                if (isset($fieldAttrs['options']) && is_array($fieldAttrs['options'])) {
                    $v->rule('in', $field, $fieldAttrs['options']);
                }

                // Valitron validation rules
                if (isset($fieldAttrs['validation']) && is_array($fieldAttrs['validation'])) {

                    foreach ($fieldAttrs['validation'] as $rule => $ruleName) {

                        $params = [];
                        if (is_string($rule)) {
                            $params = (array) $ruleName;
                            $ruleName = $rule;
                        }

                        $params = array_merge([$ruleName, $field], $params);
                        call_user_func_array([$v, 'rule'], $params);
                    }
                }
            }
        }

        // Unique validation
        if ( ! empty($uniqueWhere)) {

            foreach ($uniqueWhere as $field => $value) {

                if ( ! is_array($value)) {
                    $value = [$field => $value];
                }

                $value = $this->convertToDatabaseValues($entity, $value);

                if ( ! in_array(null, $value, true) && $this->first($value) !== false) {
                    $entity->error($field, "" . ucwords(str_replace('_', ' ', $field)) . " '" . implode('-', $value) . "' is already taken.");
                }

            }

        }

        if ( ! $v->validate()) {
            $entity->errors($v->errors(), false);
        }

        //Relations validation
        if (isset($options['relations']) && $options['relations'] === true) {
            $this->validateRelations($entity);
        }

        // Run afterValidate to run additional/custom validations
        if (false === $this->eventEmitter()->emit('afterValidate', [$entity, $this, $v])) {
            return false;
        }

        // Return error result
        return ! $entity->hasErrors();
    }

    /**
     * Prepare data to be dumped to the data store
     *
     * @param string $entityName
     * @param array $data Key/value pairs of data to store
     *
     * @return array
     * @throws DBALException
     * @throws Exception
     */
    public function convertToDatabaseValues($entityName, array $data) {
        $dbData = [];
        $fields = $entityName::fields();
        $platform = $this->connection()->getDatabasePlatform();
        foreach ($data as $field => $value) {
            $typeHandler = Type::getType($fields[$field]['type']);
            $dbData[$field] = $typeHandler->convertToDatabaseValue($value, $platform);
        }

        return $dbData;
    }

    /**
     * Validate related entities using relations
     *
     * @param EntityInterface $entity
     *
     * @return EntityInterface
     * @throws DBALException
     * @throws Exception
     */
    protected function validateRelations($entity) {
        $relations = $entity->relations($this, $entity);
        foreach ($relations as $relationName => $relation) {
            if ($relation instanceof Relation\HasOne || $relation instanceof Relation\BelongsTo) {
                $relatedEntity = $entity->$relationName;
                if ($relatedEntity instanceof EntityInterface) {
                    $errorsRelated = $this->validateRelatedEntity($relatedEntity, $entity, $relation);
                    if (count($errorsRelated)) {
                        $entity->errors([$relationName => $errorsRelated], false);
                    }
                }
            } else if ($relation instanceof Relation\HasMany || $relation instanceof Relation\HasManyThrough) {
                $relatedEntities = $entity->$relationName;
                //No point in validating Queries since they are not modified
                if ((is_array($relatedEntities) || $relatedEntities instanceof Entity\Collection) && count($relatedEntities)) {
                    $errors = [];
                    foreach ($relatedEntities as $key => $related) {
                        $errorsRelated = $this->validateRelatedEntity($related, $entity, $relation);

                        if (count($errorsRelated)) {
                            $errors[$key] = $errorsRelated;
                        }
                    }
                    if (count($errors)) {
                        $entity->errors([$relationName => $errors], false);
                    }
                }
            }
        }

        return $entity;
    }

    /**
     * Validate related entity if it is new or modified only
     *
     * @param EntityInterface $relatedEntity
     * @param EntityInterface $entity
     * @param RelationAbstract $relation
     *
     * @return array Related entity errors
     * @throws DBALException
     * @throws Exception
     */
    protected function validateRelatedEntity(EntityInterface $relatedEntity, EntityInterface $entity, RelationAbstract $relation) {
        $tainted = $relatedEntity->isNew() || $relatedEntity->isModified();

        $errorsRelated = [];

        if ($tainted && ! $this->getMapper(get_class($relatedEntity))->validate($relatedEntity)) {
            $errorsRelated = $relatedEntity->errors();
            //Disable validation on foreign key field it will be filled up later on when the new entity is persisted
            if (($relation instanceof Relation\HasMany || $relation instanceof Relation\HasOne) && $relatedEntity->isNew()) {
                unset($errorsRelated[$relation->foreignKey()]);
            }
            $relatedEntity->errors($errorsRelated);
        }

        if ($relation instanceof Relation\BelongsTo && $entity->isNew()) {
            $errors = $entity->errors();
            unset($errors[$relation->localKey()]);
            $entity->errors($errors);
        }

        return $errorsRelated;
    }

    /**
     * Save related entities that have been assigned or loaded (only BelongsTo relations)
     * See saveHasRelations.
     *
     * @param EntityInterface $entity
     * @param array $options
     *
     * @return mixed
     *
     * @throws DBALException
     * @throws Exception
     */
    public function saveBelongsToRelations(EntityInterface $entity, array $options = []) {

        $relations = $entity->relations($this, $entity);

        $lastResult = false;
        foreach ($relations as $relationName => $relation) {
            if ($relation instanceof Relation\BelongsTo) {
                $lastResult = $relation->save($entity, $relationName, $options);
            }
        }

        return $lastResult;
    }

    /**
     * Check if field exists in defined fields
     *
     * @param string $field Field name to check for existence
     *
     * @return bool|mixed
     * @throws Exception
     */
    public function fieldInfo($field) {
        if ($this->fieldExists($field)) {
            return $this->fields()[$field];
        } else {
            return false;
        }
    }

    /**
     * Check if field exists in defined fields
     *
     * @param string $field Field name to check for existence
     *
     * @return bool
     * @throws Exception
     */
    public function fieldExists($field) {
        return array_key_exists($field, $this->fields());
    }

    /**
     * Get formatted fields with all necessary array keys and values.
     * Merges defaults with defined field values to ensure all options exist for each field.
     *
     * @return array
     *  Defined fields plus all defaults for full array of all possible options
     * @throws Exception
     */
    public function fields(): array {
        return $this->entityManager()->fields();
    }

    /**
     * Query resolver class for preparing and executing queries, then returning the results
     */
    public function resolver(): Resolver {
        return new Resolver($this);
    }

    /**
     * Test to see if collection is of the given type
     *
     * @param string Database type, something like "mysql", "sqlite", "pgsql", etc.
     *
     * @return bool
     * @throws Exception
     */
    public function connectionIs($type): bool {
        return stripos(get_class($this->connection()->getDriver()), $type) !== false;
    }

    /**
     * Get connection to use
     *
     * @param string $connectionName Named connection or entity class name
     *
     * @return Connection
     * @throws Exception
     */
    public function connection($connectionName = null): Connection {
        $connectionName = $connectionName ?: $this->entityManager()->connection();
        // Try getting connection based on given name
        if (empty($connectionName)) {
            return $this->config()->defaultConnection();
        } elseif ($connection = $this->config()->connection($connectionName)) {
            return $connection;
        } elseif ($connection = $this->config()->defaultConnection()) {
            return $connection;
        }

        throw new Exception("Connection '" . $connectionName . "' does not exist. Please setup connection using Ether\Database\Orm\Config::addConnection().");
    }

    /**
     * Get config class from locator
     *
     * @return Config|null
     * @throws Exception
     */
    public function config(): ?Config {
        return $this->locator->config();
    }

    /**
     * Save related entities that have been assigned or loaded (only HasOne, HasMany and HasManyThrough relations)
     * See saveBelongsToRelations.
     *
     * @param EntityInterface $entity
     * @param array $options
     *
     * @return mixed
     */
    public function saveHasRelations(EntityInterface $entity, array $options = []) {
        if ($entity->isNew()) {
            throw new InvalidArgumentException('The provided entity is new. The entity must be persisted before saving relations.');
        }

        $relations = $entity->relations($this, $entity);

        $lastResult = false;
        foreach ($relations as $relationName => $relation) {
            if ( ! $relation instanceof Relation\BelongsTo) {
                $lastResult = $relation->save($entity, $relationName, $options);
            }
        }

        return $lastResult;
    }

    /**
     * Prepare entity and load necessary objects on it
     *
     * @param EntityInterface $entity
     *
     * @return bool
     */
    public function prepareEntity(EntityInterface $entity): bool {

        $this->loadRelations($entity);

        if (false === $this->eventEmitter()->emit('afterLoad', [$entity, $this])) {
            return false;
        }

        return true;
    }

    /**
     * Load Relations for mapped entity
     *
     * @param EntityInterface $entity
     */
    public function loadRelations(EntityInterface $entity): void {

        $entityName = $this->entity();
        $relations = $entityName::relations($this, $entity);

        foreach ($relations as $relation => $query) {
            $entity->relation($relation, $query);
        }
    }

    /**
     * Find records with custom query
     *
     * @param string $sql Raw query or SQL to run against the data store
     * @param array $params
     *
     * @return bool|Collection
     * @throws DBALException
     * @throws Exception
     */
    public function query($sql, array $params = []) {

        $result = $this->connection()->executeQuery($sql, $params);

        if ($result) {
            return $this->collection($result);
        }

        return false;
    }

    /**
     * Create collection from Query object
     *
     * @param $cursor
     * @param array $with
     *
     * @return Collection
     *
     * @throws DBALException
     * @throws Exception
     */
    public function collection($cursor, $with = []): Collection {

        $entityName = $this->entity();
        $results = [];
        $resultsIdentities = [];

        // Ensure PDO only gives key => value pairs, not index-based fields as well
        // Raw PDOStatement objects generally only come from running raw SQL queries or other custom stuff
        if ($cursor instanceof PDOStatement) {
            $cursor->setFetchMode(PDO::FETCH_ASSOC);
        }

        // Fetch all results into new entity class
        // @todo Move this to collection class so entities will be lazy-loaded by Collection iteration
        foreach ($cursor as $data) {
            // Do type conversion
            $data = $this->convertToPHPValues($entityName, $data);

            $entity = new $entityName($data);
            $entity->isNew(false);

            $this->prepareEntity($entity);

            // Store in array for Collection
            $results[] = $entity;

            $pk = is_object($this->primaryKey($entity)) ? $this->primaryKey($entity)->toString() : $this->primaryKey($entity);

            if ( ! empty($pk) && ! in_array($pk, $resultsIdentities, true)) {
                $resultsIdentities[] = $pk;
            }

        }

        $collectionClass = $this->collectionClass();

        if( ! empty($resultsIdentities) && is_string($resultsIdentities[0])) {

            $resultsIdentities = array_map(static function($item) {
                return Uuid::fromString($item);
            }, $resultsIdentities);

        }

        $collection = new $collectionClass($results, $resultsIdentities, $entityName);

        if (empty($with) || count($collection) === 0) {
            return $collection;
        }

        return $this->with($collection, $entityName, $with);
    }

    /**
     * Retrieve data from the data store and map it to PHP values
     *
     * @param string $entityName
     * @param array $data Key/value pairs of data to store
     *
     * @return array
     * @throws DBALException
     * @throws Exception
     */
    public function convertToPHPValues($entityName, array $data): array {

        $phpData = [];
        $fields = $entityName::fields();
        $platform = $this->connection()->getDatabasePlatform();
        $data = $this->resolver()->dataWithOutFieldAliasMappings($data);
        $entityData = array_intersect_key($data, $fields);

        foreach ($data as $field => $value) {

            // Field is in the Entity definitions
            if (isset($entityData[$field])) {

                $typeHandler = Type::getType($fields[$field]['type']);
                $phpData[$field] = $typeHandler->convertToPHPValue($value, $platform);
                // Extra data returned with query (like calculated values, etc.)

            } else {
                $phpData[$field] = $value;
            }

        }

        return $phpData;
    }

    /**
     * Get collection class name to use
     *
     * @return string
     */
    public function collectionClass(): string {
        return $this->_collectionClass;
    }

    /**
     * Eager-load associations for an entire collection
     *
     * @param $collection
     * @param $entityName
     * @param array $with
     *
     * @return Collection
     *
     * @throws Exception
     * @internal Implementation may change... for internal use only
     */
    protected function with($collection, $entityName, $with = []): Collection {

        $eventEmitter = $this->eventEmitter();
        $return = $eventEmitter->emit('beforeWith', [$this, $collection, $with]);

        if (false === $return) {
            return $collection;
        }

        foreach ($with as $relationName) {

            // We only need a single entity from the collection, because we're
            // going to modify the query to pass in an array of all the
            // identity keys from the collection instead of just that single entity
            $singleEntity = $collection->first();

            // Ensure we have a valid entity object
            if ( ! ($singleEntity instanceof Entity)) {
                throw new Exception("Relation object must be instance of 'Ether\Database\Orm\Entity', given '" . get_class($singleEntity) . "'");
            }

            $relationObject = $singleEntity->relation($relationName);

            // Ensure we have a valid relation name
            if ($relationObject === false) {
                throw new Exception("Invalid relation name eager-loaded in 'with' clause: No relation on $entityName with name '$relationName'");
            }

            // Ensure we have a valid relation object
            if ( ! ($relationObject instanceof Relation\RelationAbstract)) {
                throw new Exception("Relation object must be instance of 'Ether\Database\Orm\Relation\RelationAbstract', given '" . get_class($relationObject) . "'");
            }

            // Hook so user can load custom relations their own way if they
            // want to, and then bypass the normal loading process by returning
            // false from their event
            $return = $eventEmitter->emit('loadWith', [$this, $collection, $relationName]);
            if (false === $return) {
                continue;
            }

            // Eager-load relation results back to collection
            $collection = $relationObject->eagerLoadOnCollection($relationName, $collection);
        }

        $eventEmitter->emit('afterWith', [$this, $collection, $with]);

        return $collection;
    }

    /**
     * Execute custom query with no handling - just return affected rows
     * Useful for UPDATE, DELETE, and INSERT queries
     *
     * @param string $sql Raw query or SQL to run against the data store
     * @param array $params
     *
     * @return int
     * @throws DBALException
     * @throws Exception
     */
    public function exec($sql, array $params = []): int {
        return $this->connection()->executeUpdate($sql, $params);
    }

    /**
     * Find all records
     *
     * @return Query
     *
     * @throws Exception
     */
    public function all(): Query {
        return $this->select();
    }

    /**
     * Save record
     * Will update if primary key found, insert if not
     * Performs validation automatically before saving record
     *
     * @param EntityInterface $entity Entity object
     * @param array optional Array of save options
     *
     * @return bool|int|mixed|string
     * @throws DBALException
     * @throws Exception
     */
    public function save(EntityInterface $entity, array $options = []) {
        // Check entity name
        $entityName = $this->entity();
        if ( ! ($entity instanceof $entityName)) {
            throw new InvalidArgumentException('Provided entity must be instance of ' . $entityName . ', instance of ' . get_class($entity) . ' given.');
        }

        if ($entity->isNew()) {
            $result = $this->insert($entity, $options);
        } else {
            $result = $this->update($entity, $options);
        }

        return $result;
    }

    /**
     * Update given entity object
     *
     * @param EntityInterface $entity Entity object
     * @param array $options
     *
     * @return bool|int
     * @throws DBALException
     * @throws Exception
     *
     * @params array $options Array of adapter-specific options
     */
    public function update(EntityInterface $entity, array $options = []) {

        // Run beforeSave and beforeUpdate to know whether or not we can continue
        if (false === $this->eventEmitter()->emit('beforeSave', [$entity, $this]) || false === $this->eventEmitter()->emit('beforeUpdate', [$entity, $this])) {
            return false;
        }

        // Run validation unless disabled via options
        if ( ! isset($options['validate']) || (isset($options['validate']) && $options['validate'] !== false)) {

            if ( ! $this->validate($entity, $options)) {
                return false;
            }

        }

        if (isset($options['relations']) && $options['relations'] === true) {
            $this->saveBelongsToRelations($entity, $options);
        }

        // Prepare data
        $data = $entity->dataModified();

        // Save only known, defined fields
        $entityFields = $this->fields();
        $entityName = $this->entity();

        $extraData = array_diff_key($data, $entityFields);
        $data = array_intersect_key($data, $entityFields);

        // If there are extra fields here, throw an error
        if ( ! isset($options['strict']) || (isset($options['strict']) && $options['strict'] === true)) {

            if (count($extraData) > 0) {
                throw new Exception('Update error: Unknown fields provided for ' . $entityName . ": '" . implode("', '", array_keys($extraData)) . "'");
            }

        }

        if (count($data) > 0) {

            $result = $this->resolver()->update(
                $this->table(),
                $data,
                [$this->primaryKeyField() => $this->primaryKey($entity)],
                $this->getTypes($entity::fields())
            );

            $entity->data($entity->data(null, true, false), false);

            if (isset($options['relations']) && $options['relations'] === true) {
                $this->saveHasRelations($entity, $options);
            }

            // Run afterSave and afterUpdate
            if (false === $this->eventEmitter()->emit('afterSave', [$entity, $this, &$result]) || false === $this->eventEmitter()->emit('afterUpdate', [$entity, $this, &$result])) {
                $result = false;
            }

        } else {

            $result = true;

            if (isset($options['relations']) && $options['relations'] === true) {
                $this->saveHasRelations($entity, $options);
            }

        }

        return $result;
    }

    /**
     * Upsert save entity - insert or update on duplicate key. Intended to be
     * used in conjunction with fields that are marked 'unique'
     *
     * @param array $data array of key/values to set on new Entity instance
     * @param array $where array of keys to select record by for updating if it already exists
     *
     * @return bool|EntityInterface
     * $entityClass with $data set on it
     *
     * @throws DBALException
     * @throws Exception
     */
    public function upsert(array $data, array $where) {

        $entityClass = $this->entity();
        $entity = new $entityClass($data);
        $result = $this->insert($entity);

        // Unique constraint produces a validation error
        if ($result === false && $entity->hasErrors()) {

            $dataUpdate = array_diff_key($data, $where);
            $existingEntity = $this->first($where);

            if ( ! $existingEntity) {
                return $entity;
            }

            $existingEntity->data($dataUpdate);
            $entity = $existingEntity;
            $this->update($entity);
        }

        return $entity;
    }

    /**
     * Delete items matching given conditions
     *
     * @param mixed $conditions Optional array of conditions in column => value pairs
     *
     * @return bool|Query\Statement|int
     *
     * @throws DBALException
     * @throws Exception
     */
    public function delete($conditions = []) {

        $entityOrArray = $conditions;
        $beforeEvent = 'beforeDelete';
        $afterEvent = 'afterDelete';

        if (is_object($conditions)) {

            $conditions = [$this->primaryKeyField() => $this->primaryKey($conditions)];

        } elseif (is_array($conditions)) {
            $beforeEvent = 'beforeDeleteConditions';
            $afterEvent = 'afterDeleteConditions';
        }

        // Run beforeDelete to know whether or not we can continue
        if (false === $this->eventEmitter()->emit($beforeEvent, [$entityOrArray, $this])) {
            return false;
        }

        $query = $this->queryBuilder()->delete($this->table())->where($conditions);
        $result = $this->resolver()->exec($query);

        // Run afterDelete
        $this->eventEmitter()->emit($afterEvent, [$entityOrArray, $this, &$result]);

        return $result;
    }

    /**
     * Transaction with closure
     *
     * @param Closure $work
     * @param null $entityName
     *
     * @return mixed
     * @throws Exception
     * @throws ConnectionException
     */
    public function transaction(Closure $work, $entityName = null) {

        $connection = $this->connection($entityName);

        try {
            $connection->beginTransaction();

            // Execute closure for work inside transaction
            $result = $work($this);

            // Rollback on boolean 'false' return
            if ($result === false) {
                $connection->rollback();
            } else {
                $connection->commit();
            }
        } catch (\Exception $e) {
            // Rollback on uncaught exception
            $connection->rollback();

            // Re-throw exception so we don't bury it
            throw $e;
        }

        return $result;
    }

    /**
     * Truncate table
     * Should delete all rows and reset serial/auto_increment keys
     *
     * @param bool $cascade
     *
     * @return mixed
     * @throws Exception
     * @throws Throwable
     */
    public function truncateTable($cascade = false) {
        return $this->resolver()->truncate($this->table(), $cascade);
    }

    /**
     * Drop/delete table
     * Destructive and dangerous - drops entire data source and all data
     *
     * @return bool
     * @throws Exception
     */
    public function dropTable() {
        return $this->resolver()->dropTable($this->table());
    }

    /**
     * Migrate table structure changes from model to database
     *
     * @return bool
     * @throws DBALException
     * @throws Exception
     * @throws SchemaException
     */
    public function migrate() {
        return $this->resolver()->migrate();
    }

    /**
     * Extract field types from Entity fields array
     *
     * @param array $fields
     *
     * @return array
     */
    private function getTypes(array $fields): array {

        $types = [];
        foreach ($fields as $field => $attributes) {
            $types[$field] = $attributes['type'];
        }

        return $types;
    }
}
