<?php declare(strict_types=1);

namespace Ether\Database\Orm;

use Sabre\Event\EmitterTrait;

/**
 * EventEmitter stub so it's in same Orm namespace
 */
class EventEmitter {
    use EmitterTrait;
}
