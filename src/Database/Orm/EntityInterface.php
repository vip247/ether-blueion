<?php

namespace Ether\Database\Orm;

/**
 * Entity object interface
 *
 *
 */
interface EntityInterface {
    /**
     * Table name getter/setter
     *
     * @param null $tableName
     */
    public static function table($tableName = null);

    /**
     * Connection name getter/setter
     *
     * @param null $connectionName
     */
    public static function connection($connectionName = null);

    /**
     * Data source options getter/setter
     *
     * @param null $tableOpts
     */
    public static function tableOptions($tableOpts = null);

    /**
     * Mapper name getter
     */
    public static function mapper();

    /**
     * Return defined fields of the entity
     */
    public static function fields();

    /**
     * Add events to this entity
     *
     * @param EventEmitter $eventEmitter
     */
    public static function events(EventEmitter $eventEmitter);

    /**
     * Return defined fields of the entity
     *
     * @param MapperInterface $mapper
     * @param EntityInterface $entity
     */
    public static function relations(MapperInterface $mapper, EntityInterface $entity);

    /**
     * Return scopes defined by this entity. Scopes are called from the
     * Query object as a sort of in-context dynamic query method
     */
    public static function scopes();

    /**
     * Gets and sets data on the current entity
     *
     * @param array|null $data
     * @param bool $modified
     */
    public function data(?array $data = null, $modified = true);

    /**
     * Return array of field data with data from the field names listed removed
     *
     * @param array List of field names to exclude in data list returned
     */
    public function dataExcept(array $except);

    /**
     * Gets data that has been modified since object construct,
     * optionally allowing for selecting a single field
     *
     * @param null $field
     */
    public function dataModified($field = null);

    /**
     * Gets data that has not been modified since object construct,
     * optionally allowing for selecting a single field
     *
     * @param null $field
     */
    public function dataUnmodified($field = null);

    /**
     * Is entity new (unsaved)?
     *
     * @param null $new
     *
     * @return boolean
     */
    public function isNew($new = null);

    /**
     * Returns true if a field has been modified.
     * If no field name is passed in, return whether any fields have been changed
     *
     * @param null $field
     */
    public function isModified($field = null);

    /**
     * Alias of self::data()
     */
    public function toArray();

    /**
     * Check if any errors exist
     *
     * @param string $field OPTIONAL field name
     *
     * @return boolean
     */
    public function hasErrors($field = null);

    /**
     * Error message getter/setter
     *
     * @param null $msgs
     * @param bool $overwrite
     *
     * @return self|array|boolean Setter return self, getter returns array or boolean if key given and not found
     */
    public function errors($msgs = null, $overwrite = true);

    /**
     * Add an error to error messages array
     *
     * @param string $field Field name that error message relates to
     * @param mixed $msg Error message text - String or array of messages
     */
    public function error($field, $msg);

    /**
     * Enable isset() for object properties
     *
     * @param $key
     */
    public function __isset($key);

    /**
     * Getter for field properties
     *
     * @param $field
     */
    public function &__get($field);

    /**
     * Setter for field properties
     *
     * @param $field
     * @param $value
     */
    public function __set($field, $value);

    /**
     * @param $field
     *
     * @return mixed
     */
    public function get($field);

    /**
     * @param $field
     * @param $value
     * @param bool $modified
     *
     * @return mixed
     */
    public function set($field, $value, $modified = true);

    /**
     * Get/Set relation
     *
     * @param $relationName
     * @param null $relationObj
     */
    public function relation($relationName, $relationObj = null);

    /**
     * Get primary key field name
     *
     * @return string Primary key field name
     */
    public function primaryKeyField();

    /**
     * Get the value of the primary key field defined on this entity
     *
     * @return mixed Value of the primary key field
     */
    public function primaryKey();

    /**
     * Return array for json_encode()
     */
    public function jsonSerialize();

    /**
     * String representation of the class (JSON)
     */
    public function __toString();
}
