<?php declare(strict_types=1);

namespace Ether\Database\Orm\Query;

use PDO;
use Throwable;
use Ether\Database\Orm\Query;
use Ether\Database\Orm\Mapper;
use Doctrine\DBAL\Schema\Table;
use Doctrine\DBAL\Schema\Schema;
use Doctrine\DBAL\DBALException;
use Ether\Database\Orm\Exception;
use Doctrine\DBAL\Schema\SchemaException;
use Ether\Database\Orm\Entity\Collection;
use Ether\Database\Orm\Relation\BelongsTo;
use Doctrine\DBAL\Platforms\PostgreSqlPlatform;

class Resolver {

    /**
     * @var Mapper
     */
    protected $mapper;

    protected $_noQuote;

    /**
     * Constructor Method
     *
     * @param Mapper $mapper
     */
    public function __construct(Mapper $mapper) {
        $this->mapper = $mapper;
    }

    /**
     * Set field and value quoting on/off - mainly used for testing output SQL
     * since quoting is different per platform
     *
     * @param bool $noQuote
     *
     * @return $this
     */
    public function noQuote($noQuote = true) {
        $this->_noQuote = $noQuote;

        return $this;
    }

    /**
     * Migrate table structure changes to database
     *
     * @return bool
     * @throws DBALException
     * @throws Exception
     * @throws SchemaException
     */
    public function migrate() {

        // Mapper knows currently set entity
        $entity = $this->mapper->entity();
        $table = $entity::table();
        $fields = $this->mapper->entityManager()->fields();
        $fieldIndexes = $this->mapper->entityManager()->fieldKeys();
        $connection = $this->mapper->connection();
        $platform = $connection->getDatabasePlatform();

        $schemaManager = $this->mapper->connection()->getSchemaManager();

        if( ! $platform instanceof PostgreSqlPlatform){

            $tableObject = $schemaManager->listTableDetails($table);

        } else {

            $columns     = $schemaManager->listTableColumns($table);
            $foreignKeys = [];

            if ($platform->supportsForeignKeyConstraints()) {
                $foreignKeys = $schemaManager->listTableForeignKeys($table);
            }

            $indexes = $schemaManager->listTableIndexes($table);

            $tableObject = new Table($table, $columns, $indexes, $foreignKeys);
        }

        $tableObjects[] = $tableObject;
        $schema = new Schema($tableObjects);

        $tableColumns = $tableObject->getColumns();
        $tableExists = ! empty($tableColumns);
        if ($tableExists) {
            // Update existing table
            $newSchema = $this->migrateCreateSchema();
            $queries = $schema->getMigrateToSql($newSchema, $platform);
        } else {
            // Create new table
            $newSchema = $this->migrateCreateSchema();
            $queries = $newSchema->toSql($platform);
        }

        // Execute resulting queries
        $lastResult = false;
        foreach ($queries as $sql) {
            $lastResult = $connection->exec($sql);
        }

        return $lastResult;
    }

    /**
     * Migrate create schema
     *
     * @return Schema
     * @throws DBALException
     * @throws Exception
     */
    public function migrateCreateSchema(): Schema {

        $entityName = $this->mapper->entity();
        $table = $entityName::table();
        $fields = $this->mapper->entityManager()->fields();
        $fieldIndexes = $this->mapper->entityManager()->fieldKeys();

        $schema = new Schema();
        $table = $schema->createTable($this->escapeIdentifier($table));

        foreach ($fields as $field) {
            $fieldType = $field['type'];
            unset($field['type']);
            $table->addColumn($this->escapeIdentifier($field['column']), $fieldType, $field);
        }

        // PRIMARY
        if ($fieldIndexes['primary']) {
            $resolver = $this;
            $primaryKeys = array_map(static function ($value) use ($resolver) {
                return $resolver->escapeIdentifier($value);
            }, $fieldIndexes['primary']);
            $table->setPrimaryKey($primaryKeys);
        }

        // UNIQUE
        foreach ($fieldIndexes['unique'] as $keyName => $keyFields) {
            $table->addUniqueIndex($keyFields, $this->escapeIdentifier($this->trimSchemaName($keyName)));
        }

        // INDEX
        foreach ($fieldIndexes['index'] as $keyName => $keyFields) {
            $table->addIndex($keyFields, $this->escapeIdentifier($this->trimSchemaName($keyName)));
        }

        // FOREIGN KEYS
        $this->addForeignKeys($table);

        return $schema;
    }

    /**
     * Escape/quote identifier
     *
     * @param string $identifier
     *
     * @return string
     * @throws Exception
     */
    public function escapeIdentifier($identifier): string {

        if ($this->_noQuote) {
            return $identifier;
        }

        return $this->mapper->connection()->quoteIdentifier(trim($identifier));
    }

    /**
     * Trim a leading schema name separated by a dot if present
     *
     * @param string $identifier
     *
     * @return string
     */
    public function trimSchemaName($identifier): string {

        $components = explode('.', $identifier, 2);

        return end($components);
    }

    /**
     * Add foreign keys from BelongsTo relations to the table schema
     *
     * @param Table $table
     *
     * @return Table
     * @throws DBALException
     * @throws Exception
     */
    protected function addForeignKeys(Table $table): Table {

        $entityName = $this->mapper->entity();

        $entity = new $entityName;

        $relations = $entityName::relations($this->mapper, $entity);

        $fields = $this->mapper->entityManager()->fields();

        foreach ($relations as $relationName => $relation) {

            if ($relation instanceof BelongsTo) {

                $fieldInfo = $fields[$relation->localKey()];

                if ($fieldInfo['foreignkey'] === false) {
                    continue;
                }

                $foreignTableMapper = $relation->mapper()->getMapper($relation->entityName());
                $foreignTable = $foreignTableMapper->table();

                $foreignSchemaManager = $foreignTableMapper->connection()->getSchemaManager();

                $platform = $foreignTableMapper->connection()->getDatabasePlatform();

                if( ! $platform instanceof PostgreSqlPlatform){

                    $foreignTableObject = $foreignSchemaManager->listTableDetails($foreignTable);

                } else {

                    $columns     = $foreignSchemaManager->listTableColumns($foreignTable);

                    $foreignKeys = [];
                    if ($platform->supportsForeignKeyConstraints()) {
                        $foreignKeys = $foreignSchemaManager->listTableForeignKeys($foreignTable);
                    }

                    $indexes = $foreignSchemaManager->listTableIndexes($foreignTable);

                    $foreignTableObject = new Table($foreignTable, $columns, $indexes, $foreignKeys);
                }

                $foreignTableColumns = $foreignTableObject->getColumns();
                $foreignTableNotExists = empty($foreignTableColumns);
                $foreignKeyNotExists = ! array_key_exists($relation->foreignKey(), $foreignTableColumns);

                // We need to use the is_a() function because the there is some inconsistency in entity names (leading slash)
                $notRecursiveForeignKey = ! is_a($entity, $relation->entityName());

                /* Migrate foreign table if:
                 *  - the foreign table not exists
                 *  - the foreign key not exists
                 *  - the foreign table is not the same as the current table (recursion check)
                 * This migration eliminates the 'Integrity constraint violation' error
                 */
                if (($foreignTableNotExists || $foreignKeyNotExists) && $notRecursiveForeignKey) {
                    $foreignTableMapper->migrate();
                }

                $onUpdate = ! is_null($fieldInfo['onUpdate']) ? $fieldInfo['onUpdate'] : 'CASCADE';

                if ( ! is_null($fieldInfo['onDelete'])) {
                    $onDelete = $fieldInfo['onDelete'];
                } else if ($fieldInfo['notnull']) {
                    $onDelete = 'CASCADE';
                } else {
                    $onDelete = 'SET NULL';
                }

                // Field alias support
                $fieldAliasMappings = $this->mapper->entityManager()->fieldAliasMappings();
                $localKey = $fieldAliasMappings[$relation->localKey()] ?? $relation->localKey();

                $fkName = $this->mapper->table() . '_fk_' . $relationName;

                $table->addForeignKeyConstraint($foreignTable, [$localKey], [$relation->foreignKey()], ['onDelete' => $onDelete, 'onUpdate' => $onUpdate], $fkName);
            }
        }

        return $table;
    }

    /**
     * Find records with custom SQL query
     *
     * @param Query $query SQL query to execute
     *
     * @return Collection
     * @throws Exception
     * @throws DBALException
     */
    public function read(Query $query): Collection {

        $stmt = $query->builder()->execute();

        // Set PDO fetch mode
        $stmt->setFetchMode(PDO::FETCH_ASSOC);

        $collection = $query->mapper()->collection($stmt, $query->with());

        // Ensure statement is closed
        $stmt->closeCursor();

        return $collection;
    }

    /**
     * Create new row object with set properties
     *
     * @param string $table Table name
     * @param array $data Array of data to save in 'field' => 'value' format
     *
     * @param array $types
     *
     * @return int
     * @throws DBALException
     * @throws Exception
     */
    public function create($table, array $data, array $types = []): int {

        $connection = $this->mapper->connection();

        return $connection->insert(
            $this->escapeIdentifier($table),
            $this->dataWithFieldAliasMappings($data),
            $this->dataWithFieldAliasMappings($types)
        );
    }

    /**
     * Taken given field name/value inputs and map them to their aliased names
     *
     * @param array $data
     *
     * @return array
     * @throws Exception
     */
    public function dataWithFieldAliasMappings(array $data): array {

        $fields = $this->mapper->entityManager()->fields();
        $fieldMappings = [];

        foreach ($data as $field => $value) {
            $fieldAlias = $this->escapeIdentifier(isset($fields[$field]) ? $fields[$field]['column'] : $field);
            $fieldMappings[$fieldAlias] = $value;
        }

        return $fieldMappings;
    }

    /**
     * Update
     *
     * @param string $table Table name
     * @param array $data Array of data for WHERE clause in 'field' => 'value' format
     * @param array $where
     *
     * @param array $types
     *
     * @return int
     * @throws DBALException
     * @throws Exception
     */
    public function update($table, array $data, array $where, array $types = []): int {

        $connection = $this->mapper->connection();

        return $connection->update(
            $this->escapeIdentifier($table),
            $this->dataWithFieldAliasMappings($data),
            $this->dataWithFieldAliasMappings($where),
            $this->dataWithFieldAliasMappings($types)
        );
    }

    /**
     * Taken given aliased field name/value inputs and map them to their non-aliased names
     *
     * @param array $data
     *
     * @return array
     * @throws Exception
     */
    public function dataWithOutFieldAliasMappings(array $data): array {

        // have to call fields() otherwise fieldAliasMappings() would return null on the first entity
        $this->mapper->entityManager()->fields();

        $fieldAliasMappings = $this->mapper->entityManager()->fieldAliasMappings();

        foreach ($fieldAliasMappings as $field => $aliasedField) {
            if (array_key_exists($aliasedField, $data)) {
                $data[$field] = $data[$aliasedField];
                unset($data[$aliasedField]);
            }
        }

        return $data;
    }

    /**
     * Execute provided query and return result
     *
     * @param Query $query SQL query to execute
     *
     * @return Statement|int
     */
    public function exec(Query $query) {
        return $query->builder()->execute();
    }

    /**
     * Truncate Table
     *
     * @param string $table Table name
     * @param bool $cascade
     *
     * @return mixed
     * @throws Throwable
     * @internal param array $data Array of data for WHERE clause in 'field' => 'value' format
     */
    public function truncate($table, $cascade = false) {
        $mapper = $this->mapper;
        $connection = $mapper->connection();

        $table = $this->escapeIdentifier($table);

        // SQLite doesn't support TRUNCATE
        if ($mapper->connectionIs('sqlite')) {
            $sql = 'DELETE FROM ' . $table;
        } elseif ($mapper->connectionIs('pgsql')) {
            $sql = 'TRUNCATE TABLE ' . $table . ($cascade ? ' CASCADE' : '');
        } else {
            $sql = 'TRUNCATE TABLE ' . $table . '';
        }

        return $connection->transactional(static function ($conn) use ($sql) {
            $conn->exec($sql);
        });
    }

    /**
     * Drop Table
     *
     * @param string $table Table name
     *
     * @return bool
     * @throws Exception
     */
    public function dropTable($table): bool {

        $connection = $this->mapper->connection();

        try {
            $connection->getSchemaManager()->dropTable($this->escapeIdentifier($table));
            $result = true;
        } catch (Exception $e) {
            $result = false;
        }

        return $result;
    }
}
