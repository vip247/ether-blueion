<?php declare(strict_types=1);

namespace Ether\Database\Orm\Query\Operator;

use Doctrine\DBAL\Query\QueryBuilder;

/**
 * \Query\Operator
 */
class FullText {

    /**
     * @param QueryBuilder $builder
     * @param $column
     * @param $value
     *
     * @return string
     */
    public function __invoke(QueryBuilder $builder, $column, $value) {
        return 'MATCH(' . $column . ') AGAINST (' . $builder->createPositionalParameter($value) . ')';
    }
}
