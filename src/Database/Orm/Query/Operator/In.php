<?php declare(strict_types=1);

namespace Ether\Database\Orm\Query\Operator;

use Doctrine\DBAL\Connection;
use Ether\Database\Orm\Exception;
use Doctrine\DBAL\Query\QueryBuilder;

/**
 * \Query\Operator
 */
class In {
    /**
     * @param QueryBuilder $builder
     * @param $column
     * @param $value
     *
     * @return string
     * @throws Exception
     */
    public function __invoke(QueryBuilder $builder, $column, $value) {
        if ( ! is_array($value)) {
            throw new Exception('Use of IN operator expects value to be array. Got ' . gettype($value) . '.');
        }

        return $column . ' IN (' . $builder->createPositionalParameter($value, Connection::PARAM_STR_ARRAY) . ')';
    }
}
