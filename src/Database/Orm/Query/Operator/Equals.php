<?php declare(strict_types=1);

namespace Ether\Database\Orm\Query\Operator;

use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Query\QueryBuilder;

class Equals {
    /**
     * @param QueryBuilder $builder
     * @param $column
     * @param $value
     *
     * @return string
     */
    public function __invoke(QueryBuilder $builder, $column, $value) {

        if (is_array($value) && ! empty($value)) {
            return $column . ' IN (' . $builder->createPositionalParameter($value, Connection::PARAM_STR_ARRAY) . ')';
        }

        if ($value === null || (is_array($value) && empty($value))) {
            return $column . ' IS NULL';
        }

        return $column . ' = ' . $builder->createPositionalParameter($value);
    }
}
