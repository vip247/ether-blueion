<?php declare(strict_types=1);

namespace Ether\Database\Orm;

use DateTime;
use Countable;
use ArrayAccess;
use JsonSerializable;
use IteratorAggregate;
use BadMethodCallException;
use InvalidArgumentException;
use Ramsey\Uuid\UuidInterface;
use Doctrine\DBAL\DBALException;
use Ether\Database\Orm\Adapter\Mysql;
use Doctrine\DBAL\Query\QueryBuilder;
use Ether\Database\Orm\Entity\Collection;
use Ether\Database\Orm\Query\Operator\In;
use Ether\Database\Orm\Query\Operator\Not;
use Ether\Database\Orm\Query\Operator\Like;
use Ether\Database\Orm\Query\Operator\RegExp;
use Ether\Database\Orm\Query\Operator\Equals;
use Ether\Database\Orm\Query\Operator\LessThan;
use Ether\Database\Orm\Query\Operator\FullText;
use Ether\Database\Orm\Query\Operator\GreaterThan;
use Doctrine\DBAL\Query\Expression\ExpressionBuilder;
use Ether\Database\Orm\Query\Operator\LessThanOrEqual;
use Ether\Database\Orm\Query\Operator\FullTextBoolean;
use Ether\Database\Orm\Query\Operator\GreaterThanOrEqual;

/**
 * Query Object - Used to build adapter-independent queries PHP-style
 */
class Query implements Countable, IteratorAggregate, ArrayAccess, JsonSerializable {

    public const ALL_FIELDS = '*';

    /**
     * Custom methods added by extensions or plugins
     *
     * @var array
     */
    protected static $_customMethods = [];

    /**
     * @var array
     */
    protected static $_whereOperators = [
        '<'                 => LessThan::class,
        ':lt'               => LessThan::class,
        '<='                => LessThanOrEqual::class,
        ':lte'              => LessThanOrEqual::class,
        '>'                 => GreaterThan::class,
        ':gt'               => GreaterThan::class,
        '>='                => GreaterThanOrEqual::class,
        ':gte'              => GreaterThanOrEqual::class,
        '~='                => RegExp::class,
        '=~'                => RegExp::class,
        ':regex'            => RegExp::class,
        ':like'             => Like::class,
        ':fulltext'         => FullText::class,
        ':fulltext_boolean' => FullTextBoolean::class,
        'in'                => In::class,
        ':in'               => In::class,
        '<>'                => Not::class,
        '!='                => Not::class,
        ':ne'               => Not::class,
        ':not'              => Not::class,
        '='                 => Equals::class,
        ':eq'               => Equals::class,
    ];

    /**
     * Already instantiated operator objects
     *
     * @var array
     */
    protected static $_whereOperatorObjects = [];

    /**
     * @var Mapper
     */
    protected $_mapper;

    /**
     * @var string
     */
    protected $_entityName;

    /**
     * @var string
     */
    protected $_tableName;

    /**
     * @var
     */
    protected $_queryBuilder;

    /**
     * @var boolean
     */
    protected $_noQuote;

    /**
     * Storage for query properties
     *
     * @var array
     */
    protected $with = [];

    /**
     * Storage for eager-loaded relations
     *
     * @var array
     */
    protected $_data = [];

    /**
     * Constructor Method
     *
     * @param Mapper $mapper
     *
     * @throws Exception
     * @internal param Mapper
     * @internal param string $entityName Name of the entity to query on/for
     */
    public function __construct(Mapper $mapper) {
        $this->_mapper = $mapper;
        $this->_entityName = $mapper->entity();
        $this->_tableName = $mapper->table();

        // Create Doctrine DBAL query builder from Doctrine\DBAL\Connection
        $this->_queryBuilder = $mapper->connection()->createQueryBuilder();
    }

    /**
     * Add a custom user method via closure or PHP callback
     *
     * @param string $method Method name to add
     * @param callable $callback Callback or closure that will be executed when missing method call matching $method is made
     *
     * @throws InvalidArgumentException
     */
    public static function addMethod($method, callable $callback) {
        if (method_exists(__CLASS__, $method)) {
            throw new InvalidArgumentException("Method '" . $method . "' already exists on " . __CLASS__);
        }
        self::$_customMethods[$method] = $callback;
    }

    /**
     * Adds a custom type to the type map.
     *
     * @param string $operator
     * @param callable|string $action
     */
    public static function addWhereOperator($operator, $action) {
        if (isset(self::$_whereOperators[$operator])) {
            throw new InvalidArgumentException("Where operator '" . $operator . "' already exists");
        }

        static::$_whereOperators[$operator] = $action;
    }

    /**
     * Set field and value quoting on/off - mainly used for testing output SQL
     * since quoting is different per platform
     *
     * @param bool $noQuote
     *
     * @return $this
     */
    public function noQuote($noQuote = true): self {
        $this->_noQuote = $noQuote;

        return $this;
    }

    /**
     * Return DBAL Query builder expression
     *
     * @return ExpressionBuilder
     */
    public function expr(): ExpressionBuilder {
        return $this->builder()->expr();
    }

    /**
     * Get current Doctrine DBAL query builder object
     *
     * @return QueryBuilder
     */
    public function builder(): QueryBuilder {
        return $this->_queryBuilder;
    }

    /**
     * Select (pass-through to DBAL QueryBuilder)
     *
     * @return $this
     * @throws Exception
     */
    public function select(): self {
        call_user_func_array([$this->builder(), 'select'], $this->escapeIdentifier(func_get_args()));

        return $this;
    }

    /**
     * Escape/quote identifier
     *
     * @param string|array $identifier
     *
     * @return string|array
     * @throws Exception
     */
    public function escapeIdentifier($identifier) {
        if (is_array($identifier)) {
            array_walk($identifier, function (&$identifier) {
                $identifier = $this->escapeIdentifier($identifier);
            });
            return $identifier;
        }

        if ($this->_noQuote || $identifier === self::ALL_FIELDS) {
            return $identifier;
        }

        if (strpos($identifier, ' ') !== false || strpos($identifier, '(') !== false) {
            return $identifier; // complex expression, ain't quote it, do it manually!
        }

        return $this->mapper()->connection()->quoteIdentifier(trim($identifier));
    }

    /**
     * Get current adapter object
     *
     * @return Mapper
     */
    public function mapper(): MapperInterface {
        return $this->_mapper;
    }

    /**
     * Delete (pass-through to DBAL QueryBuilder)
     *
     * @return $this
     * @throws Exception
     */
    public function delete(): self {
        call_user_func_array([$this->builder(), 'delete'], $this->escapeIdentifier(func_get_args()));

        return $this;
    }

    /**
     * From (pass-through to DBAL QueryBuilder)
     *
     * @return $this
     * @throws Exception
     */
    public function from(): self {
        call_user_func_array([$this->builder(), 'from'], $this->escapeIdentifier(func_get_args()));

        return $this;
    }

    /**
     * Get all bound query parameters (pass-through to DBAL QueryBuilder)
     *
     * @return mixed
     */
    public function getParameters() {
        return call_user_func_array([$this->builder(), __FUNCTION__], func_get_args());
    }

    /**
     * Set query parameters (pass-through to DBAL QueryBuilder)
     *
     * @return $this
     */
    public function setParameters(): self {
        call_user_func_array([$this->builder(), __FUNCTION__], func_get_args());

        return $this;
    }

    /**
     * WHERE OR conditions
     *
     * @param array $where Array of conditions for this clause
     * @param string $type Keyword that will separate each condition - "AND", "OR"
     *
     * @return $this
     * @throws Exception
     * @throws DBALException
     */
    public function orWhere(array $where, $type = 'AND'): self {
        if ( ! empty($where)) {
            $whereClause = implode(' ' . $type . ' ', $this->parseWhereToSQLFragments($where));
            $this->builder()->orWhere($whereClause);
        }

        return $this;
    }

    /**
     * Parse array-syntax WHERE conditions and translate them to DBAL QueryBuilder syntax
     *
     * @param array $where Array of conditions for this clause
     * @param bool $useAlias
     *
     * @return array SQL fragment strings for WHERE clause
     * @throws Exception
     * @throws DBALException
     */
    private function parseWhereToSQLFragments(array $where, bool $useAlias = true): array {

        $builder = $this->builder();

        $sqlFragments = [];

        foreach ($where as $column => $value) {

            // Column name with comparison operator
            $colData = explode(' ', $column);
            $operator = $colData[1] ?? '=';

            if (count($colData) > 2) {
                $operator = array_pop($colData);
                $colData = [implode(' ', $colData), $operator];
            }

            $operatorCallable = $this->getWhereOperatorCallable(strtolower($operator));

            if ( ! $operatorCallable) {
                throw new InvalidArgumentException("Unsupported operator '" . $operator . "' "
                    . 'in WHERE clause. If you want to use a custom operator, you '
                    . "can add one with Ether\Database\Orm\Query::addWhereOperator('" . $operator . "', "
                    . 'function (QueryBuilder $builder, $column, $value) { ... }); ');
            }

            $col = $colData[0];

            if(is_array($value)) {

                foreach ($value as $k => $v) {

                    // Handle DateTime & Uuid value objects and
                    if ($v instanceof DateTime || $v instanceof UuidInterface) {

                        $mapper = $this->mapper();
                        $convertedValues = $mapper->convertToDatabaseValues($mapper->entity(), [$col => $v]);
                        $value[$k] = $convertedValues[$col];

                    }

                }

            } else if ($value instanceof DateTime || $value instanceof UuidInterface) {

                $mapper = $this->mapper();
                $convertedValues = $mapper->convertToDatabaseValues($mapper->entity(), [$col => $value]);
                $value = $convertedValues[$col];

            }

            // Prefix column name with alias
            if ($useAlias === true) {
                $col = $this->fieldWithAlias($col);
            }

            $sqlFragments[] = $operatorCallable($builder, $col, $value);
        }

        return $sqlFragments;
    }

    /**
     * @param string $operator
     *
     * @return callable|false
     */
    private function getWhereOperatorCallable($operator) {

        if ( ! isset(static::$_whereOperators[$operator])) {
            return false;
        }

        if (is_callable(static::$_whereOperators[$operator])) {
            return static::$_whereOperators[$operator];
        }

        if ( ! isset(static::$_whereOperatorObjects[$operator])) {
            static::$_whereOperatorObjects[$operator] = new static::$_whereOperators[$operator]();
        }

        return static::$_whereOperatorObjects[$operator];
    }

    /**
     * Get field name with table alias appended
     *
     * @param string $field
     * @param bool $escaped
     *
     * @return string
     * @throws Exception
     */
    public function fieldWithAlias($field, $escaped = true): string {

        $fieldInfo = $this->_mapper->entityManager()->fields();

        // Detect function in field name
        $field = trim($field);
        $function = strpos($field, '(');

        if ($function) {

            foreach ($fieldInfo as $key => $currentField) {
                $fieldFound = strpos($field, $key);
                if ($fieldFound) {
                    $functionStart = substr($field, 0, $fieldFound);
                    $functionEnd = substr($field, $fieldFound + strLen($key));
                    $field = $key;
                    break;
                }
            }

        }

        // Determine real field name (column alias support)
        if (isset($fieldInfo[$field])) {
            $field = $fieldInfo[$field]['column'];
        }

        $field = $this->_tableName . '.' . $field;
        $field = $escaped ? $this->escapeIdentifier($field) : $field;

        $result = $function ? $functionStart : '';
        $result .= $field;
        $result .= $function ? $functionEnd : '';

        return $result;
    }

    /**
     * WHERE AND conditions
     *
     * @param array $where Array of conditions for this clause
     * @param string $type Keyword that will separate each condition - "AND", "OR"
     *
     * @return $this|Query
     * @throws DBALException
     * @throws Exception
     */
    public function andWhere(array $where, $type = 'AND') {
        return $this->where($where, $type);
    }

    /**
     * WHERE conditions
     *
     * @param array $where Array of conditions for this clause
     * @param string $type Keyword that will separate each condition - "AND", "OR"
     *
     * @return $this
     * @throws DBALException
     * @throws Exception
     */
    public function where(array $where, $type = 'AND'): self {
        if ( ! empty($where)) {
            $whereClause = implode(' ' . $type . ' ', $this->parseWhereToSQLFragments($where));
            $this->builder()->andWhere($whereClause);
        }

        return $this;
    }

    /**
     * WHERE field + raw SQL
     *
     * @param string $field Field name for SQL statement (will be quoted)
     * @param string $sql SQL string to put in WHERE clause
     * @param array $params
     *
     * @return $this
     * @throws Exception
     */
    public function whereFieldSql($field, $sql, array $params = []): self {

        $builder = $this->builder();
        $placeholderCount = substr_count($sql, '?');
        $paramCount = count($params);

        if ($placeholderCount !== $paramCount) {
            throw new Exception('Number of supplied parameters (' . $paramCount . ') does not match the number of provided placeholders (' . $placeholderCount . ')');
        }

        $sql = preg_replace_callback('/\?/', static function ($match) use ($builder, &$params) {
            $param = array_shift($params);

            return $builder->createPositionalParameter($param);
        }, $sql);

        $builder->andWhere($this->escapeIdentifier($field) . ' ' . $sql);

        return $this;
    }

    /**
     * WHERE conditions
     *
     * @param string $sql SQL string to put in WHERE clause
     *
     * @return $this
     */
    public function whereSql($sql) {
        $this->builder()->andWhere($sql);

        return $this;
    }

    /**
     * Relations to be eager-loaded
     *
     * @param mixed|null $relations Array/string of relation(s) to be loaded.
     *
     * @return $this|array
     */
    public function with($relations = null) {
        if ($relations === null) {
            return $this->with;
        }

        $this->with = array_unique(array_merge((array) $relations, $this->with));

        return $this;
    }

    /**
     * Search criteria (FULLTEXT, LIKE, or REGEX, depending on storage engine and driver)
     *
     * @param mixed $fields Single string field or array of field names to use for searching
     * @param string $query Search keywords or query
     * @param array $options Array of options for search
     *
     * @return $this
     * @throws DBALException
     * @throws Exception
     */
    public function search($fields, $query, array $options = []): self {

        $fields = (array) $fields;
        $entityDatasourceOptions = $this->mapper()->entityManager()->datasourceOptions($this->entityName());
        $fieldString = '`' . implode('`, `', $fields) . '`';
        $fieldTypes = $this->mapper()->fields($this->entityName());

        // See if we can use FULLTEXT search
        $whereType = ':like';
        $connection = $this->mapper()->connection($this->entityName());

        // Only on MySQL
        // Only for MyISAM engine
        if (($connection instanceof Mysql) && isset($entityDatasourceOptions['engine'])) {

            $engine = $entityDatasourceOptions['engine'];

            if ('myisam' == strtolower($engine)) {

                $whereType = ':fulltext';

                // Only if ALL included columns allow fulltext according to entity definition
                if (in_array($fields, array_keys($this->mapper()->fields($this->entityName())), true)) {
                    // FULLTEXT
                    $whereType = ':fulltext';
                }

                // Boolean mode option
                if (isset($options['boolean']) && $options['boolean'] === true) {
                    $whereType = ':fulltext_boolean';
                }

            }
        }

        // @todo Normal queries can't search multiple fields, so make them separate searches instead of stringing them together

        // Resolve search criteria
        return $this->where([$fieldString . ' ' . $whereType => $query]);
    }

    /**
     * Get current entity name query is to be performed on
     *
     * @return string
     */
    public function entityName(): string {
        return $this->_entityName;
    }

    /**
     * ORDER BY columns
     *
     * @param array $order Array of field names to use for sorting
     *
     * @return $this
     * @throws Exception
     */
    public function order(array $order): self {
        foreach ($order as $field => $sorting) {
            $this->builder()->addOrderBy($this->fieldWithAlias($field), $sorting);
        }

        return $this;
    }

    /**
     * GROUP BY clause
     *
     * @param array $fields Array of field names to use for grouping
     *
     * @return $this
     * @throws Exception
     */
    public function group(array $fields = []): self {
        foreach ($fields as $field) {
            $this->builder()->addGroupBy($this->fieldWithAlias($field));
        }

        return $this;
    }

    /**
     * Having clause to filter results by a calculated value
     *
     * @param array $having Array (like where) for HAVING statement for filter records by
     * @param string $type
     *
     * @return $this
     * @throws DBALException
     * @throws Exception
     */
    public function having(array $having, $type = 'AND'): self {
        $this->builder()->having(implode(' ' . $type . ' ', $this->parseWhereToSQLFragments($having, false)));

        return $this;
    }

    // ===================================================================

    /**
     * SPL Countable function
     * Called automatically when attribute is used in a 'count()' function call
     *
     * Executes separate query with COUNT(*), and drops and ordering (not
     * important for counting)
     *
     * @return int
     */
    public function count(): int {
        $countCopy = clone $this->builder();
        $stmt = $countCopy->select('COUNT(*)')->resetQueryPart('orderBy')->execute();

        return (int) $stmt->fetchColumn(0);
    }

    /**
     * JsonSerializable
     *
     * @inheritdoc
     */
    public function jsonSerialize() {
        return $this->toArray();
    }

    /**
     * Convenience function pass-through for Collection
     *
     * @param string|null $keyColumn
     * @param string|null $valueColumn
     *
     * @return array
     * @throws DBALException
     * @throws Exception
     */
    public function toArray($keyColumn = null, $valueColumn = null): array {
        $result = $this->execute();

        return ($result !== false) ? $result->toArray($keyColumn, $valueColumn) : [];
    }

    /**
     * Execute and return query as a collection
     *
     * @return mixed Collection object on success, boolean false on failure
     * @throws DBALException
     * @throws Exception
     */
    public function execute() {
        // @TODO Add caching to execute based on resulting SQL+data so we don't execute same query w/same data multiple times
        return $this->mapper()->resolver()->read($this);
    }

    /**
     * Return the first entity matched by the query
     *
     * @return mixed Entity on success, boolean false on failure
     * @throws DBALException
     * @throws Exception
     */
    public function first() {
        $result = $this->limit(1)->execute();

        return ($result !== false) ? $result->first() : false;
    }

    /**
     * Limit executed query to specified amount of records
     * Implemented at adapter-level for databases that support it
     *
     * @param int $limit Number of records to return
     * @param int $offset Record to start at for limited result set
     *
     * @return $this
     */
    public function limit($limit, $offset = null): self {
        $this->builder()->setMaxResults($limit);
        if ($offset !== null) {
            $this->offset($offset);
        }

        return $this;
    }

    /**
     * Offset executed query to skip specified amount of records
     * Implemented at adapter-level for databases that support it
     *
     * @param int $offset Record to start at for limited result set
     *
     * @return $this
     */
    public function offset($offset): self {
        $this->builder()->setFirstResult($offset);

        return $this;
    }

    /**
     * Get raw SQL string from built query
     *
     * @return string
     * @throws DBALException
     * @throws Exception
     */
    public function toSql(): string {
        if ($this->_noQuote) {
            $escapeCharacter = $this->mapper()->connection()->getDatabasePlatform()->getIdentifierQuoteCharacter();
            return str_replace($escapeCharacter, '', $this->builder()->getSQL());
        }

        return $this->builder()->getSQL();
    }

    /**
     * Escape/quote direct user input
     *
     * @param string $string
     *
     * @return string
     * @throws Exception
     */
    public function escape($string): string {
        if ($this->_noQuote) {
            return $string;
        }

        return $this->mapper()->connection()->quote($string);
    }

    /**
     * Removes escape/quote character
     *
     * @param string $identifier
     *
     * @return string
     * @throws DBALException
     * @throws Exception
     */
    public function unescapeIdentifier($identifier) {
        if (strpos($identifier, '.') !== false) {
            $parts = array_map(array($this, 'unescapeIdentifier'), explode('.', $identifier));

            return implode('.', $parts);
        }

        return trim($identifier, $this->mapper()->connection()->getDatabasePlatform()->getIdentifierQuoteCharacter());
    }

    /**
     * SPL - ArrayAccess
     *
     * @inheritdoc
     */
    public function offsetExists($key) {
        $results = $this->getIterator();

        return isset($results[$key]);
    }

    /**
     * SPL IteratorAggregate function
     * Called automatically when attribute is used in a 'foreach' loop
     *
     * @return Collection
     * @throws DBALException
     * @throws Exception
     */
    public function getIterator(): Collection {
        // Execute query and return result set for iteration
        $result = $this->execute();

        return ($result !== false) ? $result : [];
    }

    /**
     * SPL - ArrayAccess
     *
     * @inheritdoc
     */
    public function offsetGet($key) {
        $results = $this->getIterator();

        return $results[$key];
    }

    /**
     * SPL - ArrayAccess
     *
     * @inheritdoc
     */
    public function offsetSet($key, $value) {

        $results = $this->getIterator();

        if ($key === null) {
            return $results[] = $value;
        } else {
            return $results[$key] = $value;
        }

    }

    /**
     * SPL - ArrayAccess
     *
     * @inheritdoc
     */
    public function offsetUnset($key) {
        $results = $this->getIterator();
        unset($results[$key]);
    }

    /**
     * Run user-added callback
     *
     * @param string $method Method name called
     * @param array $args Array of arguments used in missing method call
     *
     * @return mixed
     * @throws DBALException
     * @throws Exception
     */
    public function __call($method, $args) {

        $scopes = $this->mapper()->scopes();

        // Custom methods
        if (isset(self::$_customMethods[$method]) && is_callable(self::$_customMethods[$method])) {

            $callback = self::$_customMethods[$method];

            // Pass the current query object as the first parameter
            array_unshift($args, $this);

            return call_user_func_array($callback, $args);

        // Scopes
        } else if (isset($scopes[$method])) {

            // Pass the current query object as the first parameter
            array_unshift($args, $this);

            return call_user_func_array($scopes[$method], $args);

        // Methods on Collection
        } elseif (method_exists(Collection::class, $method)) {

            return call_user_func_array([$this->execute(), $method], $args);

        }

        throw new BadMethodCallException("Method '" . __CLASS__ . '::' . $method . "' not found");
    }
}
