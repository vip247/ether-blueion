<?php declare(strict_types=1);

namespace Ether\Database\Orm\Relation;

use Countable;
use Ether\Database\Orm\Query;
use ArrayAccess;
use IteratorAggregate;
use Ether\Database\Orm\Mapper;
use Ether\Database\Orm\Entity;
use Doctrine\DBAL\DBALException;
use Ether\Database\Orm\Exception;
use Ether\Database\Orm\EntityInterface;
use Ether\Database\Orm\Entity\Collection;

/**
 * Relation object for HasMany relation
 *
 *
 */
class HasMany extends RelationAbstract implements Countable, IteratorAggregate, ArrayAccess {

    /**
     * Constructor function
     *
     * @param Mapper $mapper
     * @param $entityName
     * @param $foreignKey
     * @param $localKey
     * @param $identityValue
     */
    public function __construct(Mapper $mapper, $entityName, $foreignKey, $localKey, $identityValue) {
        $this->mapper = $mapper;

        $this->entityName = $entityName;
        $this->foreignKey = $foreignKey;
        $this->localKey = $localKey;

        $this->identityValue = $identityValue;
    }

    /**
     * Map relation results to original collection of entities
     *
     * @param string Relation name
     * @param Collection $collection
     *
     * @return Collection
     */
    public function eagerLoadOnCollection($relationName, Collection $collection): Collection {

        // Get relation object and change the 'identityValue' to an array
        // of all the identities in the current collection
        $this->identityValuesFromCollection($collection);
        $relationForeignKey = $this->foreignKey();
        $relationEntityKey = $this->entityKey();
        $collectionRelations = $this->query();
        $collectionClass = $this->mapper()->getMapper($this->entityName())->collectionClass();

        // Separate up related objects for each entity by foreign key value
        // ex. comment foreignKey 'post_id' will == entity primaryKey value
        $entityRelations = [];
        foreach ($collectionRelations as $relatedEntity) {

            $foreignKey = is_object($relatedEntity->$relationForeignKey) ? $relatedEntity->$relationForeignKey->toString() : $relatedEntity->$relationForeignKey;

            $entityRelations[$foreignKey][] = $relatedEntity;

        }

        // Set relation collections back on each entity object
        foreach ($collection as $entity) {

            $key = is_object($entity->$relationEntityKey) ? $entity->$relationEntityKey->toString() : $entity->$relationEntityKey;

            if (isset($entityRelations[$key])) {

                $entityCollection = new $collectionClass($entityRelations[$key]);
                $entity->relation($relationName, $entityCollection);

            } else {

                $entity->relation($relationName, new $collectionClass());

            }
        }

        return $collection;
    }

    /**
     * Set identity values from given collection
     *
     * @param Collection $collection
     */
    public function identityValuesFromCollection(Collection $collection) {
        $this->identityValue($collection->resultsIdentities());
    }

    /**
     * Save related entities
     *
     * @param EntityInterface $entity Entity to save relation from
     * @param string $relationName Name of the relation to save
     * @param array $options Options to pass to the mappers
     *
     * @return boolean
     *
     * @throws DBALException
     * @throws Exception
     */
    public function save(EntityInterface $entity, $relationName, $options = []) {

        $relatedEntities = $entity->relation($relationName);
        $deletedIds = [];
        $lastResult = false;
        $relatedMapper = $this->mapper()->getMapper($this->entityName());

        if (is_array($relatedEntities) || $relatedEntities instanceof Entity\Collection) {

            $oldEntities = $this->execute();
            $relatedIds = [];

            foreach ($relatedEntities as $related) {

                if(is_object($related->get($this->foreignKey()))) {
                    $equalityResult = $related->get($this->foreignKey())->toString() !== $entity->primaryKey()->toString();
                } else {
                    $equalityResult = $related->get($this->foreignKey()) !== $entity->primaryKey();
                }

                if ($equalityResult || $related->isNew() || $related->isModified()) {
                    // Update the foreign key to match the main entity primary key
                    $related->set($this->foreignKey(), $entity->primaryKey());
                    $lastResult = $relatedMapper->save($related, $options);
                }

                $relatedIds[] = $related->id;
            }

            $relatedIds = array_map(static function ($item) {

                if(is_object($item)){
                    return $item->toString();
                }

                return $item;

            }, $relatedIds);

            foreach ($oldEntities as $oldRelatedEntity) {

                $primaryKey = is_object($oldRelatedEntity->primaryKey()) ? $oldRelatedEntity->primaryKey()->toString() : $oldRelatedEntity->primaryKey();

                if ( ! in_array($primaryKey, $relatedIds, true)) {
                    $deletedIds[] = $oldRelatedEntity->primaryKey();
                }
            }
        }

        if ($relatedEntities === false || count($deletedIds)) {

            $conditions = [$this->foreignKey() => $entity->primaryKey()];

            if (count($deletedIds)) {
                $conditions[$this->localKey() . ' :in'] = $deletedIds;
            }

            if ($relatedMapper->entityManager()->fields()[$this->foreignKey()]['notnull']) {
                $relatedMapper->delete($conditions);
            } else {
                $relatedMapper->queryBuilder()->builder()->update($relatedMapper->table())->set($this->foreignKey(), null)->where($conditions);
            }
        }

        return $lastResult;
    }

    /**
     * SPL Countable function
     * Called automatically when attribute is used in a 'count()' function call
     *
     * @return integer
     */
    public function count() {
        if ($this->result === null) {
            $count = $this->query()->count();
        } else {
            $count = count($this->result);
        }
        return $count;
    }

    /**
     * SPL IteratorAggregate function
     * Called automatically when attribute is used in a 'foreach' loop
     *
     * @return Collection
     */
    public function getIterator() {
        // Load related records for current row
        $data = $this->execute();

        return $data ? $data : [];
    }

    public function offsetExists($key) {
        $this->execute();

        return isset($this->result[$key]);
    }

    // SPL - ArrayAccess functions
    // ----------------------------------------------

    public function offsetGet($key) {
        $this->execute();

        return $this->result[$key];
    }

    public function offsetSet($key, $value) {
        $this->execute();

        if ($key === null) {
            return $this->result[] = $value;
        } else {
            return $this->result[$key] = $value;
        }
    }

    public function offsetUnset($key) {
        $this->execute();
        unset($this->result[$key]);
    }

    /**
     * Build query object
     *
     * @return Query
     */
    protected function buildQuery() {
        $foreignMapper = $this->mapper()->getMapper($this->entityName());

        return $foreignMapper->where([$this->foreignKey() => $this->identityValue()]);
    }
}
