<?php declare(strict_types=1);

namespace Ether\Database\Orm\Relation;

use ArrayAccess;
use Ether\Database\Orm\Query;
use Ether\Database\Orm\Entity;
use Ether\Database\Orm\Mapper;
use Doctrine\DBAL\DBALException;
use Ether\Database\Orm\Exception;
use Ether\Database\Orm\EntityInterface;
use Ether\Database\Orm\Entity\Collection;

/**
 * BelongsTo Relation
 *
 * Only used so that the query can be lazy-loaded on demand
 *
 *
 */
class BelongsTo extends RelationAbstract implements ArrayAccess {

    /**
     * Constructor function
     *
     * @param Mapper $mapper
     * @param $entityName
     * @param $foreignKey
     * @param $localKey
     * @param $identityValue
     *
     */
    public function __construct(Mapper $mapper, $entityName, $foreignKey, $localKey, $identityValue) {
        $this->mapper = $mapper;

        $this->entityName = $entityName;
        $this->foreignKey = $foreignKey;
        $this->localKey = $localKey;

        $this->identityValue = $identityValue;
    }

    /**
     * Set identity values from given collection
     *
     * @param Collection $collection
     */
    public function identityValuesFromCollection(Collection $collection) {
        $this->identityValue($collection->toArray($this->localKey()));
    }

    /**
     * Get entity key field - for BelongsTo, this will be the local key instead
     * of the primary key.
     *
     * @return string
     */
    public function entityKey() {
        return $this->localKey();
    }

    /**
     * Helper function to return the entity
     */
    public function entity() {
        return $this->execute();
    }

    /**
     * Find first entity in the set
     *
     * @return Entity
     */
    public function execute() {
        if ($this->result === null) {
            $this->result = $this->query()->execute()->first();
        }

        return $this->result;
    }

    /**
     * Save related entities
     *
     * @param EntityInterface $entity Entity to save relation from
     * @param string $relationName Name of the relation to save
     * @param array $options Options to pass to the mappers
     *
     * @return boolean|int
     * @throws DBALException
     * @throws Exception
     */
    public function save(EntityInterface $entity, $relationName, $options = []) {

        $lastResult = 0;
        $relatedEntity = $entity->relation($relationName);

        if ($relatedEntity instanceof EntityInterface) {
            if ($relatedEntity->isNew() || $relatedEntity->isModified()) {
                $relatedMapper = $this->mapper()->getMapper($this->entityName());

                $lastResult = $relatedMapper->save($relatedEntity, $options);
                //Update the local key to match the related entity primary key
                if ($entity->get($this->localKey()) !== $relatedEntity->primaryKey()) {
                    $relatedRelations = $entity->relations($relatedMapper, $relatedEntity);

                    //Check if it was a hasOne or a hasMany relation,
                    //if hasOne, we must unset old value
                    foreach ($relatedRelations as $relatedRelation) {
                        if ($relatedRelation instanceof Relation\HasOne && $relatedRelation->foreignKey() === $this->localKey()) {
                            if ($relatedMapper->entityManager()->fields()[$relatedRelation->foreignKey()]['notnull']) {
                                $lastResult = $relatedMapper->delete([$relatedRelation->foreignKey() => $entity->get($relatedRelation->foreignKey())]);
                            } else {
                                $lastResult = $relatedMapper->queryBuilder()->builder()->update($relatedMapper->table())->set($relatedRelation->foreignKey(), null)->where([$relatedRelation->foreignKey() => $entity->get($relatedRelation->foreignKey())]);
                            }
                        }
                    }
                    $entity->set($this->localKey(), $relatedEntity->primaryKey());
                }
            }
        }

        return $lastResult;
    }

    public function __get($key) {
        $entity = $this->execute();
        if ($entity) {
            return $entity->$key;
        }
        return null;
    }

    // Magic getter/setter pass through
    // ----------------------------------------------

    public function __set($key, $val) {
        $this->execute()->$key = $val;
    }

    public function __isset($key) {}

    public function offsetExists($key) {
        $entity = $this->execute();

        return isset($entity->$key);
    }

    // SPL - ArrayAccess functions
    // ----------------------------------------------

    public function offsetGet($key) {
        $entity = $this->execute();

        return $entity->$key;
    }

    public function offsetSet($key, $value) {
        $entity = $this->execute();

        if ($key === null) {
            return $entity[] = $value;
        } else {
            return $entity->$key = $value;
        }
    }

    public function offsetUnset($key) {
        $entity = $this->execute();
        unset($entity->$key);
    }

    /**
     * Build query object
     *
     * @return Query
     */
    protected function buildQuery() {
        $foreignMapper = $this->mapper()->getMapper($this->entityName());

        return $foreignMapper->where([$this->foreignKey() => $this->identityValue()]);
    }
}
