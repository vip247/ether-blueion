<?php declare(strict_types=1);

namespace Ether\Database\Orm\Relation;

use ArrayAccess;
use Ether\Database\Query;
use Ether\Database\Entity;
use Ether\Database\Orm\Mapper;
use Doctrine\DBAL\DBALException;
use Ether\Database\Orm\Exception;
use Ether\Database\Orm\EntityInterface;
use Ether\Database\Orm\Entity\Collection;

/**
 * HasOne Relation
 *
 * Only used so that the query can be lazy-loaded on demand
 *
 *
 */
class HasOne extends RelationAbstract implements ArrayAccess {

    /**
     * Constructor function
     *
     * @param Mapper $mapper
     * @param $entityName
     * @param $foreignKey
     * @param $localKey
     * @param $identityValue
     */
    public function __construct(Mapper $mapper, $entityName, $foreignKey, $localKey, $identityValue) {
        $this->mapper = $mapper;

        $this->entityName = $entityName;
        $this->foreignKey = $foreignKey;
        $this->localKey = $localKey;

        $this->identityValue = $identityValue;
    }

    /**
     * Set identity values from given collection
     *
     * @param Entity\Collection
     */
    public function identityValuesFromCollection(Collection $collection) {
        $this->identityValue($collection->resultsIdentities());
    }

    /**
     * Helper function to return the entity
     */
    public function entity() {
        return $this->execute();
    }

    /**
     * Find first entity in the set
     *
     * @return Entity
     */
    public function execute() {
        if ($this->result === null) {
            $this->result = $this->query()->execute()->first();
        }

        return $this->result;
    }

    /**
     * Save related entities
     *
     * @param EntityInterface $entity Entity to save relation from
     * @param string $relationName Name of the relation to save
     * @param array $options Options to pass to the mappers
     *
     * @return boolean|int
     *
     * @throws DBALException
     * @throws Exception
     */
    public function save(EntityInterface $entity, $relationName, $options = []) {

        $lastResult = false;
        $relatedEntity = $entity->relation($relationName);
        $relatedMapper = $this->mapper()->getMapper($this->entityName());

        // Auto-loaded relation, no need to save
        if ($relatedEntity instanceof self) {
            return 0;
        }

        if(is_object($relatedEntity->get($this->foreignKey()))) {
            $equalityResult = $relatedEntity->get($this->foreignKey())->toString() !== $entity->primaryKey()->toString();
        } else {
            $equalityResult = $relatedEntity->get($this->foreignKey()) !== $entity->primaryKey();
        }

        if ($relatedEntity === false || $equalityResult) {

            if ($relatedMapper->entityManager()->fields()[$this->foreignKey()]['notnull']) {
                $relatedMapper->delete([$this->foreignKey() => $entity->primaryKey()]);
            } else {
                $relatedMapper->queryBuilder()->builder()->update($relatedMapper->table())->set($this->foreignKey(), null)->where([$this->foreignKey() => $entity->primaryKey()]);
            }

            if ($relatedEntity instanceof EntityInterface) {
                //Update the foreign key to match the main entity primary key
                $relatedEntity->set($this->foreignKey(), $entity->primaryKey());
            }
        }

        if ($relatedEntity instanceof EntityInterface) {
            $lastResult = $relatedMapper->save($relatedEntity, $options);
        }

        return $lastResult;
    }

    public function __get($key) {
        $entity = $this->execute();
        if ($entity) {
            return $entity->$key;
        }
        return null;
    }

    // Magic getter/setter passthru
    // ----------------------------------------------

    public function __set($key, $val) {
        $this->execute()->$key = $val;
    }

    public function offsetExists($key) {
        $entity = $this->execute();

        return isset($entity->$key);
    }

    // SPL - ArrayAccess functions
    // ----------------------------------------------

    public function offsetGet($key) {
        $entity = $this->execute();

        return $entity->$key;
    }

    public function offsetSet($key, $value) {
        $entity = $this->execute();

        if ($key === null) {
            return $entity[] = $value;
        } else {
            return $entity->$key = $value;
        }
    }

    public function offsetUnset($key) {
        $entity = $this->execute();
        unset($entity->$key);
    }

    /**
     * Build query object
     *
     * @return Query
     */
    protected function buildQuery() {
        $foreignMapper = $this->mapper()->getMapper($this->entityName());

        return $foreignMapper->where([$this->foreignKey() => $this->identityValue()]);
    }
}
