<?php declare(strict_types=1);

namespace Ether\Database\Orm\Relation;

use BadMethodCallException;
use Ether\Database\Orm\Query;
use Ether\Database\Orm\Mapper;
use Ramsey\Uuid\UuidInterface;
use Doctrine\DBAL\Types\GuidType;
use Ether\Database\Orm\EntityInterface;
use Ether\Database\Orm\Entity\Collection;

abstract class RelationAbstract {

    protected $mapper;
    protected $entityName;

    protected $foreignKey;
    protected $localKey;
    protected $identityValue;

    protected $query;
    protected $queryQueue = [];

    protected $result;

    /**
     * Get local key field
     *
     * @return int|UuidInterface|GuidType
     */
    public function localKey() {
        return $this->localKey;
    }

    /**
     * Get identity value
     *
     * @param null $identityValue
     *
     * @return mixed Array, string, or id
     */
    public function identityValue($identityValue = null) {
        if ($identityValue !== null) {
            $this->identityValue = $identityValue;
        }

        return $this->identityValue;
    }

    /**
     * Map relation results to original collection of entities
     *
     * @param string Relation name
     * @param Collection $collection
     *
     * @return Collection
     */
    public function eagerLoadOnCollection($relationName, Collection $collection): Collection {

        // Get relation object and change the 'identityValue' to an array
        // of all the identities in the current collection
        $this->identityValuesFromCollection($collection);
        $relationForeignKey = $this->foreignKey();
        $relationEntityKey = $this->entityKey();
        $collectionRelations = $this->query();

        // Separate related objects for each entity by foreign key value
        // ex. comment foreignKey 'post_id' will == entity primaryKey value
        $entityRelations = [];
        foreach ($collectionRelations as $relatedEntity) {

            $foreignKey = is_object($relatedEntity->$relationForeignKey) ? $relatedEntity->$relationForeignKey->toString() : $relatedEntity->$relationForeignKey;

            $entityRelations[$foreignKey] = $relatedEntity;

        }

        // Set relation collections back on each entity object
        foreach ($collection as $entity) {

            $key = is_object($entity->$relationEntityKey) ? $entity->$relationEntityKey->toString() : $entity->$relationEntityKey;

            if (isset($entityRelations[$key])) {
                $entity->relation($relationName, $entityRelations[$key]);
            } else {
                $entity->relation($relationName, false);
            }
        }

        return $collection;
    }

    /**
     * Set identity values from given collection
     *
     * @param Collection $collection
     */
    abstract public function identityValuesFromCollection(Collection $collection);

    /**
     * Get foreign key field
     *
     * @return int|UuidInterface|GuidType
     */
    public function foreignKey() {
        return $this->foreignKey;
    }

    /**
     * Get entity key field - defaults to the primary key
     *
     * @return int|UuidInterface|GuidType
     */
    public function entityKey() {
        return $this->mapper()->primaryKeyField();
    }

    /**
     * Get Mapper object
     *
     * @return Mapper
     */
    public function mapper() {
        return $this->mapper;
    }

    /**
     * Get query object instance
     */
    public function query() {
        if ($this->query === null) {
            $this->query = $this->buildQuery();
            foreach ($this->queryQueue as $closure) {
                $this->query = $closure($this->query);
            }
        }

        return $this->query;
    }

    /**
     * Build query object
     *
     * @return Query
     */
    abstract protected function buildQuery();

    /**
     * Save related entities
     *
     * @param EntityInterface $entity Entity to save relation from
     * @param string $relationName Name of the relation to save
     * @param array $options Options to pass to the mappers
     *
     * @return boolean
     */
    abstract public function save(EntityInterface $entity, $relationName, $options = []);

    /**
     * Pass-through for missing methods on expected object result
     *
     * @param $func
     * @param $args
     *
     * @return RelationAbstract|mixed
     */
    public function __call($func, $args) {

        if (method_exists(Query::class, $func) || array_key_exists($func, $this->mapper()->getMapper($this->entityName())->scopes())) {

            // See if method exists on Query object, and if it does, add query
            // modification to queue to be executed after query is built and
            // ready so that query is not executed immediately
            $this->queryQueue[] = static function (Query $query) use ($func, $args) {
                return call_user_func_array([$query, $func], $args);
            };

            return $this;

        } else {

            // See if method exists on destination object after execution
            // (typically either Ether\Database\Orm\Entity\Collection or Ether\Database\Orm\Entity object)
            $result = $this->execute();
            if (method_exists(get_class($result), $func)) {
                return call_user_func_array([$result, $func], $args);
            }

            throw new BadMethodCallException('Method ' . static::class . "::$func does not exist");

        }
    }

    /**
     * Get entity name object
     *
     * @return string
     */
    public function entityName(): string {
        return $this->entityName;
    }

    /**
     * Execute query and return results
     */
    public function execute() {
        if ($this->result === null) {
            $this->result = $this->query()->execute();
        }

        return $this->result;
    }
}
