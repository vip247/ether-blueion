<?php declare(strict_types=1);

namespace Ether\Database\DBAL\Types;

use InvalidArgumentException;
use Doctrine\DBAL\Types\Type;
use Ramsey\Uuid\UuidInterface;
use Ramsey\Uuid\Uuid as RamseyUuid;
use Doctrine\DBAL\Types\ConversionException;
use Doctrine\DBAL\Platforms\AbstractPlatform;

final class Uuid extends Type {

    /** @var string */
    public const NAME = 'uuid';

    /**
     * {@inheritdoc}
     *
     * @return string
     */
    public function getName(): string {
        return static::NAME;
    }

    /**
     * {@inheritdoc}
     *
     * @param array $fieldDeclaration
     * @param AbstractPlatform $platform
     */
    public function getSQLDeclaration(array $fieldDeclaration, AbstractPlatform $platform): string {
        return $platform->getGuidTypeDeclarationSQL($fieldDeclaration);
    }


    /**
     * {@inheritdoc}
     *
     * @param string|UuidInterface|null $value
     * @param AbstractPlatform $platform
     *
     * @throws ConversionException
     */
    public function convertToPHPValue($value, AbstractPlatform $platform) {

        if (empty($value)) {
            return null;
        }

        if ($value instanceof UuidInterface) {
            return $value;
        }

        try {
            $uuid = RamseyUuid::fromString($value);
        } catch (InvalidArgumentException $e) {
            throw ConversionException::conversionFailed($value, static::NAME);
        }

        return $uuid;
    }

    /**
     * {@inheritdoc}
     *
     * @param UuidInterface|string|null $value
     * @param AbstractPlatform $platform
     *
     * @throws ConversionException
     */
    public function convertToDatabaseValue($value, AbstractPlatform $platform) {

        if (empty($value)) {
            return null;
        }

        if ($value instanceof UuidInterface || ((is_string($value) || method_exists($value, '__toString')) && RamseyUuid::isValid((string) $value))) {
            return (string) $value;
        }

        throw ConversionException::conversionFailed($value, static::NAME);
    }

    /**
     * {@inheritdoc}
     *
     * @param AbstractPlatform $platform
     *
     * @return boolean
     */
    public function requiresSQLCommentHint(AbstractPlatform $platform): bool {
        return true;
    }
}
