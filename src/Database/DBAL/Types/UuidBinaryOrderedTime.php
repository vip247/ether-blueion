<?php declare(strict_types=1);

namespace Ether\Database\DBAL\Types;

use Ramsey\Uuid\UuidFactory;
use Doctrine\DBAL\Types\Type;
use InvalidArgumentException;
use Ramsey\Uuid\UuidInterface;
use Ramsey\Uuid\Codec\OrderedTimeCodec;
use Doctrine\DBAL\Types\ConversionException;
use Doctrine\DBAL\Platforms\AbstractPlatform;
use Ramsey\Uuid\Codec\CodecInterface as Codec;
use Ramsey\Uuid\UuidFactoryInterface as Factory;

final class UuidBinaryOrderedTime extends Type {

    /** @var string */
	public const NAME = 'uuid_binary_ordered_time';

    /** @var Codec */
	private $codec;

    /** @var Factory */
	private $factory;

    /**
     * {@inheritdoc}
     *
     * @return string
     */
    public function getName(): string {
        return self::NAME;
    }

    /**
     * {@inheritdoc}
     *
     * @param array $fieldDeclaration
     * @param AbstractPlatform $platform
     */
    public function getSQLDeclaration(array $fieldDeclaration, AbstractPlatform $platform): string {
		return $platform->getBinaryTypeDeclarationSQL([
            'length' => '16',
            'fixed' => true
        ]);
	}

    /**
     * {@inheritdoc}
     *
     * @param string|UuidInterface|null $value
     * @param AbstractPlatform $platform
     *
     * @throws ConversionException
     */
    public function convertToPHPValue($value, AbstractPlatform $platform) {

		if (empty($value)) {
			return null;
		}

		if ($value instanceof UuidInterface) {
			return $value;
		}

		try {
			return $this->decode($value);
		} catch (InvalidArgumentException $e) {
			throw ConversionException::conversionFailed($value, static::NAME);
		}
	}

    /**
     * {@inheritdoc}
     *
     * @param UuidInterface|string|null $value
     * @param AbstractPlatform $platform
     *
     * @return string|null
     *
     * @throws ConversionException
     */
    public function convertToDatabaseValue($value, AbstractPlatform $platform): ?string {
		if (empty($value)) {
			return null;
		}

		if ($value instanceof UuidInterface) {
			return $this->encode($value);
		}

		try {
			$uuid = $this->getUuidFactory()->fromString($value);
		} catch (InvalidArgumentException $e) {
			throw ConversionException::conversionFailed($value, static::NAME);
		}

		return $this->encode($uuid);
	}

    /**
     * {@inheritdoc}
     *
     * @param AbstractPlatform $platform
     *
     * @return boolean
     */
    public function requiresSQLCommentHint(AbstractPlatform $platform): bool {
		return true;
	}

    /**
     * Creates/returns a UuidFactory instance that uses a specific codec
     * that creates a binary that can be time-ordered
     *
     * @return null|UuidFactory
     */
    private function getUuidFactory(): ?Factory {
		if (null === $this->factory) {
			$this->factory = new UuidFactory();
		}

		return $this->factory;
	}

    /**
     * @return Codec
     */
    private function getCodec(): Codec {
		if (null === $this->codec) {
			$this->codec = new OrderedTimeCodec(
				$this->getUuidFactory()->getUuidBuilder()
			);
		}

		return $this->codec;
	}

    /**
     * Using this type only makes sense with Uuid version 1 as this is the only
     * kind of UUID that can be time-ordered. Passing any other UUID into
     * this type is likely a mistake
     *
     * @param UuidInterface $value
     *
     * @throws ConversionException
     */
    private function assertUuidV1(UuidInterface $value): void {
		if (1 !== $value->getVersion()) {
			throw ConversionException::conversionFailed($value->toString(), self::NAME);
		}
	}

    /**
     * @param UuidInterface $uuid
     *
     * @return string
     */
    private function encode(UuidInterface $uuid): string {
		try {
			$this->assertUuidV1($uuid);
		} catch (ConversionException $e) {
			echo $e->getMessage();
		}

		return $this->getCodec()->encodeBinary($uuid);
	}

    /**
     * @param $bytes
     *
     * @return UuidInterface
     */
    private function decode($bytes): UuidInterface {
		$decoded = $this->getCodec()->decodeBytes($bytes);

		try {
			$this->assertUuidV1($decoded);
		} catch (ConversionException $e) {
			echo $e->getMessage();
		}

		return $decoded;
	}
}
