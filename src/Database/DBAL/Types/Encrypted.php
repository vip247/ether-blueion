<?php declare(strict_types=1);

namespace Ether\Database\DBAL\Types;

use Exception;
use Doctrine\DBAL\Types\Type;
use Doctrine\DBAL\Types\ConversionException;
use Doctrine\DBAL\Platforms\AbstractPlatform;

/**
 *
 * This class encrypts and decrypts a value using Authenticated Encryption with Associated Data AEAD (encrypt) AEAD
 * ChaCha20 + Poly1305 (IETF version)
 *
 * @link https://download.libsodium.org/doc/secret-key_cryptography/aead
 * @link https://paragonie.com/blog/2017/06/libsodium-quick-reference-quick-comparison-similar-functions-and-which-one-use#crypto-secretbox
 *
 */
class Encrypted extends Type {

    /** @var string */
    public const NAME = 'encrypted';

    /** @var string */
    public static $key;

    /**
     * @return string
     */
    public function getName(): string {
        return static::NAME;
    }

    /**
     * @param array $fieldDeclaration
     * @param AbstractPlatform $platform
     *
     * @return string
     */
    public function getSqlDeclaration(array $fieldDeclaration, AbstractPlatform $platform): string {
        return 'TEXT';
    }

    /**
     * @param mixed $value
     * @param AbstractPlatform $platform
     *
     * @return mixed|string
     *
     * @throws Exception
     *
     */
    public function convertToPHPValue($value, AbstractPlatform $platform) {
        return $this->decrypt(base64_decode($value), self::$key);
    }

    /**
     *
     * @param mixed $value
     * @param AbstractPlatform $platform
     *
     * @return mixed|string
     *
     * @throws Exception
     *
     */
    public function convertToDatabaseValue($value, AbstractPlatform $platform) {
        return base64_encode($this->encrypt($value, self::$key));
    }

    /**
     * @param string $plainText
     * @param string $key
     *
     * @return string
     *
     * @throws Exception
     */
    private function encrypt(string $plainText, string $key): string {

        $nonce = random_bytes(SODIUM_CRYPTO_AEAD_XCHACHA20POLY1305_IETF_NPUBBYTES);

        $cipherText = sodium_crypto_aead_xchacha20poly1305_ietf_encrypt($plainText, $nonce, $nonce, $key);

        return $nonce . $cipherText;
    }

    /**
     * @param string $encrypted
     * @param string $key
     *
     * @return string
     *
     * @throws ConversionException
     */
    private function decrypt(string $encrypted, string $key): string {

        $nonce = mb_substr($encrypted, 0, SODIUM_CRYPTO_AEAD_XCHACHA20POLY1305_IETF_NPUBBYTES, '8bit');

        $cipherText = mb_substr($encrypted, SODIUM_CRYPTO_AEAD_XCHACHA20POLY1305_IETF_NPUBBYTES, null, '8bit');

        $plainText = sodium_crypto_aead_xchacha20poly1305_ietf_decrypt($cipherText, $nonce, $nonce, $key);

        if ( ! is_string($plainText)) {
            throw new ConversionException('Failed to decrypt value of type Encrypted.');
        }

        return $plainText;
    }
}
