<?php declare(strict_types=1);

namespace Ether\Database\DBAL\Helpers;

use Doctrine\DBAL\Types\Type;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\DBALException;
use Ether\Database\DBAL\Types\Uuid;
use Ether\Database\DBAL\Types\UuidBinaryOrderedTime;

final class TypeHelper {

	private $connection;

	private $baseTypes = array(
		'uuid' => array (
			'name' => 'uuid',
			'class' => Uuid::class,
			'doctrine_type' => 'guid'
		),
        'uuid_binary_ordered_time' => array (
            'name' => 'uuid_binary_ordered_time',
            'class' => UuidBinaryOrderedTime::class,
            'doctrine_type' => 'binary'
        )
	);

	public function __construct(Connection $connection) {
		$this->connection = $connection;
	}

	public function registerTypes(array $types = null): Connection {

		$types = ($types !== null) ? array_merge($this->baseTypes, $types) : $this->baseTypes;

		foreach ($types as $type) {

            if( ! Type::hasType($type['name'])) {

                try {

                    Type::addType($type['name'], $type['class']);

                    $this->connection->getDatabasePlatform()->registerDoctrineTypeMapping($type['name'], $type['doctrine_type']);

                    if((Type::getType($type['name']))->requiresSQLCommentHint($this->connection->getDatabasePlatform())) {
                        $this->connection->getDatabasePlatform()->markDoctrineTypeCommented($type['name']);
                    }

                } catch(DBALException $e) {
                    error_log($e->getMessage());
                }
            }
		}

		return $this->connection;
	}
}
