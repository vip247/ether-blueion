<?php declare(strict_types=1);

namespace Ether\Database\DBAL;

use Ether\Config\Config;
use Doctrine\DBAL\DBALException;
use Doctrine\DBAL\DriverManager;
use Ether\Database\DBAL\Helpers\TypeHelper;
use Doctrine\DBAL\Driver\Connection;

final class ConnectionFactory {

	private $config;

	public function __construct(Config $config) {
		$this->config = $config;
	}

	public function create(): Connection {

		$connection = null;

		try {

			$typeHelper = new TypeHelper(DriverManager::getConnection($this->config->get('database.connection')));

			$connection = $typeHelper->registerTypes();

		} catch (DBALException $e) {

			echo $e->getMessage();

		}

		return $connection;
	}
}
