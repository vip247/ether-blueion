<?php declare(strict_types=1);

namespace Ether\Database\Migrations;

use Doctrine\DBAL\Connection;
use Ether\Application as Ether;
use Ether\Database\Migrations\Finder\GlobFinder;
use Doctrine\Migrations\Configuration\Configuration;

final class ConfigurationFactory {

	private $ether;
	private $connection;
	private $migrationsDirectory = '/app/Database/migrations';

	public function __construct(Ether $ether, Connection $connection) {
		$this->ether = $ether;
		$this->connection = $connection;
	}

	public function create(): Configuration {

        $configuration = new Configuration($this->connection);

		$configuration->setAllOrNothing(true);
		$configuration->setMigrationsFinder(new GlobFinder());
		$configuration->setCustomTemplate(__DIR__ . '/templates/migration.template');
		$configuration->setName('Migrations');
		$configuration->setMigrationsTableName('migrations');
		$configuration->setMigrationsNamespace('Migrations');
		$configuration->setMigrationsDirectory($this->ether->basePath() . $this->migrationsDirectory);
		$configuration->setMigrationsColumnName('versions');
		$configuration->setMigrationsColumnLength(255);
        $configuration->setMigrationsExecutedAtColumnName('executed_at');
        $configuration->registerMigrationsFromDirectory($this->ether->basePath() . $this->migrationsDirectory);

		return $configuration;
	}
}