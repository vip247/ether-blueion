<?php declare(strict_types=1);

namespace Ether\Database\Migrations;

use Doctrine\Migrations\Configuration\Configuration;
use Doctrine\Migrations\Generator\Exception\InvalidTemplateSpecified;

final class Generator {

	private $configuration;
	private $template;

	public function __construct(Configuration $configuration) {
		$this->configuration = $configuration;
	}

	public function generateMigration(string $version, string $name): string {
		$placeHolders = array(
			'{namespace}' => $this->configuration->getMigrationsNamespace(),
			'{name}' => str_replace('_', '', ucwords($name, '_')),
		);

		$code = str_replace(array_keys($placeHolders), array_values($placeHolders), $this->getTemplate());

		$path = $this->configuration->getMigrationsDirectory() . '/' . $version . '_' . $name . '.php';

		file_put_contents($path, $code);

		return $path;
	}

	private function getTemplate(): string {
		if ($this->template === null) {
			$this->template = $this->loadCustomTemplate();
		}

		return $this->template;
	}

	/**
	 * @throws InvalidTemplateSpecified
	 */
	private function loadCustomTemplate(): ?string {
		$customTemplate = $this->configuration->getCustomTemplate();

		if ($customTemplate === null) {
			return null;
		}

		if ( ! is_file($customTemplate) || ! is_readable($customTemplate)) {
			throw InvalidTemplateSpecified::notFoundOrNotReadable($customTemplate);
		}

		$content = file_get_contents($customTemplate);

		if ($content === false) {
			throw InvalidTemplateSpecified::notReadable($customTemplate);
		}

		if (trim($content) === '') {
			throw InvalidTemplateSpecified::empty($customTemplate);
		}

		return $content;
	}
}
