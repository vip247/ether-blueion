<?php declare(strict_types=1);

namespace Ether\Database\Migrations\Finder;

use Doctrine\Migrations\Finder\Finder;
use Doctrine\Migrations\Finder\Exception\NameIsReserved;

/**
 * The GlobFinder class finds migrations in a directory using the PHP glob() function.
 */
final class GlobFinder extends Finder {

    protected function loadMigrations(array $files, ?string $namespace): array {

        $includedFiles = [];

        foreach ($files as $file) {
            static::requireOnce($file);
            $includedFiles[] = realpath($file);
        }

        $classes = $this->loadMigrationClasses($includedFiles, $namespace);

        $versions = [];

        foreach ($classes as $class) {

            $filePathParts = explode('/', $class->getFileName());

            $version = explode('_', end($filePathParts))[0];

            if ($version === '0') {
                throw NameIsReserved::new($version);
            }

            $versions[$version] = $class->getName();
        }

        ksort($versions, SORT_STRING);

        return $versions;
    }

	public function findMigrations(string $directory, ?string $namespace = null): array {
		$dir = $this->getRealPath($directory);

		$files = glob(rtrim($dir, '/') . '/*.php');

		return $this->loadMigrations($files, $namespace);
	}

}
