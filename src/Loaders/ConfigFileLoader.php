<?php declare(strict_types=1);

namespace Ether\Loaders;

use Symfony\Component\Config\Loader\FileLoader;

final class ConfigFileLoader extends FileLoader {

    /** @var array */
    private $fileResources = [];

    public function load($resource, $type = null) {

        $this->fileResources[] = $resource;

        /** @noinspection PhpIncludeInspection */
        return require $resource;
    }

    public function getFileResources(): array {
        return $this->fileResources;
    }

    public function supports($resource, $type = null): bool {
        return is_string($resource) && 'php' === pathinfo($resource, PATHINFO_EXTENSION);
    }
}
