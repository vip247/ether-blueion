<?php declare(strict_types=1);

namespace Ether;

use Dotenv\Dotenv;
use Ether\Config\Config;
use ReflectionException;
use Ether\Bootstrap\Agent;
use Ether\Database\DBManager;
use Doctrine\DBAL\Connection;
use Ether\Container\Container;
use Ether\Routing\RouteResolver;
use Ether\Loaders\ConfigFileLoader;
use Ether\Rendering\PhpViewRenderer;
use Doctrine\Migrations\OutputWriter;
use Ether\Routing\ControllerResolver;
use Ether\Traits\DotArrayAccessTrait;
use Ether\Exceptions\RuntimeException;
use Ether\Exceptions\ErrorPageHandler;
use Ether\Cli\Kernel as EtherCliKernel;
use Ether\Events\EventDispatcherFactory;
use Overtrue\Socialite\SocialiteManager;
use Ether\Http\Kernel as EtherHttpKernel;
use Ether\Cli\HelperSet\HelperSetFactory;
use Symfony\Component\Config\ConfigCache;
use Symfony\Component\Config\FileLocator;
use Ether\Database\DBAL\ConnectionFactory;
use Dotenv\Exception\InvalidPathException;
use Ether\Rendering\Contracts\ViewRenderer;
use Ether\Rendering\TwigViewRendererFactory;
use Symfony\Component\HttpKernel\HttpKernel;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Console\Helper\HelperSet;
use Ether\Container\Exceptions\NotFoundException;
use Symfony\Component\Routing\Matcher\UrlMatcher;
use Ether\Container\Exceptions\ContainerException;
use Symfony\Component\Config\FileLocatorInterface;
use Symfony\Component\Config\ConfigCacheInterface;
use Symfony\Component\Routing\Loader\PhpFileLoader;
use Ether\Database\Migrations\ConfigurationFactory;
use Symfony\Component\Console\Output\ConsoleOutput;
use Symfony\Component\Config\Loader\LoaderInterface;
use Doctrine\Migrations\Configuration\Configuration;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Symfony\Component\HttpKernel\Controller\ArgumentResolver;
use Symfony\Component\HttpKernel\Controller\ArgumentResolverInterface;
use Symfony\Component\HttpKernel\Controller\ControllerResolverInterface;
use Symfony\Component\HttpFoundation\Session\Storage\NativeSessionStorage;
use Symfony\Component\HttpFoundation\Session\Attribute\NamespacedAttributeBag;
use Symfony\Component\HttpFoundation\Session\Storage\Handler\NativeFileSessionHandler;
use /** @noinspection PhpUndefinedClassInspection */ Symfony\Contracts\EventDispatcher\EventDispatcherInterface;

final class Application {

	use DotArrayAccessTrait;

	public const VERSION = '0.2';

	/** @var Config $config */
    public $config;

    /** @var string $basePath */
	private $basePath;

    /** @var string $subDir */
	private $subDir;

    /** @var Container|null $container */
    private $container;

    /** @var string $namespace */
    private $namespace;

    /**
     * @param string $basePath
     *
     * @param string|null $subDir
     *
     * @param Container|null $container
     *
     * @throws ContainerException
     * @throws NotFoundException
     * @throws ReflectionException
     * @throws RuntimeException
     */
    public function __construct(string $basePath, ?string $subDir = null, ?Container $container = null) {

		$this->basePath = $basePath;

		//@TODO: have a look at this one more time
		$subDir === null ? $this->subDir = '' : $this->subDir = $subDir;

		$container === null ? $this->container = new Container() : $this->container = $container;

		$this->registerEnvironmentVariables();
        $this->registerBaseContainerContextualBindings();
        $this->registerBaseContainerBindings();
        $this->registerBaseContainerFactoryBindings();

        $this->registerConfig();

        $this->registerPostConfigBaseContainerBindings();
        $this->registerPostConfigBaseContainerContextualBindings();
		$this->registerAppAgents();
    }

    /**
     * @param $abstract
     * @param array $parameters
     *
     * @return object
     *
     * @throws ContainerException
     * @throws NotFoundException
     * @throws ReflectionException
     */
    public function make($abstract, array $parameters = []) {
        return $this->container->make($abstract, $parameters);
    }

    /**
     * @param $abstract
     * @param mixed|null $concrete
     * @param bool $shared
     */
    public function bind($abstract, $concrete = null, $shared = false): void {
        $this->container->bind($abstract, $concrete, $shared);
    }

    /**
     * @param $concrete
     *
     * @return object
     *
     * @throws ContainerException
     * @throws NotFoundException
     * @throws ReflectionException
     */
    public function build($concrete) {
        return $this->container->build($concrete);
    }

    /**
     * @param $abstract
     * @param null $concrete
     */
    public function share($abstract, $concrete = null): void {
        $this->container->share($abstract, $concrete);
    }

    public function setBasePath(string $basePath): void {
        $this->basePath = $basePath;
    }

	/**
	 * @param string|null $connectionConfigKey
	 *
	 * @return Connection
     *
     * @throws ContainerException
     * @throws NotFoundException
     * @throws ReflectionException
	 */
	public function getConnection(string $connectionConfigKey = null): Connection {
        return $this->container->build(DBManager::class)->getConnection($connectionConfigKey);
    }

    /**
     * @return string
     *
     * @throws RuntimeException
     */
    public function getNamespace(): string {

		if ($this->namespace !== null) {
			return $this->namespace;
		}

		$composerJson = realpath($this->basePath . $this->subDir . '/composer.json');

		if(($composerJson !== false) && file_exists($composerJson)) {

            $composerPaths = json_decode(file_get_contents($composerJson, true), true);

            foreach((array) $this->get('autoload.psr-4', $composerPaths) as $namespace => $path) {

                foreach((array) $path as $pathChoice) {
                    if(realpath($this->appPath()) === realpath($this->basePath . $this->subDir . '/' . $pathChoice)) {
                        return $this->namespace = $namespace;
                    }
                }

            }
        }

		throw new RuntimeException('Unable to detect application namespace.');
	}

	public function basePath(): string {
		return $this->basePath;
	}

	public function configPath(): string {
		return $this->basePath . $this->subDir . '/config';
	}

	public function appPath(): string {
		return $this->basePath . $this->subDir . '/app';
	}

	public function publicPath(): string {
		return $this->basePath . '/public';
	}

	public function storagePath(): string {
		return $this->basePath . $this->subDir . '/storage';
	}

	public function routesPath(): string {
		return $this->basePath . $this->subDir . '/routes';
	}

	public function cachePath(): string {
		return $this->basePath . $this->subDir . '/storage/cache';
	}

    /**
     * @throws InvalidPathException
     */
    private function registerEnvironmentVariables(): void {

        if(getenv('APP_NAME') === false) {

            if(file_exists($this->basePath . '/.env.local')) {

                Dotenv::createImmutable($this->basePath, '.env.local')->load();

            } else {

                Dotenv::createImmutable($this->basePath)->load();

            }
        }
    }

    /**
     * @throws ContainerException
     * @throws NotFoundException
     * @throws ReflectionException
     * @throws RuntimeException
     */
    private function registerConfig(): void {

	    /** @var Config $config */
	    $config = $this->container->build(Config::class);

        //$config->clearCacheIfDebug();

        $this->config = $config->load($this->configPath());

        $this->container->shareInstance(Config::class, $this->config);
    }

    private function registerBaseContainerBindings(): void {

        $this->container->shareInstance(__CLASS__, $this);

        $this->container->share(OutputWriter::class);
        $this->container->share(SocialiteManager::class);
        $this->container->share(ConfigCache::class);
        $this->container->share(HttpKernel::class);
        $this->container->share(EtherHttpKernel::class);
        $this->container->share(EtherCliKernel::class);
        $this->container->share(Request::class);
        $this->container->share(Response::class);
        $this->container->share(UrlMatcher::class);
        $this->container->share(DBManager::class);
        $this->container->share(TwigViewRendererFactory::class);
        $this->container->share(FileLocatorInterface::class, FileLocator::class);
        $this->container->share(Session::class, function() {
            return new Session(
                new NativeSessionStorage(
                    [],
                    $this->container->build(NativeFileSessionHandler::class)
                ),
                new NamespacedAttributeBag('_sf2_attributes','.')
            );
        });
    }

    private function registerBaseContainerContextualBindings(): void {

        $this->container
            ->when(OutputWriter::class)
            ->needs('$callback')
            ->give(static function() {
                return [new ConsoleOutput(), 'writeln'];
            });

        $this->container
            ->when(ErrorPageHandler::class)
            ->needs(ViewRenderer::class)
            ->give(static function() {
                return new PhpViewRenderer(__DIR__ . '/Exceptions/Presentation/resources/views');
            });

        $this->container
            ->when(Config::class)
            ->needs(LoaderInterface::class)
            ->give(ConfigFileLoader::class);

        $this->container
            ->when(Config::class)
            ->needs(ConfigCacheInterface::class)
            ->give(ConfigCache::class);

        $this->container
            ->when(ConfigCache::class)
            ->needs('$file')
            ->give($this->cachePath() . '/config/config.php.cache');

        $this->container
            ->when(ConfigCache::class)
            ->needs('$debug')
            ->give(filter_var(getenv('APP_DEBUG'), FILTER_VALIDATE_BOOLEAN));

        $this->container
            ->when(HttpKernel::class)
            ->needs(ControllerResolverInterface::class)
            ->give(ControllerResolver::class);

        $this->container
            ->when(HttpKernel::class)
            ->needs(ArgumentResolverInterface::class)
            ->give(ArgumentResolver::class);

        /** @noinspection PhpUndefinedClassInspection */
        $this->container
            ->when(EventDispatcherFactory::class)
            ->needs(EventDispatcherInterface::class)
            ->give(EventDispatcher::class);
    }

    private function registerBaseContainerFactoryBindings(): void {

        /** @noinspection PhpUndefinedClassInspection */
        $this->container->share(EventDispatcherInterface::class, function() {

            /** @var EventDispatcherFactory $eventDispatcherFactory */
            $eventDispatcherFactory = $this->container->build(EventDispatcherFactory::class);

            return $eventDispatcherFactory->create();
        });

        $this->container->share(Connection::class, function() {

            /** @var ConnectionFactory $connectionFactory */
            $connectionFactory = $this->container->build(ConnectionFactory::class);

            return $connectionFactory->create();
        });

        $this->container->share(ViewRenderer::class, function() {

            /** @var TwigViewRendererFactory $twigViewRendererFactory */
            $twigViewRendererFactory = $this->container->build(TwigViewRendererFactory::class);

            return $twigViewRendererFactory->create();
        });

        $this->container->share(Configuration::class, function() {

            /** @var ConfigurationFactory $configurationFactory */
            $configurationFactory = $this->container->build(ConfigurationFactory::class);

            return $configurationFactory->create();
        });

        $this->container->share(HelperSet::class, function() {

            /** @var HelperSetFactory $helperSetFactory */
            $helperSetFactory = $this->container->build(HelperSetFactory::class);

            return $helperSetFactory->create();
        });
    }

    /**
     * Registers bindings that depend on existence of the Config::class
     */
    private function registerPostConfigBaseContainerBindings(): void {

        $this->container->share(Connection::class, function() {

            /** @var DBManager $dbManager */
            $dbManager = $this->container->build(DBManager::class);

            return $dbManager->getConnection();
        });

    }

    private function registerPostConfigBaseContainerContextualBindings(): void {

        $config = $this->config;

        $this->container
            ->when(SocialiteManager::class)
            ->needs('$config')
            ->give($this->config->get('auth'));

        $this->container
            ->when(RouteResolver::class)
            ->needs(LoaderInterface::class)
            ->give(PhpFileLoader::class);

        $this->container
            ->when(RouteResolver::class)
            ->needs('$routeResources')
            ->give(static function() use ($config) {

                $routeResources = [
                    __DIR__ . '/Common/routes.php'
                ];

                foreach ((array) $config->get('routing.groups') as $routeGroup){
                    $routeResources[] = $config->get('app.paths.routes') . "/{$routeGroup}.php";
                }

                return $routeResources;
            });

        $this->container
            ->when(RouteResolver::class)
            ->needs('$options')
            ->give(static function() use ($config) {
                return [
                    'cache_dir' => filter_var($config->get('app.debug'), FILTER_VALIDATE_BOOLEAN) ? null : $config->get('app.paths.cache'),
                    'debug' => filter_var($config->get('app.debug'), FILTER_VALIDATE_BOOLEAN)
                ];
            });
    }

    /**
     * Registers App specific container bindings
     *
     * @throws ContainerException
     * @throws NotFoundException
     * @throws ReflectionException
     */
    private function registerAppAgents(): void {

        foreach($this->config->get('agents') as $agent) {

            if (($agent = $this->container->build($agent)) instanceof Agent) {
                $agent->register();
            }
        }
    }
}
