<?php declare(strict_types=1);

namespace Ether\Events;

use Ether\Config\Config;
use ReflectionException;
use Ether\Application as Ether;
use Ether\Container\Exceptions\NotFoundException;
use Ether\Container\Exceptions\ContainerException;
use Symfony\Component\EventDispatcher\EventSubscriberInterface as EventSubscriber;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface as EventDispatcher;

final class EventDispatcherFactory {

    private $ether;
    private $config;
    private $dispatcher;
    private $listeners = [];
    private $subscribers = [];

    public function __construct(Ether $ether, Config $config, EventDispatcher $eventDispatcher) {
        $this->ether = $ether;
        $this->config = $config;
        $this->dispatcher = $eventDispatcher;
    }

    /**
     * @return EventDispatcher
     * @throws ContainerException
     * @throws NotFoundException
     * @throws ReflectionException
     */
	public function create(): EventDispatcher {

        $this->registerListeners();

        $this->registerSubscribers();

        return $this->dispatcher;
	}

    /**
     * @throws ContainerException
     * @throws NotFoundException
     * @throws ReflectionException
     */
    private function registerListeners(): void {

        $listeners = $this->config->get('events.listeners');

        if( ! empty($this->listeners)) {

            $mergedListeners = array_merge($listeners, $this->listeners);

            foreach($mergedListeners as $listener) {

                /** @var callable $listener */
                $listener = $this->ether->build($listener['handler']);

                $this->dispatcher->addListener($listener['name'], [$listener, $listener['method']], $listener['priority']);
            }
        }
    }

    /**
     * @throws ContainerException
     * @throws NotFoundException
     * @throws ReflectionException
     */
    private function registerSubscribers(): void {

        $subscribers = $this->config->get('events.subscribers');

        if( ! empty($subscribers)) {

            $mergedSubscribers = array_merge($subscribers, $this->subscribers);

            foreach($mergedSubscribers as $subscriber) {

                /** @var EventSubscriber $subscriber */
                $subscriber = $this->ether->build($subscriber);

                $this->dispatcher->addSubscriber($subscriber);
            }
        }
    }
}
