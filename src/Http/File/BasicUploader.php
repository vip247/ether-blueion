<?php declare(strict_types=1);

namespace Ether\Http\File;

use Ramsey\Uuid\Uuid;
use Ether\Config\Config;
use Ether\Http\Contracts\Uploader;
use Ether\Exceptions\Throwable\FileUploadException;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\File\Exception\FileException;

final class BasicUploader implements Uploader {

    private $targetDirectory;

    public function __construct(Config $config, string $targetDirectory = null) {
        $this->targetDirectory = $targetDirectory ?? $config->get('app.paths.uploads');
    }

    public function setTargetDirectory(string $targetDirectory): void {
        $this->targetDirectory = $targetDirectory;
    }

    /**
     *
     * @param UploadedFile $file
     *
     * @return string
     *
     * @throws FileUploadException
     *
     */
    public function upload(UploadedFile $file): string {

        try {

            $fileName = Uuid::uuid1()->toString() . '.' . $file->guessExtension();

            $file->move($this->targetDirectory, $fileName)->getFilename();

        } catch(FileException $e) {

            throw new FileUploadException("Failed to upload: {$file->getBasename()}. Error: {$e->getMessage()}");

        }

        return $fileName;
    }
}
