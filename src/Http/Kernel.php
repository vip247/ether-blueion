<?php declare(strict_types=1);

namespace Ether\Http;

use Exception;
use Ether\Routing\RouteResolver;
use Ether\Exceptions\ErrorHandler;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\RequestContext;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\HttpKernel as Http;

final class Kernel {

    /** @var Http $http */
	private $http;

    /** @var ErrorHandler $errorHandler */
	private $errorHandler;

    /** @var RequestContext $requestContext */
	private $requestContext;

    /** @var RouteResolver $routeResolver */
    private $routeResolver;

    public function __construct(
        Http $http,
        RequestContext $requestContext,
        RouteResolver $routeResolver,
        ErrorHandler $errorHandler
    ) {
        $this->http = $http;
        $this->errorHandler = $errorHandler;
        $this->routeResolver = $routeResolver;
		$this->requestContext = $requestContext;

        $this->configure();
    }

    private function configure(): void {
        $this->errorHandler->register();
    }

	/**
	 *
	 * @param Request $request
	 *
	 * @return Response
	 *
	 * @throws Exception
	 *
	 */
	public function handle(Request $request): Response {

        $requestContext = $this->requestContext->fromRequest($request);

        if($method = $request->get('_method')) {

            $requestContext->setMethod(strtoupper($method));

        }

        $this->routeResolver->setContext($requestContext);

		$request->attributes->add($this->routeResolver->match($request->getPathInfo()));

        return $this->http->handle($request);
    }

    public function terminate(Request $request, Response $response): void {
        $this->http->terminate($request, $response);
    }
}
