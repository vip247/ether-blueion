<?php declare(strict_types=1);

namespace Ether\Http\Contracts;

use Symfony\Component\HttpFoundation\File\UploadedFile;

interface Uploader {

    /**
     * @param UploadedFile $file
     *
     * @return string
     *
     * @throws FileUploadException
     */
    public function upload(UploadedFile $file): string;

}
