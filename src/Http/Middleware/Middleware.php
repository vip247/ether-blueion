<?php declare(strict_types=1);

namespace Ether\Http\Middleware;

use Closure;
use Ether\Application as Ether;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\EventDispatcher\EventSubscriberInterface as EventSubscriber;

abstract class Middleware implements EventSubscriber {

    protected $ether;

    public function __construct(Ether $ether) {
        $this->ether = $ether;
    }

    abstract public static function getSubscribedEvents(): array;

    abstract public function handle(Request $request, Closure $next);
}
