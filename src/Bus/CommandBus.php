<?php declare(strict_types=1);

namespace Ether\Bus;

use ReflectionException;
use Ether\Application as Ether;
use Ether\Container\Exceptions\NotFoundException;
use Ether\Container\Exceptions\ContainerException;
use Ether\Exceptions\Throwable\CommandHandlerNotFoundException;

final class CommandBus {

    private $ether;

    public function __construct(Ether $ether) {
        $this->ether = $ether;
    }

    /**
     *
     * @param $command
     *
     * @return mixed
     *
     * @throws CommandHandlerNotFoundException
     * @throws ContainerException
     * @throws NotFoundException
     * @throws ReflectionException
     */
    public function dispatch($command) {

        $handler = $this->getHandler($command);

        return $this->ether->build($handler)->handle($command);
    }

    /**
     *
     * @param $command
     *
     * @return string
     *
     * @throws CommandHandlerNotFoundException
     *
     */
    private function getHandler($command): string {

        $handler = get_class($command) . 'Handler';

        if ( ! class_exists($handler)) {
             throw new CommandHandlerNotFoundException("Command handler {$handler} does not exist.");
        }

        return $handler;
    }
}
