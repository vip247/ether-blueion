<?php declare(strict_types=1);

namespace Ether\Bus\Contracts;

interface CommandHandler {
    public function handle($command);
}
