<?php declare(strict_types=1);

namespace Ether\Bus;

interface CommandHandler {
    public function handle($command);
}
